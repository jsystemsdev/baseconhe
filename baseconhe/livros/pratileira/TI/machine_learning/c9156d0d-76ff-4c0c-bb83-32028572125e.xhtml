<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Decision tree classification with scikit-learn</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Decision tree classification with scikit-learn</h1>
                </header>
            
            <article>
                
<p>scikit-learn contains the <kbd>DecisionTreeClassifier</kbd>&#160;<span>class,</span><span>&#160;</span><span>which can train a binary decision tree with Gini and cross-entropy impurity measures. In our example, let's consider a dataset with three features and three classes:</span></p>
<pre>
<strong>from sklearn.datasets import make_classification</strong><br /><br /><strong>&gt;&gt;&gt; nb_samples = 500</strong><br /><strong>&gt;&gt;&gt; X, Y = make_classification(n_samples=nb_samples, n_features=3, n_informative=3, n_redundant=0, n_classes=3, n_clusters_per_class=1)</strong>
</pre>
<p>Let's&#160;<span>first</span><span>&#160;</span><span>consider a classification with default Gini impurity:</span></p>
<pre>
<strong>from sklearn.tree import DecisionTreeClassifier</strong><br /><strong>from sklearn.model_selection import cross_val_score</strong><br /><br /><strong>&gt;&gt;&gt; dt = DecisionTreeClassifier()</strong><br /><strong>&gt;&gt;&gt; print(cross_val_score(dt, X, Y, scoring='accuracy', cv=10).mean())</strong><br /><strong>0.970</strong>
</pre>
<p>A very interesting feature is given by the possibility&#160;of exporting the tree in <kbd>Graphviz</kbd> format and converting it into a PDF.</p>
<div class="packt_infobox">Graphviz is a free tool that can be downloaded from&#160;<a href="http://www.graphviz.org">http://www.graphviz.org</a>.</div>
<p>To export a trained tree, it is necessary to use the built-in function <kbd>export_graphviz()</kbd>:</p>
<pre>
<strong>from sklearn.tree import export_graphviz</strong><br /><br /><strong>&gt;&gt;&gt; dt.fit(X, Y)</strong><br /><strong>&gt;&gt;&gt; with open('dt.dot', 'w') as df:</strong><br /><strong>      df = export_graphviz(dt, out_file=df, </strong><br /><strong>                           feature_names=['A','B','C'], </strong><br /><strong>                           class_names=['C1', 'C2', 'C3'])</strong>
</pre>
<p>In this case, we have used <kbd>A</kbd>, <kbd>B</kbd>, and <kbd>C</kbd> as feature names and <kbd>C1</kbd>, <kbd>C2</kbd>, and <kbd>C3</kbd> as class names. Once the file has been created, it's possible converting to PDF using the command-line tool:</p>
<pre>
<strong>&gt;&gt;&gt; &lt;Graphviz Home&gt;bindot -Tpdf dt.dot -o dt.pdf</strong>
</pre>
<p>The graph for our example is rather large, so in the following feature you can see only a part of a branch:</p>
<div class="CDPAlignCenter CDPAlign"><img class="image-border" src="assets/96de430b-a565-4984-a64f-135fddd43d6a.png" /></div>
<p>As you can see, there are two kinds of nodes:</p>
<ul>
<li>Nonterminal, which contains the splitting tuple (as feature &lt;= threshold) and a positive impurity measure</li>
<li>Terminal, where the impurity measure is null and a final target class is present</li>
</ul>
<p>In both cases, you can always check the number of samples. This kind of graph is very useful in understanding how many decision steps are needed. Unfortunately, even if the process is quite simple, the dataset structure can lead to very complex trees, while other methods can immediately find out the most appropriate class. Of course, not all features have the same importance. If we consider the root of the tree and the first nodes, we find features that separate a lot of samples; therefore, their importance must be higher than that of all&#160;terminal nodes, where the residual number of samples is minimum. In scikit-learn, it's possible to assess the Gini importance of each feature after training a model:</p>
<pre>
<strong>&gt;&gt;&gt; dt.feature_importances_</strong><br /><strong>array([ 0.12066952,  0.12532507,  0.0577379 ,  0.14402762,  0.14382398,</strong><br /><strong>        0.12418921,  0.14638565,  0.13784106])</strong><br /><br /><strong>&gt;&gt;&gt; np.argsort(dt.feature_importances_)</strong><br /><strong>array([2, 0, 5, 1, 7, 4, 3, 6], dtype=int64)</strong>
</pre>
<p>The following figure shows a plot of the importances:</p>
<div class="CDPAlignCenter CDPAlign"><img class="image-border" src="assets/6aa1257e-cd1d-4c8e-bf66-4cd0b8e98ef7.png" /></div>
<p>The most important features are 6, 3, 4, and 7, while feature 2, for example, separates a very small number of samples, and can be considered noninformative for the classification task.</p>
<p>In terms of efficiency, a tree can also be pruned using the <kbd>max_depth</kbd> parameter; however, it's not always so simple to understand which value is the best (grid search can help in this task). On the other hand, it's easier to decide what the maximum number of features to consider at each split should be. The parameter <kbd>max_features</kbd> can be used for this purpose:</p>
<ul>
<li>If it's a number, the value is directly taken into account at each split</li>
<li>If it's <kbd>'auto'</kbd> or <kbd>'sqrt'</kbd>, the square root of the number of features will be adopted</li>
<li>If it's <kbd>'log2'</kbd>, the logarithm (base 2) will be used</li>
<li>If it's&#160;<kbd>'None'</kbd>, all the features will be used (this is the default value)</li>
</ul>
<p>In general, when the number of total features is not too high, the default value is the best choice, although it's useful to introduce a small compression (via <kbd>sqrt</kbd> or <kbd>log2</kbd>) when too many features can interfere among themselves, reducing the efficiency. Another parameter useful for controlling both performance and efficiency is <kbd>min_samples_split</kbd>, which specifies the minimum number of samples to consider for a split. Some examples are shown in the following snippet:</p>
<pre>
<strong>&gt;&gt;&gt; cross_val_score(DecisionTreeClassifier(), X, Y, scoring='accuracy', cv=10).mean()</strong><br /><strong>0.77308070807080698</strong><br /><br /><strong>&gt;&gt;&gt; cross_val_score(DecisionTreeClassifier(max_features='auto'), X, Y, scoring='accuracy', cv=10).mean()</strong><br /><strong>0.76410071007100711</strong><br /><br /><strong>&gt;&gt;&gt; cross_val_score(DecisionTreeClassifier(min_samples_split=100), X, Y, scoring='accuracy', cv=10).mean()</strong><br /><strong>0.72999969996999692</strong>
</pre>
<p>As already explained, finding the best parameters is generally a difficult task, and the best way to carry it out is to perform a grid search while including all the values that could affect the accuracy.</p>
<p>Using logistic regression on the previous set (only for comparison), we get:</p>
<pre>
<strong>from sklearn.linear_model import LogisticRegression</strong><br /><br /><strong>&gt;&gt;&gt; lr = LogisticRegression()</strong><br /><strong>&gt;&gt;&gt; cross_val_score(lr, X, Y, scoring='accuracy', cv=10).mean()</strong><br /><strong>0.9053368347338937</strong>
</pre>
<p>So the score is higher, as expected. However, the original dataset was quite simple, and based on the concept of having a single cluster per class. This allows a simpler and more precise linear separation. If we consider a slightly different scenario with more variables and a more complex structure (which is hard to capture by a linear classifier), we can compare an ROC curve for both linear regression and decision trees:</p>
<pre>
<strong>&gt;&gt;&gt; nb_samples = 1000</strong><br /><strong>&gt;&gt;&gt; X, Y = make_classification(n_samples=nb_samples, n_features=8, n_informative=6, n_redundant=2,     n_classes=2, n_clusters_per_class=4)</strong>
</pre>
<p>The resulting ROC curve is shown in the&#160;following figure:</p>
<div class="packt_figref CDPAlignCenter CDPAlign"><img height="511" width="500" class="image-border" src="assets/34df27cb-ecf1-4366-93fb-9a6650e38d39.png" /></div>
<p>Using a grid search with the most common parameters on the MNIST digits dataset, we can get:</p>
<pre>
<strong>from sklearn.model_selection import GridSearchCV</strong><br /><br /><strong>param_grid = [</strong><br /><strong> { </strong><br /><strong>   'criterion': ['gini', 'entropy'],</strong><br /><strong>   'max_features': ['auto', 'log2', None],</strong><br /><strong>   'min_samples_split': [ 2, 10, 25, 100, 200 ],</strong><br /><strong>   'max_depth': [5, 10, 15, None]</strong><br /><strong> }</strong><br /><strong>]</strong><br /><br /><strong>&gt;&gt;&gt; gs = GridSearchCV(estimator=DecisionTreeClassifier(), param_grid=param_grid,</strong><br /><strong> scoring='accuracy', cv=10, n_jobs=multiprocessing.cpu_count())</strong><br /><br /><strong>&gt;&gt;&gt; gs.fit(digits.data, digits.target)</strong><br /><strong>GridSearchCV(cv=10, error_score='raise',</strong><br /><strong>       estimator=DecisionTreeClassifier(class_weight=None, criterion='gini', max_depth=None,</strong><br /><strong>            max_features=None, max_leaf_nodes=None,</strong><br /><strong>            min_impurity_split=1e-07, min_samples_leaf=1,</strong><br /><strong>            min_samples_split=2, min_weight_fraction_leaf=0.0,</strong><br /><strong>            presort=False, random_state=None, splitter='best'),</strong><br /><strong>       fit_params={}, iid=True, n_jobs=8,</strong><br /><strong>       param_grid=[{'max_features': ['auto', 'log2', None], 'min_samples_split': [2, 10, 25, 100, 200], 'criterion': ['gini', 'entropy'], 'max_depth': [5, 10, 15, None]}],</strong><br /><strong>       pre_dispatch='2*n_jobs', refit=True, return_train_score=True,</strong><br /><strong>       scoring='accuracy', verbose=0)</strong><br /><br /><strong>&gt;&gt;&gt; gs.best_estimator_</strong><br /><strong>DecisionTreeClassifier(class_weight=None, criterion='entropy', max_depth=None,</strong><br /><strong>            max_features=None, max_leaf_nodes=None,</strong><br /><strong>            min_impurity_split=1e-07, min_samples_leaf=1,</strong><br /><strong>            min_samples_split=2, min_weight_fraction_leaf=0.0,</strong><br /><strong>            presort=False, random_state=None, splitter='best')</strong><br /><br /><strong>&gt;&gt;&gt; gs.best_score_</strong><br /><strong>0.8380634390651085</strong>
</pre>
<p>In this case, the element that impacted accuracy the&#160;<span>most</span><span>&#160;is the minimum number of samples to consider for a split. This is reasonable, considering the structure of this dataset and the need to have many branches to capture even small changes. &#160;</span></p>


            </article>

            
        </section>
    </body>

</html>
