<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Tf-idf vectorizing</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Tf-idf vectorizing</h1>
                </header>
            
            <article>
                
<p>The most common limitation of count vectorizing is that the algorithm doesn't consider the whole corpus while considering the frequency of each token. The goal of vectorizing is normally preparing the data for a classifier; therefore it's necessary to avoid features that are present<span>&#160;</span><span>very often</span><span>, because their information decreases when the number of global occurrences increases. For example, in a corpus about a sport, the word</span> <kbd>match</kbd> <span>could be present in a huge number of documents; therefore it's almost useless as a classification feature. To address this issue, we need a different approach. If we have a corpus</span> <kbd>C</kbd><span>&#160;with</span> <kbd>n</kbd> <span>documents, we define</span> <strong>term-frequency</strong><span>, the number of times a token occurs in a document, as:</span></p>
<div class="mce-root CDPAlignCenter CDPAlign"><img height="26" width="175" src="assets/1e9db029-61d2-47eb-9be6-ce8475a8ae94.png" /></div>
<p class="CDPAlignLeft CDPAlign">We define&#160;<strong>inverse-document-frequency</strong>, as the following measure:</p>
<div class="mce-root CDPAlignCenter CDPAlign"><img height="50" width="391" src="assets/a4923023-37e6-42d1-85ca-4cdf67ead7b9.png" /></div>
<p class="CDPAlignLeft CDPAlign">In other words, <kbd>idf(t,C)</kbd> measures how much information is provided by every single term. In fact, if <kbd>count(D,t) = n</kbd>, it means that a token is always present and <em>idf(t, C)</em>&#160;comes close to 0, and vice-versa. The term 1 in the denominator is a correction factor, which avoids null idf for count<kbd>(D,t) = n</kbd>. So, instead of considering only the term frequency, we weigh each token by defining a new measure:</p>
<div class="mce-root CDPAlignCenter CDPAlign"><img height="33" width="217" src="assets/4da3450f-4fe0-4c7a-a81f-015eefb46977.png" /></div>
<p class="CDPAlignLeft CDPAlign">scikit-learn provides the <kbd>TfIdfVectorizer</kbd> <span>class,</span><span>&#160;</span><span>which we can apply to the same toy corpus used in the previous paragraph:</span></p>
<pre class="CDPAlignLeft CDPAlign">
<strong>&gt;&gt;&gt; from sklearn.feature_extraction.text import TfidfVectorizer</strong><br /><br /><strong>&gt;&gt;&gt; tfidfv = TfidfVectorizer()</strong><br /><strong>&gt;&gt;&gt; vectorized_corpus = tfidfv.fit_transform(corpus)</strong><br /><strong>&gt;&gt;&gt; print(vectorized_corpus.todense())</strong><br /><strong>[[ 0.          0.          0.          0.          0.          0.31799276</strong><br /><strong>   0.          0.39278432  0.          0.          0.49819711  0.49819711</strong><br /><strong>   0.          0.          0.49819711  0.          0.          0.          0.        ]</strong><br /><strong> [ 0.          0.          0.          0.          0.          0.30304005</strong><br /><strong>   0.30304005  0.37431475  0.4747708   0.4747708   0.          0.</strong><br /><strong>   0.4747708   0.          0.          0.          0.          0.          0.        ]</strong><br /><strong> [ 0.31919701  0.31919701  0.          0.          0.          0.20373932</strong><br /><strong>   0.20373932  0.          0.          0.          0.          0.          0.</strong><br /><strong>   0.63839402  0.          0.31919701  0.          0.31919701  0.31919701]</strong><br /><strong> [ 0.          0.          0.47633035  0.47633035  0.47633035  0.</strong><br /><strong>   0.30403549  0.          0.          0.          0.          0.          0.</strong><br /><strong>   0.          0.          0.          0.47633035  0.          0.        ]]</strong>
</pre>
<p>Let's now check the vocabulary to make a comparison with simple count vectorizing:</p>
<pre>
<strong>&gt;&gt;&gt; print(tfidfv.vocabulary_)</strong><br /><strong>{u'and': 1, u'be': 3, u'we': 18, u'set': 9, u'simple': 10, u'text': 12, u'is': 7, u'tokenized': 16, u'want': 17, u'the': 13, u'documents': 6, u'this': 14, u'of': 8, u'to': 15, u'can': 4, u'test': 11, u'corpus': 5, u'analyze': 0, u'automatically': 2}</strong>
</pre>
<p>The term <kbd>documents</kbd> is the sixth feature in both vectorizers and appears in the last three documents. As you can see, it's weight is about 0.3, while the term <kbd>the</kbd> is present twice only in the third document and its weight is about 0.64. The general rule is: if a term is representative of a document, its weight becomes close to 1.0, while it decreases if finding it in a sample document doesn't allow us to easily&#160;<span>determine</span><span>&#160;</span><span>its category.</span></p>
<p>Also in this case, it's possible to use an external tokenizer and specify the desired n-gram range. Moreover, it's possible to normalize the vectors (through the parameter <kbd>norm</kbd>) and decide whether to include or exclude the addend 1 to the denominator of idf (through the parameter <kbd>smooth_idf</kbd>). It's also possible to define the range of accepted document frequencies using the parameters <kbd>min_df</kbd> and <kbd>max_df</kbd>&#160;so as to exclude tokens whose occurrences are below or beyond a minimum/maximum threshold. They accept&#160;both integers (number of occurrences) or floats in the range of [0.0, 1.0] (proportion of documents). In the next example, we use some of these parameters:</p>
<pre>
<strong>&gt;&gt;&gt; tfidfv = TfidfVectorizer(tokenizer=tokenizer, ngram_range=(1, 2), norm='l2')</strong><br /><strong>&gt;&gt;&gt; vectorized_corpus = tfidfv.fit_transform(corpus)</strong><br /><strong>&gt;&gt;&gt; print(vectorized_corpus.todense())</strong><br /><strong>[[ 0.          0.          0.          0.          0.30403549  0.          0.</strong><br /><strong>   0.          0.          0.          0.          0.47633035  0.47633035</strong><br /><strong>   0.47633035  0.47633035  0.          0.          0.          0.          0.        ]</strong><br /><strong> [ 0.          0.          0.          0.          0.2646963   0.</strong><br /><strong>   0.4146979   0.2646963   0.          0.4146979   0.4146979   0.          0.</strong><br /><strong>   0.          0.          0.4146979   0.4146979   0.          0.          0.        ]</strong><br /><strong> [ 0.4146979   0.4146979   0.          0.          0.2646963   0.4146979</strong><br /><strong>   0.          0.2646963   0.          0.          0.          0.          0.</strong><br /><strong>   0.          0.          0.          0.          0.          0.4146979</strong><br /><strong>   0.4146979 ]</strong><br /><strong> [ 0.          0.          0.47633035  0.47633035  0.          0.          0.</strong><br /><strong>   0.30403549  0.47633035  0.          0.          0.          0.          0.</strong><br /><strong>   0.          0.          0.          0.47633035  0.          0.        ]]</strong><br /><br /><strong>&gt;&gt;&gt; print(tfidfv.vocabulary_)</strong><br /><strong>{u'analyz corpus': 1, u'set': 9, u'simpl test': 12, u'want analyz': 19, u'automat': 2, u'want': 18, u'test corpus': 14, u'set text': 10, u'corpus set': 6, u'automat token': 3, u'corpus document': 5, u'text document': 16, u'token': 17, u'document automat': 8, u'text': 15, u'test': 13, u'corpus': 4, u'document': 7, u'simpl': 11, u'analyz': 0}</strong>
</pre>
<p>In particular, normalizing vectors is always a good choice if they must be used as input for a classifier, as we'll see in the next chapter.</p>


            </article>

            
        </section>
    </body>

</html>
