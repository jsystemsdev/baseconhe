<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Sentiment analysis</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Sentiment analysis</h1>
                </header>
            
            <article>
                
<p>One the most widespread applications of&#160;<span>NLP</span><span>&#160;i</span><span>s sentiment analysis of short texts (tweets, posts, comments, reviews, and so on). From a marketing viewpoint, it's very important to understand the semantics of these pieces of information in terms of the sentiment expressed. As you can understand, this task can be very easy when the comment is precise and contains only a set of positive/negative words, but it becomes more complex when in the same sentence there are different propositions that can conflict with each other. For example,</span> <em>I loved that hotel. It was a wonderful experience</em><span>&#160;is clearly a positive comment, while</span> <em>The hotel is good, however, the restaurant was bad and, even if the waiters were kind, I had to fight with a receptionist to have another pillow</em><span>. In this case, the situation is more difficult to manage, because there are both positive and negative elements, resulting in a neutral review. For this reason, many applications aren't based on a binary decision but admit intermediate levels (at least one to express the neutrality).</span></p>
<p>These kind of problems are normally supervised (as we're going to do), but there are also cheaper and more complex solutions. The simplest way to evaluate the sentiment is to look for particular keywords. This dictionary-based approach is fast and, together with a good stemmer, can immediately mark positive and negative documents. On the flip side, it doesn't consider the relationship among terms and cannot learn how to weight the different components. For example,&#160;<em>Lovely day, bad mood</em>&#160;will result in a neutral (+1, -1), while with a supervised approach it's possible to make the model learn that <em>mood</em>&#160;is very important and <em>bad mood</em>&#160;will normally drive to a negative sentiment. Other approaches (much more complex) are based on topic modeling (you can now understand how to apply LSA or LDA to determine the underlying topics in terms of positivity or negativity); however, they need further steps to use topic-word and topic-document distributions. It can be helpful in the real semantics of a comment, where, for example, a positive adjective is normally used together with other similar components (like verbs). Say,&#160;<em>Lovely hotel, I'm surely coming back</em>. In this case (if the number of samples is big enough), a topic can emerge from the combination of words such as&#160;<em>lovely</em>&#160;or <em>amazing</em>&#160;and (positive) verbs such as&#160;<em>returning</em>&#160;or <em>coming back</em>.</p>
<p>An alternative is to consider the topic distribution of positive and negative documents and work with a supervised approach in the topic sub-space. Other approaches include deep-learning techniques (such as Word2Vec or Doc2Vec) and are based on the idea of generating a vectorial space where similar words are close to each other, in order to easily manage synonyms. For example, if the training set contains the sentence&#160;<em>Lovely hotel</em>&#160;but it doesn't contain <em>Wonderful hotel</em>, a Word2Vec model can learn from other examples that <em>lovely</em>&#160;and <em>wonderful</em>&#160;are very close; therefore the new document <em>Wonderful hotel</em>&#160;is immediately classified using the knowledge provided by the first comment. An introduction to this technique, together with some technical papers, can be found at&#160;<a href="https://code.google.com/archive/p/word2vec/">https://code.google.com/archive/p/word2vec/</a>.</p>
<p>Let's now consider our example, which is based on a subset of the <em>Twitter Sentiment Analysis Training Corpus</em>&#160;dataset. In order to speed up the process, we have limited the experiment to 1,00,000 tweets. After downloading the file (see the box at the end of this paragraph), it's necessary to parse it (using the UTF-8 encoding):</p>
<pre>
<strong>&gt;&gt;&gt; dataset = 'dataset.csv'</strong><br /><br /><strong>&gt;&gt;&gt; corpus = []</strong><br /><strong>&gt;&gt;&gt; labels = []</strong><br /><br /><strong>&gt;&gt;&gt; with open(dataset, 'r', encoding='utf-8') as df:</strong><br /><strong>&gt;&gt;&gt;    for i, line in enumerate(df):</strong><br /><strong>&gt;&gt;&gt;    if i == 0:</strong><br /><strong>&gt;&gt;&gt;       continue</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;    parts = line.strip().split(',')</strong><br /><strong>&gt;&gt;&gt;    labels.append(float(parts[1].strip()))</strong><br /><strong>&gt;&gt;&gt;    corpus.append(parts[3].strip())</strong>
</pre>
<p>The <kbd>dataset</kbd>&#160;<span>variable</span><span>&#160;</span><span>must contain the full path to the CSV file. This procedure reads all the lines skipping the first one (which is the header), and stores each tweet as a new list entry in the</span> <kbd>corpus</kbd><span>&#160;</span><span>variable,</span><span>&#160;</span><span>and the corresponding sentiment (which is binary, 0 or 1) in the</span> <kbd>labels</kbd><span>&#160;variable</span><span>. At this point, we proceed as usual, tokenizing, vectorizing, and preparing the training and test sets:</span></p>
<pre>
<strong>from nltk.tokenize import RegexpTokenizer</strong><br /><strong>from nltk.corpus import stopwords</strong><br /><strong>from nltk.stem.lancaster import LancasterStemmer</strong><br /><br /><strong>from sklearn.feature_extraction.text import TfidfVectorizer</strong><br /><strong>from sklearn.model_selection import train_test_split</strong><br /><br /><strong>&gt;&gt;&gt; rt = RegexpTokenizer('[a-zA-Z0-9\.]+')</strong><br /><strong>&gt;&gt;&gt; ls = LancasterStemmer()</strong><br /><strong>&gt;&gt;&gt; sw = set(stopwords.words('english'))</strong><br /><br /><strong>&gt;&gt;&gt; def tokenizer(sentence):</strong><br /><strong>&gt;&gt;&gt;    tokens = rt.tokenize(sentence)</strong><br /><strong>&gt;&gt;&gt;    return [ls.stem(t.lower()) for t in tokens if t not in sw]</strong><br /><br /><strong>&gt;&gt;&gt; tfv = TfidfVectorizer(tokenizer=tokenizer, sublinear_tf=True, ngram_range=(1, 2), norm='l2')</strong><br /><strong>&gt;&gt;&gt; X = tfv.fit_transform(corpus[0:100000])</strong><br /><strong>&gt;&gt;&gt; Y = np.array(labels[0:100000])</strong><br /><br /><strong>&gt;&gt;&gt; X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.1)</strong>
</pre>
<p>We have chosen to include dots together with letters and numbers in the <kbd>RegexpTokenizer</kbd> instance because they are useful for expressing particular emotions. Moreover, the n-gram range has been set to (1, 2), so we include bigrams (the reader can try with trigrams too). At this point, we can train a random forest:</p>
<pre>
<strong>from sklearn.ensemble import RandomForestClassifier</strong><br /><br /><strong>import multiprocessing</strong><br /><br /><strong>&gt;&gt;&gt; rf = RandomForestClassifier(n_estimators=20, n_jobs=multiprocessing.cpu_count())</strong><br /><strong>&gt;&gt;&gt; rf.fit(X_train, Y_train)</strong>
</pre>
<p>Now we can produce some metrics to evaluate the model:</p>
<pre>
<strong>from sklearn.metrics import precision_score, recall_score<br /><br />&gt;&gt;&gt; print('Precision: %.3f' % precision_score(Y_test, rf.predict(X_test)))<br />Precision: 0.720</strong><br /><br /><strong>&gt;&gt;&gt; print('Recall: %.3f' % recall_score(Y_test, rf.predict(X_test)))<br />Recall: 0.784<br /></strong>
</pre>
<p>The performances are not excellent (it's possible to achieve better accuracies using Word2Vec); however, they are acceptable for many tasks. In particular, a 78% recall means that the number of false negatives is about 20% and it can be useful when using sentiment analysis for an automatic processing task (in many cases, the risk threshold to auto-publish a negative review is quite a bit lower, and, therefore, a better solution must be employed). The performances can be also confirmed by the corresponding ROC curve:</p>
<div class="CDPAlignCenter CDPAlign"><img height="417" width="409" class="image-border" src="assets/f0e3188d-63bd-4075-bc88-c0f3791de20e.png" /></div>
<div class="packt_infobox"><span>The <em>Twitter Sentiment Analysis Training Corpus</em> dataset (as a CSV file) used in the example can be downloaded from <a href="http://thinknook.com/wp-content/uploads/2012/09/Sentiment-Analysis-Dataset.zip">http://thinknook.com/wp-content/uploads/2012/09/Sentiment-Analysis-Dataset.zip</a>. Considering the amount of data, the training process can be very long (even taking hours on slower machines).</span></div>


            </article>

            
        </section>
    </body>

</html>
