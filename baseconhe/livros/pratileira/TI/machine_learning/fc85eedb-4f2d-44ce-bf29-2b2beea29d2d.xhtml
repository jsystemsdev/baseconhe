<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Pipelines</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Pipelines</h1>
                </header>
            
            <article>
                
<p>scikit-learn provides a flexible mechanism for creating pipelines made up of subsequent processing steps. This is possible thanks to a standard interface implemented by the majority of classes therefore most of the components (both data processors/transformers and classifiers/clustering tools) can be exchanged seamlessly. The class <kbd>Pipeline</kbd> accepts a single parameter <kbd>steps</kbd>, which is a list of tuples in the form (name of the component—instance), and creates a complex object with the standard fit/transform interface. For example, if we need to apply a PCA, a standard scaling, and then we want to classify using a SVM, we could create a pipeline in the following way:</p>
<pre>
<strong>from sklearn.decomposition import PCA</strong><br /><strong>from sklearn.pipeline import Pipeline</strong><br /><strong>from sklearn.preprocessing import StandardScaler</strong><br /><strong>from sklearn.svm import SVC</strong><br /><br /><strong>&gt;&gt;&gt; pca = PCA(n_components=10)</strong><br /><strong>&gt;&gt;&gt; scaler = StandardScaler()</strong><br /><strong>&gt;&gt;&gt; svc = SVC(kernel='poly', gamma=3)</strong><br /><br /><strong>&gt;&gt;&gt; steps = [</strong><br /><strong>&gt;&gt;&gt;    ('pca', pca),</strong><br /><strong>&gt;&gt;&gt;    ('scaler', scaler),</strong><br /><strong>&gt;&gt;&gt;    ('classifier', svc)</strong><br /><strong>&gt;&gt;&gt; ]</strong><br /><br /><strong>&gt;&gt;&gt; pipeline = Pipeline(steps)</strong>
</pre>
<p>At this point, the pipeline can be fitted like a single classifier (using the standard methods <kbd>fit()</kbd> and <kbd>fit_transform()</kbd>), even if the the input samples are first passed to the <kbd>PCA</kbd> instance, the reduced dataset is normalized by the <kbd>StandardScaler</kbd> instance, and finally, the resulting samples are passed to the classifier.</p>
<p>A pipeline is also very useful together with <kbd>GridSearchCV</kbd>, to evaluate different combinations of parameters, not limited to a single step but considering the whole process. Considering the previous example, we can create a dummy dataset and try to find the optimal parameters:</p>
<pre>
<strong>from sklearn.datasets import make_classification</strong><br /><br /><strong>&gt;&gt;&gt; nb_samples = 500</strong><br /><strong>&gt;&gt;&gt; X, Y = make_classification(n_samples=nb_samples, n_informative=15, n_redundant=5, n_classes=2)</strong>
</pre>
<p>The dataset is quite redundant. Therefore, we need to find the optimal number of components for PCA and the best kernel for the SVM. When working with a pipeline, the name of the parameter must be specified using the component ID followed by a double underscore and then the actual name, for example,&#160;<kbd>classifier__kernel</kbd> (if you want to check all the acceptable parameters with the right name, it's enough to execute: <kbd>print(pipeline.get_params().keys())</kbd>). Therefore, we can perform a grid search with the following parameter dictionary:</p>
<pre>
<strong>from sklearn.model_selection import GridSearchCV</strong><br /><br /><strong>&gt;&gt;&gt; param_grid = {</strong><br /><strong>&gt;&gt;&gt;    'pca__n_components': [5, 10, 12, 15, 18, 20],</strong><br /><strong>&gt;&gt;&gt;    'classifier__kernel': ['rbf', 'poly'],</strong><br /><strong>&gt;&gt;&gt;    'classifier__gamma': [0.05, 0.1, 0.2, 0.5],</strong><br /><strong>&gt;&gt;&gt;    'classifier__degree': [2, 3, 5]</strong><br /><strong>&gt;&gt;&gt; }</strong><br /><br /><strong>&gt;&gt;&gt; gs = GridSearchCV(pipeline, param_grid)</strong><br /><strong>&gt;&gt;&gt; gs.fit(X, Y)</strong>
</pre>
<p>As expected, the best estimator (which is a complete pipeline) has 15 principal components (that means they are uncorrelated) and a radial-basis function SVM with a relatively high <kbd>gamma</kbd> value (0.2):</p>
<pre>
<strong>&gt;&gt;&gt; print(gs.best_estimator_)<br />Pipeline(steps=[('pca', PCA(copy=True, iterated_power='auto', n_components=15, random_state=None,
  svd_solver='auto', tol=0.0, whiten=False)), ('scaler', StandardScaler(copy=True, with_mean=True, with_std=True)), ('classifier', SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
  decision_function_shape=None, degree=2, gamma=0.2, kernel='rbf',
  max_iter=-1, probability=False, random_state=None, shrinking=True,
  tol=0.001, verbose=False))])</strong>
</pre>
<p>The corresponding score is:</p>
<pre>
<strong>&gt;&gt;&gt; print(gs.best_score_)</strong><br /><strong>0.96</strong>
</pre>
<p>It's also possible to use a <kbd>Pipeline</kbd> together with <kbd>GridSearchCV</kbd>&#160;to evaluate different combinations. For example, it can be useful to compare some decomposition methods, mixed with various classifiers:</p>
<pre>
<strong>from sklearn.datasets import load_digits</strong><br /><strong>from sklearn.decomposition import NMF</strong><br /><strong>from sklearn.feature_selection import SelectKBest, f_classif</strong><br /><strong>from sklearn.linear_model import LogisticRegression</strong><br /><br /><strong>&gt;&gt;&gt; digits = load_digits()</strong><br /><br /><strong>&gt;&gt;&gt; pca = PCA()</strong><br /><strong>&gt;&gt;&gt; nmf = NMF()</strong><br /><strong>&gt;&gt;&gt; kbest = SelectKBest(f_classif)</strong><br /><strong>&gt;&gt;&gt; lr = LogisticRegression()</strong><br /><br /><strong>&gt;&gt;&gt; pipeline_steps = [</strong><br /><strong>&gt;&gt;&gt;    ('dimensionality_reduction', pca),</strong><br /><strong>&gt;&gt;&gt;    ('normalization', scaler),</strong><br /><strong>&gt;&gt;&gt;    ('classification', lr)</strong><br /><strong>&gt;&gt;&gt; ]</strong><br /><br /><strong>&gt;&gt;&gt; pipeline = Pipeline(pipeline_steps)</strong>
</pre>
<p>We want to compare <strong>principal component analysis</strong> (<strong>PCA</strong>), <strong>non-negative matrix factorization</strong>&#160;(<strong>NMF</strong>), and k-best feature selection based on the ANOVA criterion, together with logistic regression and kernelized SVM:</p>
<pre>
<strong>&gt;&gt;&gt; pca_nmf_components = [10, 20, 30]<br /><br />&gt;&gt;&gt; param_grid = [</strong><br /><strong>&gt;&gt;&gt;    {</strong><br /><strong>&gt;&gt;&gt;        'dimensionality_reduction': [pca],</strong><br /><strong>&gt;&gt;&gt;        'dimensionality_reduction__n_components': pca_nmf_components,</strong><br /><strong>&gt;&gt;&gt;        'classification': [lr],</strong><br /><strong>&gt;&gt;&gt;        'classification__C': [1, 5, 10, 20]</strong><br /><strong>&gt;&gt;&gt;    },</strong><br /><strong>&gt;&gt;&gt;    {</strong><br /><strong>&gt;&gt;&gt;        'dimensionality_reduction': [pca],</strong><br /><strong>&gt;&gt;&gt;        'dimensionality_reduction__n_components': pca_nmf_components,</strong><br /><strong>&gt;&gt;&gt;        'classification': [svc],</strong><br /><strong>&gt;&gt;&gt;        'classification__kernel': ['rbf', 'poly'],</strong><br /><strong>&gt;&gt;&gt;        'classification__gamma': [0.05, 0.1, 0.2, 0.5, 1.0],</strong><br /><strong>&gt;&gt;&gt;        'classification__degree': [2, 3, 5],<br />&gt;&gt;&gt;</strong>        <strong>'classification__C': [1, 5, 10, 20]</strong><br /><strong>&gt;&gt;&gt;    },</strong><br /><strong>&gt;&gt;&gt;    {</strong><br /><strong>&gt;&gt;&gt;        'dimensionality_reduction': [nmf],</strong><br /><strong>&gt;&gt;&gt;        'dimensionality_reduction__n_components': pca_nmf_components,</strong><br /><strong>&gt;&gt;&gt;        'classification': [lr],</strong><br /><strong>&gt;&gt;&gt;        'classification__C': [1, 5, 10, 20]</strong><br /><strong>&gt;&gt;&gt;    },</strong><br /><strong>&gt;&gt;&gt;    {</strong><br /><strong>&gt;&gt;&gt;        'dimensionality_reduction': [nmf],</strong><br /><strong>&gt;&gt;&gt;        'dimensionality_reduction__n_components': pca_nmf_components,</strong><br /><strong>&gt;&gt;&gt;        'classification': [svc],</strong><br /><strong>&gt;&gt;&gt;        'classification__kernel': ['rbf', 'poly'],</strong><br /><strong>&gt;&gt;&gt;        'classification__gamma': [0.05, 0.1, 0.2, 0.5, 1.0],</strong><br /><strong>&gt;&gt;&gt;        'classification__degree': [2, 3, 5],<br />&gt;&gt;&gt;        'classification__C': [1, 5, 10, 20]</strong><br /><strong>&gt;&gt;&gt;    },</strong><br /><strong>&gt;&gt;&gt;    {</strong><br /><strong>&gt;&gt;&gt;        'dimensionality_reduction': [kbest],</strong><br /><strong>&gt;&gt;&gt;        'classification': [svc],</strong><br /><strong>&gt;&gt;&gt;        'classification__kernel': ['rbf', 'poly'],</strong><br /><strong>&gt;&gt;&gt;        'classification__gamma': [0.05, 0.1, 0.2, 0.5, 1.0],</strong><br /><strong>&gt;&gt;&gt;        'classification__degree': [2, 3, 5],<br />&gt;&gt;&gt;        'classification__C': [1, 5, 10, 20]</strong><br /><strong>&gt;&gt;&gt;    },</strong><br /><strong>&gt;&gt;&gt; ]</strong><br /><br /><strong>&gt;&gt;&gt; gs = GridSearchCV(pipeline, param_grid)</strong><br /><strong>&gt;&gt;&gt; gs.fit(digits.data, digits.target)</strong>
</pre>
<p>Performing a grid search, we get the pipeline made up of PCA with 20 components (the original dataset 64 features) and an RBF SVM with a very small <kbd>gamma</kbd> value (0.05) and &#160;a medium (5.0) <em>L2</em> penalty parameter <kbd>C</kbd> :</p>
<pre>
<strong>&gt;&gt;&gt; print(gs.best_estimator_)</strong><br /><strong>Pipeline(steps=[('dimensionality_reduction', PCA(copy=True, iterated_power='auto', n_components=20, random_state=None,
  svd_solver='auto', tol=0.0, whiten=False)), ('normalization', StandardScaler(copy=True, with_mean=True, with_std=True)), ('classification', SVC(C=5.0, cache_size=200, class_weight=None, coef0=0.0,
  decision_function_shape=None, degree=2, gamma=0.05, kernel='rbf',
  max_iter=-1, probability=False, random_state=None, shrinking=True,
  tol=0.001, verbose=False))])</strong>
</pre>
<p>Considering the need to capture small details in the digit representations, these values are an optimal choice. The score for this pipeline is indeed very high:</p>
<pre>
<strong>&gt;&gt;&gt; print(gs.best_score_)</strong><br /><strong>0.968836950473</strong>
</pre>


            </article>

            
        </section>
    </body>

</html>
