<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Singular Value Decomposition strategy</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Singular Value Decomposition strategy</h1>
                </header>
            
            <article>
                
<p class="CDPAlignLeft CDPAlign">The first approach is based on the <strong>Singular Value Decomposition</strong>&#160;(<strong>SVD</strong>) of the user-item matrix. This technique allows transforming a matrix through a low-rank factorization and can also be used in an incremental way as described in&#160;Sarwar B., Karypis G., Konstan J., Riedl J.,&#160;<em>Incremental Singular Value Decomposition Algorithms for Highly&#160;Scalable Recommender Systems</em>, 2002. In particular, if the user-item matrix has <em>m</em> rows and <em>n</em> columns:</p>
<div class="CDPAlignCenter CDPAlign"><img height="38" width="377" src="assets/4842d5bd-d374-4b75-a4dc-60a165dd3443.png" /></div>
<p class="CDPAlignLeft CDPAlign">We have assumed that we have real matrices (which is often true in our case), but, in general, they are complex. <em>U</em> and <em>V</em> are unitary, while sigma is diagonal. The columns of <em>U</em> contain the left singular vectors, the rows of transposed <em>V</em> contain the right singular vectors, while the diagonal matrix Sigma contains the singular values. Selecting <em>k</em> latent factors means taking the first <em>k</em> singular values and, therefore, the corresponding <em>k</em> left and right singular vectors:</p>
<div class="CDPAlignCenter CDPAlign"><img height="37" width="110" src="assets/40bdef1c-a8c9-451a-8ce0-304cd92ddd80.png" /></div>
<p class="CDPAlignLeft CDPAlign">This technique has the advantage of minimizing the Frobenius norm of the difference between <em>M</em> and <em>M<sub>k</sub></em> for any value of <em>k</em>, and therefore, it's an optimal choice to approximate the full decomposition.&#160;Before moving to the prediction stage, let's create an example using SciPy. The first thing to do is to create a dummy user-item matrix:</p>
<pre class="CDPAlignLeft CDPAlign">
<strong>&gt;&gt;&gt; M = np.random.randint(0, 6, size=(20, 10))</strong><br /><br /><strong>&gt;&gt;&gt; print(M)</strong><br /><strong>array([[0, 4, 5, 0, 1, 4, 3, 3, 1, 3],</strong><br /><strong>       [1, 4, 2, 5, 3, 3, 3, 4, 3, 1],</strong><br /><strong>       [1, 1, 2, 2, 1, 5, 1, 4, 2, 5],</strong><br /><strong>       [0, 4, 1, 2, 2, 5, 1, 1, 5, 5],</strong><br /><strong>       [2, 5, 3, 1, 1, 2, 2, 4, 1, 1],</strong><br /><strong>       [1, 4, 3, 3, 0, 0, 2, 3, 3, 5],</strong><br /><strong>       [3, 5, 2, 1, 5, 3, 4, 1, 0, 2],</strong><br /><strong>       [5, 2, 2, 0, 1, 0, 4, 4, 1, 0],</strong><br /><strong>       [0, 2, 4, 1, 3, 1, 3, 0, 5, 4],</strong><br /><strong>       [2, 5, 1, 5, 3, 0, 1, 4, 5, 2],</strong><br /><strong>       [1, 0, 0, 5, 1, 3, 2, 0, 3, 5],</strong><br /><strong>       [5, 3, 1, 5, 0, 0, 4, 2, 2, 2],</strong><br /><strong>       [5, 3, 2, 4, 2, 0, 4, 4, 0, 3],</strong><br /><strong>       [3, 2, 5, 1, 1, 2, 1, 1, 3, 0],</strong><br /><strong>       [1, 5, 5, 2, 5, 2, 4, 5, 1, 4],</strong><br /><strong>       [4, 0, 2, 2, 1, 0, 4, 4, 3, 3],</strong><br /><strong>       [4, 2, 2, 3, 3, 4, 5, 3, 5, 1],</strong><br /><strong>       [5, 0, 5, 3, 0, 0, 3, 5, 2, 2],</strong><br /><strong>       [1, 3, 2, 2, 3, 0, 5, 4, 1, 0],</strong><br /><strong>       [1, 3, 1, 4, 1, 5, 4, 4, 2, 1]])</strong>
</pre>
<p>We're assuming that we have 20 users and 10 products. The ratings are bounded between 1 and 5, and 0 means no rating. Now we can decompose <em>M</em>:</p>
<pre>
<strong>from scipy.linalg import svd</strong><br /><br /><strong>import numpy as np</strong><br /><br /><strong>&gt;&gt;&gt; U, s, V = svd(M, full_matrices=True)</strong><br /><strong>&gt;&gt;&gt; S = np.diag(s)</strong><br /><br /><strong>&gt;&gt;&gt; print(U.shape)</strong><br /><strong>(20L, 20L)</strong><br /><br /><strong>&gt;&gt;&gt; print(S.shape)</strong><br /><strong>(10L, 10L)</strong><br /><br /><strong>&gt;&gt;&gt; print(V.shape)</strong><br /><strong>(10L, 10L)</strong>
</pre>
<p>Now let's consider only the first eight singular values, which will have eight latent factors for both the users and items:</p>
<pre>
<strong>&gt;&gt;&gt; Uk = U[:, 0:8]</strong><br /><strong>&gt;&gt;&gt; Sk = S[0:8, 0:8]</strong><br /><strong>&gt;&gt;&gt; Vk = V[0:8, :]</strong>
</pre>
<p>Bear in mind that in SciPy SVD implementation,&#160;<em>V</em> is already transposed. According to Sarwar B., Karypis G., Konstan J., Riedl J.,&#160;<em>Incremental Singular Value Decomposition Algorithms for Highly Scalable Recommender Systems</em>, 2002, we can easily get a prediction considering the cosine similarity (which is proportional to the dot product) between customers and products. The two latent factor matrices are:</p>
<div class="CDPAlignCenter CDPAlign"><img height="63" width="123" src="assets/3d3ce953-09ff-488d-9708-7641667766fa.png" /></div>
<p class="CDPAlignLeft CDPAlign">In order to take into account the loss of precision, it's useful also&#160;<span>to</span><span>&#160;</span><span>consider the average rating per user (which corresponds to the mean row value of the user-item matrix), so that the result rating prediction for the user</span> <em>i</em> <span>and the item</span> <em>j</em> <span>becomes:</span></p>
<div class="CDPAlignCenter CDPAlign"><img height="34" width="179" src="assets/67fb7238-03dd-4daa-888f-6424b61c821c.png" /></div>
<p class="CDPAlignLeft CDPAlign">Here <em>S<sub>U</sub>(i)</em> and <em>S<sub>I</sub>(j)</em> are the user and product vectors respectively. Continuing with our example, let's determine the rating prediction for user 5 and item 2:</p>
<pre class="CDPAlignLeft CDPAlign">
<strong>&gt;&gt;&gt; Su = Uk.dot(np.sqrt(Sk).T)</strong><br /><strong>&gt;&gt;&gt; Si = np.sqrt(Sk).dot(Vk).T</strong><br /><strong>&gt;&gt;&gt; Er = np.mean(M, axis=1)</strong><br /><br /><strong>&gt;&gt;&gt; r5_2 = Er[5] + Su[5].dot(Si[2])<br />&gt;&gt;&gt; print(r5_2)</strong><br /><strong>2.38848720112</strong>
</pre>
<p>This approach has medium complexity. In particular, the SVD is <em>O(m<sup>3</sup>)</em> and an incremental strategy (as described in Sarwar B., Karypis G., Konstan J., Riedl J.,&#160;<em>Incremental Singular Value Decomposition Algorithms for Highly&#160;</em><em>Scalable Recommender Systems</em>, 2002) must be employed when new users or items are added; however, it can be effective when the number of elements is not too big. In all the other cases, the next strategy (together with a parallel architecture) can be adopted.</p>


            </article>

            
        </section>
    </body>

</html>
