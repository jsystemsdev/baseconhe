<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Model-free (or memory-based) collaborative filtering</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Model-free (or memory-based) collaborative filtering</h1>
                </header>
            
            <article>
                
<p>As with the user-based approach, let's consider&#160;having two sets of elements: users and items. However, in this case, we don't assume that they have explicit features. Instead, we try to model a user-item matrix based on the preferences of each user (rows) for each item&#160;(columns). For example:</p>
<div class="CDPAlignCenter CDPAlign"><img height="189" width="303" src="assets/6349a75c-d8d0-4b61-8fee-9597dd79615c.png" /></div>
<p class="CDPAlignLeft CDPAlign">In this case, the ratings are bounded between 1 and 5 (0 means no rating), and our goal is to cluster the users according to their rating vector (which is, indeed, an internal representation based on a particular kind of feature). This allows producing recommendations even&#160;when there are no explicit pieces of information about the user. However,&#160;it has a drawback, called <strong>cold-startup</strong>, which means that when a new user has no ratings, it's impossible to find the right neighborhood, because he/she can belong to&#160;<span>virtually</span><span>&#160;</span><span>any cluster.</span></p>
<p class="CDPAlignLeft CDPAlign">Once the clustering is done, it's easy to check which products (not rated yet) have the higher rating for a given user and therefore are more likely to be bought. It's possible to implement a solution in scikit-learn as we've done before, but I'd like to introduce a small framework called <strong>Crab</strong> (see the box at the end of this section) that simplifies this process.</p>
<p class="CDPAlignLeft CDPAlign">In order to build the model, we&#160;<span>first</span><span>&#160;</span><span>need to define the user-item matrix as a Python dictionary with the structure:</span></p>
<pre class="CDPAlignLeft CDPAlign">
<strong>{ user_1: { item1: rating, item2: rating, ... }, ..., user_n: ... }</strong>
</pre>
<p>A missing value in a user internal dictionary means no rating. In our example, we consider 5 users with 5 items:</p>
<pre>
<strong>from scikits.crab.models import MatrixPreferenceDataModel</strong><br /><br /><strong>&gt;&gt;&gt; user_item_matrix = {</strong><br /><strong>       1: {1: 2, 2: 5, 3: 3},</strong><br /><strong>       2: {1: 5, 4: 2},</strong><br /><strong>       3: {2: 3, 4: 5, 3: 2},</strong><br /><strong>       4: {3: 5, 5: 1},</strong><br /><strong>       5: {1: 3, 2: 3, 4: 1, 5: 3}</strong><br /><strong>   }</strong><br /><br /><strong>&gt;&gt;&gt; model = MatrixPreferenceDataModel(user_item_matrix)</strong>
</pre>
<p>Once the user-item matrix has been defined, we need to pick a metric and therefore, a distance function <em>d(u<sub>i</sub>, u<sub>j</sub>)</em>, to build a similarity matrix:</p>
<div class="CDPAlignCenter CDPAlign"><img height="79" width="234" src="assets/8590ebef-bee1-4f1c-bf79-bf847fa03661.png" /></div>
<p class="CDPAlignLeft CDPAlign">Using Crab, we do this in the following way (using a Euclidean metric):</p>
<pre class="CDPAlignLeft CDPAlign">
<strong>from scikits.crab.similarities import UserSimilarity</strong><br /><strong>from scikits.crab.metrics import euclidean_distances</strong><br /><br /><strong>&gt;&gt;&gt; similarity_matrix = UserSimilarity(model, euclidean_distances)</strong>
</pre>
<p>There are many metrics, like Pearson or Jaccard, so I suggest visiting the website (<a href="http://muricoca.github.io/crab">http://muricoca.github.io/crab</a>) to retrieve further information. At this point, it's possible to build the recommendation system (based on <span>the</span>&#160;k-nearest neighbors&#160;clustering method) and test it:</p>
<pre>
<strong>from scikits.crab.recommenders.knn import UserBasedRecommender</strong><br /><br /><strong>&gt;&gt;&gt; recommender = UserBasedRecommender(model, similarity_matrix, with_preference=True)</strong><br /><br /><strong>&gt;&gt;&gt; print(recommender.recommend(2))</strong><br /><strong>[(2, 3.6180339887498949), (5, 3.0), (3, 2.5527864045000417)]</strong>
</pre>
<p>So the recommender suggests the following predicted rating for user 2:</p>
<ul>
<li><strong>Item 2</strong>: 3.6 (which can be rounded to 4.0)</li>
<li><strong>Item 5</strong>: 3</li>
<li><strong>Item 3</strong>: 2.5 (which can be rounded to 3.0)</li>
</ul>
<p>When running the code, it's possible to see some warnings (Crab is still under development); however, they don't condition the functionality. If you want to avoid them, you can use the <kbd>catch_warnings()</kbd> context manager:</p>
<pre>
<strong>import warnings</strong><br /><br /><strong>&gt;&gt;&gt; with warnings.catch_warnings():</strong><br /><strong>&gt;&gt;&gt;    warnings.simplefilter("ignore")</strong><br /><strong>&gt;&gt;&gt;    print(recommender.recommend(2))</strong>
</pre>
<p>It's possible to suggest all the items, or limit the list to the higher ratings (so, for example, avoiding the item 3). This approach is quite similar to the user-based model. However, it's faster (very big matrices can be processed in parallel) and it doesn't take care of details that can produce misleading results. Only the ratings are considered as useful features to define a user. Like model-based collaborative filtering, the cold-startup problem can be addressed in two ways:</p>
<ul>
<li>Asking the user to rate some items (this approach is often adopted because it's easy to show some movie/book covers, asking the user to select what they like and what they don't).</li>
<li>Placing the user in an average neighborhood by randomly assigning some mean ratings. In this approach, it's possible to start using the recommendation system immediately. However, it's necessary to accept a certain degree of error at the beginning and to correct the dummy ratings when the real ones are produced.</li>
</ul>
<div class="packt_infobox">Crab is an open-source framework for building collaborative filtering systems. It's still under development and therefore, doesn't implement all possible features. However, it's very easy to use and is quite powerful for many tasks. The home page with installation instructions and documentation is:&#160;<a href="http://muricoca.github.io/crab/index.html" target="_blank">http://muricoca.github.io/crab/index.html</a>. Crab depends on scikits.learn, which still has some issues with Python 3. Therefore, I recommend using Python 2.7 for this example. It's possible to install both packages using pip: <kbd>pip install -U scikits.learn</kbd> and <kbd>pip install -U crab</kbd>.</div>


            </article>

            
        </section>
    </body>

</html>
