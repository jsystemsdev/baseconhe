<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Alternating least squares with Apache Spark MLlib</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Alternating least squares with Apache Spark MLlib</h1>
                </header>
            
            <article>
                
<p>Apache Spark is beyond the scope of this book, so if you want to know more about this powerful framework, I suggest you read the online documentation or one the many books available. In Pentreath N., <em>Machine Learning with Spark</em>, Packt, there's an interesting introduction on the library MLlib and how to implement most of the algorithms discussed in this book.</p>
<p>Spark is a parallel computational engine that is now part of the Hadoop project (even if it doesn't use its code), that can run in local mode or on very large clusters (with thousands of nodes), to execute complex tasks using huge amounts of data. It's mainly based on Scala, though there are interfaces for Java, Python, and R. In this example, we're going to use PySpark, which is the built-in shell for running Spark with Python code.</p>
<p>After launching PySpark in local mode, we get a standard Python prompt and we can start working, just like with any other standard Python environment:</p>
<pre>
<strong># Linux<br />&gt;&gt;&gt; ./pyspark<br /><br /># Mac OS X<br />&gt;&gt;&gt; pyspark<br /><br /># Windows<br />&gt;&gt;&gt; pyspark</strong><br /><br /><strong>Python 2.7.12 |Anaconda 4.0.0 (64-bit)| (default, Jun 29 2016, 11:07:13) [MSC v.1500 64 bit (AMD64)] on win32</strong><br /><strong>Type "help", "copyright", "credits" or "license" for more information.</strong><br /><strong>Anaconda is brought to you by Continuum Analytics.</strong><br /><strong>Please check out: http://continuum.io/thanks and https://anaconda.org</strong><br /><strong>Using Spark's default log4j profile: org/apache/spark/log4j-defaults.properties</strong><br /><strong>Setting default log level to "WARN".</strong><br /><strong>To adjust logging level use sc.setLogLevewl(newLevel).</strong><br /><strong>Welcome to</strong><br /><strong> ____ __</strong><br /><strong> / __/__ ___ _____/ /__</strong><br /><strong> _\ \/ _ \/ _ `/ __/ '_/</strong><br /><strong> /__ / .__/\_,_/_/ /_/\_\ version 2.0.2</strong><br /><strong> /_/</strong><br /><br /><strong>Using Python version 2.7.12 (default, Jun 29 2016 11:07:13)</strong><br /><strong>SparkSession available as 'spark'.</strong><br /><strong>&gt;&gt;&gt;</strong>
</pre>
<p>Spark MLlib implements the ALS algorithm through a very simple mechanism. The class <kbd>Rating</kbd> is a wrapper for the tuple (user, product, rating), so we can easily define a dummy dataset (which must be considered only as an example, because it's very limited):</p>
<pre>
<strong>from pyspark.mllib.recommendation import Rating</strong><br /><br /><strong>import numpy as np</strong><br /><br /><strong>&gt;&gt;&gt; nb_users = 200</strong><br /><strong>&gt;&gt;&gt; nb_products = 100</strong><br /><br /><strong>&gt;&gt;&gt; ratings = []</strong><br /><br /><strong>&gt;&gt;&gt; for _ in range(10):</strong><br /><strong>&gt;&gt;&gt;    for i in range(nb_users):</strong><br /><strong>&gt;&gt;&gt;        rating = Rating(user=i, </strong><br /><strong>&gt;&gt;&gt;                        product=np.random.randint(1, nb_products), </strong><br /><strong>&gt;&gt;&gt;                        rating=np.random.randint(0, 5))</strong><br /><strong>&gt;&gt;&gt;        ratings.append(rating)</strong><br /><br /><strong>&gt;&gt;&gt; ratings = sc.parallelize(ratings)</strong>
</pre>
<p>We assumed that we have 200 users and 100 products and we have populated a list of ratings by iterating 10 times the main loop which assigns a rating to a random product. We're not controlling repetitions or other uncommon situations. The last command <kbd>sc.parallelize()</kbd>&#160;is a way to ask Spark to transform our list into a structure called <strong>resilient distributed dataset</strong> (<strong>RDD</strong>), which will be used for the remaining operations. There are no actual limits to the size of these structures, because they are distributed across different executors (if in clustered mode) and can work with petabytes datasets just like we work with kilobytes ones.</p>
<p>At this point, we can train an <kbd>ALS</kbd> model (which is formally&#160;<kbd>MatrixFactorizationModel</kbd>) and use it to make some predictions:</p>
<pre>
<strong>from pyspark.mllib.recommendation import ALS<br /></strong><br /><strong>&gt;&gt;&gt; model = ALS.train(ratings, rank=5, iterations=10)</strong>
</pre>
<p>We want 5 latent factors and 10 optimization iterations. As discussed before, it's not very easy to determine the right rank for each model, so, after a training phase, there should always be a validation phase with known data. The mean squared error is a good measure to understand how the model is working. We can do it using the same training data set. The first thing to do is to remove the ratings (because we need only the tuple made up of user and product):</p>
<pre>
<strong>&gt;&gt;&gt; test = ratings.map(lambda rating: (rating.user, rating.product))</strong>
</pre>
<p>If you're not familiar with the MapReduce paradigm, you only need to know that <kbd>map()</kbd> applies the same function (in this case, a lambda) to all the elements. Now we can massively predict the ratings:</p>
<pre>
<strong>&gt;&gt;&gt; predictions = model.predictAll(test)</strong>
</pre>
<p>However, in order to compute the error, we also need to add the user and product, to have tuples that can be compared:</p>
<pre>
<strong>&gt;&gt;&gt; full_predictions = predictions.map(lambda pred: ((pred.user, pred.product), pred.rating))</strong>
</pre>
<p>The result is a sequence of rows with a structure&#160;<kbd>((user, item), rating)</kbd>, just like a standard dictionary entry <kbd>(key, value)</kbd>. This is useful because, using Spark, we can join two RDDs by using their keys. We do the same thing for the original dataset also, and then we proceed by joining the training values with the predictions:</p>
<pre>
<strong>&gt;&gt;&gt; split_ratings = ratings.map(lambda rating: ((rating.user, rating.product), rating.rating))</strong><br /><strong>&gt;&gt;&gt; joined_predictions = split_ratings.join(full_predictions)</strong>
</pre>
<p>Now for each key <kbd>(user, product)</kbd>, we have two values: target and prediction. Therefore, we can compute the mean squared error:</p>
<pre>
<strong>&gt;&gt;&gt; mse = joined_predictions.map(lambda x: (x[1][0] - x[1][1]) ** 2).mean()</strong>
</pre>
<p>The first map transforms each row into the squared difference between the target and prediction, while the <kbd>mean()</kbd> function computes the average value. At this point, let's check our error and produce a prediction:</p>
<pre>
<strong>&gt;&gt;&gt; print('MSE: %.3f' % mse)</strong><br /><strong>MSE: 0.580</strong><br /><br /><strong>&gt;&gt;&gt; prediction = model.predict(10, 20)</strong><br /><strong>&gt;&gt;&gt; print('Prediction: %3.f' % prediction)</strong><br /><strong>Prediction: 2.810</strong>
</pre>
<p>So, our error is quite low but<span>&#160;</span>it can be improved by changing the rank or the number of iterations. The prediction for the rating of the product 20 by the user 10 is about 2.8 (that can be rounded to 3). If you run the code, these values can be different as we're using a random user-item matrix. Moreover, if you don't want to use the shell and run the code directly, you need to declare a <kbd>SparkContext</kbd> explicitly at the beginning of your file:</p>
<pre>
<strong>from pyspark import SparkContext, SparkConf</strong><br /><br /><strong>&gt;&gt;&gt; conf = SparkConf().setAppName('ALS').setMaster('local[*]')</strong><br /><strong>&gt;&gt;&gt; sc = SparkContext(conf=conf)</strong>
</pre>
<p>We have created a configuration through the <kbd>SparkConf</kbd> class and specified both an application name and a master (in local mode with all cores available). This is enough to run our code. However, if you need further information, visit the page mentioned in the information box at the end of the chapter. To run the application (since Spark 2.0), you must execute the following command:</p>
<pre>
<strong># Linux, Mac OSx</strong><br /><strong>./spark-submit als_spark.py</strong><br /><br /><strong># Windows</strong><br /><strong>spark-submit als_spark.py</strong>
</pre>
<div class="packt_infobox">When running a script using <kbd>spark-submit</kbd>, you will see hundreds of log lines that inform you about all the operations that are being performed. Among them, at the end of the computation, you'll also see the print function messages (<kbd>stdout</kbd>).</div>
<p>Of course, this is only an introduction to Spark ALS, but I hope it was useful to understand how easy this process can be and, at the same time, how the dimensional limitations can be effectively addressed.</p>
<div class="packt_tip packt_infobox">If you don't know how to set up the environment and launch PySpark, I suggest reading the online quick-start guide (<a href="https://spark.apache.org/docs/2.1.0/quick-start.html">https://spark.apache.org/docs/2.1.0/quick-start.html</a>) that can be useful even if you don't know all the details and configuration parameters.</div>


            </article>

            
        </section>
    </body>

</html>
