<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Principal component analysis</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Principal component analysis</h1>
                </header>
            
            <article>
                
<p>In many cases, the dimensionality of the input dataset <em>X</em> is high and so is the complexity of every related machine learning algorithm. Moreover, the information is seldom spread uniformly across all the features and, as discussed in the previous chapter, there will be high entropy features together with low entropy ones, which, of course, don't contribute dramatically to the final outcome. In general, if we consider a Euclidean space, we have:</p>
<div class="CDPAlignCenter CDPAlign"><img height="41" width="490" src="assets/52b8cfd5-0935-47c7-bebe-386a6b1d2415.png" /></div>
<p class="CDPAlignLeft CDPAlign">So each point is expressed using an orthonormal basis made of <em>m</em> linearly independent vectors. Now, considering a dataset <em>X</em>, a natural question arises: is it possible to reduce <em>m</em> without a drastic loss of information? Let's consider the following figure (without any particular interpretation):</p>
<div class="CDPAlignCenter CDPAlign"><img class="image-border" src="assets/42254781-861e-47de-8ed7-2f2a942744f2.png" /></div>
<p class="mce-root">It doesn't matter which distributions generated <em>X=(x,y)</em>, however, the variance of the horizontal component is clearly higher than the vertical one. As discussed, it means that the amount of information provided by the first component is higher and, for example, if the&#160;<em>x</em> axis is stretched horizontally keeping the vertical one fixed, the distribution becomes similar to a segment where the depth has lower and lower importance.</p>
<p class="mce-root">In order to assess how much information is brought by each component, and the correlation among them, a useful tool is the covariance matrix (if the dataset has zero mean,&#160;we can use the&#160;correlation matrix):</p>
<div class="packt_figref CDPAlignCenter CDPAlign">&#160;<img height="139" width="291" src="assets/1b582361-8dd7-4b97-94b7-62dbd29fd260.png" /></div>
<p class="mce-root"><em>C</em> is symmetric and positive semidefinite, so all the eigenvalues are non-negative, but what's the meaning of each value?&#160;The covariance matrix for the previous example is:</p>
<p class="packt_figref CDPAlignCenter CDPAlign"><img height="46" width="134" src="assets/958809f4-a0b1-4202-9b6a-887a9b263c25.png" /></p>
<p class="mce-root">As expected, the horizontal variance is quite a bit higher than the vertical one. Moreover, the other values are close to zero. If you remember the definition and, for simplicity, remove the mean term, they represent the cross-correlation between couples of components. It's obvious that in our example, <em>X</em> and <em>Y</em> are uncorrelated (they're orthogonal), but in real-life examples, there could be features which present a residual cross-correlation. In terms of information theory, it means that knowing <em>Y</em> gives us some information about <em>X</em> (which we already know), so they share information which is indeed doubled. So our goal is also to decorrelate <em>X</em> while trying to reduce its dimensionality.</p>
<p class="mce-root">This can be achieved considering the sorted eigenvalues of <em>C</em> and selecting&#160;<em>g &lt; m</em> values:</p>
<div class="packt_figref CDPAlignCenter CDPAlign"><img height="35" width="426" src="assets/758ae107-2231-4c89-ae10-0f00bda15761.png" /></div>
<div class="packt_figref CDPAlignCenter CDPAlign"><img height="36" width="357" src="assets/7676b255-ac1f-4671-be01-f9996ac8ee59.png" /></div>
<p class="mce-root">So, it's possible to project the original feature vectors into this new (sub-)space, where each component carries a portion of total variance and where the new covariance matrix is decorrelated to reduce useless information sharing (in terms of correlation) among different features. In scikit-learn, there's the <kbd>PCA</kbd>&#160;class which can do all this in a very smooth way:</p>
<pre>
<strong>from sklearn.datasets import load_digits<br />from sklearn.decomposition import PCA<br /><br />&gt;&gt;&gt; digits = load_digits()</strong>
</pre>
<p>A figure with a few random MNIST handwritten digits is shown as follows:</p>
<div class="CDPAlignCenter CDPAlign"><img height="474" width="476" class="image-border" src="assets/6500cfc1-8599-4b0e-9412-4801d5a69e26.png" /></div>
<p class="mce-root">Each image is a vector of 64 unsigned int (8 bit) numbers (0, 255), so the initial number of components is indeed 64. However, the total amount of black pixels is often predominant and the basic signs needed to write 10 digits are similar, so it's reasonable to assume both high cross-correlation and a low variance on several components. Trying with 36 principal components, we get:</p>
<pre>
<strong>&gt;&gt;&gt; pca = PCA(n_components=36, whiten=True)<br />&gt;&gt;&gt; X_pca = pca.fit_transform(digits.data / 255)</strong>
</pre>
<p>In order to improve performance, all integer values are normalized into the range [0, 1] and, through the parameter <kbd>whiten=True</kbd>, the variance of each component is scaled to one. As also the official scikit-learn documentation says, this process is particularly useful when an isotropic distribution is needed for many algorithms to perform efficiently. It's possible to access the explained variance ratio through the instance variable <kbd>explained_variance_ratio_</kbd><em><strong>,&#160;</strong></em>which shows which part of the total variance is carried by each single component:</p>
<pre>
<strong>&gt;&gt;&gt; pca.explained_variance_ratio_<br />array([ 0.14890594,  0.13618771,  0.11794594,  0.08409979,  0.05782415,<br />        0.0491691 ,  0.04315987,  0.03661373,  0.03353248,  0.03078806,<br />        0.02372341,  0.02272697,  0.01821863,  0.01773855,  0.01467101,<br />        0.01409716,  0.01318589,  0.01248138,  0.01017718,  0.00905617,<br />        0.00889538,  0.00797123,  0.00767493,  0.00722904,  0.00695889,<br />        0.00596081,  0.00575615,  0.00515158,  0.00489539,  0.00428887,<br />        0.00373606,  0.00353274,  0.00336684,  0.00328029,  0.0030832 ,<br />        0.00293778])</strong>
</pre>
<p>A plot for the example of MNIST digits is shown next. The left graph represents the variance ratio while the right one is the cumulative variance. It can be immediately seen how the first components are normally the most important ones in terms of information, while the following ones provide details that a classifier&#160;could also discard:</p>
<div class="CDPAlignCenter CDPAlign"><img height="197" width="488" class="image-border" src="assets/378293b8-af36-4351-9ef4-fd5387b23bcc.png" /></div>
<p>As expected, the contribution to the total variance decreases dramatically starting from the fifth component, so it's possible to reduce the original dimensionality without an unacceptable loss of information, which could drive an algorithm to learn wrong classes. In the preceding graph, there are the same handwritten digits rebuilt using the first 36 components&#160;with whitening and normalization between 0 and 1. To obtain the original images, we need to inverse-transform all new vectors and project them into the original space:</p>
<pre>
<strong>&gt;&gt;&gt; X_rebuilt = pca.inverse_transform(X_pca)</strong>
</pre>
<p>The result is shown in the following figure:</p>
<div class="packt_figref CDPAlignCenter CDPAlign"><img height="462" width="463" class="image-border" src="assets/6c2cc12d-7cd1-4efe-9db1-1e7dae8975d3.png" /></div>
<p class="mce-root">This process can also partially denoise the original images by removing residual variance, which is often associated with noise or unwanted contributions (almost every calligraphy&#160;distorts some of the structural elements which are used for recognition).</p>
<p class="mce-root">I suggest the reader try different numbers of components (using the explained variance data) and also <kbd>n_components='mle'</kbd>, which implements an automatic selection of the best dimensionality (Minka T.P,&#160;<em>Automatic Choice of Dimensionality for PCA</em>, NIPS 2000: 598-604).</p>
<div class="packt_infobox">scikit-learn solves the PCA problem with <strong>SVD</strong> (<strong>Singular Value Decomposition</strong>), which can be studied in detail in&#160;Poole D., <em>Linear Algebra</em><span>, Brooks Cole</span>. It's possible to control the algorithm through the parameter <kbd>svd_solver</kbd>, whose values are&#160;<kbd>'auto', 'full', 'arpack', 'randomized'</kbd>. Arpack implements a truncated SVD. Randomized is based on an approximate algorithm which drops many singular vectors and can achieve very good performances also with high-dimensional datasets where the actual number of components is sensibly smaller.</div>


            </article>

            
        </section>
    </body>

</html>
