<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Maximum-likelihood learning</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Maximum-likelihood learning</h1>
                </header>
            
            <article>
                
<p>We have defined likelihood as a filtering term in the Bayes formula. In general, it has the form of:</p>
<div class="CDPAlignCenter CDPAlign"><img height="40" width="150" src="assets/fae273c6-7f76-41dd-9801-4af69fa299f7.png" /></div>
<p class="CDPAlignLeft CDPAlign">Here the first term expresses the actual likelihood of a hypothesis, given a dataset <em>X</em>. As you can imagine, in this formula there are no more Apriori probabilities, so, maximizing it doesn't imply accepting a theoretical preferential hypothesis, nor considering unlikely ones. A very common approach, known as <strong>expectation-maximization</strong>&#160;and used in many algorithms (we're going to see an example in logistic regression), is split into two main parts:</p>
<ul>
<li>
<p>Determining a log-likelihood expression based on model parameters (they will be optimized accordingly)</p>
</li>
<li>
<p>Maximizing it until residual error is small enough</p>
</li>
</ul>
<p>A log-likelihood (normally called&#160;<strong>L</strong>) is a&#160;useful trick that can simplify gradient calculations. A generic likelihood expression is:</p>
<div class="CDPAlignCenter CDPAlign"><img height="53" width="153" src="assets/14849d03-bcd9-44ae-8b08-faaefa2ed0ac.png" /></div>
<p class="CDPAlignLeft CDPAlign">As all parameters are inside <em>h<sub>i</sub></em>, the gradient is a complex expression which isn't very manageable. However our goal is maximizing the likelihood, but it's easier minimizing its reciprocal:</p>
<div class="CDPAlignCenter CDPAlign"><img height="60" width="303" src="assets/2257881d-0d4a-4956-b4cb-3a1af54d2318.png" /></div>
<p class="CDPAlignLeft CDPAlign">This can be turned into a very simple expression by applying natural logarithm (which is monotonic):</p>
<div class="CDPAlignCenter CDPAlign"><img height="50" width="406" src="assets/6dd31fe9-4d43-41d5-a999-c6be71f1b073.png" /></div>
<p class="CDPAlignLeft CDPAlign">The last term is a summation which can be easily derived and used in most of the optimization algorithms. At the end of this process, we can find a set of parameters which provides the maximum likelihood without any strong statement about prior distributions. This approach can seem very technical, but its logic is really simple and intuitive. To understand how it works, I propose a simple exercise, which is part of Gaussian mixture technique discussed also in&#160;Russel S., Norvig P., <em>Artificial Intelligence: A Modern Approach</em>, Pearson<em>.</em></p>
<p class="CDPAlignLeft CDPAlign">Let's consider 100 points drawn from a Gaussian distribution with zero mean and a standard deviation equal to 2.0 (quasi-white noise made of independent samples):</p>
<pre>
<strong>import numpy as np</strong><br /><br /><strong>nb_samples = 100</strong><br /><strong>X_data = np.random.normal(loc=0.0, scale=np.sqrt(2.0), size=nb_samples)</strong>
</pre>
<p class="CDPAlignLeft CDPAlign">The plot is shown next:</p>
<div class="CDPAlignCenter CDPAlign"><img class="image-border" src="assets/6afb3715-a150-45da-b597-0c3ff8c20419.png" /></div>
<p class="mce-root">In this case, there's no need for a deep exploration (we know how they are generated), however, after restricting the hypothesis space to the Gaussian family (the most suitable considering only the graph), we'd like to find the best value for mean and variance. First of all, we need to compute the log-likelihood (which is rather simple thanks to the exponential function):</p>
<div class="CDPAlignCenter CDPAlign"><img height="63" width="365" src="assets/a52d6108-2adc-43e2-9271-660280846585.png" /></div>
<p class="mce-root">A simple Python implementation is provided next (for ease of use, there's only a single array which contains both mean (0) and variance (1)):</p>
<pre>
<strong>def negative_log_likelihood(v):</strong><br /><strong>  l = 0.0</strong><br /><strong>  f1 = 1.0 / np.sqrt(2.0 * np.pi * v[1]) </strong><br /><strong>  f2 = 2.0 * v[1]</strong><br /> <br /><strong>  for x in X_data:</strong><br /><strong>    l += np.log(f1 * np.exp(-np.square(x - v[0]) / f2))</strong><br /> <br /><strong> return -l</strong>
</pre>
<p class="mce-root">Then we need to find its minimum (in terms of mean and variance) with any of the available methods (gradient descent or another numerical optimization algorithm). For example, using the&#160;<kbd>scipy</kbd> minimization function, we can easily get:</p>
<pre>
<strong>from scipy.optimize import minimize<br /><br />&gt;&gt;&gt; minimize(fun=negative_log_likelihood, x0=[0.0, 1.0])<br /><br /> fun: 172.33380423827057<br /> hess_inv: array([[ 0.01571807,  0.02658017],<br />       [ 0.02658017,  0.14686427]])<br />      jac: array([  0.00000000e+00,  -1.90734863e-06])<br />  message: 'Optimization terminated successfully.'<br />     nfev: 52<br />      nit: 9<br />     njev: 13<br />   status: 0<br />  success: True<br />        x: array([ 0.04088792,  1.83822255])</strong>
</pre>
<p class="mce-root">A graph of the&#160;negative log-likelihood function is plotted next. The global minimum of this function corresponds to an optimal likelihood given a certain distribution. It doesn't mean that the problem has been completely solved, because the first step of this algorithm is determining an expectation, which must be always realistic. The likelihood function, however, is quite sensitive to wrong distributions because it can easily get close to zero when the probabilities are low. For this reason, <strong>maximum-likelihood</strong> (<strong>ML</strong>) learning is often preferable to MAP learning, which needs Apriori distributions and can fail when they are not selected in the most appropriate way:</p>
<div class="mce-root CDPAlignCenter CDPAlign"><img height="353" width="513" class="image-border" src="assets/0f18e55f-1cd1-481b-b86c-09f948f318ff.png" /></div>
<p class="mce-root">This approach has been applied to a specific distribution family (which is indeed very easy to manage), but it also works perfectly when the model is more complex. Of course, it's always necessary to have an initial awareness about how the likelihood should be determined&#160;because more than one feasible family can generate the same dataset. In all these cases, <strong>Occam's razor</strong> is the best way to proceed: the simplest hypothesis should be considered first. If it doesn't fit, an extra level of complexity can be added to our model. As we'll see, in many situations, the easiest solution is the winning one, and increasing the number of parameters or using a more detailed model can only add noise and a higher possibility of overfitting.</p>
<div class="packt_figref CDPAlignLeft CDPAlign packt_infobox">SciPy (<a href="https://www.scipy.org" target="_blank">https://www.scipy.org</a>) is a set of high-end scientific and data-oriented libraries available for Python. It includes NumPy, Pandas, and many other useful frameworks. If you want to read more about Python scientific computing, refer to&#160;Johansson R., <em>Numerical Python</em>, Apress or Landau R. H., Pàez M. J., Bordeianu C. C., <em>Computational Physics. Problem Solving with Python,</em> Wiley-VCH.</div>


            </article>

            
        </section>
    </body>

</html>
