<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Logistic regression</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Logistic regression</h1>
                </header>
            
            <article>
                
<p>Now we can try a more complex example implementing a logistic regression algorithm. The first step, as usual, is creating a dummy dataset:</p>
<pre>
<strong>from sklearn.datasets import make_classification</strong><br /><br /><strong>&gt;&gt;&gt; nb_samples = 500</strong><br /><strong>&gt;&gt;&gt; X, Y = make_classification(n_samples=nb_samples, n_features=2, n_redundant=0, n_classes=2)</strong>
</pre>
<p>The dataset is shown in the following figure:</p>
<div class="CDPAlignCenter CDPAlign"><img height="381" width="484" class="image-border" src="assets/3b969285-5ef7-4a0a-9667-ec0b7f39e68d.png" /></div>
<p class="CDPAlignLeft CDPAlign">At this point, we can create the graph and all placeholders, variables, and operations:</p>
<pre class="CDPAlignLeft CDPAlign">
<strong>import tensorflow as tf</strong><br /><br /><strong>&gt;&gt;&gt; graph = tf.Graph()</strong><br /><br /><strong>&gt;&gt;&gt; with graph.as_default():</strong><br /><strong>&gt;&gt;&gt;    Xt = tf.placeholder(tf.float32, shape=(None, 2), name='points')</strong><br /><strong>&gt;&gt;&gt;    Yt = tf.placeholder(tf.float32, shape=(None, 1), name='classes')</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;    W = tf.Variable(tf.zeros((2, 1)), name='weights')</strong><br /><strong>&gt;&gt;&gt;    bias = tf.Variable(tf.zeros((1, 1)), name='bias')</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;    Ye = tf.matmul(Xt, W) + bias</strong><br /><strong>&gt;&gt;&gt;    Yc = tf.round(tf.sigmoid(Ye))</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;    loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=Ye, labels=Yt))</strong><br /><strong>&gt;&gt;&gt;    training_step = tf.train.GradientDescentOptimizer(0.025).minimize(loss)</strong>
</pre>
<p>The placeholder <kbd>Xt</kbd> is needed for the points, while <kbd>Yt</kbd> represents the labels. At this point, we need to involve a couple of variables: if you remember, they store values that are updated by the training algorithm. In this case, we need a weight vector <kbd>W</kbd> (with two elements) and a single <kbd>bias</kbd>. When a variable is declared, its initial value must be provided; we've decided to set both to zero using the function <kbd>tf.zeros()</kbd>, which accepts as argument the shape of the desired tensor.&#160;</p>
<p>Now we can compute the output (if you don't remember how logistic regression works, please step back to <a href="7d3b20b9-0cc4-482b-a0b1-522361617ecc.xhtml" target="_blank">Chapter 5</a>, <em>Logistic Regression</em>) in two steps: first the sigmoid exponent <kbd>Ye</kbd> and then the actual binary&#160;output <kbd>Yc</kbd>, which is obtained by rounding the sigmoid value. The training algorithm for a logistic regression minimizes the negative log-likelihood, which corresponds to the cross-entropy between the real distribution <kbd>Y</kbd> and <kbd>Yc</kbd>. It's easy to implement this loss function; however, the function <kbd>tf.log()</kbd> is numerically unstable (when its value becomes close to zero, it tends to negative infinity and yields a <kbd>NaN</kbd> value); therefore, TensorFlow has implemented a more robust function, <kbd>tf.nn.sigmoid_cross_entropy_with_logits()</kbd>, which computes the cross-entropy assuming the output is produced by a sigmoid. It takes two parameters, the <kbd>logits</kbd> (which corresponds to the exponent <kbd>Ye</kbd>) and the target <kbd>labels</kbd>, that are stored in&#160;<kbd>Yt</kbd>.</p>
<p>Now we can work with one of the most powerful TensorFlow features: the training optimizers. After defining a loss function, it will be&#160;dependent on placeholders, constants, and variables. A training optimizer (such as&#160;<kbd>tf.train.GradientDescentOptimizer()</kbd>), through its method <kbd>minimize()</kbd>, accepts the loss function to optimize. Internally, according to every specific algorithm, it will compute the gradients of the loss function with respect to all trainable variables and will apply the corresponding corrections to the values. The parameter passed to the optimizer is the learning rate.</p>
<p>Therefore, we have defined an extra operation called <kbd>training_step</kbd>, which corresponds to a single stateful update step. It doesn't matter how complex the graph is; all trainable variables involved in a loss function will be optimized with a single instruction.</p>
<p>Now it's time to train our logistic regression. The first thing to do is to ask TensorFlow to initialize all variables so that they are ready when the operations have to work with them:</p>
<pre>
<strong>&gt;&gt;&gt; session = tf.InteractiveSession(graph=graph)</strong><br /><strong>&gt;&gt;&gt; tf.global_variables_initializer().run()</strong>
</pre>
<p>At this point, we can create a simple training loop (it should be stopped when the loss stops decreasing; however, we have a fixed number of iterations):</p>
<pre>
<strong>&gt;&gt;&gt; feed_dict = {</strong><br /><strong>&gt;&gt;&gt;    Xt: X,</strong><br /><strong>&gt;&gt;&gt;    Yt: Y.reshape((nb_samples, 1))</strong><br /><strong>&gt;&gt;&gt; }</strong><br /><br /><strong>&gt;&gt;&gt; for i in range(5000):</strong><br /><strong>&gt;&gt;&gt;    loss_value, _ = session.run([loss, training_step], feed_dict=feed_dict)</strong><br /><strong>&gt;&gt;&gt;    if i % 100 == 0:</strong><br /><strong>&gt;&gt;&gt;    print('Step %d, Loss: %.3f' % (i, loss_value))<br />Step 0, Loss: 0.269<br />Step 100, Loss: 0.267<br />Step 200, Loss: 0.265<br />Step 300, Loss: 0.264<br />Step 400, Loss: 0.263<br />Step 500, Loss: 0.262<br />Step 600, Loss: 0.261<br />Step 700, Loss: 0.260<br />Step 800, Loss: 0.260<br />Step 900, Loss: 0.259</strong><br /><strong>...</strong>
</pre>
<p>As you can see, at each iteration we ask TensorFlow to compute the loss function and a training step, and we always pass the same dictionary containing <kbd>X</kbd> and <kbd>Y</kbd>. At the end of this loop, the loss function is stable and we can check the quality of this logistic regression by plotting the separating hyperplane:</p>
<div class="CDPAlignCenter CDPAlign"><img height="500" width="509" class="image-border" src="assets/102620a4-514e-4175-8140-5779926f5fbf.png" /></div>
<p class="CDPAlignLeft CDPAlign">The result is approximately equivalent to the one obtained with the&#160;scikit-learn implementation. If we want to know the values of both coefficients (weights) and intercept (bias), we can ask TensorFlow to retrieve them by calling the method <kbd>eval()</kbd> on each variable:</p>
<pre class="CDPAlignLeft CDPAlign">
<strong>&gt;&gt;&gt; Wc, Wb = W.eval(), bias.eval()</strong><br /><br /><strong>&gt;&gt;&gt; print(Wc)</strong><br /><strong>[[-1.16501403]<br /> [ 3.10014033]]</strong><br /><br /><strong>&gt;&gt;&gt; print(Wb)</strong><br /><strong>[[-0.12583369]]</strong>
</pre>


            </article>

            
        </section>
    </body>

</html>
