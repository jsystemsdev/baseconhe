<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Managing categorical data</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Managing categorical data</h1>
                </header>
            
            <article>
                
<p>In many classification problems, the target dataset is made up of categorical labels which cannot immediately be processed by any algorithm. An encoding is needed and scikit-learn offers at least two valid options. Let's consider a very small dataset made of 10 categorical samples with two features each:</p>
<pre>
<strong>import numpy as np<br /><br />&gt;&gt;&gt; X = np.random.uniform(0.0, 1.0, size=(10, 2))<br />&gt;&gt;&gt; Y = np.random.choice(('Male','Female'), size=(10))<br />&gt;&gt;&gt; X[0]<br />array([ 0.8236887 ,  0.11975305])<br />&gt;&gt;&gt; Y[0]<br />'Male'</strong>
</pre>
<p>The first option is to use the&#160;<kbd>LabelEncoder</kbd> class, which adopts a dictionary-oriented approach, associating to each category label a progressive integer number, that is an index of an instance array called <kbd>classes_</kbd>:</p>
<pre>
<strong>from sklearn.preprocessing import LabelEncoder<br /><br />&gt;&gt;&gt; le = LabelEncoder()<br />&gt;&gt;&gt; yt = le.fit_transform(Y)<br />&gt;&gt;&gt; print(yt)<br />[0 0 0 1 0 1 1 0 0 1]<br /><br />&gt;&gt;&gt; le.classes_array(['Female', 'Male'], dtype='|S6')</strong>
</pre>
<p>The inverse transformation can be obtained in this simple way:</p>
<pre>
<strong>&gt;&gt;&gt; output = [1, 0, 1, 1, 0, 0]<br />&gt;&gt;&gt; decoded_output = [le.classes_[i] for i in output]<br />['Male', 'Female', 'Male', 'Male', 'Female', 'Female']</strong>
</pre>
<p>This approach is simple and works well in many cases, but it has a drawback: all labels are turned into sequential numbers. A classifier&#160;which works with real values will then consider similar numbers according to their distance, without any concern for semantics. For this reason, it's often preferable to use so-called <strong>one-hot</strong> <strong>encod</strong><strong>ing</strong>, which binarizes the data. For labels, it can be achieved using the <kbd>LabelBinarizer</kbd> class:</p>
<pre>
<strong>from sklearn.preprocessing import LabelBinarizer<br /><br />&gt;&gt;&gt; lb = LabelBinarizer()<br />&gt;&gt;&gt; Yb = lb.fit_transform(Y)<br />array([[1],<br />       [0],<br />       [1],<br />       [1],<br />       [1],<br />       [1],<br />       [0],<br />       [1],<br />       [1],<br />       [1]])<br /><br />&gt;&gt;&gt; lb.inverse_transform(Yb)<br />array(['Male', 'Female', 'Male', 'Male', 'Male', 'Male', 'Female', 'Male',<br />       'Male', 'Male'], dtype='|S6')</strong>
</pre>
<p>In this case, each categorical label is first turned into a positive integer and then transformed into a vector where only one feature is 1 while all the others are 0. It means, for example, that using a softmax distribution with a peak corresponding to the main class can be easily turned into a discrete vector where the only non-null element corresponds to the right class. For example:</p>
<pre>
<strong>import numpy as np<br /><br />&gt;&gt;&gt; Y = lb.fit_transform(Y)<br />array([[0, 1, 0, 0, 0],<br />       [0, 0, 0, 1, 0],<br />       [1, 0, 0, 0, 0]])<br /><br />&gt;&gt;&gt; Yp = model.predict(X[0])<br />array([[0.002, 0.991, 0.001, 0.005, 0.001]])<br /><br />&gt;&gt;&gt; Ypr = np.round(Yp)<br />array([[ 0.,  1.,  0.,  0.,  0.]])<br /><br />&gt;&gt;&gt; lb.inverse_transform(Ypr)<br />array(['Female'], dtype='|S6')</strong>
</pre>
<p>Another approach to categorical features can be adopted when they're structured like a list of dictionaries (not necessarily dense, they can have values only for a few features). For example:</p>
<pre>
<strong>data = [<br />   { 'feature_1': 10.0, 'feature_2': 15.0 },<br />   { 'feature_1': -5.0, 'feature_3': 22.0 },<br />   { 'feature_3': -2.0, 'feature_4': 10.0 }<br />]</strong>
</pre>
<p>In this case, scikit-learn offers the classes <kbd>DictVectorizer</kbd> and <kbd>FeatureHasher</kbd>; they both produce sparse matrices of real numbers that can be fed into any machine learning model. The latter has a limited memory consumption and adopts <strong>MurmurHash 3</strong> (read&#160;<a href="https://en.wikipedia.org/wiki/MurmurHash">https://en.wikipedia.org/wiki/MurmurHash</a>, for further information). The code for these two methods is shown as follows:</p>
<pre>
<strong>from sklearn.feature_extraction import DictVectorizer, FeatureHasher<br /><br />&gt;&gt;&gt; dv = DictVectorizer()<br />&gt;&gt;&gt; Y_dict = dv.fit_transform(data)<br /><br />&gt;&gt;&gt; Y_dict.todense()<br />matrix([[ 10.,  15.,   0.,   0.],<br />        [ -5.,   0.,  22.,   0.],<br />        [  0.,   0.,  -2.,  10.]])<br /><br />&gt;&gt;&gt; dv.vocabulary_<br />{'feature_1': 0, 'feature_2': 1, 'feature_3': 2, 'feature_4': 3}<br /><br />&gt;&gt;&gt; fh = FeatureHasher()<br />&gt;&gt;&gt; Y_hashed = fh.fit_transform(data)<br /><br />&gt;&gt;&gt; Y_hashed.todense()<br />matrix([[ 0.,  0.,  0., ...,  0.,  0.,  0.],<br />        [ 0.,  0.,  0., ...,  0.,  0.,  0.],<br />        [ 0.,  0.,  0., ...,  0.,  0.,  0.]])</strong>
</pre>
<p>In both cases, I suggest you read the original scikit-learn documentation to know all possible options and parameters.&#160;</p>
<p>When working with categorical features (normally converted into positive integers through <kbd>LabelEncoder</kbd>), it's also possible to filter the dataset in order to apply one-hot encoding using the&#160;<kbd>OneHotEncoder</kbd> class. In the following example, the first feature is a binary index which indicates <kbd>'Male'</kbd> or <kbd>'Female'</kbd>:</p>
<pre>
<strong>from sklearn.preprocessing import OneHotEncoder<br /><br />&gt;&gt;&gt; data = [<br />   [0, 10],<br />   [1, 11],<br />   [1, 8],<br />   [0, 12],<br />   [0, 15]<br />]<br /><br />&gt;&gt;&gt; oh = OneHotEncoder(categorical_features=[0])<br />&gt;&gt;&gt; Y_oh = oh.fit_transform(data1)<br /><br />&gt;&gt;&gt; Y_oh.todense()<br />matrix([[  1.,   0.,  10.],<br />        [  0.,   1.,  11.],<br />        [  0.,   1.,   8.],<br />        [  1.,   0.,  12.],<br />        [  1.,   0.,  15.]])</strong>
</pre>
<p>Considering that these approaches increase the number of values (also exponentially with binary versions), all the classes adopt sparse matrices based on SciPy implementation. See&#160;<a href="https://docs.scipy.org/doc/scipy-0.18.1/reference/sparse.html">https://docs.scipy.org/doc/scipy-0.18.1/reference/sparse.html</a>&#160; for further information.</p>


            </article>

            
        </section>
    </body>

</html>
