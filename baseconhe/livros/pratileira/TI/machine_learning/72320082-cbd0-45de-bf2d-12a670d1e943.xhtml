<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Content-based systems</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Content-based systems</h1>
                </header>
            
            <article>
                
<p>This is probably the simplest method and it's based only on the products, modeled as feature vectors:</p>
<div class="CDPAlignCenter CDPAlign"><img height="36" width="229" src="assets/9da31dbd-aeb2-4f7d-a522-c1526ca32439.png" /></div>
<p class="CDPAlignLeft CDPAlign">Just like the users, the features can also be categorical (indeed, for products it's easier), for example, the genre of a book or a movie, and they can be used together with numerical values (like price, length, number of positive reviews, and so on) after encoding them.</p>
<p class="CDPAlignLeft CDPAlign">Then a clustering strategy is adopted, even if the most used is <strong>k-nearest neighbors</strong>&#160;as it allows controlling the size of each neighborhood to determine, given a sample product, the quality and the number of suggestions.</p>
<p class="CDPAlignLeft CDPAlign">Using scikit-learn, first of all we create a dummy product dataset:</p>
<pre class="CDPAlignLeft CDPAlign">
<strong>&gt;&gt;&gt; nb_items = 1000</strong><br /><strong>&gt;&gt;&gt; items = np.zeros(shape=(nb_items, 4))</strong><br /><br /><strong>&gt;&gt;&gt; for i in range(nb_items):</strong><br /><strong>&gt;&gt;&gt;    items[i, 0] = np.random.randint(0, 100)</strong><br /><strong>&gt;&gt;&gt;    items[i, 1] = np.random.randint(0, 100)</strong><br /><strong>&gt;&gt;&gt;    items[i, 2] = np.random.randint(0, 100)</strong><br /><strong>&gt;&gt;&gt;    items[i, 3] = np.random.randint(0, 100)</strong>
</pre>
<p>In this case, we have 1000 samples with four integer features bounded between 0 and 100. Then we proceed, as in the previous example, towards clustering them:</p>
<pre>
<strong>&gt;&gt;&gt; nn = NearestNeighbors(n_neighbors=10, radius=5.0)</strong><br /><strong>&gt;&gt;&gt; nn.fit(items)</strong>
</pre>
<p>At this point, it's possible to query our model with the method&#160;<kbd>radius_neighbors()</kbd>,<strong>&#160;</strong>which allows us to restrict our research only to a limited subset. The default radius (set through the parameter <kbd>radius</kbd>) is 5.0, but we can change it dynamically:</p>
<pre>
<strong>&gt;&gt;&gt; test_product = np.array([15, 60, 28, 73])</strong><br /><strong>&gt;&gt;&gt; d, suggestions = nn.radius_neighbors(test_product.reshape(1, -1), radius=20)</strong><br /><br /><strong>&gt;&gt;&gt; print(suggestions)</strong><br /><strong>[array([657, 784, 839, 342, 446, 196], dtype=int64)]</strong><br /><br /><strong>&gt;&gt;&gt; d, suggestions = nn.radius_neighbors(test_product.reshape(1, -1), radius=30)</strong><br /><br /><strong>&gt;&gt;&gt; print(suggestions)</strong><br /><strong>[ array([844, 340, 657, 943, 461, 799, 715, 863, 979, 784, 54, 148, 806,</strong><br /><strong> 465, 585, 710, 839, 695, 342, 881, 864, 446, 196, 73, 663, 580, 216], dtype=int64)]</strong>
</pre>
<p>Of course, when trying these examples, the number of suggestions can be different, as we are using random datasets, so I suggest trying different values for the radius (in particular when using different metrics).</p>
<p>When clustering with <strong>k-nearest neighbors</strong>, it's important to consider the metric adopted for determining the distance between the samples. The default for scikit-learn is the&#160;Minkowski distance, which is a generalization of Euclidean and Manhattan distance, and is defined as:</p>
<div class="CDPAlignCenter CDPAlign"><img height="73" width="217" src="assets/4269263c-f483-451e-a33b-031cca2ef5c1.png" /></div>
<p class="CDPAlignLeft CDPAlign">The parameter <em>p</em> controls the type of distance and the default value is 2, so that the resulting metric&#160;is a classical Euclidean distance. Other distances are offered by SciPy (in the package&#160;<span><kbd>scipy.spatial.distance</kbd>) and include, for example, the <strong>Hamming</strong> and <strong>Jaccard</strong> distances. The former is defined as the disagree proportion between two vectors (if they are binary this is the normalized number of different bits). For example:</span></p>
<pre class="CDPAlignLeft CDPAlign">
<strong>from scipy.spatial.distance import hamming</strong><br /><br /><strong>&gt;&gt;&gt; a = np.array([0, 1, 0, 0, 1, 0, 1, 1, 0, 0])</strong><br /><strong>&gt;&gt;&gt; b = np.array([1, 1, 0, 0, 0, 1, 1, 1, 1, 0])</strong><br /><strong>&gt;&gt;&gt; d = hamming(a, b)</strong><br /><br /><strong>&gt;&gt;&gt; print(d)</strong><br /><strong>0.40000000000000002</strong>
</pre>
<p>It means there's a disagree&#160;proportion of 40 percent, or, considering that both vectors are binary, there 4 different bits (out of 10). This measure can be useful when it's necessary to emphasize the presence/absence of a particular feature.</p>
<p>The Jaccard distance is defined as:</p>
<div class="CDPAlignCenter CDPAlign"><img height="56" width="256" src="assets/5d73b254-d088-4f1e-a365-4c0de95dfb8b.png" /></div>
<p class="CDPAlignLeft CDPAlign">It's particularly useful to measure the dissimilarity between two different sets (<em>A</em> and <em>B</em>) of items. If our feature vectors are binary, it's immediate to apply this distance using Boolean logic. Using the previous test values, we get:</p>
<pre class="CDPAlignLeft CDPAlign">
<strong>from scipy.spatial.distance import jaccard</strong><br /><br /><strong>&gt;&gt;&gt; d = jaccard(a, b)</strong><br /><strong>&gt;&gt;&gt; print(d)</strong><br /><strong>0.5714285714285714</strong>
</pre>
<p>This measure is bounded between 0 (equal vectors) and 1 (total dissimilarity).</p>
<p>As for the Hamming distance, it can be very useful when it's necessary to compare items where their representation is made up of binary states (like present/absent, yes/no, and so forth). If you want to adopt a different metric for <strong>k-nearest neighbors</strong>, it's possible to specify it directly using the <kbd>metric</kbd> parameter:</p>
<pre>
<strong>&gt;&gt;&gt; nn = NearestNeighbors(n_neighbors=10, radius=5.0, metric='hamming')</strong><br /><strong>&gt;&gt;&gt; nn.fit(items)<br /><br /></strong><strong>&gt;&gt;&gt; nn = NearestNeighbors(n_neighbors=10, radius=5.0, metric='jaccard')</strong><br /><strong>&gt;&gt;&gt; nn.fit(items)</strong>
</pre>


            </article>

            
        </section>
    </body>

</html>
