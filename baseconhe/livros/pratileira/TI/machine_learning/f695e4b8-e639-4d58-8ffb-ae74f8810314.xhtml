<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Finding the optimal hyperparameters through grid search</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Finding the optimal hyperparameters through grid search</h1>
                </header>
            
            <article>
                
<p>Finding the best hyperparameters (called this because they influence the parameters learned during the training phase) is not always easy and there are seldom good methods to start from. The personal experience (a fundamental element) must be aided by an efficient tool such as&#160;<kbd>GridSearchCV</kbd>, which automates the training process of different models and provides the user with optimal values using cross-validation.</p>
<p>As an example, we show how to use it to find the best penalty and strength factors for a linear regression with the Iris toy dataset:</p>
<pre>
<strong>import multiprocessing</strong><br /><br /><strong>from sklearn.datasets import load_iris</strong><br /><strong>from sklearn.model_selection import GridSearchCV</strong><br /><br /><strong>&gt;&gt;&gt; iris = load_iris()</strong><br /><br /><strong>&gt;&gt;&gt; param_grid = [</strong><br /><strong>   { </strong><br /><strong>      'penalty': [ 'l1', 'l2' ],</strong><br /><strong>      'C': [ 0.5, 1.0, 1.5, 1.8, 2.0, 2.5]</strong><br /><strong>   }</strong><br /><strong>]</strong><br /><br /><strong>&gt;&gt;&gt; gs = GridSearchCV(estimator=LogisticRegression(), param_grid=param_grid,</strong><br /><strong>   scoring='accuracy', cv=10, n_jobs=multiprocessing.cpu_count())</strong><br /><br /><strong>&gt;&gt;&gt; gs.fit(iris.data, iris.target)</strong><br /><strong>GridSearchCV(cv=10, error_score='raise',</strong><br /><strong>       estimator=LogisticRegression(C=1.0, class_weight=None, dual=False, fit_intercept=True,</strong><br /><strong>          intercept_scaling=1, max_iter=100, multi_class='ovr', n_jobs=1,</strong><br /><strong>          penalty='l2', random_state=None, solver='liblinear', tol=0.0001,</strong><br /><strong>          verbose=0, warm_start=False),</strong><br /><strong>       fit_params={}, iid=True, n_jobs=8,</strong><br /><strong>       param_grid=[{'penalty': ['l1', 'l2'], 'C': [0.1, 0.2, 0.4, 0.5, 1.0, 1.5, 1.8, 2.0, 2.5]}],</strong><br /><strong>       pre_dispatch='2*n_jobs', refit=True, return_train_score=True,</strong><br /><strong>       scoring='accuracy', verbose=0)</strong><br /><br /><strong>&gt;&gt;&gt; gs.best_estimator_</strong><br /><strong>LogisticRegression(C=1.5, class_weight=None, dual=False, fit_intercept=True,</strong><br /><strong>          intercept_scaling=1, max_iter=100, multi_class='ovr', n_jobs=1,</strong><br /><strong>          penalty='l1', random_state=None, solver='liblinear', tol=0.0001,</strong><br /><strong>          verbose=0, warm_start=False)</strong><br /><br /><strong>&gt;&gt;&gt; cross_val_score(gs.best_estimator_, iris.data, iris.target, scoring='accuracy', cv=10).mean()</strong><br /><strong>0.96666666666666679</strong>
</pre>
<p>It's possible to insert into the <kbd>param</kbd> dictionary any parameter supported by the model with a list of values. <kbd>GridSearchCV</kbd> will process in parallel and return the best estimator (through the instance variable <kbd>best_estimator_</kbd>, which is an instance of the same classifier specified through the parameter <kbd>estimator</kbd>).</p>
<div class="packt_tip"><span>When working with parallel algorithms, scikit-learn provides the <kbd>n_jobs</kbd> parameter, which allows us to specify how many threads must be used. Setting</span> <kbd>n_jobs=multiprocessing.cpu_count()</kbd> is useful to exploit<span>&#160;all CPU cores available on the current machine.</span></div>
<p>In the next example, we're going to find the best parameters of an&#160;<kbd>SGDClassifier</kbd> trained with perceptron loss. The dataset is plotted in the following figure:</p>
<div class="CDPAlignCenter CDPAlign"><img class="image-border" src="assets/48ef29c1-5eed-4a62-989b-2e9d94bd7fa7.png" /></div>
<pre>
<strong>import multiprocessing</strong><br /><br /><strong>from sklearn.model_selection import GridSearchCV</strong><br /><br /><strong>&gt;&gt;&gt; param_grid = [</strong><br /><strong>   { </strong><br /><strong>       'penalty': [ 'l1', 'l2', 'elasticnet' ],</strong><br /><strong>       'alpha': [ 1e-5, 1e-4, 5e-4, 1e-3, 2.3e-3, 5e-3, 1e-2],</strong><br /><strong>       'l1_ratio': [0.01, 0.05, 0.1, 0.15, 0.25, 0.35, 0.5, 0.75, 0.8]</strong><br /><strong>   }</strong><br /><strong>]</strong><br /><br /><strong>&gt;&gt;&gt; sgd = SGDClassifier(loss='perceptron', learning_rate='optimal')</strong><br /><strong>&gt;&gt;&gt; gs = GridSearchCV(estimator=sgd, param_grid=param_grid, scoring='accuracy', cv=10, n_jobs=multiprocessing.cpu_count())</strong><br /><br /><strong>&gt;&gt;&gt; gs.fit(X, Y)</strong><br /><strong>GridSearchCV(cv=10, error_score='raise',</strong><br /><strong>       estimator=SGDClassifier(alpha=0.0001, average=False, class_weight=None, epsilon=0.1,</strong><br /><strong>       eta0=0.0, fit_intercept=True, l1_ratio=0.15,</strong><br /><strong>       learning_rate='optimal', loss='perceptron', n_iter=5, n_jobs=1,</strong><br /><strong>       penalty='l2', power_t=0.5, random_state=None, shuffle=True,</strong><br /><strong>       verbose=0, warm_start=False),</strong><br /><strong>       fit_params={}, iid=True, n_jobs=8,</strong><br /><strong>       param_grid=[{'penalty': ['l1', 'l2', 'elasticnet'], 'alpha': [1e-05, 0.0001, 0.0005, 0.001, 0.0023, 0.005, 0.01], 'l1_ratio': [0.01, 0.05, 0.1, 0.15, 0.25, 0.35, 0.5, 0.75, 0.8]}],</strong><br /><strong>       pre_dispatch='2*n_jobs', refit=True, return_train_score=True,</strong><br /><strong>       scoring='accuracy', verbose=0)</strong><br /><br /><strong>&gt;&gt;&gt; gs.best_score_</strong><br /><strong>0.89400000000000002</strong><br /><br /><strong>&gt;&gt;&gt; gs.best_estimator_</strong><br /><strong>SGDClassifier(alpha=0.001, average=False, class_weight=None, epsilon=0.1,</strong><br /><strong>       eta0=0.0, fit_intercept=True, l1_ratio=0.1, learning_rate='optimal',</strong><br /><strong>       loss='perceptron', n_iter=5, n_jobs=1, penalty='elasticnet',</strong><br /><strong>       power_t=0.5, random_state=None, shuffle=True, verbose=0,</strong><br /><strong>       warm_start=False)</strong>
</pre>


            </article>

            
        </section>
    </body>

</html>
