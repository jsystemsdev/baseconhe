<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>A quick glimpse inside Keras</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>A quick glimpse inside Keras</h1>
                </header>
            
            <article>
                
<p>Keras (<a href="https://keras.io">https://keras.io</a>) is a high-level deep learning framework that works seamlessly with low-level backends like TensorFlow, Theano or CNTK. In Keras a model is like a sequence of layers where each output is fed into the following computational block until the final layer is reached. The generic structure of a model is:</p>
<pre>
<strong>from keras.models import Sequential</strong><br /><br /><strong>&gt;&gt;&gt; model = Sequential()</strong><br /><br /><strong>&gt;&gt;&gt; model.add(...)</strong><br /><strong>&gt;&gt;&gt; model.add(...)</strong><br /><strong>...</strong><br /><strong>&gt;&gt;&gt; model.add(...)</strong>
</pre>
<p>The class <kbd>Sequential</kbd> defines a generic empty model, that already implements all the methods needed to <kbd>add</kbd> layers, <kbd>compile</kbd> the model according to the underlying framework, to&#160;<kbd>fit</kbd> and <kbd>evaluate</kbd> the model and to <kbd>predict</kbd> the output given an input. All the most common layers are already implemented, including:</p>
<ul>
<li>Dense, Dropout and Flattening layers</li>
<li>Convolutional (1D, 2D and 3D) layers</li>
<li>Pooling layers</li>
<li>Zero padding layers</li>
<li>RNN layers</li>
</ul>
<p>A model can be compiled using several loss functions (like MSE or cross-entropy) and all the most diffused Stochastic Gradient Descent optimization algorithms (like RMSProp or Adam). For further details about the mathematical foundation of these methods, please refer to&#160;Goodfellow I., Bengio Y., Courville A., <em>Deep Learning</em>, MIT Press. As it's impossible to discuss all important elements in such a short space, I prefer to create a complete example of image classification based on a convolutional network. The dataset we're going to use is the CIFAR-10 (<a href="https://www.cs.toronto.edu/~kriz/cifar.html">https://www.cs.toronto.edu/~kriz/cifar.html</a>) which is made up of 60000 small RGB images (32 x 32) belonging to 10 different categories (airplane, automobile, bird, cat, deer, dog, frog, horse, ship, truck). In the following figure, a subset of images is shown:</p>
<div class="CDPAlignCenter CDPAlign"><img height="534" width="544" class="image-border" src="assets/0e3e99bb-8140-40a3-afd5-d31c425c89fe.png" /></div>
<p>Since the last release, Keras allows us to download this dataset using a built-in function; therefore, no further actions are required to use it.</p>
<p>The first step is loading the dataset and splitting it into training and test subsets:</p>
<pre>
<strong>from keras.datasets import cifar10</strong><br /><br /><strong>&gt;&gt;&gt; (X_train, Y_train), (X_test, Y_test) = cifar10.load_data()</strong>
</pre>
<p>The training dataset contains 50000 images, while the test set 10000. Now it's possible to build the model. We want to use a few convolutional layers to capture the specific elements of each category. As explained in the previous section, these particular layers can learn to identify specific geometric properties and generalize in an excellent way. In our small architecture, we start with a (5 x 5) filter size to capture all the low-level features (like the orientation) and proceed by increasing the number of filters and reducing their size. In this way, the high-level features (like the shape of a wheel or the relative position of eyes, nose, and mouth) can also be captured. &#160;&#160;</p>
<pre class="mce-root">
<strong>from keras.models import Sequential<br />from keras.layers.convolutional import Conv2D, ZeroPadding2D<br />from keras.layers.pooling import MaxPooling2D<br /><br />&gt;&gt;&gt; model = Sequential()</strong><br /><strong> </strong><br /><strong>&gt;&gt;&gt; model.add(Conv2D(32, kernel_size=(5, 5), activation='relu', input_shape=(32 ,32, 3)))</strong><br /><strong>&gt;&gt;&gt; model.add(MaxPooling2D(pool_size=(2, 2)))</strong><br /><br /><strong>&gt;&gt;&gt; model.add(Conv2D(64, kernel_size=(4, 4), activation='relu'))</strong><br /><strong>&gt;&gt;&gt; model.add(ZeroPadding2D((1, 1)))</strong><br /><strong> </strong><br /><strong>&gt;&gt;&gt; model.add(Conv2D(128, kernel_size=(3, 3), activation='relu'))</strong><br /><strong>&gt;&gt;&gt; model.add(MaxPooling2D(pool_size=(2, 2)))</strong><br /><strong>&gt;&gt;&gt; model.add(ZeroPadding2D((1, 1)))</strong>
</pre>
<p>The first instruction creates a new empty model. At this point, we can all the layers we want to include in the computational graph. The most common parameters of a convolutional layer are:</p>
<ul>
<li><strong>The number of filters</strong></li>
<li><strong>Kernel size</strong> (as tuple)</li>
<li><strong>Strides</strong> (the default value is [1, 1]). This parameter specifies how many pixels the sliding window must consider when shifting on the image. [1, 1] means that no pixels are discarded. [2, 2] means that every horizontal and vertical shift will have a width of 2 pixels and so forth.</li>
<li><strong>Activation</strong> (the default value is None, meaning that the identity function will be used)</li>
<li><strong>Input shape</strong> (only for the first layer is this parameter mandatory)</li>
</ul>
<p>Our first layer has 32 (5 x 5) filters with a <strong>ReLU</strong> (<strong>Rectified Linear Unit</strong>) activation. This function is defined as:</p>
<div class="CDPAlignCenter CDPAlign"><img height="35" width="170" src="assets/47e36c16-5771-4fca-b08f-30dac5d7ab7e.png" /></div>
<p>The second layer reduces the dimensionality with a max pooling considering (2 x 2) blocks. Then we apply another convolution with 64 (4 x 4) filters followed by a zero padding (1 pixel at the top, bottom, left and right side of the input) and finally, we have the third convolutional layer with 128 (3 x 3) filters followed by a max pooling and a zero padding.</p>
<p>At this point, we need to flatten the output of the last layer, so to work like in a MLP:</p>
<pre>
<strong>from keras.layers.core import Dense, Dropout, Flatten<br /><br />&gt;&gt;&gt; model.add(Dropout(0.2))<br />&gt;&gt;&gt; model.add(Flatten())<br />&gt;&gt;&gt; model.add(Dense(128, activation='relu'))<br />&gt;&gt;&gt; model.add(Dropout(0.2))<br />&gt;&gt;&gt; model.add(Dense(10, activation='softmax'))<br /></strong>
</pre>
<p>A dropout (with a probability of 0.2) is applied to the output of the last zero-padding layer; then this multidimensional value is flattened and transformed in a vector. This value is fed into a fully-connected layer with 128 neurons and ReLU activation. Another dropout is applied to the output (to prevent the overfitting) and, finally, this vector is fed into another fully connected layer with 10 neurons with a <em>softmax</em> activation:</p>
<div class="CDPAlignCenter CDPAlign"><img height="46" width="151" src="assets/9d09d699-e462-4244-b2e3-e6d24ca1c858.png" /></div>
<p>In this way, the output of the model represents a discrete probability distribution (each value is the probability of the corresponding class).</p>
<p>The last step before training the model is compiling it:</p>
<pre>
<strong>&gt;&gt;&gt; model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])</strong>
</pre>
<p>Keras will transform the high-level description into low-level operations (like the ones we have discussed in the previous section) with a categorical cross-entropy loss function (see the example of TensorFlow logistic regression) and the Adam optimizer. Moreover, it will apply an accuracy metric to&#160;<span>dynamically</span><span>&#160;</span><span>evaluate the performance.</span></p>
<p>At this point, the model can be trained. We need only two preliminary operations:</p>
<ul>
<li>Normalizing the images so they have values between 0 and 1</li>
<li>Applying the one-hot encoding to the integer label</li>
</ul>
<p>The first operation can be simply performed by dividing the dataset by 255, while the second can be easily carried out using the built-in function <kbd>to_categorical()</kbd>:</p>
<pre>
<strong>from keras.utils import to_categorical<br /><br />&gt;&gt;&gt; model.fit(X_train / 255.0, to_categorical(Y_train), batch_size=32, epochs=15)</strong>
</pre>
<p>We want to train with batches made up of 32 images and for a period of 15 epochs. The reader is free to change all these values to compare the results. The output provided by Keras shows the progress in the learning phase:</p>
<pre>
<strong>Epoch 1/15</strong><br /><strong>50000/50000 [==============================] - 25s - loss: 1.5845 - acc: 0.4199 </strong><br /><strong>Epoch 2/15</strong><br /><strong>50000/50000 [==============================] - 24s - loss: 1.2368 - acc: 0.5602 </strong><br /><strong>Epoch 3/15</strong><br /><strong>50000/50000 [==============================] - 26s - loss: 1.0678 - acc: 0.6247 </strong><br /><strong>Epoch 4/15</strong><br /><strong>50000/50000 [==============================] - 25s - loss: 0.9495 - acc: 0.6658 </strong><br /><strong>Epoch 5/15</strong><br /><strong>50000/50000 [==============================] - 26s - loss: 0.8598 - acc: 0.6963 </strong><br /><strong>Epoch 6/15</strong><br /><strong>50000/50000 [==============================] - 26s - loss: 0.7829 - acc: 0.7220 </strong><br /><strong>Epoch 7/15</strong><br /><strong>50000/50000 [==============================] - 26s - loss: 0.7204 - acc: 0.7452 </strong><br /><strong>Epoch 8/15</strong><br /><strong>50000/50000 [==============================] - 26s - loss: 0.6712 - acc: 0.7629 </strong><br /><strong>Epoch 9/15</strong><br /><strong>50000/50000 [==============================] - 27s - loss: 0.6286 - acc: 0.7779 </strong><br /><strong>Epoch 10/15</strong><br /><strong>50000/50000 [==============================] - 27s - loss: 0.5753 - acc: 0.7952 </strong><br /><strong>Epoch 11/15</strong><br /><strong>50000/50000 [==============================] - 27s - loss: 0.5433 - acc: 0.8049 </strong><br /><strong>Epoch 12/15</strong><br /><strong>50000/50000 [==============================] - 27s - loss: 0.5112 - acc: 0.8170 </strong><br /><strong>Epoch 13/15</strong><br /><strong>50000/50000 [==============================] - 27s - loss: 0.4806 - acc: 0.8293 </strong><br /><strong>Epoch 14/15</strong><br /><strong>50000/50000 [==============================] - 28s - loss: 0.4551 - acc: 0.8365 </strong><br /><strong>Epoch 15/15</strong><br /><strong>50000/50000 [==============================] - 28s - loss: 0.4342 - acc: 0.8444</strong>
</pre>
<p>At the end of the 15th epoch, the accuracy on the training set is about 84% (a very good result). The final operation is evaluating the model with the test set:</p>
<pre>
<strong>&gt;&gt;&gt; scores = model.evaluate(X_test / 255.0, to_categorical(Y_test))</strong><br /><strong>&gt;&gt;&gt; print('Loss: %.3f' % scores[0])</strong><br /><strong>&gt;&gt;&gt; print('Accuracy: %.3f' % scores[1])</strong><br /><strong>Loss: 0.972</strong><br /><strong>Accuracy: 0.719</strong>
</pre>
<p>The final validation accuracy is lower (about 72%) than the one achieved during the training phase. This is a normal behavior for deep models, therefore, when optimizing the algorithm, it's always a good practice to use the cross validation or a well-defined test set (with the same distribution of the training set and 25-30% of total samples).</p>
<p>Of course, we have presented a very simple architecture, but the reader can go deeper into these topics and create more complex models (Keras&#160;<span>also</span><span>&#160;</span><span>contains some very famous pre-trained architectures like VGG16/19 and Inception V3 that can&#160;</span><span>also</span><span>&#160;</span><span>be used to perform image</span> classifications <span>with 1000 categories).</span></p>
<div class="packt_infobox">All the information needed to install Keras with different backends, and the official documentation can be found on the website:&#160;<br />
<a href="https://keras.io">https://keras.io</a></div>


            </article>

            
        </section>
    </body>

</html>
