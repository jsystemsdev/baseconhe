<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Classification metrics</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Classification metrics</h1>
                </header>
            
            <article>
                
<p>A classification task can be evaluated in many different ways to achieve specific objectives. Of course, the most important metric is the accuracy, often expressed&#160;as:</p>
<div class="CDPAlignCenter CDPAlign"><img height="42" width="372" src="assets/5ccb6209-aa2a-4d06-954a-32883d2f8a04.png" /></div>
<p class="CDPAlignLeft CDPAlign">In scikit-learn, it can be assessed using the built-in <kbd>accuracy_score()</kbd> function:</p>
<pre>
<strong>from sklearn.metrics import accuracy_score</strong><br /><br /><strong>&gt;&gt;&gt; accuracy_score(Y_test, lr.predict(X_test))</strong><br /><strong>0.94399999999999995</strong>
</pre>
<p class="CDPAlignLeft CDPAlign">Another very common approach is based on zero-one loss function, which we saw in <a href="3a0b1360-dff6-4bbd-bd19-98972ee44a13.xhtml" target="_blank">Chapter 2</a>, <em>Important Elements in Machine Learning</em>, which is defined as the normalized average of <em>L<sub>0/1</sub></em>&#160;(where <em>1</em> is assigned to misclassifications) over all samples. In the following example, we show a normalized score (if it's close to 0, it's better) and then the same unnormalized value (which is the actual number of misclassifications):</p>
<pre>
<strong>from sklearn.metrics import zero_one_loss</strong><br /><br /><strong>&gt;&gt;&gt; zero_one_loss(Y_test, lr.predict(X_test))</strong><br /><strong>0.05600000000000005</strong><br /><br /><strong>&gt;&gt;&gt; zero_one_loss(Y_test, lr.predict(X_test), normalize=False)</strong><br /><strong>7L</strong>
</pre>
<p class="CDPAlignLeft CDPAlign">A similar but opposite metric is the <strong>Jaccard similarity coefficient</strong>, defined as:</p>
<div class="CDPAlignCenter CDPAlign"><img height="134" width="240" src="assets/2db364ee-d139-4308-8008-4f6b5ff6bb5b.png" /></div>
<p class="CDPAlignLeft CDPAlign">This index measures the similarity and is bounded between 0 (worst performances) and 1 (best performances). In the former case, the intersection is null, while in the latter, the intersection and union are equal because there are no misclassifications. In scikit-learn, the implementation is:</p>
<pre>
<strong>from sklearn.metrics import jaccard_similarity_score</strong><br /><br /><strong>&gt;&gt;&gt; jaccard_similarity_score(Y_test, lr.predict(X_test))</strong><br /><strong>0.94399999999999995</strong>
</pre>
<p class="CDPAlignLeft CDPAlign">These measures provide a good insight into our classification algorithms. However, in many cases, it's necessary to be able to differentiate between different kinds of misclassifications (we're considering the binary case with the conventional notation: 0-negative, 1-positive), because the relative weight is quite different. For this reason, we introduce the following definitions:</p>
<ul>
<li><strong>True positive</strong>:&#160;A positive sample correctly classified</li>
<li><strong>False positive</strong>: A negative sample classified as positive</li>
<li><strong>True negative</strong>: A negative sample correctly classified</li>
<li><strong>False negative</strong>: A positive sample classified as negative</li>
</ul>
<p>At a glance, false positive and false negative can be considered as similar errors, but think about a medical prediction: while a false positive can be easily discovered with further tests, a false negative is often neglected, with repercussions following the consequences of this action. For this reason, it's useful to introduce the concept of a confusion matrix:</p>
<div class="CDPAlignCenter CDPAlign"><img height="301" width="304" class="image-border" src="assets/6c254fdf-5bc8-4f97-ad88-015c5c1ed0c5.png" /></div>
<p>In scikit-learn, it's possible to build a confusion matrix using a built-in function. Let's consider a generic logistic regression on a dataset <em>X</em> with labels <em>Y</em>:</p>
<pre>
<strong>&gt;&gt;&gt; X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.25)</strong><br /><strong>&gt;&gt;&gt; lr = LogisticRegression()</strong><br /><strong>&gt;&gt;&gt; lr.fit(X_train, Y_train)</strong><br /><strong>LogisticRegression(C=1.0, class_weight=None, dual=False, fit_intercept=True,</strong><br /><strong>          intercept_scaling=1, max_iter=100, multi_class='ovr', n_jobs=1,</strong><br /><strong>          penalty='l2', random_state=None, solver='liblinear', tol=0.0001,</strong><br /><strong>          verbose=0, warm_start=False)</strong>
</pre>
<p>Now we can compute our confusion matrix and immediately see how the classifier is working:</p>
<pre>
<strong>from sklearn.metrics import confusion_matrix</strong><br /><br /><strong>&gt;&gt;&gt; cm = confusion_matrix(y_true=Y_test, y_pred=lr.predict(X_test))</strong><br /><strong>cm[::-1, ::-1]</strong><br /><strong>[[50  5]</strong><br /><strong> [ 2 68]]</strong>
</pre>
<p>The last operation is needed because scikit-learn adopts an inverse axle. However, in many books, the confusion matrix has true values on the main diagonal, so I preferred to invert the axle.</p>
<div class="packt_infobox">In order to avoid mistakes, I suggest you visit the page at&#160;<a href="http://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html">http://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html,</a> and check for true/false positive/negative position.</div>
<p>So we have five false negatives and two false positives. If needed, a further analysis can allow for the detection of the misclassifications to decide how to treat them (for example, if their variance overcomes a predefined threshold, it's possible to consider them as outliers and remove them).</p>
<p>Another useful direct measure is:</p>
<div class="CDPAlignCenter CDPAlign">&#160;<img height="51" width="287" src="assets/d27856fb-c8aa-4a93-9059-7f390c46c6ec.png" /></div>
<p class="CDPAlignCenter CDPAlign CDPAlignLeft">This is directly connected with the ability to capture the features that determine the positiveness of a sample, to avoid the misclassification as negative. In scikit-learn, the implementation is:</p>
<pre>
<strong>from sklearn.metrics import precision_score</strong><br /><br /><strong>&gt;&gt;&gt; precision_score(Y_test, lr.predict(X_test))</strong><br /><strong>0.96153846153846156</strong>
</pre>
<div class="packt_infobox">If you don't flip the confusion matrix, but want to get the same measures, it's necessary to add the <kbd>pos_label=0</kbd> parameter to all metric score functions.</div>
<p>The ability to detect true positive samples among all the potential positives can be assessed using another measure:</p>
<div class="CDPAlignCenter CDPAlign"><img height="48" width="273" src="assets/d7be7a22-c0e3-4309-8dfe-dec9b7aa9bff.png" /></div>
<p class="CDPAlignLeft CDPAlign">The scikit-learn implementation is:</p>
<pre>
<strong>from sklearn.metrics import recall_score</strong><br /><br /><strong>&gt;&gt;&gt; recall_score(Y_test, lr.predict(X_test))</strong><br /><strong>0.90909090909090906</strong>
</pre>
<p>It's not surprising that we have a 90 percent recall with 96 percent precision, because the number of false negatives (which impact recall) is proportionally higher than the number of false positives (which impact precision). A weighted harmonic mean between precision and recall is provided by:</p>
<div class="CDPAlignCenter CDPAlign"><img height="56" width="269" src="assets/b8e39b7b-284d-4d72-b441-0131c0c7ff87.png" /></div>
<p class="CDPAlignLeft CDPAlign">A beta value equal to 1 determines the so-called <em>F<sub>1</sub></em> score, which is a perfect balance between the two measures. A beta less than 1 gives more importance to <em>precision</em> and a value greater than 1 gives more importance to <em>recall</em>.&#160;The following snippet shows how to implement it with scikit-learn:</p>
<pre>
<strong>from sklearn.metrics import fbeta_score</strong><br /><br /><strong>&gt;&gt;&gt; fbeta_score(Y_test, lr.predict(X_test), beta=1)</strong><br /><strong>0.93457943925233655</strong><br /><br /><strong>&gt;&gt;&gt; fbeta_score(Y_test, lr.predict(X_test), beta=0.75)</strong><br /><strong>0.94197437829691033</strong><br /><br /><strong>&gt;&gt;&gt; fbeta_score(Y_test, lr.predict(X_test), beta=1.25)</strong><br /><strong>0.92886270956048933</strong>
</pre>
<div class="packt_infobox">For <em>F<sub>1</sub></em> score, scikit-learn provides the function <kbd>f1_score()</kbd>, which is equivalent to <kbd>fbeta_score()</kbd> with <kbd>beta=1</kbd>.</div>
<p>The highest score is achieved by giving more importance to precision (which is higher), while the least one corresponds to a&#160;recall predominance. <em>F<sub>Beta</sub></em> is hence useful to have a compact picture of the accuracy as a&#160;trade-off between high precision and a limited number of false negatives.</p>


            </article>

            
        </section>
    </body>

</html>
