<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Artificial neural networks</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Artificial neural networks</h1>
                </header>
            
            <article>
                
<p>An <strong>artificial neural network</strong> (<strong>ANN</strong>) or simply neural network is a directed structure that connects an input layer with an output one. Normally, all operations are differentiable and the overall vectorial function can be easily written as:</p>
<div class="CDPAlignCenter CDPAlign"><img height="32" width="73" src="assets/db159c08-095a-44a4-b87b-f803a6cf2cd0.png" /></div>
<p class="CDPAlignLeft CDPAlign">Here:</p>
<div class="CDPAlignCenter CDPAlign"><img height="35" width="289" src="assets/388ededf-4fd7-42fc-976a-6dffcd3fd086.png" /></div>
<p class="CDPAlignLeft CDPAlign">The adjective "neural" comes from two important elements: the internal structure of a basic computational unit and the interconnections among them. Let's start with the former. In the following figure, there's a schematic representation of an artificial neuron:</p>
<div class="CDPAlignCenter CDPAlign"><img height="356" width="438" class="image-border" src="assets/64dcc6cf-4bea-41ee-a65d-1183a22d8095.png" /></div>
<p class="CDPAlignLeft CDPAlign">A neuron core is connected with <em>n</em> input channels, each of them characterized by a synaptic weight <em>w<sub>i</sub></em>. The input is split into its components and they are multiplied by the corresponding weight and summed. An optional bias can be added to this sum (it works like another weight connected to a unitary input). The resulting sum is filtered by an activation function <em>f<sub>a</sub></em> (for example a sigmoid, if you recall how a logistic regression works) and the output is therefore produced. In&#160;<a href="7d3b20b9-0cc4-482b-a0b1-522361617ecc.xhtml" target="_blank">Chapter 5</a>, <em>Logistic Regression</em>, we also discussed perceptrons (the first artificial neural networks), which correspond exactly to this architecture with a binary-step activation function. On the other hand, even a logistic regression can be represented as a single neuron neural network, where <em>f<sub>a</sub>(x)</em> is a sigmoid. The main problem with this architecture is that it's intrinsically linear because the output is always a function of the dot product between the input vector and the weight one. You already know all the limitations that such a system has; therefore it's necessary to step forward and create the first <strong>Multi-layer Perceptron</strong> (<strong>MLP</strong>). In the following figure, there's a schematic representation of an MLP with an n-dimensional input, <em>p</em> hidden neurons, and a <em>k</em>-dimensional output:</p>
<div class="CDPAlignCenter CDPAlign"><img height="375" width="397" class="image-border" src="assets/f9771892-f0d4-469a-ba96-1203a1438c8c.png" /></div>
<p class="CDPAlignLeft CDPAlign">There are three layers (even though the number can be larger): the input layer, which receives the input vectors; a hidden layer; and the output one, which is responsible for producing the output. As you can see, every neuron is connected to all the neurons belonging the next layer and now we have two weight matrices,&#160;<em>W = (w<sub>ij</sub>)</em><strong>&#160;</strong>and <em>H = (h<sub>jk</sub>)</em>, using the convention that the first index is referred to the previous layer and the second to the following one.</p>
<p class="CDPAlignLeft CDPAlign">Therefore, the net input to each hidden neuron and the corresponding output is:</p>
<div class="CDPAlignCenter CDPAlign"><img height="100" width="387" src="assets/bafd574d-ef42-43e3-ba3a-52a146594c54.png" /></div>
<p class="CDPAlignLeft CDPAlign">In the same way, we can compute the network output:</p>
<div class="CDPAlignCenter CDPAlign"><img height="93" width="494" src="assets/5d6c0cb4-f114-4c5b-b9ea-e3c0250e5434.png" /></div>
<p class="CDPAlignLeft CDPAlign">As you can see, the network has become highly non-linear and this feature allows us to model complex scenarios that were impossible to manage with linear methods. But how can we determine the values for all synaptic weights and biases? The most famous algorithm is called <strong>back-propagation</strong>&#160;and it works in a very simple way (the only important assumption is that both <em>f<sub>a</sub>(x)</em> must be differentiable).</p>
<p class="CDPAlignLeft CDPAlign">First of all, we need to define an error (loss) function; for many classification tasks, it can be the total squared error:</p>
<div class="CDPAlignCenter CDPAlign"><img height="56" width="257" src="assets/c9bed2c5-0094-415b-88d6-b5fcca20d7d2.png" /></div>
<p class="CDPAlignLeft CDPAlign">Here we have assumed to have <em>N</em> input samples. Expanding it, we obtain:</p>
<div class="CDPAlignCenter CDPAlign"><img height="64" width="420" src="assets/7ebd0486-a763-45a6-9d67-50d50b37d614.png" /></div>
<p class="CDPAlignLeft CDPAlign">This function depends on all variables (weights and biases), but we can start from the bottom and consider first only&#160;<em>h</em><sub><em>jk</em>&#160;</sub>(for simplicity I'm not considering the biases as normal weights); therefore we can compute the gradients and update the weights:</p>
<div class="CDPAlignCenter CDPAlign"><strong><sub><img height="54" width="444" src="assets/536347d9-a079-4ce4-a363-5ccd08d05dce.png" />&#160;</sub></strong></div>
<p class="CDPAlignLeft CDPAlign">In the same way, we can derive the gradient with respect to <em>w<sub>ij</sub></em>:</p>
<div class="CDPAlignCenter CDPAlign"><img height="113" width="513" src="assets/454f2afd-312d-4e88-a041-c08bd4a9c15c.png" /></div>
<p class="CDPAlignLeft CDPAlign">As you can see, the term alpha (which is proportional to the error delta) is back-propagated from the output layer to the hidden one. If there are many hidden layers, this procedure should be repeated recursively until the first layer. The algorithm adopts the gradient descent method; therefore it updates the weights iteratively until convergence:</p>
<div class="CDPAlignCenter CDPAlign"><img height="90" width="167" src="assets/b04b5238-f62e-45a3-a5a5-45633e7e6448.png" /></div>
<p class="CDPAlignLeft CDPAlign">Here, the parameter <kbd>eta</kbd>&#160;(Greek letter in the formula) is the learning rate.</p>
<p class="CDPAlignLeft CDPAlign">In many real problems, the stochastic gradient descent method is adopted (read <a href="https://en.wikipedia.org/wiki/Stochastic_gradient_descent">https://en.wikipedia.org/wiki/Stochastic_gradient_descent</a>,<a href="https://en.wikipedia.org/wiki/Stochastic_gradient_descent">﻿</a>&#160;for further information), which works with batches of input samples, instead of considering the entire dataset. Moreover, many optimizations can be employed to speed up the convergence, but they are beyond the scope of this book. In Goodfellow I., Bengio Y., Courville A., <em>Deep Learning</em>, MIT Press<em>,</em> the reader can find all the details about the majority of them.&#160;For our purposes, it's important to know that we can build a complex network and, after defining a global loss function, optimize all the weights with a standard procedure. In the section dedicated to TensorFlow, we're going to show an example of MLP, but we're not implementing the learning algorithm because, luckily, all optimizers have already been built and can be applied to every architecture.</p>


            </article>

            
        </section>
    </body>

</html>
