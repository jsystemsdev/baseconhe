<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Probabilistic latent semantic analysis</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Probabilistic latent semantic analysis</h1>
                </header>
            
            <article>
                
<p>The previous model was based on a deterministic approach, but it's also possible to define a probabilistic model over the space determined by documents and words. In this case, we're not making any assumption about Apriori probabilities (this will be done in the next approach), and we're going to determine the parameters that maximize the log-likelihood of our model. In particular, consider the plate notation (if you want to know more about this technique, read <a href="https://en.wikipedia.org/wiki/Plate_notation">https://en.wikipedia.org/wiki/Plate_notation</a>) shown in the following figure:</p>
<div class="CDPAlignCenter CDPAlign"><img class="image-border" src="assets/afb88718-b28c-4c15-8042-ca70b2f65140.png" /></div>
<p class="CDPAlignLeft CDPAlign">We assume we have a corpus of <em>m</em> documents and each of them is composed of <em>n</em> words (both elements are observed and therefore represented as gray circles); however, we also assume the presence of a limited set of <em>k</em> common latent factors (topics) that link a document with a group of words (as they are not observed, the circle is white). As already written, we cannot observe them directly, but we're allowed to assume their existence.</p>
<p class="CDPAlignLeft CDPAlign">The joint probability to find a document with a particular word is:</p>
<div class="CDPAlignCenter CDPAlign"><img height="27" width="144" src="assets/9deeb670-4fed-497c-8e79-4dd19cf9ebf5.png" />&#160;</div>
<p class="CDPAlignLeft CDPAlign">Therefore, after introducing the latent factors, the conditional probability to find a word in a specific document can be written as:</p>
<div class="CDPAlignCenter CDPAlign"><img height="61" width="203" src="assets/981e8415-a05f-43a1-8757-32ab252654cc.png" />&#160;</div>
<p class="CDPAlignLeft CDPAlign">The initial joint probability <em>P(d, w)</em>&#160;can be also expressed using the latent factors:</p>
<div class="CDPAlignCenter CDPAlign"><img height="53" width="215" src="assets/ffdd9a76-d5f4-4788-8be5-40591bd8b25c.png" /></div>
<p class="CDPAlignLeft CDPAlign">This includes the prior probability <em>P(t)</em>. As we don't want to work with it, it's preferable to use the expression <em>P(w|d)</em>. To determine the two conditional probability distributions, a common approach is the <strong>expectation-maximization</strong> (<strong>EM</strong>) strategy. A full description can be found in Hofmann T., <em>Unsupervised Learning by Probabilistic Latent Semantic Analysis</em>, Machine Learning 42, 177-196, 2001, Kluwer Academic Publishers<em>.&#160;</em>In this context, we show only the final results without any proof.</p>
<p class="CDPAlignLeft CDPAlign">The log-likelihood can be written as:</p>
<div class="CDPAlignCenter CDPAlign"><img height="51" width="217" src="assets/74deb10f-02fb-4b3e-a299-adcd058744b7.png" /></div>
<p class="CDPAlignLeft CDPAlign">Which becomes:</p>
<div class="CDPAlignCenter CDPAlign"><img height="53" width="440" src="assets/8dec7f8c-b79e-4f85-bac7-97e8b6fd7588.png" /></div>
<p class="CDPAlignLeft CDPAlign"><em>M<sub>dw</sub></em> is an occurrence matrix (normally obtained with a count vectorizer) and <em>M<sub>dw</sub>(d, w)</em> is the frequency of the word <em>w</em> in document <em>d</em>. For simplicity, we are going to approximate it by excluding the first term (which doesn't depend on <em>t<sub>k</sub></em>):</p>
<div class="CDPAlignCenter CDPAlign"><img height="50" width="304" src="assets/8269eb46-4b9c-400b-b190-463c5f89da96.png" /></div>
<p class="CDPAlignLeft CDPAlign">Moreover, it's useful to introduce the conditional probability <em>P(t|d,w)</em>, which is the probability of a topic given a document and a word.&#160;<span>The EM algorithm maximizes the expected complete log-likelihood under the posterior&#160;probability&#160;<em>P(t|d,w)</em>:</span></p>
<div class="CDPAlignCenter CDPAlign"><img height="56" width="389" src="assets/1c373bc8-6a7c-4357-96a5-3fb8c7f38fd0.png" /></div>
<p class="CDPAlignLeft CDPAlign">The <strong>E</strong> phase of the algorithm can be expressed as:</p>
<div class="CDPAlignCenter CDPAlign"><img height="51" width="207" src="assets/010ab7b8-572d-4d83-9139-03a02b96241b.png" /></div>
<p class="CDPAlignLeft CDPAlign">It must be extended to all topics, words, and documents and must be normalized with the sum per topic in order to&#160;<span>always</span><span>&#160;</span><span>have consistent probabilities.</span></p>
<p class="CDPAlignLeft CDPAlign">The <strong>M</strong> phase is split into two computations:</p>
<div class="CDPAlignCenter CDPAlign"><img height="91" width="273" src="assets/6942fa1f-3781-4171-bc4e-ab5ba1445098.png" /></div>
<p class="CDPAlignLeft CDPAlign">Also in this case, the calculations must be extended to all topics, words, and documents. But in the first case, we sum by document and normalize by summing by word and document, while in the second, we sum by word and normalize by the length of the document.</p>
<p class="CDPAlignLeft CDPAlign">The algorithm must be iterated until the log-likelihood stops increasing its magnitude. Unfortunately, scikit-learn doesn't provide a PLSA implementation (maybe because the next strategy, LDA, is considered much more powerful and efficient), so we need to write some code from scratch. Let's start by defining a small subset of the Brown corpus, taking 10 sentences from the <kbd>editorial</kbd> category and 10 from the <kbd>fiction</kbd> one:</p>
<pre class="CDPAlignLeft CDPAlign">
<strong>&gt;&gt;&gt; sentences_1 = brown.sents(categories=['editorial'])[0:10]</strong><br /><strong>&gt;&gt;&gt; sentences_2 = brown.sents(categories=['fiction'])[0:10]</strong><br /><strong>&gt;&gt;&gt; corpus = []</strong><br /><br /><strong>&gt;&gt;&gt; for s in sentences_1 + sentences_2:</strong><br /><strong>&gt;&gt;&gt;    corpus.append(' '.join(s))</strong>
</pre>
<p>Now we can vectorize using the <kbd>CountVectorizer</kbd> class:</p>
<pre>
<strong>import numpy as np</strong><br /><br /><strong>from sklearn.feature_extraction.text import CountVectorizer</strong><br /><br /><strong>&gt;&gt;&gt; cv = CountVectorizer(strip_accents='unicode', stop_words='english')</strong><br /><strong>&gt;&gt;&gt; Xc = np.array(cv.fit_transform(corpus).todense())</strong>
</pre>
<p>At this point, we can define the rank (we choose 2 for simplicity), two constants that will be used later, and the matrices to hold the probabilities <em>P(t|d)</em>, <em>P(w|t)</em>, and <em>P(t|d,w)</em>:</p>
<pre>
<strong>&gt;&gt;&gt; rank = 2</strong><br /><strong>&gt;&gt;&gt; alpha_1 = 1000.0</strong><br /><strong>&gt;&gt;&gt; alpha_2 = 10.0</strong><br /><br /><strong>&gt;&gt;&gt; Ptd = np.random.uniform(0.0, 1.0, size=(len(corpus), rank))</strong><br /><strong>&gt;&gt;&gt; Pwt = np.random.uniform(0.0, 1.0, size=(rank, len(cv.vocabulary_)))</strong><br /><strong>&gt;&gt;&gt; Ptdw = np.zeros(shape=(len(cv.vocabulary_), len(corpus), rank))</strong><br /><br /><strong>&gt;&gt;&gt; for d in range(len(corpus)):</strong><br /><strong>&gt;&gt;&gt;    nf = np.sum(Ptd[d, :])</strong><br /><strong>&gt;&gt;&gt;    for t in range(rank):</strong><br /><strong>&gt;&gt;&gt;       Ptd[d, t] /= nf</strong><br /><br /><strong>&gt;&gt;&gt; for t in range(rank):</strong><br /><strong>&gt;&gt;&gt;    nf = np.sum(Pwt[t, :])</strong><br /><strong>&gt;&gt;&gt;    for w in range(len(cv.vocabulary_)):</strong><br /><strong>&gt;&gt;&gt;       Pwt[t, w] /= nf</strong>
</pre>
<p>The two matrices&#160;<em>P(t|d)</em><span>,</span> <em>P(w|t)</em> must be normalized so as to be coherent with the algorithm; the other one is initialized to zero. Now we can define the log-likelihood function:</p>
<pre>
<strong>&gt;&gt;&gt; def log_likelihood():</strong><br /><strong>&gt;&gt;&gt;    value = 0.0</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;    for d in range(len(corpus)):</strong><br /><strong>&gt;&gt;&gt;       for w in range(len(cv.vocabulary_)):</strong><br /><strong>&gt;&gt;&gt;          real_topic_value = 0.0</strong><br /><strong>&gt;&gt;&gt;</strong><br /><strong>&gt;&gt;&gt;          for t in range(rank):</strong><br /><strong>&gt;&gt;&gt;             real_topic_value += Ptd[d, t] * Pwt[t, w]</strong><br /><strong>&gt;&gt;&gt;</strong><br /><strong>&gt;&gt;&gt;          if real_topic_value &gt; 0.0:</strong><br /><strong>&gt;&gt;&gt;             value += Xc[d, w] * np.log(real_topic_value)</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;    return value</strong>
</pre>
<p>And finally the expectation-maximization functions:</p>
<pre>
<strong>&gt;&gt;&gt; def expectation():</strong><br /><strong>&gt;&gt;&gt;    global Ptd, Pwt, Ptdw</strong><br /><strong>&gt;&gt;&gt;</strong><br /><strong>&gt;&gt;&gt;    for d in range(len(corpus)):</strong><br /><strong>&gt;&gt;&gt;       for w in range(len(cv.vocabulary_)):</strong><br /><strong>&gt;&gt;&gt;          nf = 0.0</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;          for t in range(rank):</strong><br /><strong>&gt;&gt;&gt;             Ptdw[w, d, t] = Ptd[d, t] * Pwt[t, w]</strong><br /><strong>&gt;&gt;&gt;             nf += Ptdw[w, d, t]</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;          Ptdw[w, d, :] = (Ptdw[w, d, :] / nf) if nf != 0.0 else 0.0</strong>
</pre>
<p>In the preceding function, when the normalization factor is 0, the probability <em>P(t|w, d)</em> is set to 0.0 for each topic:</p>
<pre>
<strong>&gt;&gt;&gt; def maximization():</strong><br /><strong>&gt;&gt;&gt;    global Ptd, Pwt, Ptdw</strong><br /><strong>&gt;&gt;&gt;</strong><br /><strong>&gt;&gt;&gt;    for t in range(rank):</strong><br /><strong>&gt;&gt;&gt;       nf = 0.0</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;       for d in range(len(corpus)):</strong><br /><strong>&gt;&gt;&gt;          ps = 0.0</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;          for w in range(len(cv.vocabulary_)):</strong><br /><strong>&gt;&gt;&gt;             ps += Xc[d, w] * Ptdw[w, d, t]</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;          Pwt[t, w] = ps</strong><br /><strong>&gt;&gt;&gt;          nf += Pwt[t, w]</strong><br /><strong>&gt;&gt;&gt;</strong><br /><strong>&gt;&gt;&gt;       Pwt[:, w] /= nf if nf != 0.0 else alpha_1</strong><br /><strong>&gt;&gt;&gt;</strong><br /><strong>&gt;&gt;&gt;    for d in range(len(corpus)):</strong><br /><strong>&gt;&gt;&gt;       for t in range(rank):</strong><br /><strong>&gt;&gt;&gt;          ps = 0.0</strong><br /><strong>&gt;&gt;&gt;          nf = 0.0</strong><br /><strong>&gt;&gt;&gt;</strong><br /><strong>&gt;&gt;&gt;          for w in range(len(cv.vocabulary_)):</strong><br /><strong>&gt;&gt;&gt;             ps += Xc[d, w] * Ptdw[w, d, t]</strong><br /><strong>&gt;&gt;&gt;             nf += Xc[d, w]</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;          Ptd[d, t] = ps / (nf if nf != 0.0 else alpha_2)</strong>
</pre>
<p>The constants <kbd>alpha_1</kbd> and <kbd>alpha_2</kbd>&#160;are used when a normalization factor becomes 0. In that case, it can be useful to assign the probability a small value; therefore we divided the numerator for those constants. I suggest trying with different values so as to tune up the algorithm for different tasks.</p>
<p>At this point, we can try our algorithm with a limited number of iterations:</p>
<pre>
<strong>&gt;&gt;&gt; print('Initial Log-Likelihood: %f' % log_likelihood())</strong><br /><br /><strong>&gt;&gt;&gt; for i in range(50):</strong><br /><strong>&gt;&gt;&gt;    expectation()</strong><br /><strong>&gt;&gt;&gt;    maximization()</strong><br /><strong>&gt;&gt;&gt;    print('Step %d - Log-Likelihood: %f' % (i, log_likelihood()))<br /><br />Initial Log-Likelihood: -1242.878549<br />Step 0 - Log-Likelihood: -1240.160748<br />Step 1 - Log-Likelihood: -1237.584194<br />Step 2 - Log-Likelihood: -1236.009227<br />Step 3 - Log-Likelihood: -1234.993974<br />Step 4 - Log-Likelihood: -1234.318545<br />Step 5 - Log-Likelihood: -1233.864516<br />Step 6 - Log-Likelihood: -1233.559474<br />Step 7 - Log-Likelihood: -1233.355097<br />Step 8 - Log-Likelihood: -1233.218306<br />Step 9 - Log-Likelihood: -1233.126583<br />Step 10 - Log-Likelihood: -1233.064804<br />Step 11 - Log-Likelihood: -1233.022915<br />Step 12 - Log-Likelihood: -1232.994274<br />Step 13 - Log-Likelihood: -1232.974501<br />Step 14 - Log-Likelihood: -1232.960704<br />Step 15 - Log-Likelihood: -1232.950965</strong><br /><strong>...</strong>
</pre>
<p>It's possible to verify the convergence after the 30th step. At this point, we can check the top five words per topic considering the <em>P(w|t)</em> conditional distribution sorted in descending mode per topic weight:</p>
<pre>
<strong>&gt;&gt;&gt; Pwts = np.argsort(Pwt, axis=1)[::-1]</strong><br /><br /><strong>&gt;&gt;&gt; for t in range(rank):</strong><br /><strong>&gt;&gt;&gt;    print('\nTopic ' + str(t))</strong><br /><strong>&gt;&gt;&gt;       for i in range(5):</strong><br /><strong>&gt;&gt;&gt;          print(cv.get_feature_names()[Pwts[t, i]])</strong><br /><br /><strong>Topic 0</strong><br /><strong>years</strong><br /><strong>questions</strong><br /><strong>south</strong><br /><strong>reform</strong><br /><strong>social</strong><br /><br /><strong>Topic 1</strong><br /><strong>convened</strong><br /><strong>maintenance</strong><br /><strong>penal</strong><br /><strong>year</strong><br /><strong>legislators</strong>
</pre>


            </article>

            
        </section>
    </body>

</html>
