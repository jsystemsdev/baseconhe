<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Latent Dirichlet Allocation</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Latent Dirichlet Allocation</h1>
                </header>
            
            <article>
                
<p>In the previous method, we didn't make any assumptions about the topic prior to distribution and this can result in a limitation because the algorithm isn't driven by any real-world intuition. LDA, instead, is based on the idea that a topic is characterized by a small ensemble of important words and normally a document doesn't cover many topics. For this reason, the main assumption is that the prior topic distribution is a symmetric&#160;<strong>Dirichlet&#160;</strong>one. The probability density function is defined as:</p>
<div class="CDPAlignCenter CDPAlign"><img height="65" width="328" src="assets/c592d623-ce60-44a0-afcd-a7a4d92b12ed.png" /></div>
<p class="CDPAlignLeft CDPAlign">If the concentration parameter alpha is below 1.0, the distribution will be sparse as desired. This allows us to model topic-document and topic-word distributions, which will always be concentrated on a few values. In this way we can&#160;avoid the following:</p>
<ul>
<li>The topic mixture assigned to a document could becoming flat (many topics with similar weight)</li>
<li>The structure of a topic considering the word ensemble could becoming similar to a background&#160;(in fact, only a limited number of words must be important; otherwise the semantic boundaries fade out).</li>
</ul>
<p>Using the plate notation, we can represent the relationship among documents, topics, and words as shown in the following figure:</p>
<div class="CDPAlignCenter CDPAlign"><img height="269" width="504" class="image-border" src="assets/385fe68d-bcef-4870-bbcb-28acd010ae4d.png" /></div>
<p class="CDPAlignLeft CDPAlign">In the previous figure, alpha is the Dirichlet parameter for the topic-document distribution, while gamma has the same role for the topic-word distribution. Theta, instead, is the topic distribution for a specific document, while beta is the topic distribution for a specific word.</p>
<p class="CDPAlignLeft CDPAlign">If we have a corpus of <em>m</em> documents and a vocabulary of <em>n</em> words (each document has <em>n<sub>i</sub></em> words) and we assume to have <em>k</em> different topics, the generative algorithm can be described with the following steps:</p>
<ul>
<li>For each document, draw a sample (a topic mixture) from the topic-document distribution:</li>
</ul>
<div class="CDPAlignCenter CDPAlign"><img height="31" width="169" src="assets/fc1cbf58-0226-4e73-a63d-e0b758fa6e6e.png" /></div>
<ul>
<li>For each topic, draw a sample from the from the topic-word distribution:</li>
</ul>
<div class="CDPAlignCenter CDPAlign"><img height="28" width="157" src="assets/7bc13948-7f11-46c2-9384-3085c11633a4.png" /></div>
<p class="CDPAlignLeft CDPAlign">Both parameters must be estimated. At this point, considering the occurrence matrix <em>M<sub>dw</sub></em> and the notation <em>z<sub>mn</sub></em> to define the topic assigned to the <em>n</em>-th word in the <em>m</em>-th document, we can iterate over documents (index <em>d</em>) and words (index <em>w</em>):</p>
<ul>
<li>A topic for document <em>d</em> and word <em>w</em> is chosen according to:</li>
</ul>
<div class="CDPAlignCenter CDPAlign"><img height="25" width="172" src="assets/24cee476-bec2-4e5f-9236-2626c745aa44.png" /></div>
<ul>
<li>A word is chosen according to:</li>
</ul>
<div class="CDPAlignCenter CDPAlign"><img height="28" width="175" src="assets/090afa80-73d5-4b4c-9b2c-eca9dc38c1bb.png" /></div>
<p class="CDPAlignLeft CDPAlign">In both cases, a categorical distribution is a one-trial multinomial one. A complete description of how the parameters are estimated is quite complex and it's beyond the scope of this book; however, the main problem is finding the distribution of latent variables:</p>
<div class="CDPAlignCenter CDPAlign"><img height="39" width="211" src="assets/f1321e73-ab6e-4d11-8631-858846f28d2e.png" /></div>
<p class="CDPAlignLeft CDPAlign">The reader can find a lot more information in Blei D., Ng A., Jordan M., <em>Latent Dirichlet Allocation</em>,&#160;Journal of Machine Learning Research, 3, (2003) 993-1022<em>.&#160;</em>However, a very important difference between LDA and PLSA is about the generative ability of LDA, which allows working with unseen documents. In fact, the PLSA training process finds the optimal parameters <em>p(t|d)</em> only for the corpus, while LDA adopts random variables. It's possible to understand this concept by defining the probability of theta (a topic mixture) as joint with a set of topics and a set of words, and conditioned to the model parameters:</p>
<div class="CDPAlignCenter CDPAlign">&#160;<img height="47" width="297" src="assets/d053b6e5-891d-414b-a788-0b590aaa968b.png" /></div>
<p class="CDPAlignLeft CDPAlign">As shown in the previously mentioned paper, the probability of a document (a set of words) conditioned to the model parameters, can be obtained by integration:</p>
<div class="CDPAlignCenter CDPAlign"><img height="67" width="356" src="assets/8899c8d9-32d6-4ca1-84a4-d83e28441b52.png" /></div>
<p class="CDPAlignLeft CDPAlign">This expression shows the difference between PLSA and LDA. Once learned <em>p(t|d)</em>, PLSA cannot generalize, while LDA, sampling from the random variables, can always find a suitable topic mixture for an unseen document.</p>
<p class="CDPAlignLeft CDPAlign">scikit-learn provides a full LDA implementation through the class <kbd>LatentDirichletAllocation</kbd>. We're going to use it with a bigger dataset (4,000 documents) built from a subset of the Brown corpus:</p>
<pre class="CDPAlignLeft CDPAlign">
<strong>&gt;&gt;&gt; sentences_1 = brown.sents(categories=['reviews'])[0:1000]</strong><br /><strong>&gt;&gt;&gt; sentences_2 = brown.sents(categories=['government'])[0:1000]</strong><br /><strong>&gt;&gt;&gt; sentences_3 = brown.sents(categories=['fiction'])[0:1000]</strong><br /><strong>&gt;&gt;&gt; sentences_4 = brown.sents(categories=['news'])[0:1000]</strong><br /><strong>&gt;&gt;&gt; corpus = []</strong><br /><br /><strong>&gt;&gt;&gt; for s in sentences_1 + sentences_2 + sentences_3 + sentences_4:</strong><br /><strong>&gt;&gt;&gt;    corpus.append(' '.join(s))</strong>
</pre>
<p>Now we can vectorize, define, and train our LDA model by assuming that we have eight main topics:</p>
<pre>
<strong>from sklearn.decomposition import LatentDirichletAllocation</strong><br /><br /><strong>&gt;&gt;&gt; cv = CountVectorizer(strip_accents='unicode', stop_words='english', analyzer='word', token_pattern='[a-z]+')</strong><br /><strong>&gt;&gt;&gt; Xc = cv.fit_transform(corpus)</strong><br /><br /><strong>&gt;&gt;&gt; lda = LatentDirichletAllocation(n_topics=8, learning_method='online', max_iter=25)</strong><br /><strong>&gt;&gt;&gt; Xl = lda.fit_transform(Xc)</strong>
</pre>
<p>In <kbd>CountVectorizer</kbd>, we added a regular expression to filter the tokens through the parameter <kbd>token_pattern</kbd>. This is useful as we are not using a full tokenizer and, in the corpus, there are also many numbers that we want to filter out. The class <kbd>LatentDirichletAllocation</kbd> allows us to specify the learning method (through <kbd>learning_method</kbd>), which can be either batch or online. We have chosen online because it's faster; however, both methods adopt variational Bayes to learn the parameters. The former adopts the whole dataset, while the latter works with mini-batches. The online option will be removed in the 0.20 release; therefore, you can see a deprecation warning when using it now. Both theta and beta Dirichlet parameters can be specified through <kbd>doc_topic_prior</kbd>&#160;(theta) and <kbd>topic_word_prior</kbd>&#160;(beta). The default value (adopted by us too) is 1.0 / <kbd>n_topics</kbd>&#160;. It's important to keep both values small and, in particular, less than 1.0 in order to encourage sparseness. The maximum number of iterations (<kbd>max_iter</kbd>) and other learning-related parameters can be applied by reading the built-in documentation or visiting <a href="http://scikit-learn.org/stable/modules/generated/sklearn.decomposition.LatentDirichletAllocation.html">http://scikit-learn.org/stable/modules/generated/sklearn.decomposition.LatentDirichletAllocation.html</a>.</p>
<p>Now we can test our model by extracting the top five keywords per topic. Just like <kbd>TruncatedSVD</kbd>, the topic-word distribution results are stored in the instance variable <kbd>components_</kbd>:</p>
<pre>
<strong>&gt;&gt;&gt; Mwts_lda = np.argsort(lda.components_, axis=1)[::-1]</strong><br /><br /><strong>&gt;&gt;&gt; for t in range(8):</strong><br /><strong>&gt;&gt;&gt;    print('\nTopic ' + str(t))</strong><br /><strong>&gt;&gt;&gt;       for i in range(5):</strong><br /><strong>&gt;&gt;&gt;          print(cv.get_feature_names()[Mwts_lda[t, i]])</strong><br /><br /><strong>Topic 0</strong><br /><strong>code</strong><br /><strong>cadenza</strong><br /><strong>unlocks</strong><br /><strong>ophthalmic</strong><br /><strong>quo</strong><br /><br /><strong>Topic 1</strong><br /><strong>countless</strong><br /><strong>harnick</strong><br /><strong>leni</strong><br /><strong>addle</strong><br /><strong>chivalry</strong><br /><br /><strong>Topic 2</strong><br /><strong>evasive</strong><br /><strong>errant</strong><br /><strong>tum</strong><br /><strong>rum</strong><br /><strong>orations</strong><br /><br /><strong>Topic 3</strong><br /><strong>grigory</strong><br /><strong>tum</strong><br /><strong>absurdity</strong><br /><strong>tarantara</strong><br /><strong>suitably</strong><br /><br /><strong>Topic 4</strong><br /><strong>seventeenth</strong><br /><strong>conant</strong><br /><strong>chivalrous</strong><br /><strong>janitsch</strong><br /><strong>knight</strong><br /><br /><strong>Topic 5</strong><br /><strong>hypocrites</strong><br /><strong>errantry</strong><br /><strong>adventures</strong><br /><strong>knight</strong><br /><strong>errant</strong><br /><br /><strong>Topic 6</strong><br /><strong>counter</strong><br /><strong>rogues</strong><br /><strong>tum</strong><br /><strong>lassus</strong><br /><strong>wars</strong><br /><br /><strong>Topic 7</strong><br /><strong>pitch</strong><br /><strong>cards</strong><br /><strong>cynicism</strong><br /><strong>silences</strong><br /><strong>shrewd</strong>
</pre>
<p>There are some repetitions, probably due to the composition of some topics, and the reader can try different prior parameters to observe the changes. It's possible to do an experiment to check whether the model works correctly.</p>
<p>Let's consider two documents:</p>
<pre>
<strong>&gt;&gt;&gt; print(corpus[0])</strong><br /><strong>It is not news that Nathan Milstein is a wizard of the violin .</strong><br /><br /><strong>&gt;&gt;&gt; print(corpus[2500])</strong><br /><strong>The children had nowhere to go and no place to play , not even sidewalks .</strong>
</pre>
<p>They are quite different and so are their topic distributions:</p>
<pre>
<strong>&gt;&gt;&gt; print(Xl[0])</strong><br /><strong>[ 0.85412134 0.02083335 0.02083335 0.02083335 0.02083335 0.02083677</strong><br /><strong> 0.02087515 0.02083335]</strong><br /><br /><strong>&gt;&gt;&gt; print(Xl[2500])</strong><br /><strong>[ 0.22499749 0.02500001 0.22500135 0.02500221 0.025 0.02500219</strong><br /><strong> 0.02500001 0.42499674]</strong>
</pre>
<p>We have a dominant topic <kbd>(0.85t<sub>0</sub>)</kbd> for the first document and a mixture <kbd>(0.22t<sub>0</sub> + 0.22t<sub>2&#160;</sub>+ 0.42t<sub>7</sub>)</kbd> for the second one. Now let's consider the concatenation of both documents:</p>
<pre>
<strong>&gt;&gt;&gt; test_doc = corpus[0] + ' ' + corpus[2500]</strong><br /><strong>&gt;&gt;&gt; y_test = lda.transform(cv.transform([test_doc]))</strong><br /><br /><strong>&gt;&gt;&gt; print(y_test)</strong><br /><strong>[[ 0.61242771 0.01250001 0.11251451 0.0125011 0.01250001 0.01250278</strong><br /><strong> 0.01251778 0.21253611]]</strong>
</pre>
<p>In the resulting document, as expected, the mixture has changed: <kbd>0.61t<sub><span>0</span></sub>&#160;+ 0.11t<sub>2</sub> + 0.21t<sub>7</sub></kbd>. In other words, the algorithm introduced the previously dominant topic 5 (which is now stronger) by weakening both topic 2 and topic 7. This is reasonable, because the length of the first document is less than the second one, and therefore topic 5 cannot completely cancel the other topics out.</p>


            </article>

            
        </section>
    </body>

</html>
