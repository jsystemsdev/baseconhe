<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Silhouette score</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Silhouette score</h1>
                </header>
            
            <article>
                
<p>The silhouette score is based on the principle of "maximum internal cohesion and maximum cluster separation". In other words, we would like to find the number of clusters that produce a subdivision of the dataset into dense blocks that are well separated from each other. In this way, every cluster will contain very similar elements and,&#160;selecting two elements belonging to different clusters, their distance should be greater than the maximum intracluster one. &#160;</p>
<p>After defining a distance metric (Euclidean is normally a good choice), we can compute the average intracluster distance for each element:</p>
<div class="CDPAlignCenter CDPAlign"><img height="39" width="239" src="assets/c112af5c-a6e6-404a-b9f3-f9a8260a0e98.png" /></div>
<p>We can also define the average nearest-cluster distance (which corresponds to the lowest intercluster distance):</p>
<div class="mce-root CDPAlignCenter CDPAlign"><img height="40" width="439" src="assets/f626ffe9-8b87-4d5c-afb3-2157ed02b658.png" /></div>
<p>The silhouette score for an element <em>x<sub>i</sub></em> is defined as:</p>
<div class="CDPAlignCenter CDPAlign"><img height="54" width="176" src="assets/03cd30d0-0832-446b-ba06-f928392adab7.png" /></div>
<p>This value is bounded between -1 and 1, with the following interpretation:</p>
<ul>
<li>A value close to 1 is good (1 is the best condition) because it means that <em>a(x<sub>i</sub>) &lt;&lt; b(x<sub>i</sub>)</em></li>
<li>A value close to 0 means that the difference between intra and inter cluster measures is almost null and therefore there's a cluster overlap</li>
<li>A value close to -1 means that the sample has been assigned to a wrong cluster because <em>a(x<sub>i</sub>) &gt;&gt; b(x<sub>i</sub>)</em></li>
</ul>
<p>scikit-learn allows computing the average silhouette score to have an immediate overview for different numbers of clusters:</p>
<pre>
<strong>from sklearn.metrics import silhouette_score</strong><br /><br /><strong>&gt;&gt;&gt; nb_clusters = [2, 3, 5, 6, 7, 8, 9, 10]</strong><br /><br /><strong>&gt;&gt;&gt; avg_silhouettes = []</strong><br /><br /><strong>&gt;&gt;&gt; for n in nb_clusters:</strong><br /><strong>&gt;&gt;&gt;    km = KMeans(n_clusters=n)</strong><br /><strong>&gt;&gt;&gt;    Y = km.fit_predict(X)</strong><br /><strong>&gt;&gt;&gt;    avg_silhouettes.append(silhouette_score(X, Y))</strong>
</pre>
<p>The corresponding plot is shown in the following figure:</p>
<div class="CDPAlignCenter CDPAlign"><img height="411" width="516" class="image-border" src="assets/0e9eacf0-fefe-44fd-abdc-3876abc7e3de.png" /></div>
<p>The best value is 3 (which is very close to 1.0), however, bearing in mind the previous method, 4 clusters provide a smaller inertia, together with a reasonable silhouette score. Therefore, a good choice could be 4 instead of 3. However, the decision between 3 and 4 is not immediate and should be evaluated by also considering the nature of the dataset. The silhouette score indicates that there are 3 dense agglomerates, but the inertia diagram&#160;suggests that one of them (at least) can probably be split into two clusters. To have a better understanding of how the clustering is working, it's also possible to graph the silhouette plots, showing the sorted score for each sample in all clusters. In the following snippet we create the plots for a number of clusters equal to 2, 3, 4, and 8:</p>
<pre class="mce-root">
<strong>from sklearn.metrics import silhouette_samples<br /><br />&gt;&gt;&gt; fig, ax = subplots(2, 2, figsize=(15, 10))</strong><br /><br /><strong>&gt;&gt;&gt; nb_clusters = [2, 3, 4, 8]</strong><br /><strong>&gt;&gt;&gt; mapping = [(0, 0), (0, 1), (1, 0), (1, 1)]</strong><br /><br /><strong>&gt;&gt;&gt; for i, n in enumerate(nb_clusters):</strong><br /><strong>&gt;&gt;&gt;    km = KMeans(n_clusters=n)</strong><br /><strong>&gt;&gt;&gt;    Y = km.fit_predict(X)</strong><br /><br /><strong>&gt;&gt;&gt;    silhouette_values = silhouette_samples(X, Y)</strong><br />    <br /><strong>&gt;&gt;&gt;    ax[mapping[i]].set_xticks([-0.15, 0.0, 0.25, 0.5, 0.75, 1.0])</strong><br /><strong>&gt;&gt;&gt;    ax[mapping[i]].set_yticks([])</strong><br /><strong>&gt;&gt;&gt;    ax[mapping[i]].set_title('%d clusters' % n)<br />&gt;&gt;&gt;    ax[mapping[i]].set_xlim([-0.15, 1])</strong><br /><strong>&gt;&gt;&gt;    ax[mapping[i]].grid()</strong><br /><strong>&gt;&gt;&gt;    y_lower = 20</strong><br /><br /><strong>&gt;&gt;&gt;    for t in range(n):</strong><br /><strong>&gt;&gt;&gt;        ct_values = silhouette_values[Y == t]</strong><br /><strong>&gt;&gt;&gt;        ct_values.sort()</strong><br />        <br /><strong>&gt;&gt;&gt;        y_upper = y_lower + ct_values.shape[0]</strong><br /><br /><strong>&gt;&gt;&gt;        color = cm.Accent(float(t) / n)</strong><br /><strong>&gt;&gt;&gt;        ax[mapping[i]].fill_betweenx(np.arange(y_lower, y_upper), 0, </strong><br /><strong>&gt;&gt;&gt;                                     ct_values, facecolor=color, edgecolor=color)</strong><br /><br /><strong>&gt;&gt;&gt;        y_lower = y_upper + 20</strong>
</pre>
<p>The silhouette coefficients for each sample are computed using the function <kbd>silhouette_values</kbd> (which are always bounded between -1 and 1). In this case, we are limiting the graph between -0.15 and 1 because there are no smaller values. However, it's important to check the whole range before restricting it.</p>
<p>The resulting graph is shown in the following figure:</p>
<div class="CDPAlignCenter CDPAlign"><img height="349" width="524" class="image-border" src="assets/921a7df7-e698-4367-b902-5120cd05ae2e.png" /></div>
<p>The width of each silhouette is proportional to the number of samples belonging to a specific cluster, and its shape is determined by the scores of each sample. An ideal plot should contain homogeneous and long silhouettes&#160;without peaks (they must be similar to trapezoids rather than triangles) because we expect to have a very low score variance among samples in the same cluster. For 2 clusters, the shapes are acceptable, but one cluster has an average score of 0.5, while the other has a value greater than 0.75; therefore, the first cluster has a low internal coherence. A completely different situation is shown in the plot corresponding to 8 clusters. All the silhouettes are triangular and their maximum score is slightly greater than 0.5. It means that all the clusters are internally coherent, but the separation is unacceptable. With 3 clusters, the plot is almost perfect, except for the width of the second silhouette. Without further metrics, we could consider this number as the best choice (confirmed also by the average score), but the inertia is lower for a higher numbers of clusters. With 4 clusters, the plot is slightly worse, with two silhouettes having a maximum score of about 0.5. This means that two clusters are perfectly coherent and separated, while the remaining two are rather coherent, but they aren't probably well separated. Right now, our choice should be made between 3 and 4. The next methods will help us in banishing all doubts.</p>


            </article>

            
        </section>
    </body>

</html>
