<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Classification with a multi-layer perceptron</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Classification with a multi-layer perceptron</h1>
                </header>
            
            <article>
                
<p>We can now build an architecture with two dense layers and train a classifier for a more complex dataset. Let's start by creating it:</p>
<pre>
<strong>from sklearn.datasets import make_classification</strong><br /><br /><strong>&gt;&gt;&gt; nb_samples = 1000</strong><br /><strong>&gt;&gt;&gt; nb_features = 3</strong><br /><br /><strong>&gt;&gt;&gt; X, Y = make_classification(n_samples=nb_samples, n_features=nb_features, </strong><br /><strong>&gt;&gt;&gt; n_informative=3, n_redundant=0, n_classes=2, n_clusters_per_class=3)</strong>
</pre>
<p>Even if we have only two classes, the dataset has three features and three clusters per class; therefore it's almost impossible that a linear classifier can separate it with very high accuracy. A plot of the dataset is shown in the following figure:</p>
<div class="CDPAlignCenter CDPAlign"><img class="image-border" src="assets/ba8e9e34-6fc2-4918-a777-19fa32ca1b1b.png" /></div>
<p class="CDPAlignLeft CDPAlign">For benchmarking purposes, it's useful to test a logistic regression:</p>
<pre class="CDPAlignLeft CDPAlign">
<strong>from sklearn.model_selection import train_test_split</strong><br /><strong>from sklearn.linear_model import LogisticRegression</strong><br /><br /><strong>&gt;&gt;&gt; X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2)</strong><br /><br /><strong>&gt;&gt;&gt; lr = LogisticRegression()</strong><br /><strong>&gt;&gt;&gt; lr.fit(X_train, Y_train)</strong><br /><strong>&gt;&gt;&gt; print('Score: %.3f' % lr.score(X_test, Y_test))</strong><br /><strong>Score: 0.715</strong>
</pre>
<p>The score computed on the test set is about 71%, which is not really bad but below an acceptable threshold. Let's try with an MLP with 50 hidden neurons (with hyperbolic tangent activation) and 1 sigmoid output neuron. The hyperbolic tangent is:</p>
<div class="CDPAlignCenter CDPAlign"><img height="66" width="150" src="assets/3ba68640-b6d9-4b26-8e04-d7eda7b4cb99.png" /></div>
<p>And it's bounded asymptotically between -1.0 and 1.0.</p>
<p>We are not going to implement each layer manually, but we're using the built-in class <kbd>tf.contrib.layers.fully_connected()</kbd>. It accepts the input tensor or placeholder&#160;<span>as the first argument</span><span>&#160;</span><span>and the number of layer-output neurons&#160;</span><span>as the second one</span><span>. The activation function can be specified using the attribute</span> <kbd>activation_fn</kbd><span>:</span></p>
<pre>
<strong>import tensorflow as tf</strong><br /><strong>import tensorflow.contrib.layers as tfl</strong><br /><br /><strong>&gt;&gt;&gt; graph = tf.Graph()</strong><br /><br /><strong>&gt;&gt;&gt; with graph.as_default():</strong><br /><strong>&gt;&gt;&gt;    Xt = tf.placeholder(tf.float32, shape=(None, nb_features), name='X')</strong><br /><strong>&gt;&gt;&gt;    Yt = tf.placeholder(tf.float32, shape=(None, 1), name='Y')</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;    layer_1 = tfl.fully_connected(Xt, num_outputs=50, activation_fn=tf.tanh)</strong><br /><strong>&gt;&gt;&gt;    layer_2 = tfl.fully_connected(layer_1, num_outputs=1,</strong><br /><strong>&gt;&gt;&gt;                                  activation_fn=tf.sigmoid)</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;    Yo = tf.round(layer_2)</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;    loss = tf.nn.l2_loss(layer_2 - Yt)</strong><br /><strong>&gt;&gt;&gt;    training_step = tf.train.GradientDescentOptimizer(0.025).minimize(loss)</strong>
</pre>
<p>As in the previous example, we have defined two placeholders,&#160;<kbd>Xt</kbd> and <kbd>Yt</kbd>, and two fully connected layers. The first one accepts as input <kbd>Xt</kbd> and has 50 output neurons (with <kbd>tanh</kbd> activation), while the second accepts as input the output of the previous layer (<kbd>layer_1</kbd>) and has only one sigmoid neuron, representing the class. The rounded output is provided by <kbd>Yo</kbd>, while the loss function is the total squared error, and it's implemented using the function <kbd>tf.nn.l2_loss()</kbd>&#160;computed on the difference between the output of the network (<kbd>layer_2</kbd>) and the target class placeholder <kbd>Yt</kbd>. The training step is implemented using a standard gradient descent optimizer, as for the logistic regression example.</p>
<p>We can now implement a training loop, splitting our dataset into a fixed number of batches (the number of samples is defined in the variable <kbd>batch_size</kbd>) and repeating a complete cycle for <kbd>nb_epochs</kbd> epochs:</p>
<pre>
<strong>&gt;&gt;&gt; session = tf.InteractiveSession(graph=graph)</strong><br /><strong>&gt;&gt;&gt; tf.global_variables_initializer().run()</strong><br /><br /><strong>&gt;&gt;&gt; nb_epochs = 200</strong><br /><strong>&gt;&gt;&gt; batch_size = 50</strong><br /><br /><strong>&gt;&gt;&gt; for e in range(nb_epochs):</strong><br /><strong>&gt;&gt;&gt;    total_loss = 0.0</strong><br /><strong>&gt;&gt;&gt;    Xb = np.ndarray(shape=(batch_size, nb_features), dtype=np.float32)</strong><br /><strong>&gt;&gt;&gt;    Yb = np.ndarray(shape=(batch_size, 1), dtype=np.float32)</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;    for i in range(0, X_train.shape[0]-batch_size, batch_size):</strong><br /><strong>&gt;&gt;&gt;       Xb[:, :] = X_train[i:i+batch_size, :]</strong><br /><strong>&gt;&gt;&gt;       Yb[:, 0] = Y_train[i:i+batch_size]</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;       loss_value, _ = session.run([loss, training_step], </strong><br /><strong>&gt;&gt;&gt;                                   feed_dict={Xt: Xb, Yt: Yb})</strong><br /><strong>&gt;&gt;&gt;       total_loss += loss_value</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;        Y_predicted = session.run([Yo], </strong><br /><strong>&gt;&gt;&gt;               feed_dict={Xt: X_test.reshape((X_test.shape[0], nb_features))})</strong><br /><strong>&gt;&gt;&gt;        accuracy = 1.0 -</strong><br /><strong>&gt;&gt;&gt;            (np.sum(np.abs(np.array(Y_predicted[0]).squeeze(axis=1) -Y_test)) /</strong><br /><strong>&gt;&gt;&gt;            float(Y_test.shape[0]))</strong><br /><strong>&gt;&gt;&gt; </strong><br /><strong>&gt;&gt;&gt;        print('Epoch %d) Total loss: %.2f - Accuracy: %.2f' % </strong><br /><strong>&gt;&gt;&gt;              (e, total_loss, accuracy))</strong><br /><br /><strong>Epoch 0) Total loss: 78.19 - Accuracy: 0.66<br />Epoch 1) Total loss: 75.02 - Accuracy: 0.67<br />Epoch 2) Total loss: 72.28 - Accuracy: 0.68<br />Epoch 3) Total loss: 68.52 - Accuracy: 0.71<br />Epoch 4) Total loss: 63.50 - Accuracy: 0.79<br />Epoch 5) Total loss: 57.51 - Accuracy: 0.84</strong><br /><strong>...</strong><br /><strong>Epoch 195) Total loss: 15.34 - Accuracy: 0.94<br />Epoch 196) Total loss: 15.32 - Accuracy: 0.94<br />Epoch 197) Total loss: 15.31 - Accuracy: 0.94<br />Epoch 198) Total loss: 15.29 - Accuracy: 0.94<br />Epoch 199) Total loss: 15.28 - Accuracy: 0.94</strong>
</pre>
<p class="CDPAlignLeft CDPAlign">As it's possible to see, without particular attention to all details, the accuracy computed on the test set is 94%. This is an acceptable value, considering the structure of the dataset. In&#160;Goodfellow I., Bengio Y., Courville A., <em>Deep Learning</em>, MIT Press<em>,&#160;</em>the reader will find details of many important concepts that can still improve the performance and speed up the convergence process.</p>


            </article>

            
        </section>
    </body>

</html>
