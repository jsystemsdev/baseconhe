<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Voting classifier</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Voting classifier</h1>
                </header>
            
            <article>
                
<p>A very interesting ensemble solution is offered by the class <kbd>VotingClassifier</kbd>, which isn't an actual classifier but a wrapper for a set of different ones that are trained and evaluated in parallel. The final decision for a prediction is taken by majority vote according to two different strategies:</p>
<ul>
<li><strong>Hard voting</strong>: In this case, the class that received the major number of votes, <em>N<sub>c</sub>(y<sub>t</sub>)</em>,&#160;will be chosen:</li>
</ul>
<div class="CDPAlignCenter CDPAlign"><img height="28" width="244" src="assets/db915ce4-a64d-4a62-8978-bbbbf1bb11f7.png" /></div>
<ul>
<li><strong>Soft voting</strong>: In this case, the probability vectors for each predicted class (<span>for</span> <span>all</span><span>&#160;classifiers</span>) are&#160;summed up and averaged. The winning class is the one corresponding to the highest value:</li>
</ul>
<div class="CDPAlignCenter CDPAlign"><img height="52" width="312" src="assets/299f90ef-803c-498b-a8a9-458597cdf2ba.png" /></div>
<p>Let's consider a dummy dataset and compute the accuracy with a hard voting strategy:</p>
<pre>
<strong>from sklearn.datasets import make_classification</strong><br /><br /><strong>&gt;&gt;&gt; nb_samples = 500</strong><br /><br /><strong>&gt;&gt;&gt; X, Y = make_classification(n_samples=nb_samples, n_features=2, n_redundant=0, n_classes=2)</strong>
</pre>
<p>For our examples, we are going to consider three classifiers: logistic regression, decision tree (with default Gini impurity), and an SVM (with a polynomial kernel and <kbd>probability=True</kbd>&#160;in order to generate the probability vectors). This choice has been made only for didactic purposes and may not be the best one. When creating an ensemble, it's useful to consider the different features of each involved classifier and avoid "duplicate" algorithms (for example, a logistic regression and a linear SVM or a perceptron are likely to yield very similar performances). In many cases, it can be useful to mix nonlinear classifiers with random forests or AdaBoost classifiers. The reader can repeat this experiment with other combinations, comparing the performance of each single estimator and the accuracy of the voting classifier:</p>
<pre>
<strong>from sklearn.linear_model import LogisticRegression</strong><br /><strong>from sklearn.svm import SVC</strong><br /><strong>from sklearn.tree import DecisionTreeClassifier<br />from sklearn.ensemble import VotingClassifier</strong><br /><br /><strong>&gt;&gt;&gt; lr = LogisticRegression()</strong><br /><strong>&gt;&gt;&gt; svc = SVC(kernel='poly', probability=True)</strong><br /><strong>&gt;&gt;&gt; dt = DecisionTreeClassifier()</strong><br /><br /><strong>&gt;&gt;&gt; classifiers = [('lr', lr),</strong><br /><strong>                   ('dt', dt),</strong><br /><strong>                   ('svc', svc)]<br /><br />&gt;&gt;&gt; vc = VotingClassifier(estimators=classifiers, voting='hard')</strong>
</pre>
<p>Computing the cross-validation accuracies, we get:</p>
<pre>
<strong>from sklearn.model_selection import cross_val_score</strong><br /><br /><strong>&gt;&gt;&gt; a = []</strong><br /><br /><strong>&gt;&gt;&gt; a.append(cross_val_score(lr, X, Y, scoring='accuracy', cv=10).mean())</strong><br /><strong>&gt;&gt;&gt; a.append(cross_val_score(dt, X, Y, scoring='accuracy', cv=10).mean())</strong><br /><strong>&gt;&gt;&gt; a.append(cross_val_score(svc, X, Y, scoring='accuracy', cv=10).mean())</strong><br /><strong>&gt;&gt;&gt; a.append(cross_val_score(vc, X, Y, scoring='accuracy', cv=10).mean())<br /><br />&gt;&gt;&gt; print(np.array(a))<br />[ 0.90182873  0.84990876  0.87386955  0.89982873]<br /></strong>
</pre>
<p>The accuracies of each single classifier and of the ensemble are plotted in the following figure:</p>
<div class="CDPAlignCenter CDPAlign"><img class="image-border" src="assets/bbaede25-88fd-40e6-a5e6-5beafaee0dcb.png" /></div>
<p class="CDPAlignLeft CDPAlign">As expected, the ensemble takes advantage of the different algorithms and yields better performance than any single one. We can now repeat the experiment&#160;with soft voting, considering that it's also possible to introduce a weight vector (through the parameter <kbd>weights</kbd>) to give more or less importance to each classifier:</p>
<div class="CDPAlignCenter CDPAlign"><img height="58" width="319" src="assets/b6544182-7ff2-4da3-97c1-75fc55c89cab.png" /></div>
<p class="CDPAlignLeft CDPAlign">For example, considering the previous figure, we can decide to give more importance to the logistic regression and less to the decision tree and SVM:</p>
<pre class="CDPAlignLeft CDPAlign">
<strong>&gt;&gt;&gt; weights = [1.5, 0.5, 0.75]</strong><br /><br /><strong>&gt;&gt;&gt; vc = VotingClassifier(estimators=classifiers, weights=weights, voting='soft')</strong>
</pre>
<p>Repeating the same calculations for the cross-validation accuracies, we get:</p>
<pre>
&gt;&gt;&gt; <strong>print(np.array(a))<br />[ 0.90182873  0.85386795  0.87386955  0.89578952]</strong>
</pre>
<p>The resulting plot is shown in the following figure:</p>
<div class="CDPAlignCenter CDPAlign"><img height="338" width="501" class="image-border" src="assets/62a95cab-4d66-4641-900e-ba7783271b7c.png" /></div>
<p class="CDPAlignLeft CDPAlign">Weighting is not limited to the soft strategy. It can also be applied to hard voting, but in that case, it will be used to filter (reduce or increase) the number of actual occurrences.</p>
<div class="CDPAlignCenter CDPAlign"><img height="31" width="304" src="assets/b8a75a42-8da7-40db-aa66-e5c0ad7279e9.png" /></div>
<p class="CDPAlignLeft CDPAlign">Here,&#160;<em>N<sub>c</sub>(y<sub>t</sub>,w)</em> is the number of votes for each target class, each of them multiplied by the corresponding classifier weighting factor.</p>
<p class="CDPAlignLeft CDPAlign">A voting classifier can be a good choice whenever a single strategy is not able to reach the desired accuracy threshold; while exploiting the different approaches, it's possible to capture many microtrends using only a small set of strong (but sometimes limited) learners.</p>


            </article>

            
        </section>
    </body>

</html>
