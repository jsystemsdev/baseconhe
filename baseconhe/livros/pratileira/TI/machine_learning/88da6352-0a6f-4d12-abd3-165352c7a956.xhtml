<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>K-means</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>K-means</h1>
                </header>
            
            <article>
                
<p>The k-means algorithm is based on the (strong) initial condition to decide the number of clusters through the assignment of k initial&#160;<strong>centroids</strong> or <strong>means</strong>:</p>
<div class="CDPAlignCenter CDPAlign"><img height="34" width="175" src="assets/99622594-d1a7-4bb6-80c6-c04b9934706d.png" /></div>
<p>Then the distance between each sample and each centroid is computed and the sample is assigned to the cluster where the distance is minimum. This approach is often called <strong>minimizing the inertia</strong> of the clusters, which is defined as follows:</p>
<div class="CDPAlignCenter CDPAlign"><img height="57" width="225" src="assets/edffcdff-056f-4bcf-ab1f-f71c63bdb6b4.png" /></div>
<p>The process is iterative—once all the samples have been processed, a new set of centroids <em>K<sup>(1)</sup></em>&#160;is computed (now considering the actual elements belonging to the cluster), and all the distances are recomputed. The algorithm&#160;stops when the desired tolerance is reached, or in other words, when the centroids become stable and, therefore, the inertia is minimized.<br />
Of course, this approach is quite sensitive to the initial conditions, and some methods have been studied to improve the convergence speed. One of them is called <strong>k-means++</strong>&#160;(Karteeka&#160;Pavan K., Allam Appa Rao, Dattatreya Rao A. V., and Sridhar G.R., <em>Robust Seed Selection Algorithm for K-Means Type Algorithms</em>,&#160;International Journal of Computer Science and Information Technology 3, no. 5, October 30, 2011), which selects the initial centroids so that they are statistically close to the final ones. The mathematical explanation is quite difficult; however, this method is the default choice for scikit-learn, and it's normally the best choice for any clustering problem solvable with this algorithm.<br />
Let's consider a simple example with a dummy dataset:</p>
<pre>
<strong>from sklearn.datasets import make_blobs</strong><br /><br /><strong>nb_samples = 1000</strong><br /><strong>X, _ = make_blobs(n_samples=nb_samples, n_features=2, centers=3, cluster_std=1.5)</strong>
</pre>
<p>We expect to have three clusters with bidimensional features and a partial overlap due to the standard deviation of each blob. In our example, we won't use the <em>Y</em> variable (which contains the expected cluster) because we want to generate only a set of locally coherent points to try our algorithms.</p>
<p>The resultant plot is shown in the following figure:</p>
<div class="CDPAlignCenter CDPAlign"><img height="423" width="513" class="image-border" src="assets/19ad3dd2-8dc8-46c5-bc5d-e4607b0bfe16.png" /></div>
<p>In this case, the problem is quite simple to solve, so we expect k-means to separate the three groups with minimum error in the region of <em>X</em> bounded between [-5, 0]. Keeping the default values, we get:</p>
<pre>
<strong>from sklearn.cluster import KMeans</strong><br /><br /><strong>&gt;&gt;&gt; km = KMeans(n_clusters=3)</strong><br /><strong>&gt;&gt;&gt; km.fit(X)</strong><br /><strong>KMeans(algorithm='auto', copy_x=True, init='k-means++', max_iter=300,</strong><br /><strong>    n_clusters=3, n_init=10, n_jobs=1, precompute_distances='auto',</strong><br /><strong>    random_state=None, tol=0.0001, verbose=0)</strong><br /><br /><strong>&gt;&gt;&gt; print(km.cluster_centers_)</strong><br /><strong>[[ 1.39014517,  1.38533993]</strong><br /><strong> [ 9.78473454,  6.1946332 ]</strong><br /><strong> [-5.47807472,  3.73913652]]</strong>
</pre>
<p>Replotting the data using three different markers, it's possible to verify how k-means successfully separated the data:</p>
<div class="CDPAlignCenter CDPAlign"><img height="423" width="519" class="image-border" src="assets/ead1c912-5064-454d-8e68-be7b4724565a.png" /></div>
<p>In this case, the separation was very easy because k-means is based on Euclidean distance, which is radial, and therefore the clusters are expected to be convex. When this doesn't happen, the problem cannot be solved using this algorithm. Most of the time, even if the convexity is not fully guaranteed, k-means can produce good results, but there are several situations when the expected clustering is impossible and letting k-means find out the centroid can lead to completely wrong solutions.<br />
Let's consider the case of concentric circles. scikit-learn provides a built-in function to generate such datasets:</p>
<pre>
<strong>from sklearn.datasets import make_circles</strong><br /><br /><strong>&gt;&gt;&gt; nb_samples = 1000</strong><br /><strong>&gt;&gt;&gt; X, Y = make_circles(n_samples=nb_samples, noise=0.05)</strong>
</pre>
<p>The plot of this dataset is shown in the following figure:</p>
<div class="CDPAlignCenter CDPAlign"><img height="400" width="503" class="image-border" src="assets/57f248aa-65af-4d56-b0a7-102264e80fa7.png" /></div>
<p>We would like to have an internal cluster (corresponding to the samples depicted with triangular markers) and an external one (depicted by dots). However, such sets are not convex, and it's impossible for k-means to separate&#160;them correctly (the means should be the same!). In fact, suppose we try to apply the algorithm to two clusters:</p>
<pre>
<strong>&gt;&gt;&gt; km = KMeans(n_clusters=2)</strong><br /><strong>&gt;&gt;&gt; km.fit(X)</strong><br /><strong>KMeans(algorithm='auto', copy_x=True, init='k-means++', max_iter=300,</strong><br /><strong>    n_clusters=2, n_init=10, n_jobs=1, precompute_distances='auto',</strong><br /><strong>    random_state=None, tol=0.0001, verbose=0)</strong>
</pre>
<p>We get the separation shown in the following figure:</p>
<div class="CDPAlignCenter CDPAlign"><img height="411" width="516" class="image-border" src="assets/d2afc467-883c-488f-9845-f2ebfff0e1c9.png" /></div>
<p>As expected, k-means converged on the two centroids in the middle of the two half-circles, and the resulting clustering is quite different from what we expected. Moreover, if the samples must be considered different according to the distance from the common center, this result will lead to completely wrong predictions. It's obvious that another method must be employed.</p>


            </article>

            
        </section>
    </body>

</html>
