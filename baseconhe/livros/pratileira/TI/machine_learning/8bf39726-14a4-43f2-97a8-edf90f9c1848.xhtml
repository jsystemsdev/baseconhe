<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title>Ridge, Lasso, and ElasticNet</title>
        <link rel="stylesheet" href="css/style.css" type="text/css"/>
        <meta charset="utf-8"/>
    </head>

    <body>
        <section>

                            <header>
                    <h1 class='header-title'>Ridge, Lasso, and ElasticNet</h1>
                </header>
            
            <article>
                
<p><strong>Ridge</strong> regression imposes an additional shrinkage penalty to the ordinary least squares loss function to limit its squared <em>L2</em> norm:</p>
<div class="CDPAlignCenter CDPAlign"><img height="36" width="186" src="assets/29f5a319-985d-4975-9f1f-182d2457daaa.png" /></div>
<p class="CDPAlignLeft CDPAlign">In this case, <em>X</em> is a matrix containing all samples as columns and the term&#160;<em>w</em>&#160;represents the weight vector. The additional term (through the coefficient alpha—if large it implies a stronger regularization and smaller values) forces the loss function to disallow an infinite growth of <em>w</em>, which can be caused by&#160;multicollinearity or ill-conditioning. In the following figure, there's a representation of what happens when a Ridge penalty is applied:</p>
<div class="CDPAlignCenter CDPAlign"><img height="431" width="618" class="image-border" src="assets/27ba4446-3975-4074-9046-f59a1142b833.png" /></div>
<p>The gray surface represents the loss function (here, for simplicity, we're working with only two weights), while the circle center&#160;<strong>O</strong> is the boundary imposed by the Ridge condition. The minimum will have smaller <em>w</em> values and potential explosions are avoided.</p>
<p>In the following snippet, we're going to compare <kbd>LinearRegression</kbd>&#160;and&#160;<kbd>Ridge</kbd><em><strong>&#160;</strong></em>with a cross-validation:</p>
<pre>
<strong>from sklearn.datasets import load_diabetes</strong><br /><strong>from sklearn.linear_model import LinearRegression, Ridge</strong><br /><br /><strong>&gt;&gt;&gt; diabetes = load_diabetes()</strong><br /><br /><strong>&gt;&gt;&gt; lr = LinearRegression(normalize=True)</strong><br /><strong>&gt;&gt;&gt; rg = Ridge(0.001, normalize=True)</strong><br /><br /><strong>&gt;&gt;&gt; lr_scores = cross_val_score(lr, diabetes.data, diabetes.target, cv=10)</strong><br /><strong>&gt;&gt;&gt; lr_scores.mean()</strong><br /><strong>0.46196236195833718</strong><br /><br /><strong>&gt;&gt;&gt; rg_scores = cross_val_score(rg, diabetes.data, diabetes.target, cv=10)</strong><br /><strong>&gt;&gt;&gt; rg_scores.mean()</strong><br /><strong>0.46227174692391299</strong>
</pre>
<p>Sometimes, finding the right value for alpha (Ridge coefficient) is not so immediate. scikit-learn provides the class <kbd>RidgeCV</kbd>, which allows performing an automatic grid search (among a set and returning the best estimation):</p>
<pre>
<strong>from sklearn.linear_model import RidgeCV</strong><br /><br /><strong>&gt;&gt;&gt; rg = RidgeCV(alphas=(1.0, 0.1, 0.01, 0.005, 0.0025, 0.001, 0.00025), normalize=True)</strong><br /><strong>&gt;&gt;&gt; rg.fit(diabetes.data, diabetes.target)</strong><br /><br /><strong>&gt;&gt;&gt; rg.alpha_</strong><br /><strong>0.0050000000000000001</strong>
</pre>
<p>A&#160;Lasso regressor imposes a penalty on the <em>L1</em> norm of <em>w</em>&#160;to determine a potentially higher number of null coefficients:</p>
<p class="packt_figref CDPAlignCenter CDPAlign"><img height="48" width="199" src="assets/fc98ec6a-260a-45ae-94f2-42241fd0ac0e.png" /></p>
<p>The sparsity is a consequence of the penalty term (the mathematical proof is non-trivial and will be omitted).</p>
<div class="packt_figref CDPAlignCenter CDPAlign"><img class="image-border" src="assets/c45ad4a8-e0f5-428e-a9c0-997f3665c279.png" /></div>
<p>In this case, there are vertices where a component is non-null while all the other weights are zero. The probability of an intersection with a vertex is proportional to the dimensionality of <em>w</em> and, therefore, it's normal to discover a rather sparse model after training a Lasso regressor.</p>
<p>In the following snippet, the diabetes dataset is used to fit a Lasso model:</p>
<pre>
<strong>from sklearn.linear_model import Lasso</strong><br /><br /><strong>&gt;&gt;&gt; ls = Lasso(alpha=0.001, normalize=True)</strong><br /><strong>&gt;&gt;&gt; ls_scores = cross_val_score(ls, diabetes.data, diabetes.target, cv=10)</strong><br /><strong>&gt;&gt;&gt; ls_scores.mean()</strong><br /><strong>0.46215747851504058</strong>
</pre>
<p>Also for Lasso, there's the possibility of running a grid search for the best alpha parameter. The class, in this case, is <kbd>LassoCV</kbd> and its internal dynamics are similar to what was already seen for Ridge. Lasso can also perform efficiently on the sparse data generated through the&#160;<kbd>scipy.sparse</kbd> class, allowing for training bigger models without the need for partial fitting:</p>
<pre>
<strong>from scipy import sparse</strong><br /><br /><strong>&gt;&gt;&gt; ls = Lasso(alpha=0.001, normalize=True)</strong><br /><strong>&gt;&gt;&gt; ls.fit(sparse.coo_matrix(diabetes.data), diabetes.target)</strong><br /><strong>Lasso(alpha=0.001, copy_X=True, fit_intercept=True, max_iter=1000,</strong><br /><strong>   normalize=True, positive=False, precompute=False, random_state=None,</strong><br /><strong>   selection='cyclic', tol=0.0001, warm_start=False)</strong>
</pre>
<div class="packt_infobox">When working with a huge amount of data, some models cannot fit completely in memory, so it's impossible to train them. scikit-learn offers some models, such as&#160;<strong>stochastic gradient descent</strong> (<strong>SGD</strong>), which work in a&#160;way quite similar to <kbd>LinearRegression</kbd> with <kbd>Ridge</kbd>/<kbd>Lasso</kbd>; however, they also implement the method <kbd>partial_fit()</kbd>,&#160;which also allows continuous training&#160;through Python generators. See&#160;<a href="http://scikit-learn.org/stable/modules/linear_model.html#stochastic-gradient-descent-sgd">http://scikit-learn.org/stable/modules/linear_model.html#stochastic-gradient-descent-sgd</a>, for further details.</div>
<p>The last alternative is <strong>ElasticNet</strong>, which combines both Lasso and Ridge into a single model with two penalty factors: one proportional to <em>L1</em> norm and the other to <em>L2</em> norm. In this way, the resulting model will be sparse like a pure Lasso, but with the same regularization ability as provided by Ridge. The resulting loss function is:</p>
<p class="packt_figref CDPAlignCenter CDPAlign"><img height="50" width="337" src="assets/359fbc7d-fc64-4227-bca6-a71ea6a89dc2.png" /></p>
<p>The <kbd>ElasticNet</kbd> class provides an implementation where the alpha parameter works in conjunction with <kbd>l1_ratio</kbd>&#160;(beta in the formula<strong>)</strong>. The main peculiarity of <kbd>ElasticNet</kbd> is avoiding&#160;a selective exclusion of correlated features, thanks to the balanced action of the&#160;<em>L1</em> and <em>L2</em> norms.</p>
<p>In the following snippet, there's an example using both the <kbd>ElasticNet</kbd> and <kbd>ElasticNetCV</kbd> classes:</p>
<pre>
<strong>from sklearn.linear_model import ElasticNet, ElasticNetCV</strong><br /><br /><strong>&gt;&gt;&gt; en = ElasticNet(alpha=0.001, l1_ratio=0.8, normalize=True)</strong><br /><strong>&gt;&gt;&gt; en_scores = cross_val_score(en, diabetes.data, diabetes.target, cv=10)</strong><br /><strong>&gt;&gt;&gt; en_scores.mean()</strong><br /><strong>0.46358858847836454</strong><br /><br /><strong>&gt;&gt;&gt; encv = ElasticNetCV(alphas=(0.1, 0.01, 0.005, 0.0025, 0.001), l1_ratio=(0.1, 0.25, 0.5, 0.75, 0.8), normalize=True)</strong><br /><strong>&gt;&gt;&gt; encv.fit(dia.data, dia.target)</strong><br /><strong>ElasticNetCV(alphas=(0.1, 0.01, 0.005, 0.0025, 0.001), copy_X=True, cv=None,</strong><br /><strong>       eps=0.001, fit_intercept=True, l1_ratio=(0.1, 0.25, 0.5, 0.75, 0.8),</strong><br /><strong>       max_iter=1000, n_alphas=100, n_jobs=1, normalize=True,</strong><br /><strong>       positive=False, precompute='auto', random_state=None,</strong><br /><strong>       selection='cyclic', tol=0.0001, verbose=0)</strong><br /><br /><strong>&gt;&gt;&gt; encv.alpha_</strong><br /><strong>0.001</strong><br /><strong>&gt;&gt;&gt; encv.l1_ratio_</strong><br /><strong>0.75</strong>
</pre>


            </article>

            
        </section>
    </body>

</html>
