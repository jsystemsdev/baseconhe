<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Events</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Events"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-2-SECT-14"/>Events</h1></div></div></div><p><span class="emphasis"><em>Event</em></span> <a class="indexterm" id="csharpess2-IDXTERM-465"/><span class="emphasis"><em>handling</em></span> is essentially a process by
      which one object can notify other objects that an event has occurred.
      This process is largely encapsulated by multicast delegates, which have
      this ability built in.</p><div class="sect2" title="Defining a Delegate for an Event"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-14.1"/>Defining a Delegate for an Event</h2></div></div></div><p>The<a class="indexterm" id="IXT-2-218249"/> <a class="indexterm" id="IXT-2-218250"/> FCL defines numerous public delegates used for event
        handling, but you can also write your own. For example:</p><a id="I_2_tt218"/><pre class="programlisting">delegate void MoveEventHandler(object source, MoveEventArgs e);</pre><p>By convention, an event delegate’s first parameter denotes the
        source of the event, and the delegate’s second parameter derives from
        <code class="literal">System.EventArgs</code><a class="indexterm" id="IXT-2-218251"/> and stores data about the event.</p></div><div class="sect2" title="Storing Data for an Event with EventArgs"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-14.2"/>Storing Data for an Event with EventArgs</h2></div></div></div><p>You can define subclasses of <a class="indexterm" id="IXT-2-218252"/> <a class="indexterm" id="IXT-2-218253"/><code class="literal">EventArgs</code> to include
        information relevant to a particular event:</p><a id="I_2_tt219"/><pre class="programlisting">public class MoveEventArgs : EventArgs {
  public int newPosition;
  public bool cancel;
  public MoveEventArgs(int newPosition) {
    this.newPosition = newPosition;
  }
}</pre></div><div class="sect2" title="Declaring and Firing an Event"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-14.3"/>Declaring and Firing an Event</h2></div></div></div><p>A <a class="indexterm" id="IXT-2-218254"/>class or struct can declare an event by applying the
        event modifier to a delegate field. In this example, the <code class="literal">Slider</code> class has a <code class="literal">Position</code> property that fires a <code class="literal">Move</code> event whenever its <code class="literal">Position</code> changes:</p><a id="I_2_tt220"/><pre class="programlisting">class Slider {
  int position;
  public event MoveEventHandler Move;
  public int Position {
    get { return position; }
    set {
      if (position != value) { // if position changed
        if (Move != null) { // if invocation list not empty
          MoveEventArgs args = new MoveEventArgs(value);
          Move(this, args); // fire event
          if (args.cancel)
            return;
        }
        position = value;
      }
    }  
  }
}</pre><p>The <code class="literal">event</code><a class="indexterm" id="IXT-2-218255"/> keyword promotes encapsulation by ensuring that only
        the <code class="literal">+=</code><a class="indexterm" id="IXT-2-218256"/> and <code class="literal">-=</code><a class="indexterm" id="IXT-2-218257"/> operations can be performed on the delegate. This means
        other classes can register themselves to be notified of the event, but
        only the <code class="literal">Slider</code> can invoke the
        delegate (fire the event) or clear the delegate’s invocation
        list.</p></div><div class="sect2" title="Acting on an Event with Event Handlers"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-14.4"/>Acting on an Event with Event Handlers</h2></div></div></div><p>You <a class="indexterm" id="IXT-2-218258"/>can act on an event by adding an event handler to an
        event. An <span class="emphasis"><em>event handler</em></span> is a delegate that wraps
        the method you want invoked when the event is fired.</p><p>In the next example, we want our <code class="literal">Form</code> to act on changes made to a <code class="literal">Slider</code>’s <code class="literal">Position</code>. You do this by creating a <code class="literal">MoveEventHandler</code> delegate that wraps the
        event-handling method, the <code class="literal">slider.Move</code> method. This delegate is added
        to the <code class="literal">Move</code> event’s existing list
        of <code class="literal">MoveEventHandler</code>s (which starts
        off empty). Changing the position on the <code class="literal">Slider</code> object fires the <code class="literal">Move</code> event, which invokes the <code class="literal">slider.Move</code> method:</p><a id="I_2_tt221"/><pre class="programlisting">class Form {
  static void Main(  ) {
    Slider slider = new Slider(  );
    // register with the Move event
    slider.Move += new MoveEventHandler(slider_Move);
    slider.Position = 20;
    slider.Position = 60;
  }
  static void slider_Move(object source, MoveEventArgs e) {
    if(e.newPosition &lt; 50)
      Console.WriteLine("OK");
    else {
      e.cancel = true;
      Console.WriteLine("Can't go that high!");
    }
  }
}</pre><p>Typically, the <code class="literal">Slider</code> class
        would be enhanced to fire the <code class="literal">Move</code>
        event whenever its <code class="literal">Position</code> is
        changed by a mouse movement, keypress, or other user action.</p></div><div class="sect2" title="Event Accessors"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-14.5"/>Event Accessors</h2></div></div></div><div class="informaltable"><a id="ch02-172-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt222"/><pre class="programlisting"><em class="replaceable"><code>attributes</code></em>? unsafe? <em class="replaceable"><code>access-modifier</code></em>?
[
  [[sealed | abstract]? override] |
  new? [virtual | static]?
]? 
event <em class="replaceable"><code>delegate type event-property accessor-name</code></em>
{   
<em class="replaceable"><code>attributes</code></em>? add <em class="replaceable"><code>statement-block</code></em>   
<em class="replaceable"><code>attributes</code></em>? remove <em class="replaceable"><code>statement-block</code></em>
}</pre></td></tr></tbody></table></div><p>Note that <code class="literal">abstract</code> accessors
        don’t specify an implementation, so they replace an add/remove block
        with a semicolon.</p><p><a class="indexterm" id="IXT-2-218259"/> <a class="indexterm" id="IXT-2-218260"/>Similar to the way properties provide controlled access
        to a field, event accessors provide controlled access to an event.
        Consider the following field declaration:</p><a id="I_2_tt223"/><pre class="programlisting">public event MoveEventHandler Move;</pre><p>Except for the underscore prefix added to the field (to avoid a
        name collision), this is semantically identical to:</p><a id="I_2_tt224"/><pre class="programlisting">private MoveEventHandler _Move;
public event MoveEventHandler Move {
  add {
    _Move += value;
  }
  remove {
    _Move -= value;
 }
}</pre><p>The ability to specify a custom implementation of add and remove
        handlers for an event allows a class to proxy an event generated by
        another class, thus acting as a relay for an event rather than as the
        generator of that event. Another advantage of this technique is to
        eliminate the need to store a delegate as a field, which can be costly
        in terms of storage space. For instance, a class with 100 event fields
        would store 100 delegate fields, even though maybe only four of those
        events are actually assigned. Instead, you can store these <a class="indexterm" id="IXTR3-13"/>delegates in a
        dictionary and add and remove the delegates from that dictionary
        (assuming the dictionary holding four elements uses less storage space
        than 100 delegate references).</p><div class="tip" title="Tip"><h3 class="title"><a id="ch02-174-fm2xml"/>Tip</h3><p>The add and remove parts of an event are compiled to <code class="literal">add_XXX</code> and <code class="literal">remove_XXX</code> methods.</p></div></div></div></body></html>