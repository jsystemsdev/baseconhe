<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Inheritance</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Inheritance"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-2-SECT-7"/>Inheritance</h1></div></div></div><p>A <a class="indexterm" id="csharpess2-IDXTERM-270"/>C# class can <span class="emphasis"><em>inherit</em></span> from another
      class to extend or customize that class. A class can inherit only from a
      single class but can be inherited by many classes, thus forming a class
      hierarchy. At the root of any class hierarchy is the <code class="literal">object</code><a class="indexterm" id="IXT-2-218064"/> class, which all objects implicitly inherit from.
      Inheriting from a class requires specifying the class to inherit from in
      the class declaration, using the C++ colon notation:</p><a id="I_2_tt135"/><pre class="programlisting">class Location { // Implicitly inherits from object
  string name;

  // The constructor that initializes Location
  public Location(string name) {
    this.name = name;
  }
  public string Name {get {return name;}}
  public void Display(  ) {
    Console.WriteLine(Name);
  }
}
class URL : Location { // Inherit from Location
  public void Navigate(  ) {
    Console.WriteLine("Navigating to "+Name);
  }
  // The constructor for URL, which calls Location's constructor
  public URL(string name) : base(name) {}
}</pre><p><code class="literal">URL</code> has all the members of
      <code class="literal">Location</code> and a new member, <code class="literal">Navigate</code>:</p><a id="I_2_tt136"/><pre class="programlisting">class Test {
  public static void Main(  ) {
    URL u = new URL("http://microsoft.com");
    u.Display(  );
    u.Navigate(  );
  }
}</pre><div class="tip" title="Tip"><h3 class="title"><a id="ch02-84-fm2xml"/>Tip</h3><p>The <a class="indexterm" id="IXT-2-218065"/>specialized class and general class are referred to as
        either the <span class="emphasis"><em>derived class</em></span><a class="indexterm" id="IXT-2-218066"/> and <span class="emphasis"><em>base class</em></span><a class="indexterm" id="IXT-2-218067"/> or the <span class="emphasis"><em>subclass</em></span><a class="indexterm" id="IXT-2-218068"/> and <span class="emphasis"><em>superclass</em></span><a class="indexterm" id="IXT-2-218069"/>.</p></div><div class="sect2" title="Class Conversions"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-7.1"/>Class Conversions</h2></div></div></div><p>A class<a class="indexterm" id="IXT-2-218070"/> D may be implicitly
        <span class="emphasis"><em>upcast</em></span><a class="indexterm" id="IXT-2-218071"/> to the class B it derives from, and a class B may be
        explicitly <span class="emphasis"><em>downcast</em></span><a class="indexterm" id="IXT-2-218072"/> to a class D that derives from it. For instance:</p><a id="I_2_tt137"/><pre class="programlisting">URL u = new URL("http://microsoft.com");
Location l = u; // upcast
u = (URL)l; // downcast</pre><p>If the downcast fails, an <code class="literal">InvalidCastException</code><a class="indexterm" id="IXT-2-218073"/> is thrown.</p><div class="sect3" title="as operator"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-7.1.1"/>as operator</h3></div></div></div><p>The <code class="literal">as</code><a class="indexterm" id="IXT-2-218074"/> operator allows a downcast that evaluates to <code class="literal">null</code> to be made if the downcast
          fails:</p><a id="I_2_tt138"/><pre class="programlisting">u = l as URL;</pre></div><div class="sect3" title="is operator"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-7.1.2"/>is operator</h3></div></div></div><p>The <code class="literal">is</code><a class="indexterm" id="IXT-2-218075"/> operator can test if an objectis or derives from a
          specified class (or implements an interface). It is often used to
          perform a test before a downcast:</p><a id="I_2_tt139"/><pre class="programlisting">if (l is URL)
  ((URL)l).Navigate(  );</pre></div></div><div class="sect2" title="Polymorphism"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-7.2"/>Polymorphism</h2></div></div></div><p><span class="emphasis"><em>Polymorphism</em></span><a class="indexterm" id="IXT-2-218076"/> is the ability to perform the same operation on many
        types, as long as each type shares a common subset of characteristics.
        C# custom types exhibit polymorphism by inheriting classes and
        implementing interfaces (see <a class="link" href="ch02s10.html" title="Interfaces">Section 2.10</a> later in this
        chapter).</p><p>In the following example, the <code class="literal">Show</code> method can perform the operation
        <code class="literal">Display</code> on both a <code class="literal">URL</code> and a <code class="literal">LocalFile</code> because both types inherit the
        characteristics of <code class="literal">Location</code>:</p><a id="I_2_tt140"/><pre class="programlisting">class LocalFile : Location {
  public void Execute(  ) {
    Console.WriteLine("Executing "+Name);
  }
  // The constructor for LocalFile, which calls Location's constructor
  public LocalFile(string name) : base(name) {}
}
class Test {
  static void Main(  ) {
    URL u = new URL("http://www.microsoft.com");
    LocalFile l = new LocalFile( "C:\\LOCAL\\README.TXT");
    Show(u);
    Show(l);
  }
  public static void Show(Location loc) {
    Console.Write("Location is: ");
    loc.Display(  );
  }
}</pre></div><div class="sect2" title="Virtual Function Members"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-7.3"/>Virtual Function Members</h2></div></div></div><p>A <a class="indexterm" id="IXT-2-218077"/>key aspect of polymorphism is that each type can
        implement a shared characteristic in its own way. One way to permit
        such flexibility is for a base class to declare function members as
        <code class="literal">virtual</code>. Derived classes can
        provide their own implementations for any function members marked
        <code class="literal">virtual</code> in the base class (see
        <a class="link" href="ch02s10.html" title="Interfaces">Section 2.10</a> later in
        this chapter):</p><a id="I_2_tt141"/><pre class="programlisting">class Location {
  public virtual void Display(  ) {
    Console.WriteLine(Name);
  }
    ...
}
class URL : Location {
  // chop off the http:// at the start
  public override void Display(  ) {
    Console.WriteLine(Name.Substring(7));
  }
  ...
}</pre><p><code class="literal">URL</code> now has a custom way of
        displaying itself. The <code class="literal">Show</code> method
        of the <code class="literal">Test</code> class in the previous
        section will now call the new implementation of <code class="literal">Display</code>. The signatures of the overridden
        method and the virtual method must be identical, but unlike Java and
        C++, the <code class="literal">override</code> keyword is also
        required.</p></div><div class="sect2" title="Abstract Classes and Members"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-7.4"/>Abstract Classes and Members</h2></div></div></div><p>A<a class="indexterm" id="IXT-2-218078"/> <a class="indexterm" id="IXT-2-218079"/> <a class="indexterm" id="IXT-2-218080"/> class can be declared <span class="emphasis"><em>abstract</em></span>. An
        <code class="literal">abstract</code> class may have <code class="literal">abstract</code>members, which are function members
        without implementation that are implicitly virtual. In earlier
        examples, we specified a <code class="literal">Navigate</code>
        method for the <code class="literal">URL</code> type and
        an<code class="literal">Execute</code> method for the <code class="literal">LocalFile</code> type. You can, instead, declare
        <code class="literal">Location</code> an <code class="literal">abstract</code> class with an <code class="literal">abstract</code> method called <code class="literal">Launch</code>:</p><a id="I_2_tt142"/><pre class="programlisting">abstract class Location {
  public abstract void Launch(  );
}
class URL : Location {
  public override void Launch(  ) {
    Console.WriteLine("Run Internet Explorer...");
  }
}
class LocalFile : Location {
  public override void Launch(  ) {
    Console.WriteLine("Run Win32 Program...");
  }
}</pre><p>A derived class must override all its inherited <code class="literal">abstract</code> members or must itself be declared
        <code class="literal">abstract</code>. An <code class="literal">abstract</code> class can’t be instantiated. For
        instance, if <code class="literal">LocalFile</code> doesn’t
        override <code class="literal">Launch</code>, <code class="literal">LocalFile</code> itself must be declared <code class="literal">abstract</code>, perhaps to allow <code class="literal">Shortcut</code> and <code class="literal">PhysicalFile</code> to derive from it.</p></div><div class="sect2" title="Sealed Methods and Sealed Classes"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-7.5"/>Sealed Methods and Sealed Classes</h2></div></div></div><p>An overridden function member may seal its implementation so it
        cannot be overridden. In our previous example, we could have made the
        <code class="literal">URL</code>’s implementation of <code class="literal">Display</code> sealed. This prevents a class that
        derives from <code class="literal">URL</code> from overriding
        <code class="literal">Display</code>, which provides a guarantee
        on the behavior of a <code class="literal">URL</code>.</p><a id="I_2_tt143"/><pre class="programlisting">public sealed override void Display(  ) {...}</pre><p>A <a class="indexterm" id="IXT-2-218081"/> <a class="indexterm" id="IXT-2-218082"/>class can prevent other classes from inheriting from it
        by specifying the <code class="literal">sealed</code> modifier
        in the class declaration:</p><a id="I_2_tt144"/><pre class="programlisting">sealed class Math {
  ...
}</pre><p>The most common scenario for sealing a class is when that class
        comprises only static members, as is the case with the <code class="literal">Math</code> class of the FCL. Another effect of
        sealing a class is that it enables the compiler to seal all virtual
        method invocations made on that class into faster, nonvirtual method
        invocations.</p></div><div class="sect2" title="Hiding Inherited Members"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-7.6"/>Hiding Inherited Members</h2></div></div></div><p>Aside from its use for calling a constructor, the <code class="literal">new</code> keyword can also hide the data members,
        function members, and type members of a base class. Overriding a
        virtual method with the <code class="literal">new</code> keyword
        hides, rather than overrides, the base class implementation of the
        method:</p><a id="I_2_tt145"/><pre class="programlisting">class B {
  public virtual void Foo(  ) {
    Console.WriteLine("In B.");
  }
}
class D : B {
  public override void Foo(  ) {
    Console.WriteLine("In D.");
  }
}
class N : D {
  public new void Foo(  ) {  // hides D's Foo
    Console.WriteLine("In N.");
  }
}
public static void Main(  ) {
  N n = new N(  );
  n.Foo(  ); // calls N's Foo
  ((D)n).Foo(  ); // calls D's Foo
  ((B)n).Foo(  ); // calls D's Foo
}</pre><p>A method declaration with the same signature as its base class
        should explicitly state whether it overrides or hides the inherited
        member.</p></div><div class="sect2" title="Versioning Virtual Function Members"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-7.7"/>Versioning Virtual Function Members</h2></div></div></div><p>In <a class="indexterm" id="IXT-2-218083"/> <a class="indexterm" id="IXT-2-218084"/>C#, a method is compiled with a flag that is <code class="literal">true</code> if the method overrides a <code class="literal">virtual</code> method. This flag is important for
        versioning. Suppose you write a class that derives from a base class
        in the .NET Framework and then deploy your application to a client
        computer. The client later upgrades the .NET Framework, and the .NET
        base class now contains a <code class="literal">virtual</code>
        method that happens to match the signature of one of your methods in
        the derived class:</p><a id="I_2_tt146"/><pre class="programlisting">public class Base { // written by the library people
  public virtual void Foo(  ) {...} // added in latest update
}
public class Derived : Base { // written by you
  public void Foo(  ) {...} // not intended as an override
}</pre><p>In most object-oriented languages, such as Java, methods are not
        compiled with this flag, so a derived class’s method with the same
        signature is <span class="emphasis"><em>assumed</em></span> to override the base class’s
        <code class="literal">virtual</code> method. This means a
        <code class="literal">virtual</code> call is made to type
        <code class="literal">Derived</code>’s <code class="literal">Foo</code> method, even though <code class="literal">Derived</code>’s <code class="literal">Foo</code> is unlikely to have been implemented
        according to the specification intended by the author of type <code class="literal">Base</code>. This can easily break your
        application. In C#, the flag for <code class="literal">Derived</code>’s <code class="literal">Foo</code> will be <code class="literal">false</code>, so the runtime knows to treat
        <code class="literal">Derived</code>’s <code class="literal">Foo</code> as <code class="literal">new</code>, which ensures that your application
        will function as it was originally intended. When you get the chance
        to recompile with the latest framework, you can add the <code class="literal">new</code> modifier to <code class="literal">Foo</code>, or perhaps rename <code class="literal">Foo</code> something <a class="indexterm" id="IXTR3-8"/>else.</p></div></div></body></html>