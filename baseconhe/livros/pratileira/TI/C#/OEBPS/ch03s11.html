<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Custom Attributes</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Custom Attributes"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-3-SECT-11"/>Custom Attributes</h1></div></div></div><p>Types, <a class="indexterm" id="csharpess2-IDXTERM-865"/> <a class="indexterm" id="csharpess2-IDXTERM-866"/> <a class="indexterm" id="csharpess2-IDXTERM-867"/>members, modules, and assemblies all have associated
      metadata that is used by all the major CLR services, is considered an
      indivisible part of an application, and can be accessed via
      reflection(see <a class="link" href="ch03s10.html" title="Reflection">Section
      3.10</a>).</p><p>A key characteristic of metadata is that it can be extended. You
      extend the metadata with <span class="emphasis"><em>custom attributes</em></span>, which
      allow you to “decorate” a code element with additional information
      stored in the metadata associated with the element.</p><p>This additional information can then be retrieved at runtime and
      used to build services that work <span class="emphasis"><em>declaratively</em></span>,
      which is the way that the CLR implements core features such as
      serialization and interception.</p><div class="sect2" title="Language Support for Custom Attributes"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-11.1"/>Language Support for Custom Attributes</h2></div></div></div><p>Decorating <a class="indexterm" id="IXT-3-218731"/>an element with a custom attribute is known as
        <span class="emphasis"><em>specifying</em></span> the custom attribute and is done by
        writing the name of the attribute enclosed in <a class="indexterm" id="IXT-3-218732"/>brackets <span class="bold"><strong>(</strong></span><code class="literal">[]</code>) immediately before the element
        declaration as follows:</p><a id="I_3_tt337"/><pre class="programlisting">[Serializable] public class Foo {...}</pre><p>In this example, the <code class="literal">Foo</code>
        class is specified as serializable. This information is saved in the
        metadata for <code class="literal">Foo</code> and affects the
        way the CLR treats an instance of this class.</p><p>A useful way to think about custom attributes is that they
        expand the built-in set of declarative constructs such as <code class="literal">public</code>, <code class="literal">private</code>, and <code class="literal">sealed</code> in the C# language.</p></div><div class="sect2" title="Compiler Support for Custom Attributes"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-11.2"/>Compiler Support for Custom Attributes</h2></div></div></div><p>In <a class="indexterm" id="IXT-3-218733"/> <a class="indexterm" id="IXT-3-218734"/>reality, custom attributes are simply types derived from
        <code class="literal">System.Attribute</code><a class="indexterm" id="IXT-3-218735"/> with language constructs for specifying them on an
        element (see <a class="link" href="ch02s16.html" title="Attributes">Section
        2.16</a> in <a class="link" href="ch02.html" title="Chapter 2. C# Language Reference">Chapter
        2</a>).</p><p>These language constructs are recognized by the compiler, which
        emits a small chunk of data into the metadata. This custom data
        includes a serialized call to the constructor of the custom attribute
        type (containing the values for the positional parameters) and a
        collection of property set operations (containing the values for the
        named parameters).</p><p>The compiler also recognizes a small number of<a class="indexterm" id="IXT-3-218736"/> <span class="emphasis"><em>pseudo-custom attributes.</em></span> These
        are special attributes that have direct representation in metadata and
        are stored natively (i.e., not as chunks of custom data). This is
        primarily a runtime performance optimization, although it has some
        implications for retrieving attributes via reflection, as discussed
        later.</p><p>To understand this, consider the following class with two
        specified attributes:</p><a id="I_3_tt338"/><pre class="programlisting">[Serializable, Obsolete]
class Foo {...}</pre><p>When compiled, the metadata for the <code class="literal">Foo</code> class looks like this in MSIL:</p><a id="I_3_tt339"/><pre class="programlisting">.class private auto ansi serializable beforefieldinit Foo
       extends [mscorlib]System.Object
{
  .custom instance void 
  [mscorlib]System.ObsoleteAttribute::.ctor(  ) = ( 01 00 00 00 ) 
  ...
}</pre><p>Compare the different treatment by the compiler of the <code class="literal">Obsolete</code><a class="indexterm" id="IXT-3-218737"/> attribute, which is a custom attribute and represented
        by a <code class="literal">.custom</code> directive containing
        the serialized attribute parameters, to the treatment of the<a class="indexterm" id="IXT-3-218738"/> <code class="literal">Serializable</code>
        attribute, which is a pseudo-custom attribute represented directly in
        the metadata with the <code class="literal">serializable</code>
        token.</p></div><div class="sect2" title="Runtime Support for Custom Attributes"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-11.3"/>Runtime Support for Custom Attributes</h2></div></div></div><p>At <a class="indexterm" id="IXT-3-218739"/> <a class="indexterm" id="IXT-3-218740"/>runtime the core CLR services such as serialization and
        remoting inspect the custom and pseudo-custom attributes to determine
        how to handle an instance of a type.</p><p>In the case of custom attributes, this is done by creating an
        instance of the attribute (invoking the relevant constructor call and
        property-set operations), and then performing whatever steps are
        needed to determine how to handle an instance of the type.</p><p>In the case of pseudo-custom attributes, this is done by simply
        inspecting the metadata directly and determining how to handle an
        instance of the type. Consequently, handling pseudo-custom attributes
        is more efficient than handling custom attributes.</p><p>Note that none of these steps is initiated until a service or
        user program actually tries to access the attributes, so there is
        little runtime overhead unless it is required.</p></div><div class="sect2" title="Predefined Attributes"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-11.4"/>Predefined Attributes</h2></div></div></div><p>The<a class="indexterm" id="IXT-3-218741"/> <a class="indexterm" id="IXT-3-218742"/> <a class="indexterm" id="IXT-3-218743"/> .NET Framework makes extensive use of attributes for
        purposes ranging from simple documentation to advanced support for
        threading, remoting, serialization, and COM interop. These attributes
        are all defined in the FCL and can be used, extended, and retrieved by
        your own code.</p><p>However, certain attributes are treated specially by the
        compiler and the runtime. Three attributes considered general enough
        to be defined in the C# specification are <code class="literal">AttributeUsage</code>, <code class="literal">Conditional</code>, and <code class="literal">Obsolete</code>. Other attributes such as <code class="literal">CLSCompliant</code>, <code class="literal">Serializable</code>, and <code class="literal">NonSerialized</code> are also treated
        specially.</p><div class="sect3" title="AttributeUsage attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-11.4.1"/>AttributeUsage attribute</h3></div></div></div><div class="informaltable"><a id="ch03-82-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt340"/><pre class="programlisting">[AttributeUsage(<em class="replaceable"><code>target-enum</code></em>
  [, AllowMultiple=[true|false]]?
  [, Inherited=[true|false]]?
] <em class="lineannotation"><span class="lineannotation">(for classes)</span></em></pre></td></tr></tbody></table></div><p>The<a class="indexterm" id="IXT-3-218744"/> <code class="literal">AttributeUsage</code>
          attribute is applied to a new attribute class declaration. It
          controls how the new attribute should be treated by the compiler,
          specifically, what set of targets (classes, interfaces, properties,
          methods, parameters, etc.) the new attribute can be specified on,
          whether multiple instances of this attribute may be applied to the
          same target, and whether this attribute propagates to subtypes of
          the target.</p><p><em class="replaceable"><code>target-enum</code></em><a class="indexterm" id="IXT-3-218745"/> is a bitwise mask of values from the <code class="literal">System.AttributeTargets</code><a class="indexterm" id="IXT-3-218746"/> enum, which looks like this:</p><a id="I_3_tt341"/><pre class="programlisting">namespace System {
  [Flags]
  public enum AttributeTargets {
    Assembly     = 0x0001,
    Module       = 0x0002,
    Class        = 0x0004,
    Struct       = 0x0008,
    Enum         = 0x0010,
    Constructor  = 0x0020,
    Method       = 0x0040,
    Property     = 0x0080,
    Field        = 0x0100,
    Event        = 0x0200,
    Interface    = 0x0400,
    Parameter    = 0x0800,
    Delegate     = 0x1000,
    ReturnValue  = 0x2000,
    All          = 0x3fff,
  }
}</pre></div><div class="sect3" title="Conditional attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-11.4.2"/>Conditional attribute</h3></div></div></div><div class="informaltable"><a id="ch03-84-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt342"/><pre class="programlisting">[Conditional(<em class="replaceable"><code>symbol</code></em>)] <em class="lineannotation"><span class="lineannotation">(for methods)</span></em></pre></td></tr></tbody></table></div><p>The <code class="literal">Conditional</code>attribute
          can be applied to any method with a <code class="literal">void</code> return type. The presence of this
          attribute tells the compiler to conditionally omit calls to the
          method unless <em class="replaceable"><code>symbol</code></em>is defined in the
          calling code. This is similar to wrapping every call to the method
          with<code class="literal">#if</code>and <code class="literal">#endif</code> preprocessor directives, but
          <code class="literal">Conditional</code> has the advantage of
          needing to be specified only in one place. <code class="literal">Conditional</code> can be found in the <code class="literal">System.Diagnostics</code> namespace.</p></div><div class="sect3" title="Obsolete attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-11.4.3"/>Obsolete attribute</h3></div></div></div><div class="informaltable"><a id="ch03-86-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt343"/><pre class="programlisting">[Obsolete(<em class="replaceable"><code>message</code></em>[, true | false]] <em class="lineannotation"><span class="lineannotation">(for all attribute targets)</span></em></pre></td></tr></tbody></table></div><p>Applied<a class="indexterm" id="IXT-3-218747"/> to any valid attribute target, the <code class="literal">Obsolete</code> attribute indicates that the
          target is obsolete. <code class="literal">Obsolete</code> can
          include a message that explains which alternative types or members
          to use and a flag that tells the compiler to treat the use of this
          type or member as either a warning or an error.</p><p>For example, referencing type <code class="literal">Bar</code> in the following example causes the
          compiler to display an error message and halts compilation:</p><a id="I_3_tt344"/><pre class="programlisting">[Obsolete("Don't try this at home", true)]
class Bar { ... }</pre></div><div class="sect3" title="CLSCompliant attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-11.4.4"/>CLSCompliant attribute</h3></div></div></div><div class="informaltable"><a id="ch03-88-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt345"/><pre class="programlisting">[CLSCompliant(true|false)] <em class="lineannotation"><span class="lineannotation">(for all attribute targets)</span></em></pre></td></tr></tbody></table></div><p>Applied to an assembly, the <code class="literal">CLSCompliant</code><a class="indexterm" id="IXT-3-218748"/> attribute tells the compiler whether to validate CLS
          compliance for all the exported types in the assembly. Applied to
          any other attribute target, this attribute allows the target to
          declare if it should be considered CLS-compliant. In order to mark a
          target as CLS-compliant, the entire assembly needs to be considered
          as such.</p><p>In the following example, the <code class="literal">CLSCompliant</code> attribute is used to specify
          an assembly as CLS-compliant and a class within it as not
          CLS-compliant:</p><a id="I_3_tt346"/><pre class="programlisting">[assembly:CLSCompliant(true)]

[CLSCompliant(false)]
public class Bar { 
  public ushort Answer { get {return 42;} } 
}</pre></div><div class="sect3" title="Serializable attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-11.4.5"/>Serializable attribute</h3></div></div></div><div class="informaltable"><a id="ch03-90-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt347"/><pre class="programlisting">[Serializable] <em class="lineannotation"><span class="lineannotation">(for classes, structs, enums, delegates)</span></em></pre></td></tr></tbody></table></div><p>Applied to a class, struct, enum, or delegate, the <code class="literal">Serializable</code><a class="indexterm" id="IXT-3-218749"/> attribute marks it as being serializable. This
          attribute is a pseudo-custom attribute and is represented specially
          in the metadata.</p></div><div class="sect3" title="NonSerialized attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-11.4.6"/>NonSerialized attribute</h3></div></div></div><div class="informaltable"><a id="ch03-92-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt348"/><pre class="programlisting">[NonSerialized] <em class="lineannotation"><span class="lineannotation">(for fields)</span></em></pre></td></tr></tbody></table></div><p>Applied to a field, the <code class="literal">NonSerialized</code><a class="indexterm" id="IXT-3-218750"/> attribute prevents it from being serialized along
          with its containing class or struct. This attribute is a
          pseudo-custom attribute and is represented specially in the
          metadata.</p></div></div><div class="sect2" title="Defining a New Custom Attribute"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-11.5"/>Defining a New Custom Attribute</h2></div></div></div><p>In <a class="indexterm" id="IXT-3-218751"/>addition to using the predefined attributes supplied by
        the .NET Framework, you can also create your own.</p><p>To create a custom attribute:</p><div class="orderedlist"><ol class="orderedlist"><li class="listitem"><p>Derive a class from <code class="literal">System.Attribute</code><a class="indexterm" id="IXT-3-218752"/> or from a descendent of <code class="literal">System.Attribute</code>. By convention the
            class name should end with the word"Attribute,” although this
            isn’t required.</p></li><li class="listitem"><p>Provide the class with a public constructor. The parameters
            to the constructor define the positional parameters of the
            attribute and are mandatory when specifying the attribute on an
            element.</p></li><li class="listitem"><p>Declare public-instance fields, public-instance read/write
            properties, or public-instance write-only properties to specify
            the named parameters of the attribute. Unlike positional
            parameters, these are optional when specifying the attribute on an
            element.</p><p>The types that can be used for attribute constructor
            parameters and properties are <code class="literal">bool</code>, <code class="literal">byte</code>, <code class="literal">char</code>, <code class="literal">double</code>, <code class="literal">float</code>, <code class="literal">int</code>, <code class="literal">long</code>, <code class="literal">short</code>, <code class="literal">string</code>, <code class="literal">object</code>, the <code class="literal">Type</code> type, <code class="literal">enum</code>, or a one-dimensional array of the
            aforementioned types.</p></li><li class="listitem"><p>Finally, define what the attribute may be specified on using
            the <code class="literal">AttributeUsage</code><a class="indexterm" id="IXT-3-218753"/> attribute, as described in the preceding
            section.</p></li></ol></div><p>Consider the following example of a custom attribute, <code class="literal">CrossRef-Attribute</code><a class="indexterm" id="IXT-3-218754"/>, which removes the limitation that the CLR metadata
        contain information about statically linked types but not dynamically
        linked ones:</p><a id="I_3_tt349"/><pre class="programlisting">// XRef.cs - cross-reference custom attribute
// Compile with: csc /t:library XRef.cs
using System;
[AttributeUsage(AttributeTargets.All, AllowMultiple=true)]
public class CrossRefAttribute : Attribute {
  Type   xref;
  string desc = "";
  public string Description { set { desc=value; } } 
  public CrossRefAttribute(Type xref) { this.xref=xref; }
  public override string ToString(  ) {
    string tmp = (desc.Length&gt;0) ? " ("+desc+")" : "";
    return "CrossRef to "+xref.ToString(  )+tmp;
  }
}</pre><p>From the attribute user’s perspective, this attribute can be
        applied to any target multiple times (note the use of the <code class="literal">AttributeUsage</code> attribute to control this).
        <code class="literal">CrossRefAttribute</code> takes one
        mandatory positional parameter (namely the type to cross-reference)
        and one optional named parameter (the description), and is used as
        follows:</p><a id="I_3_tt350"/><pre class="programlisting">[CrossRef(typeof(Bar), Description="Foos often hang around Bars")]
class Foo {...}</pre><p>Essentially, this attribute embeds cross-references to
        dynamically linked types (with optional descriptions) in the metadata.
        This information can then be retrieved at runtime by a class browser
        to present a more complete view of a type’s dependencies.</p></div><div class="sect2" title="Retrieving a Custom Attribute at Runtime"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-11.6"/>Retrieving a Custom Attribute at Runtime</h2></div></div></div><p>Retrieving attributes<a class="indexterm" id="IXT-3-218755"/> at runtime is done using reflection via one of <code class="literal">System.Attribute</code>’s <code class="literal">GetCustomAttribute</code><a class="indexterm" id="IXT-3-218756"/> or <code class="literal">GetCustomAttributes</code><a class="indexterm" id="IXT-3-218757"/><a class="indexterm" id="IXT-3-218758"/> overloads. This is one of the few circumstances in
        which the difference between customattributes and pseudo-custom
        attributes becomes apparent, since pseudo-custom attributes can’t be
        retrieved with <code class="literal">GetCustomAttribute</code>.</p><p>Here is an example that uses reflection to determine which
        attributes are on a specific type:</p><a id="I_3_tt351"/><pre class="programlisting">using System;
[Serializable, Obsolete]
class Test {
  static void Main(  ) {
    Type t = typeof(Test);
    object[] caarr = Attribute.GetCustomAttributes(t);
    Console.WriteLine("{0} has {1} custom attribute(s)",
                      t, caarr.Length);
    foreach (object ca in caarr)
      Console.WriteLine(ca);
  }
}</pre><p>Although the <code class="literal">Test</code><a class="indexterm" id="IXT-3-218759"/> class of the preceding example has two attributes
        specified, the sample produces the following output:</p><a id="I_3_tt352"/><pre class="programlisting">Test has 1 custom attribute(s)
System.ObsoleteAttribute</pre><p>This demonstrates how the <code class="literal">Serializable</code><a class="indexterm" id="IXT-3-218760"/> attribute (a pseudo-custom attribute) isn’t accessible
        via reflection, while the <code class="literal">Obsolete</code>
        attribute (a custom attribute) still<a class="indexterm" id="IXTR3-56"/> <a class="indexterm" id="IXTR3-57"/> <a class="indexterm" id="IXTR3-58"/> is.</p></div></div></body></html>