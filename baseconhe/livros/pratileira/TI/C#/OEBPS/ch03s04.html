<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Collections</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Collections"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-3-SECT-4"/>Collections</h1></div></div></div><p>Collections<a class="indexterm" id="csharpess2-IDXTERM-669"/> <a class="indexterm" id="csharpess2-IDXTERM-670"/> are standard data structures that supplement arrays, the
      only built-in data structures in C#. This differs from languages such as
      Perl and Python, which incorporate key-value data structures and
      dynamically sized arrays into the language itself.</p><p>The FCL includes a set of types that provide commonly required
      data structures and support for creating your own. These types are
      typically broken down into two categories:
      <span class="emphasis"><em>interfaces</em></span><a class="indexterm" id="IXT-3-218573"/> that define a standardized set of design patterns for
      collection classes in general, and concrete <span class="emphasis"><em>classes</em></span>
      that implement these interfaces and provide a usable range of data
      structures.</p><p>This section introduces all the concrete collection classes and
      abstract collection interfaces and provides examples of their use.
      Unless otherwise stated, the types mentioned in this section all exist
      in the <code class="literal">System.Collections</code>
      namespace.</p><div class="sect2" title="Concrete Collection Classes"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-4.1"/>Concrete Collection Classes</h2></div></div></div><p>The<a class="indexterm" id="IXT-3-218574"/> <a class="indexterm" id="IXT-3-218575"/> <a class="indexterm" id="IXT-3-218576"/> <a class="indexterm" id="IXT-3-218577"/> FCL includes the concrete implementations of the
        collection design patterns that are described in this section.</p><p>Unlike C++, C# doesn’t yet support templates, so these
        implementations work generically by accepting elements of type
        <code class="literal">System.Object</code>.</p><div class="sect3" title="ArrayList class"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-4.1.1"/>ArrayList class</h3></div></div></div><p><code class="literal">ArrayList</code><a class="indexterm" id="IXT-3-218578"/> is a dynamically sized array of objects that
          implements the <code class="literal">ILis</code><a class="indexterm" id="IXT-3-218579"/>t interface (see <a class="link" href="ch03s04.html#csharpess2-CHP-3-SECT-4.2.5" title="IList interface">Section 3.4.2.5</a> later
          in this chapter). An <code class="literal">ArrayList</code>
          works by maintaining an internal array of objects that is replaced
          with a larger array when it reaches its capacity of elements. It is
          very efficient at adding elements (since there is usually a free
          slot at the end) but is inefficient at inserting elements (since all
          elements have to be shifted to make a free slot). Searching can be
          efficient if the <code class="literal">BinarySearch</code>
          method is used, but you must <code class="literal">Sort(           )</code> the <code class="literal">ArrayList</code> first.
          You could use the <code class="literal">Contains( )</code>
          method, but it performs a linear search in <code class="literal">O(</code><em class="replaceable"><code>n</code></em><code class="literal">)</code> time.</p><a id="I_3_tt287"/><pre class="programlisting">ArrayList a = new ArrayList(  );
a.Add("Vernon");
a.Add("Corey");
a.Add("William");
a.Add("Muzz");
a.Sort(  );
for(int i = 0; i &lt; a.Count; i++)
   Console.WriteLine(a [i]);</pre></div><div class="sect3" title="BitArray class"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-4.1.2"/>BitArray class</h3></div></div></div><p>A <code class="literal">BitArray</code><a class="indexterm" id="IXT-3-218580"/> is a dynamically sized array of Boolean values. It is
          more memory-efficient than a simple array of <code class="literal">bool</code>s because it uses only one bit for
          each value, whereas a <code class="literal">bool</code> array
          uses two bytes for each value. Here is an example of its use:</p><a id="I_3_tt288"/><pre class="programlisting">BitArray bits = new BitArray( 0 ); // initialize a zero-length bit array
bits.Length = 2;
bits[1] = true;
bits.Xor(bits); // Xor the array with itself</pre></div><div class="sect3" title="Hashtable class"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-4.1.3"/>Hashtable class</h3></div></div></div><p>A <code class="literal">Hashtable</code><a class="indexterm" id="IXT-3-218581"/> is a standard dictionary (key/value) data structure
          that uses a hashing algorithm to store and index values efficiently.
          This hashing algorithm is performed using the hashcode returned by
          the <code class="literal">GetHashCode</code> method on
          <code class="literal">System.Object</code>. Types used as keys
          in a <code class="literal">Hashtable</code> should therefore
          override <code class="literal">GetHashCode</code> to return a
          good hash of the object’s internal value.</p><a id="I_3_tt289"/><pre class="programlisting">Hashtable ht = new Hashtable(  );
ht["One"] = 1;
ht["Two"] = 2;
ht["Three"] = 3;
Console.WriteLine(ht["Two"]); // Prints "2"</pre><p><code class="literal">Hashtable</code> also implements
          <code class="literal">IDictionary</code> (see <a class="link" href="ch03s04.html#csharpess2-CHP-3-SECT-4.2.6" title="IDictionary interface">Section 3.4.2.6</a> later
          in this chapter), and therefore, can be manipulated as a normal
          dictionary data structure.</p></div><div class="sect3" title="Queue class"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-4.1.4"/>Queue class</h3></div></div></div><p>A <code class="literal">Queue</code><a class="indexterm" id="IXT-3-218582"/> is a standard first-in, first-out (FIFO) data
          structure, providing simple operations to enqueue, dequeue, peek,
          etc. Here is an example:</p><a id="I_3_tt290"/><pre class="programlisting">Queue q = new Queue(  );
q.Enqueue(1);
q.Enqueue(2);
Console.WriteLine(q.Dequeue(  )); // Prints "1"
Console.WriteLine(q.Dequeue(  )); // Prints "2"</pre></div><div class="sect3" title="SortedList class"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-4.1.5"/>SortedList class</h3></div></div></div><p>A <code class="literal">SortedList</code><a class="indexterm" id="IXT-3-218583"/> is a standard dictionary data structure that uses a
          binary-chop search to index efficiently. <code class="literal">SortedList</code> implements <code class="literal">IDictionary</code> (see <a class="link" href="ch03s04.html#csharpess2-CHP-3-SECT-4.2.6" title="IDictionary interface">Section
          3.4.2.6</a>):</p><a id="I_3_tt291"/><pre class="programlisting">SortedList s = new SortedList(  );
s["Zebra"] = 1;
s["Antelope"] = 2;
s["Eland"] = 3;
s["Giraffe"] = 4;
s["Meerkat"] = 5;
s["Dassie"] = 6;
s["Tokoloshe"] = 7;
Console.WriteLine(s["Meerkat"]); // Prints "5" in 3 lookups</pre></div><div class="sect3" title="Stack class"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-4.1.6"/>Stack class</h3></div></div></div><p>A <code class="literal">Stack</code><a class="indexterm" id="IXT-3-218584"/> is a standard <a class="indexterm" id="IXT-3-218585"/> <a class="indexterm" id="IXT-3-218586"/>last-in first-out (LIFO) data structure:</p><a id="I_3_tt292"/><pre class="programlisting">Stack s = new Stack(  );
s.Push(1); // Stack = 1
s.Push(2); // Stack = 1,2
s.Push(3); // Stack = 1,2,3
Console.WriteLine(s.Pop(  )); // Prints 3, Stack=1,2
Console.WriteLine(s.Pop(  )); // Prints 2, Stack=1
Console.WriteLine(s.Pop(  )); // Prints 1, Stack=</pre></div><div class="sect3" title="StringCollection class"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-4.1.7"/>StringCollection class</h3></div></div></div><p>A <code class="literal">StringCollection</code><a class="indexterm" id="IXT-3-218587"/> is a standard collection data structure for storing
          strings. <code class="literal">StringCollection</code> lives
          in the <code class="literal">System.Collections.Specialized</code> namespace
          and implements <code class="literal">ICollection</code>. It
          can be manipulated like a normal collection (see <a class="link" href="ch03s04.html#csharpess2-CHP-3-SECT-4.2.3" title="ICollection interface">Section
          3.4.2.3</a>):</p><a id="I_3_tt293"/><pre class="programlisting">StringCollection sc = new StringCollection(  );
sc.Add("s1");
string[] sarr =  {"s2", "s3", "s4"};
sc.AddRange(sarr);
foreach (string s in sc)
  Console.Write("{0} ", s); // s1 s2 s3 s4</pre></div></div><div class="sect2" title="Collection Interfaces"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-4.2"/>Collection Interfaces</h2></div></div></div><p>The <a class="indexterm" id="csharpess2-IDXTERM-686"/> <a class="indexterm" id="csharpess2-IDXTERM-687"/>collection interfaces provide standard ways to
        enumerate, populate, and author collections. The FCL defines the
        interfaces in this section to support the standard collection design
        patterns.</p><div class="sect3" title="IEnumerable interface"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-4.2.1"/>IEnumerable interface</h3></div></div></div><a class="indexterm" id="IXT-3-218588"/><a id="I_3_tt294"/><pre class="programlisting">public interface IEnumerable {
  IEnumerator GetEnumerator(  );
}</pre><p>The C# <code class="literal">foreach</code><a class="indexterm" id="IXT-3-218589"/> statement works on any collection that implements the
          <code class="literal">IEnumerable</code> interface. The
          <code class="literal">IEnumerable</code> interface has a
          single method that returns an <code class="literal">IEnumerator</code> object.</p></div><div class="sect3" title="IEnumerator interface"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-4.2.2"/>IEnumerator interface</h3></div></div></div><a id="I_3_tt295"/><pre class="programlisting">public interface IEnumerator {
   bool MoveNext(  );
   object Current {get;}
   void Reset(  );
}</pre><p>The <code class="literal">IEnumerator</code><a class="indexterm" id="IXT-3-218590"/> interface provides a standard way to iterate over
          collections. Internally, an <code class="literal">IEnumerator</code> maintains the current position
          of an item in the collection. If the items are numbered (inclusive)
          to <span class="emphasis"><em>n</em></span> (exclusive), the current position starts
          off as -1, and finishes at <span class="emphasis"><em>n</em></span>.</p><p><code class="literal">IEnumerator</code> is typically
          implemented as a nested type and is initialized by passing the
          collection to the constructor of the <code class="literal">IEnumerator</code>:</p><a id="I_3_tt296"/><pre class="programlisting">using System;
using System.Collections;
public class MyCollection : IEnumerable {
  // Contents of this collection.
  private string[] items = {"hello", "world"};
  // Accessors for this collection.
  public string this[int index] { 
    get { return items[index]; }
  }
  public int Count {
    get { return items.Length; }
  }
  // Implement IEnumerable.
  public virtual IEnumerator GetEnumerator (  ) {
    return new MyCollection.Enumerator(this);
  }
  // Define a custom enumerator.
  private class Enumerator : IEnumerator { 
    private MyCollection collection;
    private int currentIndex = -1;
    internal Enumerator (MyCollection collection) {
      this.collection = collection;
    }
    public object Current {
      get {
        if (currentIndex==-1 || currentIndex == collection.Count)
          throw new InvalidOperationException(  );
        return collection [currentIndex];
      }
    }
    public bool MoveNext (  ) {
      if (currentIndex &gt; collection.Count)
        throw new InvalidOperationException(  );
      return ++currentIndex &lt; collection.Count;
    }
    public void Reset (  ) {
      currentIndex = -1;
    }
  }
}</pre><p>The collection can then be enumerated in either of these two
          ways:</p><a id="I_3_tt297"/><pre class="programlisting">MyCollection mcoll = new MyCollection(  );
// Using foreach: substitute your typename for 
string
foreach (
string item in mcoll) {
  Console.WriteLine(item);
}
// Using IEnumerator: substitute your typename for 
string
IEnumerator ie = mcoll.GetEnumerator(  );
while (ie.MoveNext(  )) {
  
string item = (
string) ie.Current;
  Console.WriteLine(item);
}</pre></div><div class="sect3" title="ICollection interface"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-4.2.3"/>ICollection interface</h3></div></div></div><a id="I_3_tt298"/><pre class="programlisting">public interface ICollection : IEnumerable {
   void CopyTo(Array 
array, int 
index);
   int Count {get;}
   bool IsSynchronized {get;}
   object SyncRoot {get;}
}</pre><p><code class="literal">ICollection</code><a class="indexterm" id="IXT-3-218591"/> is the interface implemented by all collections,
          including arrays, and provides the following methods:</p><div class="variablelist"><dl><dt><span class="term"><a class="indexterm" id="IXT-3-218592"/><code class="literal">CopyTo(Array</code>
              <em class="replaceable"><code>array</code></em><code class="literal">,               int</code> <em class="replaceable"><code>index</code></em><code class="literal">)</code></span></dt><dd><p>This method copies all the elements into the array
                starting at the specified index in the source
                collection.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-3-218593"/><code class="literal">Count</code></span></dt><dd><p>This<a class="indexterm" id="IXT-3-218594"/> property returns the number of elements in the
                collection.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-3-218595"/><code class="literal">IsSynchronized(               )</code></span></dt><dd><p>This method allows you to determine whether or not a
                collection is thread-safe. The collections provided in the FCL
                are not themselves thread-safe, but each one includes a
                <code class="literal">Synchronized</code> method that
                returns a thread-safe wrapper of the collection.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-3-218596"/><code class="literal">SyncRoot(               )</code></span></dt><dd><p>This property returns an object (usually the collection
                itself) that can be locked to provide basic thread-safe
                support for the collection.</p></dd></dl></div></div><div class="sect3" title="IComparer interface"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-4.2.4"/>IComparer interface</h3></div></div></div><p><code class="literal">IComparer</code><a class="indexterm" id="IXT-3-218597"/> is a standard interface that compares two objects for
          sorting in <code class="literal">Array</code>s. You generally
          don’t need to implement this interface, since a default
          implementation that uses the <code class="literal">IComparable</code> interface is already provided
          by the <code class="literal">Comparer</code> type, which is
          used by the <code class="literal">Array</code> type.</p><a id="I_3_tt299"/><pre class="programlisting">public interface IComparer {
   int Compare(object x, object y);
}</pre></div><div class="sect3" title="IList interface"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-4.2.5"/>IList interface</h3></div></div></div><a class="indexterm" id="IXT-3-218598"/><p><code class="literal">IList</code> is an interface for
          array-indexable collections, such as <code class="literal">ArrayList</code>.</p><a id="I_3_tt300"/><pre class="programlisting">public interface IList : ICollection, IEnumerable {
   object this [int index] {get; set}
   bool IsFixedSize { get; }
   bool IsReadOnly { get; }
   int Add(object o);
   void Clear(  );
   bool Contains(object value);
   int IndexOf(object value);
   void Insert(int index, object value);
   void Remove(object value);
   void RemoveAt(int index);
}</pre></div><div class="sect3" title="IDictionary interface"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-4.2.6"/>IDictionary interface</h3></div></div></div><p><code class="literal">IDictionary</code><a class="indexterm" id="IXT-3-218599"/> is an interface for key/value-based collections, such
          as <code class="literal">Hashtable</code> and <code class="literal">SortedList</code>.</p><a id="I_3_tt301"/><pre class="programlisting">public interface IDictionary : ICollection, IEnumerable {
   object this [object key] {get; set};
   bool IsFixedSize { get; }
   bool IsReadOnly { get; }
   ICollection Keys {get;}
   ICollection Values {get;}
   void Clear(  );
   bool Contains(object key);
   IDictionaryEnumerator GetEnumerator(  );
   void Remove(object key);
}</pre></div><div class="sect3" title="IDictionaryEnumerator interface"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-4.2.7"/>IDictionaryEnumerator interface</h3></div></div></div><p><code class="literal">IDictionaryEnumerator</code><a class="indexterm" id="IXT-3-218600"/> is a standardized interface that enumerates over the
          contents of a dictionary.</p><a id="I_3_tt302"/><pre class="programlisting">public interface IDictionaryEnumerator : IEnumerator {
   DictionaryEntry Entry {get;}
   object Key {get;}
   object Value {get;}
}</pre></div><div class="sect3" title="IHashCodeProvider interface"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-4.2.8"/>IHashCodeProvider interface</h3></div></div></div><p><code class="literal">IHashCodeProvider</code><a class="indexterm" id="IXT-3-218601"/> is a standard interface used by the <code class="literal">Hashtable</code> collection to hash its objects
          <a class="indexterm" id="IXTR3-37"/> <a class="indexterm" id="IXTR3-38"/>for <a class="indexterm" id="IXTR3-39"/> <a class="indexterm" id="IXTR3-40"/>storage.</p><a id="I_3_tt303"/><pre class="programlisting">public interface IHashCodeProvider {
   int GetHashCode(object o);
}</pre></div></div></div></body></html>