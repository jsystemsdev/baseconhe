<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Regular Expressions</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Regular Expressions"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-3-SECT-5"/>Regular Expressions</h1></div></div></div><p>The <a class="indexterm" id="IXT-3-218602"/> <a class="indexterm" id="IXT-3-218603"/>FCL includes support for performing regular expression
      matching and replacement capabilities. The expressions are based on
      Perl5 <span class="emphasis"><em>regexp</em></span>, including <a class="indexterm" id="IXT-3-218604"/>lazy quantifiers (e.g., <code class="literal">??</code><a class="indexterm" id="IXT-3-218605"/>, <code class="literal">*?</code><a class="indexterm" id="IXT-3-218606"/>, <code class="literal">+?</code><a class="indexterm" id="IXT-3-218607"/>, and <code class="literal">{n,m}?</code>), positive
      and negative lookahead, and conditional evaluation.</p><p>The types mentioned in this section all exist in the <code class="literal">System.Text.RegularExpressions</code><a class="indexterm" id="IXT-3-218608"/> namespace.</p><div class="sect2" title="Regex Class"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-5.1"/>Regex Class</h2></div></div></div><p>The<a class="indexterm" id="IXT-3-218609"/> <code class="literal">Regex</code> class is the
        heart of the FCL regular expression support. Used both as an object
        instance and a static type, the <code class="literal">Regex</code> class represents an immutable,
        compiled instance of a regular expression that can be applied to a
        string via a matching process.</p><p>Internally, the regular expression is stored as either a
        sequence of internal regular expression bytecodes that are interpreted
        at match time or as compiled MSIL opcodes that are JIT-compiled by the
        CLR at runtime. This allows you to make a tradeoff between worsened
        regular expression startup time and memory utilization versus higher
        raw match performance at runtime.</p><p>For more information on the regular expression options,
        supported character escapes, substitution patterns, character sets,
        positioning assertions, quantifiers, grouping constructs,
        backreferences, and alternation, see <a class="link" href="apb.html" title="Appendix B. Regular Expressions">Appendix
        B</a><span class="emphasis"><em>.</em></span></p></div><div class="sect2" title="Match and MatchCollection Classes"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-5.2"/>Match and MatchCollection Classes</h2></div></div></div><p>The <code class="literal">Match</code><a class="indexterm" id="IXT-3-218610"/> <a class="indexterm" id="IXT-3-218611"/> class represents the result of applying a regular
        expression to a string, looking for the first successful match. The
        <code class="literal">MatchCollection</code> class contains a
        collection of <code class="literal">Match</code> instances that
        represent the result of applying a regular expression to a string
        recursively until the first unsuccessful match occurs.</p></div><div class="sect2" title="Group Class"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-5.3"/>Group Class</h2></div></div></div><p>The <code class="literal">Group</code><a class="indexterm" id="IXT-3-218612"/> class represents the results from a single grouping
        expression. From this class, it is possible to drill down to the
        individual subexpression matches with the <code class="literal">Captures</code> property.</p></div><div class="sect2" title="Capture and CaptureCollection Classes"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-5.4"/>Capture and CaptureCollection Classes</h2></div></div></div><p>The <code class="literal">CaptureCollection</code><a class="indexterm" id="IXT-3-218613"/> class contains a collection of <code class="literal">Capture</code><a class="indexterm" id="IXT-3-218614"/> instances, each representing the results of a single
        subexpression match.</p></div><div class="sect2" title="Using Regular Expressions"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-5.5"/>Using Regular Expressions</h2></div></div></div><p>Combining these classes, you can create the following
        example:</p><a id="I_3_tt304"/><pre class="programlisting">/*
 * Sample showing multiple groups
 * and groups with multiple captures
 */
using System;
using System.Text.RegularExpressions;
class Test {
  static void Main(  ) {
    string text = "abracadabra1abracadabra2abracadabra3";
    string pat = @"
      (       # start the first group
        abra  # match the literal 'abra'
        (     # start the second (inner) group
        cad   # match the literal 'cad'
        )?    # end the second (optional) group
      )       # end the first group
     +        # match one or more occurences
     ";
    Console.WriteLine("Original text = [{0}]", text);
    // Create the Regex. IgnorePatternWhitespace permits 
    // whitespace and comments.
    Regex r = new Regex(pat, RegexOptions.IgnorePatternWhitespace);
    int[] gnums = r.GetGroupNumbers(  ); // get the list of group numbers
    Match m = r.Match(text); // get first match
    while (m.Success) {
      Console.WriteLine("Match found:");
      // start at group 1
      for (int i = 1; i &lt; gnums.Length; i++) {
        Group g = m.Groups[gnums[i]]; // get the group for this match
        Console.WriteLine("\tGroup{0}=[{1}]", gnums[i], g);
        CaptureCollection cc = g.Captures; // get caps for this group
        for (int j = 0; j &lt; cc.Count; j++) {
          Capture c = cc[j];
          Console.WriteLine("\t\tCapture{0}=[{1}] Index={2} Length={3}",
                            j, c, c.Index, c.Length);
        }
      }
      m = m.NextMatch(  ); // get next match
    } // end while
  }
}</pre><p>The preceding example produces the following output:</p><a id="I_3_tt305"/><pre class="programlisting">Original text = [abracadabra1abracadabra2abracadabra3]
Match found:
        Group1=[abra]
                Capture0=[abracad] Index=0 Length=7
                Capture1=[abra] Index=7 Length=4
        Group2=[cad]
                Capture0=[cad] Index=4 Length=3
Match found:
        Group1=[abra]
                Capture0=[abracad] Index=12 Length=7
                Capture1=[abra] Index=19 Length=4
        Group2=[cad]
                Capture0=[cad] Index=16 Length=3
Match found:
        Group1=[abra]
                Capture0=[abracad] Index=24 Length=7
                Capture1=[abra] Index=31 Length=4
        Group2=[cad]
                Capture0=[cad] Index=28 Length=3</pre></div></div></body></html>