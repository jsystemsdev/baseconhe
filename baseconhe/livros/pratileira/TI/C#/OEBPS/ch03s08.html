<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Threading</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Threading"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-3-SECT-8"/>Threading</h1></div></div></div><p>A <a class="indexterm" id="csharpess2-IDXTERM-788"/> <a class="indexterm" id="csharpess2-IDXTERM-789"/>C# application runs in one or more
      <span class="emphasis"><em>threads</em></span> that effectively execute in parallel within
      the same application. Here is a simple multithreaded application:</p><a id="I_3_tt314"/><pre class="programlisting">using System;
using System.Threading;
class ThreadTest {
  static void Main(  ) {
    Thread t = new Thread(new ThreadStart(Go));
    t.Start(  );
    Go(  );
  }
  static void Go(  ) {
    for (char c='a'; c&lt;='z'; c++ )
      Console.Write(c);
  }
}</pre><p>In this example, a new thread object is constructed by passing it
      a <code class="literal">ThreadStart</code> delegate that wraps the
      method that specifies where to start execution for that thread. You then
      start the thread and call <code class="literal">Go</code>, so two
      separate threads are running <code class="literal">Go</code> in
      parallel. However, there’s a problem: both threads share a common
      resource—the console. If you run <code class="literal">ThreadTest</code>, you could get output like
      this:</p><a id="I_3_tt315"/><pre class="programlisting">abcdabcdefghijklmnopqrsefghijklmnopqrstuvwxyztuvwxyz</pre><div class="sect2" title="Thread Synchronization"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-8.1"/>Thread Synchronization</h2></div></div></div><p>Thread<a class="indexterm" id="csharpess2-IDXTERM-790"/> <a class="indexterm" id="csharpess2-IDXTERM-791"/> synchronization comprises techniques for ensuring that
        multiple threads coordinate their access to shared resources.</p><div class="sect3" title="The lock statement"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-8.1.1"/>The lock statement</h3></div></div></div><p>C# provides the <code class="literal">lock</code><a class="indexterm" id="IXT-3-218672"/> statement to ensure that only one thread at a time
          can access a block of code. Consider the following example:</p><a id="I_3_tt316"/><pre class="programlisting">using System;
using System.Threading;
class LockTest {
  static void Main(  ) {
    LockTest lt = new LockTest (  );
    Thread t = new Thread(new ThreadStart(lt.Go));
    t.Start(  );
    lt.Go(  );
  }
  void Go(  ) {
    lock(this)
      for ( char c='a'; c&lt;='z'; c++)
        Console.Write(c);
  }
}</pre><p>Running <code class="literal">LockTest</code> produces
          the following output:</p><a id="I_3_tt317"/><pre class="programlisting">abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz</pre><p>The <code class="literal">lock</code> statement acquires
          a lock on any reference-type instance. If another thread has already
          acquired the lock, the thread doesn’t continue until the other
          thread relinquishes its lock on that instance.</p><p>The <code class="literal">lock</code> statement is
          actually a syntactic shortcut for calling the <code class="literal">Enter</code><a class="indexterm" id="IXT-3-218673"/> <a class="indexterm" id="IXT-3-218674"/> and <code class="literal">Exit</code> methods
          of the<a class="indexterm" id="IXT-3-218675"/> FCL <code class="literal">Monitor</code> class
          (see <a class="link" href="ch03s08.html#csharpess2-CHP-3-SECT-8.3" title="Monitor Class">Section
          3.8.3</a>):</p><a id="I_3_tt318"/><pre class="programlisting">System.Threading.Monitor.Enter(expression);
try {
  ...
}
finally {
  System.Threading.Monitor.Exit(expression);
}</pre></div><div class="sect3" title="Pulse and Wait operations"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-8.1.2"/>Pulse and Wait operations</h3></div></div></div><p>In <a class="indexterm" id="IXT-3-218676"/> <a class="indexterm" id="IXT-3-218677"/>combination with locks, the next most common threading
          operations are <code class="literal">Pulse</code> and <code class="literal">Wait</code>. These operations let threads
          communicate with each other via a monitor that maintains a list of
          threads waiting to grab an object’s lock:</p><a id="I_3_tt319"/><pre class="programlisting">using System;
using System.Threading;
class MonitorTest {
  static void Main(  ) {
    MonitorTest mt = new MonitorTest(  );
    Thread t = new Thread(new ThreadStart(mt.Go));
    t.Start(  );
    mt.Go(  );
  }
  void Go(  ) {
    for ( char c='a'; c&lt;='z'; c++)
      lock(this) {
        Console.Write(c);
        Monitor.Pulse(this);
        Monitor.Wait(this);
      }
  }
}</pre><p>Running <code class="literal">MonitorTest</code>
          produces the following result:</p><a id="I_3_tt320"/><pre class="programlisting">aabbccddeeffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxyyzz</pre><p>The <code class="literal">Pulse</code> method tells the
          monitor to wake up the next thread that is waiting to get a lock on
          that object as soon as the current thread has released it. The
          current thread typically releases the monitor in one of two ways.
          First, execution may leave the scope of the <code class="literal">lock</code> statement block. The second way is to
          call the <code class="literal">Wait</code> method, which
          temporarily releases the lock on an object and makes the thread fall
          asleep until another thread wakes it up by pulsing the
          object.</p></div><div class="sect3" title="Deadlocks"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-8.1.3"/>Deadlocks</h3></div></div></div><p>The<a class="indexterm" id="IXT-3-218678"/> <code class="literal">MonitorTest</code>
          example actually contains a bug. When you run the program, it prints
          the correct output, but then the console window locks up. This
          occurs because the last thread printing <code class="literal">z</code> goes to sleep but never gets pulsed. You
          can solve the problem by replacing the <code class="literal">Go</code> method with this new
          implementation:</p><a id="I_3_tt321"/><pre class="programlisting">void Go(  ) {
  for ( char c='a'; c&lt;='z'; c++)
    lock(this) {
      Console.Write(c);
      Monitor.Pulse(this);
      if (c&lt;'z')
        Monitor.Wait(this);
    }
}</pre><p>In general, the danger of using locks is that two threads may
          both end up being blocked waiting for a resource held by the other
          thread. This situation is known as a deadlock. Most common deadlock
          situations can be avoided by ensuring that you always acquire
          resources in the same order.</p></div><div class="sect3" title="Atomic operations"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-8.1.4"/>Atomic operations</h3></div></div></div><p><span class="emphasis"><em>Atomic operations</em></span><a class="indexterm" id="IXT-3-218679"/> are operations the system promises will not be
          interrupted. In the previous examples, the <code class="literal">Go</code> method isn’t atomic because it can be
          interrupted while it is running so another thread can run. However,
          updating a variable is atomic because the operation is guaranteed to
          complete without control being passed to another thread. The
          <code class="literal">Interlocked</code> class provides
          additional atomic operations, which allows basic operations to be
          performed without requiring a lock. This can be useful, since
          acquiring a lock is many times slower than a simple <a class="indexterm" id="IXTR3-47"/> <a class="indexterm" id="IXTR3-48"/>atomic
          operation.</p></div></div><div class="sect2" title="Common Thread Types"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-8.2"/>Common Thread Types</h2></div></div></div><p>Much <a class="indexterm" id="IXT-3-218680"/>of the functionality of threads is provided through the
        classes in the <code class="literal">System.Threading</code>
        namespace. The most basic thread class to understand is the <code class="literal">Monitor</code> class, which is explained in the
        following section.</p></div><div class="sect2" title="Monitor Class"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-8.3"/>Monitor Class</h2></div></div></div><p>The <code class="literal">System.Threading.Monitor</code><a class="indexterm" id="IXT-3-218681"/> class provides an<a class="indexterm" id="IXT-3-218682"/> <a class="indexterm" id="IXT-3-218683"/> implementation of Hoare’s Monitor that allows you to
        use any reference-type instance as a monitor.</p><div class="sect3" title="Enter and Exit methods"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-8.3.1"/>Enter and Exit methods</h3></div></div></div><p>The <code class="literal">Enter</code><a class="indexterm" id="IXT-3-218684"/> and <code class="literal">Exit</code><a class="indexterm" id="IXT-3-218685"/> methods, respectively, obtain and release a lock on
          an object. If the object is already held by another thread, <code class="literal">Enter</code> waits until the lock is released or
          the thread is interrupted by a <code class="literal">ThreadInterruptedException</code><a class="indexterm" id="IXT-3-218686"/>. Every call to <code class="literal">Enter</code> for a given object on a thread
          should be matched with a call to <code class="literal">Exit</code> for the same object on the same
          thread.</p></div><div class="sect3" title="TryEnter methods"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-8.3.2"/>TryEnter methods</h3></div></div></div><p>The <code class="literal">TryEnter</code><a class="indexterm" id="IXT-3-218687"/> methodsare similar to the <code class="literal">Enter</code> method but don’t require a lock on
          the object to proceed. These methods return <code class="literal">true</code> if the lock is obtained and <code class="literal">false</code> if it isn’t, optionally passing in a
          timeout parameter that specifies the maximum time to wait for the
          other threads to relinquish the lock.</p></div><div class="sect3" title="Wait methods"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-8.3.3"/>Wait methods</h3></div></div></div><p>The <a class="indexterm" id="IXT-3-218688"/>thread holding a lock on an object may call one of the
          <code class="literal">Wait</code> methods to temporarily
          release the lock and block itself while it waits for another thread
          to notify it by executing a pulse on the monitor. This approach can
          tell a worker thread that there is work to perform on that object.
          The overloaded versions of <code class="literal">Wait</code>
          allow you to specify a timeout that reactivates the thread if a
          pulse hasn’t arrived within the specified duration. When the thread
          wakes up, it reacquires the monitor for the object (potentially
          blocking until the monitor becomes available). <code class="literal">Wait</code> returns <code class="literal">true</code> if the thread is reactivated by
          another thread pulsing the monitor and returns <code class="literal">false</code> if the <code class="literal">Wait</code> call times out without receiving a
          pulse.</p></div><div class="sect3" title="Pulse and PulseAll methods"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-8.3.4"/>Pulse and PulseAll methods</h3></div></div></div><p>A <a class="indexterm" id="IXT-3-218689"/> <a class="indexterm" id="IXT-3-218690"/>thread holding a lock on an object may call <code class="literal">Pulse</code> on that object to wake up a blocked
          thread as soon as the thread calling <code class="literal">Pulse</code> has released its lock on the
          monitor. If multiple threads are waiting on the same monitor,
          <code class="literal">Pulse</code> activates only the first in
          the queue (successive calls to <code class="literal">Pulse</code> wake up other waiting threads, one
          per call). The <code class="literal">PulseAll</code> method
          successively wakes up all the <a class="indexterm" id="IXTR3-49"/> <a class="indexterm" id="IXTR3-50"/>threads.</p></div></div></div></body></html>