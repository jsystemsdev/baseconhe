<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Input/Output</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Input/Output"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-3-SECT-6"/>Input/Output</h1></div></div></div><p>The <a class="indexterm" id="csharpess2-IDXTERM-719"/> <a class="indexterm" id="csharpess2-IDXTERM-720"/> <a class="indexterm" id="csharpess2-IDXTERM-721"/>FCL provides a streams-based I/O framework that can handle
      a wide range of stream and backing store types. This support for streams
      also infuses the rest of the FCL, with the pattern repeating in non-I/O
      areas such as cryptography, HTTP support, and more.</p><p>This section describes the core stream types and provides
      examples. The types mentioned in this section all exist in the <code class="literal">System.IO</code> namespace.</p><div class="sect2" title="Streams and Backing Stores"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-6.1"/>Streams and Backing Stores</h2></div></div></div><p>A <span class="emphasis"><em>stream</em></span><a class="indexterm" id="IXT-3-218615"/> represents the flow of data coming in and out of a
        backing store. A <span class="emphasis"><em>backing store</em></span><a class="indexterm" id="IXT-3-218616"/> represents the endpoint of a stream. Although a backing
        store is often a file or network connection, in reality it can
        represent any medium capable of reading or writing raw data.</p><p>A simple example would be to use a stream to read and write to a
        file on disk. However, streams and backing stores are not limited to
        disk and network I/O. A more sophisticated example would be to use the
        cryptography support in the FCL to encrypt or decrypt a stream of
        bytes as they move around in memory.</p><div class="sect3" title="Abstract Stream class"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-6.1.1"/>Abstract Stream class</h3></div></div></div><p><code class="literal">Stream</code> is an<a class="indexterm" id="IXT-3-218617"/> abstract class that defines operations for reading
          and writing a stream of raw, typeless data as bytes. Once a stream
          has been opened, it stays open and can be read from or written to
          until the stream is flushed and closed. <a class="indexterm" id="IXT-3-218618"/>Flushing a stream updates the writes made to the
          stream; closing a stream first flushes the stream, then closes the
          stream.</p><p><code class="literal">Stream</code> has the properties
          <code class="literal">CanRead</code><a class="indexterm" id="IXT-3-218619"/>, <code class="literal">CanWrite</code><a class="indexterm" id="IXT-3-218620"/>, <a class="indexterm" id="IXT-3-218621"/><code class="literal">Length</code>, <code class="literal">CanSeek</code><a class="indexterm" id="IXT-3-218622"/>, and <a class="indexterm" id="IXT-3-218623"/><code class="literal">Position</code>. <code class="literal">CanSeek</code> is <code class="literal">true</code> if the stream supports random access
          and <code class="literal">false</code> if it only supports
          sequential access. If a stream supports random access, set
          the<a class="indexterm" id="IXT-3-218624"/> <code class="literal">Position</code> property
          to move to a linear position on that stream.</p><p>The <code class="literal">Stream</code><a class="indexterm" id="IXT-3-218625"/> class provides synchronous and asynchronous read and
          write operations. By default, an asynchronous method calls the
          stream’s corresponding synchronous method by wrapping the
          synchronous method in a delegate type and starting a new thread.
          Similarly, by default, a synchronous method calls the stream’s
          corresponding asynchronous method and waits until the thread has
          completed its operation. Classes that derive from <code class="literal">Stream</code> must override either the
          synchronous or asynchronous methods but may override both sets of
          methods if the need arises.</p></div><div class="sect3" title="Concrete Stream-derived classes"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-6.1.2"/>Concrete Stream-derived classes</h3></div></div></div><p>The <a class="indexterm" id="IXT-3-218626"/>FCL includes a number of different concrete
          implementations of the abstract base class <code class="literal">Stream</code>. Each implementation represents a
          different storage medium and allows a raw stream of bytes to be read
          from and written to the backing store.</p><p>Examples of this include the <code class="literal">FileStream</code><a class="indexterm" id="IXT-3-218627"/> class (which reads and writes bytes to and from a
          file) and the <code class="literal">System.Net.Sockets.NetworkStream</code><a class="indexterm" id="IXT-3-218628"/> class (which sends and receives bytes over the
          network).</p><p>In addition, a stream may act as the frontend to another
          stream, performing additional processing on the underlying stream as
          needed. Examples of this include stream encryption/decryption and
          stream buffering.</p><p>Here is an example that creates a text file on disk and uses
          the abstract <code class="literal">Stream</code> type to write
          data to it:</p><a id="I_3_tt306"/><pre class="programlisting">using System.IO;
class Test {
  static void Main(  ) {
    Stream s = new FileStream("foo.txt", FileMode.Create);
    s.WriteByte(67);
    s.WriteByte(35);
    s.Close(  );
  }
}</pre></div><div class="sect3" title="Encapsulating raw streams"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-6.1.3"/>Encapsulating raw streams</h3></div></div></div><p>The<a class="indexterm" id="IXT-3-218629"/> <code class="literal">Stream</code> class
          defines operations for reading and writing raw, typeless data in the
          form of bytes. Typically, however, you need to work with a stream of
          characters, not a stream of bytes. To solve this problem, the FCL
          provides the abstract base classes <code class="literal">TextReader</code> and <code class="literal">TextWriter</code>, which define a contract to
          read and write a stream of characters, as well as a set of concrete
          implementations.</p></div><div class="sect3" title="Abstract TextReader/TextWriter classes"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-6.1.4"/>Abstract TextReader/TextWriter classes</h3></div></div></div><p><code class="literal">TextReader</code><a class="indexterm" id="IXT-3-218630"/> <a class="indexterm" id="IXT-3-218631"/> <a class="indexterm" id="IXT-3-218632"/> and <code class="literal">TextWriter</code> are
          abstract base classes that define operations for reading and writing
          a stream of characters. The most fundamental operations of the
          <code class="literal">TextReader</code> and <code class="literal">TextWriter</code> classes are the methods that
          read and write a single character to or from a stream.</p><p>The <code class="literal">TextReader</code> class
          provides default implementations for methods that read in an array
          of characters or a string representing a line of characters. The
          <code class="literal">TextWriter</code> class provides default
          implementations for methods that write an array of characters, as
          well as methods that convert common types (optionally with
          formatting options) to a sequence of characters.</p><p>The FCL includes a number of different concrete
          implementations of the abstract base classes <code class="literal">TextReader</code> and <code class="literal">TextWriter</code>. Some of the most prominent
          include <code class="literal">StreamReader</code> and <code class="literal">StreamWriter</code>, and <code class="literal">StringReader</code> and <code class="literal">StringWriter</code>.</p></div><div class="sect3" title="StreamReader and StreamWriter classes"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-6.1.5"/>StreamReader and StreamWriter classes</h3></div></div></div><p><code class="literal">StreamReader</code><a class="indexterm" id="IXT-3-218633"/> <a class="indexterm" id="IXT-3-218634"/> <a class="indexterm" id="IXT-3-218635"/> and <code class="literal">StreamWriter</code>
          are concrete classes that derive from <code class="literal">TextReader</code> and <code class="literal">TextWriter</code>, respectively, and operate on a
          <code class="literal">Stream</code> (passed as a constructor
          parameter).</p><p>These classes allow you to combine a <code class="literal">Stream</code> (which can have a backing store but
          only knows about raw data) with a <code class="literal">TextReader</code>/<code class="literal">TextWriter</code> (which knows about character
          data, but doesn’t have a backing store).</p><p>In addition, <code class="literal">StreamReader</code>
          and <code class="literal">StreamWriter</code> can perform
          special translations between characters and raw bytes. Such
          translations include translating Unicode characters to ANSI
          characters to either big- or little-endian format.</p><p>Here is an example that uses a <code class="literal">StreamWriter</code> wrapped around a <code class="literal">FileStream</code> class to write to a
          file:</p><a id="I_3_tt307"/><pre class="programlisting">using System.Text;
using System.IO;
class Test {
  static void Main(  ) {
    Stream fs = new FileStream ("foo.txt", FileMode.Create);
    StreamWriter sw = new StreamWriter(fs, Encoding.ASCII);
    sw.Write("Hello!");
    sw.Close(  );
  }
}</pre></div><div class="sect3" title="StringReader and StringWriter classes"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-6.1.6"/>StringReader and StringWriter classes</h3></div></div></div><p><code class="literal">StringReader</code><a class="indexterm" id="IXT-3-218636"/> <a class="indexterm" id="IXT-3-218637"/> <a class="indexterm" id="IXT-3-218638"/> and <code class="literal">StringWriter</code>
          are concrete classes that derive from <code class="literal">TextReader</code> and <code class="literal">TextWriter</code>, respectively, and operate on a
          string (passed as a constructor parameter).</p><p>The <code class="literal">StringReader</code> class can
          be thought of as the simplest possible read-only backing store
          because it simply performs read operations on that string. The
          <code class="literal">StringWriter</code> class can be thought
          of as the simplest possible write-only backing store because it
          simply performs write operations on that <code class="literal">StringBuilder</code>.</p><p>Here is an example that uses a <code class="literal">StringWriter</code> wrapped around an underlying
          <code class="literal">StringBuilder</code> backing store to
          write to a string:</p><a id="I_3_tt308"/><pre class="programlisting">using System;
using System.IO;
using System.Text;
class Test {
  static void Main(  ) {
    StringBuilder sb = new StringBuilder(  );
    StringWriter sw = new StringWriter(sb);
    WriteHello(sw);
    Console.WriteLine(sb);
  }
  static void WriteHello(TextWriter tw) {
    tw.Write("Hello, String I/O!");
  }
}</pre></div></div><div class="sect2" title="Directories and Files"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-6.2"/>Directories and Files</h2></div></div></div><p>The <code class="literal">File</code><a class="indexterm" id="IXT-3-218639"/> <a class="indexterm" id="IXT-3-218640"/> <a class="indexterm" id="IXT-3-218641"/> and <code class="literal">Directory</code>
        classes encapsulate the operations typically associated with file I/O,
        such as copying, moving, deleting, renaming, and enumerating files and
        directories.</p><p>The actual manipulation of the contents of a file is done with a
        <code class="literal">FileStream</code>. The <code class="literal">File</code> class has methods that return a
        <code class="literal">FileStream</code>, though you may directly
        instantiate a <code class="literal">FileStream</code>.</p><p>In this example, you read in and print out the first line of a
        text file specified on the command <a class="indexterm" id="IXTR3-41"/> <a class="indexterm" id="IXTR3-42"/> <a class="indexterm" id="IXTR3-43"/>line:</p><a id="I_3_tt309"/><pre class="programlisting">using System;
using System.IO;
class Test {
   static void Main(string[] args) {
      Stream s = File.OpenRead(args[0]);
      StreamReader sr = new StreamReader(s);
      Console.WriteLine(sr.ReadLine(  ));
      sr.Close(  );
   }
}</pre></div></div></body></html>