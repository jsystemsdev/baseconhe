<html xmlns="http://www.w3.org/1999/xhtml"><head><title>try Statements and Exceptions</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="try Statements and Exceptions"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-2-SECT-15"/>try Statements and Exceptions</h1></div></div></div><div class="informaltable"><a id="ch02-175-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt225"/><pre class="programlisting">try <em class="replaceable"><code>statement-block</code></em>
[catch (<em class="replaceable"><code>exception type value</code></em>?)? <em class="replaceable"><code>statement-block</code></em>]+ |
finally <em class="replaceable"><code>statement-block</code></em> |
[catch (<em class="replaceable"><code>exception type value</code></em>?)? <em class="replaceable"><code>statement-block</code></em>]+
finally <em class="replaceable"><code>statement-block</code></em></pre></td></tr></tbody></table></div><div class="sect2" title="try Statement"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-15.1"/>try Statement</h2></div></div></div><p>The <a class="indexterm" id="csharpess2-IDXTERM-479"/> <a class="indexterm" id="csharpess2-IDXTERM-480"/>purpose of a <code class="literal">try</code>
        statement is to simplify dealing with program execution in exceptional
        circumstances. A <code class="literal">try</code> statement does
        two things. First, it lets exceptions thrown during the <code class="literal">try</code> block’s execution be caught by the
        <code class="literal">catch</code><a class="indexterm" id="IXT-2-218261"/> block. Second, it ensures that execution can’t leave
        the <code class="literal">try</code> block without first
        executing the <code class="literal">finally</code><a class="indexterm" id="IXT-2-218262"/> block. A <code class="literal">try</code> block
        must be followed by one or more <code class="literal">catch</code> blocks, a <code class="literal">finally</code> block, or both.</p></div><div class="sect2" title="Exceptions"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-15.2"/>Exceptions</h2></div></div></div><p>C# <span class="emphasis"><em>exceptions</em></span> are objects that contain
        information representing the occurrence of an exceptional program
        state. When an exceptional state has occurred (e.g., a method receives
        an illegal value), an exception object may be thrown, and the call
        stack is unwound until the exception is caught by an exception
        handling block.</p><p>In the following example, we have an <code class="literal">Account</code> class. An exceptional state for the
        account class is when its balance is below zero, which occurs when the
        <code class="literal">Withdraw</code> method receives a withdraw
        amount larger than the current balance. Our test class makes our
        customer <code class="literal">tiffanyTaylor</code> perform
        several actions that involve despositing and withdrawing from an
        account. When she attempts to withdraw more money than she has in her
        account, a <code class="literal">FundException</code> is thrown,
        which we will catch so we can notify the user and display her current
        account balance.</p><a id="I_2_tt226"/><pre class="programlisting">using System;

public class FundException : Exception {
  public FundException(decimal balance) :
    base("Total funds are "+balance+" dollars") {}
}

class Account {
  decimal balance;
  public decimal Balance {
    get {return balance;}
  }
  public void Deposit(decimal amount) {
    balance += amount;
  }
  public void Withdraw(decimal amount) {
    if (amount &gt; balance)
      throw new FundException(balance);
    balance -= amount;
  }
}

class Customer {
  Account savingsAccount = new Account(  );
  public void SellBike(  ) {
    savingsAccount.Deposit (1000);
  }
  public void BuyCar(  ) {
    savingsAccount.Withdraw (30000);
  }
  public void GoToMovie(  ) {
    savingsAccount.Withdraw (20);
  }
}

class Test {
  static void Main(  ) {
    Customer tiffanyTaylor = new Customer(  );
    bool todaysTasksDone = false;
    try {
      tiffanyTaylor.SellBike(  );
      tiffanyTaylor.BuyCar(  );
      tiffanyTaylor.GoToMovie(  );
      todaysTasksDone = true;
    }
    catch (FundException ex) {
      Console.WriteLine(ex.Message);
    }
    finally {
      Console.WriteLine(todaysTasksDone);
    }
  }
}</pre><p>At the point when the <code class="literal">FundException</code> is thrown, the call stack is
        comprised of three methods: <code class="literal">Withdraw</code>, <code class="literal">BuyCar</code>, and <code class="literal">Main</code>. Each of these methods will in
        succession return immediately to its calling method until one of them
        has a <code class="literal">catch</code> block that can handle
        the exception, in this case <code class="literal">Main</code>.
        Without a <code class="literal">try</code> statement, the call
        stack would have been unwound completely, and our program would have
        terminated without displaying what caused the problem and without
        letting the user know whether today’s tasks were completed.</p></div><div class="sect2" title="catch"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-15.3"/>catch</h2></div></div></div><p>A <code class="literal">catch</code><a class="indexterm" id="IXT-2-218263"/> <a class="indexterm" id="IXT-2-218264"/> clause specifies which exception type (including
        derived types) to catch. An exception must be of type <code class="literal">System.Exception</code><a class="indexterm" id="IXT-2-218265"/> or of a type that derives from <code class="literal">System.Exception</code>. Catching <code class="literal">System.Exception</code> provides the widest
        possible net for catching errors, which is useful if your handling of
        the error is totally generic, such as with an error-logging mechanism.
        Otherwise, you should catch a more specific exception type to prevent
        your <code class="literal">catch</code> block from dealing with
        a circumstance it wasn’t designed to handle (for example, an
        out-of-memory exception).</p><div class="sect3" title="Omitting the exception variable"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-15.3.1"/>Omitting the exception variable</h3></div></div></div><p>Specifying <a class="indexterm" id="IXT-2-218266"/>only an exception type without a variable name allows
          an exception to be caught when we don’t need to use the exception
          instance, and merely knowing its type will suffice. The previous
          example could have been written like this:</p><a id="I_2_tt227"/><pre class="programlisting">catch(FundException) { // don't specify variable
  Console.WriteLine("Problem with funds");
}</pre></div><div class="sect3" title="Omitting the catch expression"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-15.3.2"/>Omitting the catch expression</h3></div></div></div><p>You may also completely omit the <code class="literal">catch</code> expression. This will catch an
          exception of any type, even types that are not derived from <code class="literal">System.Exception</code> (these could be thrown by
          non-CLS-compliant languages). The previous example could have been
          written like this:</p><a id="I_2_tt228"/><pre class="programlisting">catch {
  Console.WriteLine("Something went wrong...");
}</pre><p>The fact that most exceptions do inherit from <code class="literal">System.Exception</code> is a CLS convention, not
          a CLR requirement.</p></div><div class="sect3" title="Specifying multiple catch clauses"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-15.3.3"/>Specifying multiple catch clauses</h3></div></div></div><p>When declaring multiple <code class="literal">catch</code> clauses, only the first <code class="literal">catch</code> clause with an exception type that
          matches the thrown exception executes its <code class="literal">catch</code> block. It is illegal for an
          exception type B to precede an exception type D if B is a base class
          of D, since it would be unreachable.</p><a id="I_2_tt229"/><pre class="programlisting">try {...}
catch (NullReferenceException) {...}
catch (IOException) {...}
catch {...}</pre></div></div><div class="sect2" title="finally"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-15.4"/>finally</h2></div></div></div><p><code class="literal">finally</code><a class="indexterm" id="IXT-2-218267"/> blocks are always executed when control leaves the
        <code class="literal">try</code> block. A <code class="literal">finally</code> block is executed at one of the
        following times:</p><div class="itemizedlist"><ul class="itemizedlist"><li class="listitem"><p>Immediately after the <code class="literal">try</code>
            block completes</p></li><li class="listitem"><p>Immediately after the <code class="literal">try</code>
            block prematurely exits with a <code class="literal">jump</code> statement (e.g., <code class="literal">return</code>, <code class="literal">goto</code>) and immediately before the target
            of the <code class="literal">jump</code> statement</p></li><li class="listitem"><p>Immediately after a <code class="literal">catch</code>
            block executes</p></li></ul></div><p><code class="literal">finally</code> blocks add
        determinism to a program’s execution by ensuring that particular code
        always gets executed.</p><p>In our main example, if some other exception occurred such as a
        memory exception, the <code class="literal">finally</code> block
        would have still been executed. This ensures that the user would know
        whether today’s tasks were completed. The <code class="literal">finally</code> block can also be used to gracefully
        restore program state when an exception occurs. Had our example used a
        connection to a remote account object, it would have been appropriate
        to close the account in the <code class="literal">finally</code>
        block.</p></div><div class="sect2" title="Key Properties of the System.Exception Class"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-15.5"/>Key Properties of the System.Exception Class</h2></div></div></div><p>You will most frequently use the following properties of
        <code class="literal">System.Exception</code><a class="indexterm" id="IXT-2-218268"/>:</p><div class="variablelist"><dl><dt><span class="term"><a class="indexterm" id="IXT-2-218269"/><code class="literal">StackTrace</code></span></dt><dd><p>This is a string representing all the methods called from
              the origin of the exception to the <code class="literal">catch</code> block.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-2-218270"/><code class="literal">Message</code></span></dt><dd><p>This is a string with a description of the error.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-2-218271"/><code class="literal">InnerException</code></span></dt><dd><p>Sometimes it is useful to catch an exception, then throw a
              new, more specific exception. For instance, you can catch an
              <code class="literal">IOException</code>, and then throw a
              <code class="literal">DocumentLoadException</code> that
              contains more specific information on what went wrong. In this
              scenario, the <code class="literal">DocumentLoadException</code> should include
              the <code class="literal">IOException</code> as the
              <code class="literal">InnerException</code> argument in
              its constructor, which is assigned to the <code class="literal">InnerException</code> property. This
              cascading exception structure can be particularly useful for
              <a class="indexterm" id="IXTR3-14"/> <a class="indexterm" id="IXTR3-15"/>debugging.</p></dd></dl></div><div class="tip" title="Tip"><h3 class="title"><a id="ch02-185-fm2xml"/>Tip</h3><p>In C# all exceptions are runtime exceptions; there is no
          equivalent of Java’s compile-time exceptions.</p></div></div></div></body></html>