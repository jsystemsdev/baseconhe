<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Automatic Memory Management</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Automatic Memory Management"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-3-SECT-12"/>Automatic Memory Management</h1></div></div></div><p>Almost <a class="indexterm" id="csharpess2-IDXTERM-901"/> <a class="indexterm" id="csharpess2-IDXTERM-902"/>all modern programming languages allocate memory in two
      places: on the stack and on the heap.</p><p>Memory allocated on the stack stores local variables, parameters,
      and return values, and is generally managed automatically by the
      operating system.</p><p>Memory allocated on the heap, however, is treated differently by
      different languages. In C and C++, memory allocated on the heap is
      managed manually. In C# and Java, however, memory allocated on the heap
      is managed automatically<span class="emphasis"><em>.</em></span></p><p>While manual memory management has the advantage of being simple
      for runtimes to implement, it has drawbacks that tend not to exist in
      systems that offer automaticmemory management. For example, a large
      percentage of bugs in C and C++ programs stem from using an object after
      it has been deleted (dangling pointers) or from forgetting to delete an
      object when it is no longer needed (memory leaks).</p><p>The process of automaticallymanaging memory is known as
      <span class="emphasis"><em>garbage collection</em></span>. While generally more complex
      for runtimes to implement than traditional manualmemory management,
      garbage collection greatly simplifies development and eliminates many
      common errors related to manualmemory management.</p><p>For example, it is almost impossible to generate a traditional
      memory leak in C#, and common bugs such as circular references in
      traditional COM development simply go away.</p><div class="sect2" title="The Garbage Collector"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-12.1"/>The Garbage Collector</h2></div></div></div><p>C#<a class="indexterm" id="IXT-3-218761"/> <a class="indexterm" id="IXT-3-218762"/> <a class="indexterm" id="IXT-3-218763"/> <a class="indexterm" id="IXT-3-218764"/> depends on the CLR for many of its runtime services,
        and garbage collection is no exception.</p><p>The CLR includes a high-performing generational mark-and-compact
        garbage collector (GC)that performs automatic memory management for
        type instances stored on the managed heap.</p><p>The GC is considered to be a <span class="emphasis"><em>tracing</em></span>
        <a class="indexterm" id="IXT-3-218765"/>garbage collector in that it doesn’t interfere with
        every access to an object, but rather wakes up intermittently and
        traces the graph of objects stored on the managed heap to determine
        which objects can be considered garbage and therefore
        collected.</p><p>The GC generally initiates a garbage collection when a memory
        allocation occurs, and memory is too low to fulfill the request. This
        process can also be initiated manually using the <code class="literal">System.GC</code><a class="indexterm" id="IXT-3-218766"/> type. Initiating a garbage collection freezes all
        threads in the process to allow the GC time to examine the managed
        heap.</p><p>The GC begins with the set of object references considered
        <span class="emphasis"><em>roots</em></span><a class="indexterm" id="IXT-3-218767"/> and walks the object graph, markingall the objects it
        touches as reachable. Once this process is complete, all objects that
        have not been marked are considered garbage.</p><p>Objects that are considered garbage and don’t have finalizers
        are immediately discarded, and the memory is reclaimed. Objects that
        are considered garbage and do have finalizers are flagged for
        additional asynchronous processing on a separate thread to invoke
        their <code class="literal">Finalize</code> methods before they
        can be considered garbage and reclaimed at the next collection.</p><p>Objects considered still live are then shifted down to the
        bottom of the heap (<span class="emphasis"><em>compacted</em></span> ), hopefully
        freeing space to allow the memory allocation to succeed.</p><p>At this point the memory allocation is attempted again, the
        threads in the process are unfrozen, and either normal processing
        continues or an <code class="literal">OutOfMemoryException</code> is thrown.</p></div><div class="sect2" title="Optimization Techniques"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-12.2"/>Optimization Techniques</h2></div></div></div><p>Although <a class="indexterm" id="IXT-3-218768"/>this may sound like an inefficient process compared to
        simply managing memory manually, the GC incorporates various
        optimization techniques to reduce the time an application is frozen
        waiting for the GC to complete (known as<a class="indexterm" id="IXT-3-218769"/> <span class="emphasis"><em>pause time</em></span>).</p><p>The most important of these optimizations is what makes the GC
        generational. This techniques takes advantage of the fact that while
        many objects tend to be allocated and discarded rapidly, certain
        objects are long-lived and thus don’t need to be traced during every
        collection.</p><p>Basically, the GC divides the managed heap into three
        <span class="emphasis"><em>generations</em></span><a class="indexterm" id="IXT-3-218770"/>. Objects that have just been allocated are considered
        to be in Gen0, objects that have survived one collection cycle are
        considered to be in Gen1, and all other objects are considered to be
        in Gen2.</p><p>When it performs a collection, the GC initially collects only
        Gen0objects. If not enough memory is reclaimed to fulfill the request,
        both Gen0 and Gen1 objects are collected, and if that fails as well, a
        full collection of Gen0, Gen1, and Gen2 objects is attempted.</p><p>Many other optimizations are also used to enhance the
        performance of automatic memory management, and generally, a GC-based
        application can be expected to approach the performance of an
        application that uses manual memory management.</p></div><div class="sect2" title="Finalizers"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-12.3"/>Finalizers</h2></div></div></div><p>When <a class="indexterm" id="IXT-3-218771"/> <a class="indexterm" id="IXT-3-218772"/>implementing your own types, you can choose to give them
        <span class="emphasis"><em>finalizers</em></span> (via C# destructors), which are
        methods called asynchronously by the GC once an object is determined
        to be garbage.</p><p>Although this is required in certain cases, generally, there are
        many good technical reasons to avoid the use of finalizers.</p><p>As described in the previous section, objects with finalizers
        incur significant overhead when they are collected, requiring
        asynchronous invocation of their <code class="literal">Finalize</code><a class="indexterm" id="IXT-3-218773"/> methods and taking two full GC cycles for their memory
        to be reclaimed.</p><p>Other reasons not to use finalizers include:</p><div class="itemizedlist"><ul class="itemizedlist"><li class="listitem"><p>Objects with finalizers take longer to allocate on the
            managed heap than objects without finalizers.</p></li><li class="listitem"><p>Objects with finalizers that refer to other objects (even
            those without finalizers) can prolong the life of the referred
            objects unnecessarily.</p></li><li class="listitem"><p>It’s impossible to predict in what order the finalizers for
            a set of objects will be called.</p></li><li class="listitem"><p>You have limited control over when (or even if!) the
            finalizer for an object will be called.</p></li></ul></div><p>In summary, finalizers are somewhat like lawyers: while there
        are cases when you really need them, generally, you don’t want to use
        them unless absolutely necessary, and if you do use them, you need to
        be 100% sure you understand what they are doing for you.</p><p>If you have to implement a finalizer, follow these guidelines or
        have a very good reason for not doing so:</p><div class="itemizedlist"><ul class="itemizedlist"><li class="listitem"><p>Ensure that your finalizer executes quickly.</p></li><li class="listitem"><p>Never block in your finalizer.</p></li><li class="listitem"><p>Free any unmanaged resources you own.</p></li><li class="listitem"><p>Don’t reference any other objects.</p></li><li class="listitem"><p>Don’t throw any unhandled exceptions.</p></li></ul></div></div><div class="sect2" title="Dispose and Close Methods"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-12.4"/>Dispose and Close Methods</h2></div></div></div><p>It is <a class="indexterm" id="IXT-3-218774"/> <a class="indexterm" id="IXT-3-218775"/> <a class="indexterm" id="IXT-3-218776"/>generally desirable to explicitly call clean-up code
        once you have determined that an object will no longer be used.
        Microsoft recommends that you write a method named either <code class="literal">Dispose</code> or <code class="literal">Close</code> (depending on the semantics of the
        type) to perform the cleanup required. If you also have a destructor,
        include a special call to the static<a class="indexterm" id="IXT-3-218777"/> <a class="indexterm" id="IXT-3-218778"/> <code class="literal">SuppressFinalize</code>
        method on the <code class="literal">System.GC</code> type to
        indicate that the destructor no longer needs to be called. Typically,
        the real destructoris written to call the <code class="literal">Dispose</code>/<code class="literal">Close</code> method, as <a class="indexterm" id="IXTR3-59"/> <a class="indexterm" id="IXTR3-60"/>follows:</p><a id="I_3_tt353"/><pre class="programlisting">using System;
public class Worker : IDisposable {
  bool disposed = false;
  int id;
  public Worker(int id) {
    this.id=id;
  }
  // ...
  protected virtual void Dispose(bool disposing) {
    if (!this.disposed) { // don't dispose more than once
      if (disposing) {
        // disposing==true means you're not in the finalizer, so 
        // you can reference other objects here
        Console.WriteLine("#{0}: OK to clean up other objects.", id);
      }
      Console.WriteLine("#{0}: disposing.", id);
      // Perform normal cleanup
    }
    this.disposed = true;
  }
  public void Dispose(  ) {
    Dispose(true); 
    // Mark this object finalized
    GC.SuppressFinalize(this); // no need to destruct this instance 
  }
  ~Worker(  ) {
    Dispose(  false);
    Console.WriteLine("#{0}: destructing.", id);
  }
  public static void Main(  ) {
    // create a worker and call Dispose when we're done.
    using(Worker w1 = new Worker(1)) {
      // ...
    }
    // create a worker that will get cleaned up when the CLR
    // gets around to it.
    Worker w2 = new Worker(2);
  }
}</pre><p>If you run this code, you will see that Worker 1 is never
        finalized, since its <code class="literal">Dispose( )</code>
        method is implicitly called (the <code class="literal">using</code> block guarantees this). Worker 2 is
        finalized and disposed when the CLR gets around to it, but it’s never
        given a chance to clean up other objects. The disposable pattern gives
        you a way to close or dispose of any external objects you might be
        using, such as an I/O stream:</p><a id="I_3_tt354"/><pre class="programlisting">#1: OK to clean up other objects.
#1: disposing.
#2: disposing.
#2: destructing.</pre></div></div></body></html>