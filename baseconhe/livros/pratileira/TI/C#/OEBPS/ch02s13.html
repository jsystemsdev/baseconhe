<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Delegates</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Delegates"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-2-SECT-13"/>Delegates</h1></div></div></div><div class="informaltable"><a id="ch02-161-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt212"/><pre class="programlisting"><em class="replaceable"><code>attributes?</code></em> unsafe? <em class="replaceable"><code>access-modifier?</code></em>
new?
delegate
[ void | <em class="replaceable"><code>type</code></em> ]
<em class="replaceable"><code>delegate-name</code></em> (<em class="replaceable"><code>parameter-list</code></em>);</pre></td></tr></tbody></table></div><p>A <span class="emphasis"><em>delegate</em></span><a class="indexterm" id="csharpess2-IDXTERM-452"/> is a type that defines a method signature so delegate
      instances can hold and invoke a method or list of methods that match its
      signature. A delegate declaration consists of a name and a <a class="indexterm" id="IXT-2-218238"/>method signature.<sup>[<a class="footnote" href="#ftn.ch02-FTNOTE-2" id="ch02-FTNOTE-2">2</a>]</sup></p><p>Here’s an example:</p><a id="I_2_tt213"/><pre class="programlisting">delegate bool Filter(string s);</pre><p>This declaration lets you create delegate instances that can hold
      and invoke methods that return <code class="literal">bool</code>
      and have a single string parameter. In the following example a <code class="literal">Filter</code> is created that holds the <code class="literal">FirstHalfOfAlphabet</code> method. You then pass the
      <code class="literal">Filter</code> to the <code class="literal">Display</code> method, which invokes the <code class="literal">Filter</code>:</p><a id="I_2_tt214"/><pre class="programlisting">class Test {
  static void Main(  ) {
    Filter f = new Filter(FirstHalfOfAlphabet);
    Display(new String [] {"Ant","Lion","Yak"}, f);
  }
  static bool FirstHalfOfAlphabet(string s) {
    return "N".CompareTo(s) &gt; 0;
  }
  static void Display(string[] names, Filter f) {
    int count = 0;
    foreach(string s in names)
      if(f(s)) // invoke delegate
        Console.WriteLine("Item {0} is {1}", count++, s);
  }
}</pre><div class="sect2" title="Multicast Delegates"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-13.1"/>Multicast Delegates</h2></div></div></div><p><a class="indexterm" id="IXT-2-218240"/> <a class="indexterm" id="IXT-2-218241"/>Delegates can hold and invoke multiple methods. In this
        example, we declare a simple delegate called <code class="literal">MethodInvoker</code>, which can hold and then
        invoke the <code class="literal">Foo</code> and <code class="literal">Goo</code> methods sequentially. The <code class="literal">+=</code> method creates a new delegate by adding
        the right delegate operand to the left delegate operand.</p><a id="I_2_tt215"/><pre class="programlisting">using System;
delegate void MethodInvoker(  );
class Test {
  static void Main(  ) {
     new Test(  ); // prints "Foo", "Goo"
  }
  Test(  ) {
    MethodInvoker m = null;
    m += new MethodInvoker(Foo);
    m += new MethodInvoker(Goo);
    m(  );
  }
  void Foo(  ) {
    Console.WriteLine("Foo");
  }
  void Goo(  ) {
    Console.WriteLine("Goo");
  }
}</pre><p>A delegate can also be removed from another delegate using the
        <code class="literal">-=</code><a class="indexterm" id="IXT-2-218242"/> operator:</p><a id="I_2_tt216"/><pre class="programlisting">Test(  ) {
  MethodInvoker m = null;
  m += new MethodInvoker(Foo);
  m -= new MethodInvoker(Foo);
  m(  ); // m is now null, throws NullReferenceException
}</pre><p>Delegates are invoked in the order they are added. If a delegate
        has a non-void return type, then the value of the last delegate
        invoked is returned. Note that the<a class="indexterm" id="IXT-2-218243"/> <code class="literal">+=</code> and <code class="literal">-=</code> operations on a delegate are not
        thread-safe.</p><div class="tip" title="Tip"><h3 class="title"><a id="ch02-164-fm2xml"/>Tip</h3><p>To work with the .NET runtime, C# compiles <code class="literal">+=</code> and <code class="literal">-=</code> operations made on a delegate to the
          static <code class="literal">Combine</code> and <code class="literal">Remove</code> methods of the <code class="literal">System.Delegate</code> class.</p></div></div><div class="sect2" title="Delegates Compared with Function Pointers"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-13.2"/>Delegates Compared with Function Pointers</h2></div></div></div><p>A <a class="indexterm" id="IXT-2-218244"/> <a class="indexterm" id="IXT-2-218245"/>delegate is behaviorally similar to a C function pointer
        (or <a class="indexterm" id="IXT-2-218246"/>Delphi closure) but can hold multiple methods and the
        instance associated with each nonstatic method. In addition,
        delegates, like all other C# constructs used outside unsafe blocks,
        are type-safe and secure, which means you’re protected from pointing
        to the wrong type of method or a method that you don’t have permission
        to access.</p></div><div class="sect2" title="Delegates Compared with Interfaces"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-13.3"/>Delegates Compared with Interfaces</h2></div></div></div><p>A <a class="indexterm" id="IXT-2-218247"/> <a class="indexterm" id="IXT-2-218248"/>problem that can be solved with a delegate can also be
        solved with an interface. For instance, here is how to solve the
        filter problem using an <code class="literal">IFilter</code>
        interface:</p><a id="I_2_tt217"/><pre class="programlisting">using System;
interface IFilter {
   bool Filter(string s);
}
class Test {
  class FirstHalfOfAlphabetFilter : IFilter {
    public bool Filter(string s) {
      return ("N".CompareTo(s) &gt; 0);
    }      
  }
  static void Main(  ) {
    FirstHalfOfAlphabetFilter f = new FirstHalfOfAlphabetFilter(  );
    Display(new string [] {"Ant", "Lion", "Yak"}, f);
  }
  static void Display(string[] names, IFilter f) {
    int count = 0;
    foreach (string s in names)
      if (f.Filter(s))
        Console.WriteLine("Item {0} is {1}", count++, s);
  }
}</pre><p>In this case, the problem was slightly more elegantly handled
        with a delegate, but generally delegates are best used for event
        <a class="indexterm" id="IXTR3-12"/>handling.</p></div><div class="footnotes"><br/><hr/><div class="footnote"><p><sup>[<a class="para" href="#ch02-FTNOTE-2" id="ftn.ch02-FTNOTE-2">2</a>] </sup>The signature of a delegate method includes its return type
          and allows the use of a <a class="indexterm" id="IXT-2-218239"/>params modifier in its parameter list, expanding the
          list of elements that characterize an ordinary method signature. The
          actual name of the target method is irrelevant to the
          delegate.</p></div></div></div></body></html>