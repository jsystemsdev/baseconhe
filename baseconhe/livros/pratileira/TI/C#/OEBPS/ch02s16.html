<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Attributes</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Attributes"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-2-SECT-16"/>Attributes</h1></div></div></div><div class="informaltable"><a id="ch02-186-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt230"/><pre class="programlisting">[[<em class="replaceable"><code>target</code></em>:]? <em class="replaceable"><code>attribute-name</code></em> (
<em class="replaceable"><code>positional-param</code></em>+ |
[<em class="replaceable"><code>named-param</code></em> = <em class="replaceable"><code>expression</code></em>]+ |
<em class="replaceable"><code>positional-param</code></em>+, [<em class="replaceable"><code>named-param</code></em> = <em class="replaceable"><code>expression</code></em>]+)?]</pre></td></tr></tbody></table></div><p><span class="emphasis"><em>Attributes</em></span> <a class="indexterm" id="csharpess2-IDXTERM-494"/>are language constructs that can decorate code elements
      (e.g., assemblies, modules, types, members, return values, and
      parameters) with additional information.</p><p>In every language, you specify information associated with the
      types, methods, parameters, and other elements of your program. For
      example, a type can specify a list of interfaces that it derives from,
      or a parameter can specify how its values are to be passed with
      modifiers such as the <code class="literal">ref</code> modifier in
      C#. The limitation of this approach is that you can only associate
      information with code elements using the predefined constructs that the
      language itself provides.</p><p>Attributes allow programmers to add to the types of information
      associated with these code elements. For example, serialization in the
      .NET Framework uses various serialization attributes applied to types
      and fields to define how these code elements are serialized. This is
      more flexible than requiring the language to have special syntax for
      serialization.</p><div class="sect2" title="Attribute Classes"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-16.1"/>Attribute Classes</h2></div></div></div><p>An attribute is defined by a class that inherits (directly or
        indirectly) from the <a class="indexterm" id="IXT-2-218272"/>abstract class <code class="literal">System.Attribute</code><a class="indexterm" id="IXT-2-218273"/>. When specifying an attribute on an element, the
        attribute name is the name of the type. By convention the derived type
        name ends with the word “Attribute”, but this suffix isn’t
        required.</p><p>In this example we specify that the <code class="literal">Foo</code> class is serializable using the <code class="literal">Serializable</code> attribute:</p><a id="I_2_tt231"/><pre class="programlisting">[Serializable]
public class Foo {...}</pre><p>The <code class="literal">Serializable</code> attribute is
        actually a type declared in the <code class="literal">System</code> namespace, as follows:</p><a id="I_2_tt232"/><pre class="programlisting">class SerializableAttribute : Attribute {...}</pre><p>We could also specify the <code class="literal">Serializable</code> attribute using its fully
        qualified typename, as follows:</p><a id="I_2_tt233"/><pre class="programlisting">[System.SerializableAttribute]
public class Foo {...}</pre><p>The preceding two examples that use the <code class="literal">Serializable</code> attribute are semantically
        identical.</p><div class="tip" title="Tip"><h3 class="title"><a id="ch02-189-fm2xml"/>Tip</h3><p>The C# language and the FCL include a number of predefined
          attributes. For more information on the other attributes included in
          the FCL, and on creating your own attributes, see <a class="link" href="ch03s11.html" title="Custom Attributes">Section 3.11</a> in <a class="link" href="ch03.html" title="Chapter 3. Programming the.NET Framework">Chapter 3</a>.</p></div></div><div class="sect2" title="Named and Positional Parameters"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-16.2"/>Named and Positional Parameters</h2></div></div></div><p>Attributes <a class="indexterm" id="IXT-2-218274"/> <a class="indexterm" id="IXT-2-218275"/> <a class="indexterm" id="IXT-2-218276"/> <a class="indexterm" id="IXT-2-218277"/>can take parameters, which specify additional
        information on the code element beyond the mere presence of the
        attribute.</p><p>In this next example, we use the <code class="literal">WebPermission</code> attribute to specify that
        methods in class <code class="literal">Foo</code> cannot make
        web connections to any URL starting with <a class="ulink" href="http://www.oreilly.com/">http://www.oreilly.com/</a>. This attribute allows
        you to include parameters that specify a security action and a
        URL:</p><a id="I_2_tt234"/><pre class="programlisting">[WebPermission(SecurityAction.Deny, 
    ConnectPattern="http://www.oreilly.com/.*")]
public class Foo {...}</pre><p>Attribute parameters fall into one of two categories: positional
        and named parameters. In the preceding example, <code class="literal">SecurityAction.Deny</code> is a positional
        parameter, and <code class="literal">ConnectPattern="http://www.oreilly.com/.*"</code>
        is a named parameter.</p><p>The positional parameters for an attribute correspond to the
        parameters passed to one of the attribute type’s public constructors.
        The named parameters for an attribute correspond to the set of public
        read/write or write-only instance properties and fields on the
        attribute type.</p><p>Since the parameters used when specifying an attribute are
        evaluated at compile time, they are generally limited to constant
        expressions.</p></div><div class="sect2" title="Explicitly Specifying Attribute Targets"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-16.3"/>Explicitly Specifying Attribute Targets</h2></div></div></div><p>Implicitly, <a class="indexterm" id="IXT-2-218278"/> <a class="indexterm" id="IXT-2-218279"/>the target of an attribute is the code element it
        immediately precedes. However, sometimes it is necessary to explicitly
        specify that the attribute applies to a particular target. The
        possible targets are <code class="literal">assembly</code>,
        <code class="literal">module</code>, <code class="literal">type</code>, <code class="literal">method</code>, <code class="literal">property</code>, <code class="literal">field</code>, <code class="literal">param</code>, <code class="literal">event</code>, and <code class="literal">return</code>.</p><p>Here is an example that uses the <code class="literal">CLSCompliant</code> attribute to specify the level
        of CLS compliance for an entire assembly:</p><a id="I_2_tt235"/><pre class="programlisting">[assembly:CLSCompliant(true)]</pre></div><div class="sect2" title="Specifying Multiple Attributes"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-16.4"/>Specifying Multiple Attributes</h2></div></div></div><p>You <a class="indexterm" id="IXT-2-218280"/><a class="indexterm" id="IXT-2-218281"/>can specify multiple attributes on a single code
        element. Each attribute can be listed within the same pair of square
        brackets (separated by a comma), in separate pairs of <a class="indexterm" id="IXT-2-218282"/>square brackets, or any combination of the two.</p><p>Consequently, the following three examples are semantically
        <a class="indexterm" id="IXTR3-16"/>identical:</p><a id="I_2_tt236"/><pre class="programlisting">[Serializable, Obsolete, CLSCompliant(false)]
public class Bar {...}

[Serializable] 
[Obsolete] 
[CLSCompliant(false)]
public class Bar {...}

[Serializable, Obsolete] 
[CLSCompliant(false)]
public class Bar {...}</pre></div></div></body></html>