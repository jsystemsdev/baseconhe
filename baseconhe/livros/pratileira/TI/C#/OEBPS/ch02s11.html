<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Arrays</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Arrays"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-2-SECT-11"/>Arrays</h1></div></div></div><div class="informaltable"><a id="ch02-149-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt201"/><pre class="programlisting">type[*]+ <em class="replaceable"><code>array-name</code></em> = new <em class="replaceable"><code>type</code></em> [ <em class="replaceable"><code>dimension+</code></em> ][*]*;</pre></td></tr></tbody></table></div><p>Note that <code class="literal">[*]</code> is the set:
      <code class="literal">[] [,] [,,] ...</code></p><p><span class="emphasis"><em>Arrays</em></span><a class="indexterm" id="csharpess2-IDXTERM-411"/> allow a group of elements of a particular type to be
      stored in a contiguous block of memory. Array types derive from <code class="literal">System.Array</code><a class="indexterm" id="IXT-2-218199"/> and are declared in C# using brackets (<code class="literal">[]</code>). For instance:</p><a id="I_2_tt202"/><pre class="programlisting">char[] vowels = new char[] {'a','e','i','o','u'};
Console.WriteLine(vowels [1]); // Prints "e"</pre><p>The preceding function call prints “e” because array indexes start
      at 0. To support other languages, .NET can create arrays based on
      arbitrary start indexes, but the FCL libraries always use zero-based
      indexing. Once an array has been created, its length can’t be changed.
      However, the <code class="literal">System.Collection</code>
      classes provide dynamically sized arrays, as well as other data
      structures, such as associative (key/value) arrays (see <a class="link" href="ch03s04.html" title="Collections">Section 3.4</a> in <a class="link" href="ch03.html" title="Chapter 3. Programming the.NET Framework">Chapter 3</a>).</p><div class="sect2" title="Multidimensional Arrays"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-11.1"/>Multidimensional Arrays</h2></div></div></div><p><span class="emphasis"><em>Multidimensional arrays</em></span><a class="indexterm" id="IXT-2-218200"/> <a class="indexterm" id="IXT-2-218201"/> come in two varieties, rectangular and jagged.
        <span class="emphasis"><em>Rectangular</em></span><a class="indexterm" id="IXT-2-218202"/> arrays represent an <span class="emphasis"><em>n</em></span>-dimensional
        block; <span class="emphasis"><em>jagged</em></span><a class="indexterm" id="IXT-2-218203"/> arrays are arrays of arrays:</p><a id="I_2_tt203"/><pre class="programlisting">// rectangular
int [,,] matrixR = new int [3, 4, 5]; // creates one big cube
// jagged
int [][][] matrixJ = new int [3][][];
for (int i = 0; i &lt; 3; i++) {
   matrixJ[i] = new int [4][];
   for (int j = 0; j &lt; 4; j++)
      matrixJ[i][j] = new int [5];
} 
// assign an element
matrixR [1,1,1] = matrixJ [1][1][1] = 7;</pre></div><div class="sect2" title="Local and Field Array Declarations"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-11.2"/>Local and Field Array Declarations</h2></div></div></div><p>For <a class="indexterm" id="IXT-2-218204"/>convenience, <span class="emphasis"><em>local</em></span> and
        <span class="emphasis"><em>field declarations</em></span> can omit the array type when
        assigning a known value, because the type is specified in the
        declaration:</p><a id="I_2_tt204"/><pre class="programlisting">int[,] array = {{1,2},{3,4}};</pre></div><div class="sect2" title="Array Length and Rank"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-11.3"/>Array Length and Rank</h2></div></div></div><p>Arrays <a class="indexterm" id="IXT-2-218205"/> <a class="indexterm" id="IXT-2-218206"/>know their own length. For multidimensional arrays, the
        <code class="literal">GetLength</code><a class="indexterm" id="IXT-2-218207"/> method returns the number of elements for a given
        dimension, which is counted from (the outermost) to the array’s Rank-1
        (the innermost):</p><a id="I_2_tt205"/><pre class="programlisting">// one-dimensional
for(int i = 0; i &lt; vowels.Length; i++);
// multidimensional
for(int i = 0; i &lt; matrixR.GetLength(2); i++);</pre></div><div class="sect2" title="Bounds Checking"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-11.4"/>Bounds Checking</h2></div></div></div><p>All <a class="indexterm" id="IXT-2-218208"/> <a class="indexterm" id="IXT-2-218209"/>array indexing is bounds-checked by the CLR, with
        <code class="literal">IndexOutOf-RangeException</code> thrown
        for invalid indexes. As in Java, <span class="emphasis"><em>bounds checking</em></span>
        prevents program faults and debugging difficulties while enabling code
        to be executed with security restrictions.</p><div class="tip" title="Tip"><h3 class="title"><a id="ch02-155-fm2xml"/>Tip</h3><p>Generally the performance hit from bounds checking is minor,
          and the JIT can perform optimizations such as determining each array
          index is safe before entering a loop, thus avoiding a check made for
          each iteration. In addition, C# provides unsafe code to explicitly
          bypass bounds checking (see <a class="link" href="ch02s17.html" title="Unsafe Code and Pointers">Section 2.17</a> later in this
          chapter).</p></div></div><div class="sect2" title="Array Conversions"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-11.5"/>Array Conversions</h2></div></div></div><p>Arrays<a class="indexterm" id="IXT-2-218210"/> <a class="indexterm" id="IXT-2-218211"/> of reference types can be converted to other arrays
        using the same logic you apply to its element type (this is called
        <span class="emphasis"><em>array covariance</em></span>). All arrays implement <code class="literal">System.Array</code><a class="indexterm" id="IXT-2-218212"/>, which provides methods to generic <code class="literal">get</code> and <code class="literal">set</code> elements regardless of array <a class="indexterm" id="IXTR3-11"/>type.</p></div></div></body></html>