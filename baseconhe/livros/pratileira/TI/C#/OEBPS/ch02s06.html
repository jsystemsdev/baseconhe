<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Organizing Types</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Organizing Types"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-2-SECT-6"/>Organizing Types</h1></div></div></div><p>A <a class="indexterm" id="csharpess2-IDXTERM-258"/>C# program is basically a group of types. These types are
      defined in <span class="emphasis"><em>files</em></span>, organized by
      <span class="emphasis"><em>namespaces</em></span>, compiled into
      <span class="emphasis"><em>modules</em></span>, and then grouped into an
      <span class="emphasis"><em>assembly</em></span>.</p><p>Generally, these organizational units overlap: an assembly can
      contain many namespaces, and a namespace can be spread across several
      assemblies. A module can be part of many assemblies, and an assembly can
      contain many modules. A source file can contain many namespaces, and a
      namespace can span many source files. For more information, see <a class="link" href="ch03s09.html" title="Assemblies">Section 3.9</a> in <a class="link" href="ch03.html" title="Chapter 3. Programming the.NET Framework">Chapter 3</a>.</p><div class="sect2" title="Files"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-6.1"/>Files</h2></div></div></div><p>File<a class="indexterm" id="csharpess2-IDXTERM-259"/> <a class="indexterm" id="csharpess2-IDXTERM-260"/> organization is of almost no significance to the C#
        compiler: an entire project can be merged into a single
        <span class="emphasis"><em>.cs</em></span> file and still compile successfully
        (preprocessor statements are the only exception to this). However,
        it’s generally tidy to have one type in one file, with a filename that
        matches the name of the class and a directory name that matches the
        name of the class’s namespace.</p></div><div class="sect2" title="Namespaces"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-6.2"/>Namespaces</h2></div></div></div><div class="informaltable"><a id="ch02-77-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Namespace declaration syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt129"/><pre class="programlisting">namespace name+<sup>a</sup> {
  using-statement*
  [namespace-declaration | type-declaration]*<sup>b</sup>
}</pre> <p><sup>a</sup> Dot-delimited.</p>
                <p><sup>b</sup> No
                delimiters.</p></td></tr></tbody></table></div><p>A namespace enables you to group related types into a
        hierarchical categorization. Generally the first name in a namespace
        name is the name of your organization, followed by names that group
        types with finer granularity. For example:</p><a id="I_2_tt130"/><pre class="programlisting">namespace MyCompany.MyProduct.Drawing {
  class Point {int x, y, z;}
  delegate void PointInvoker(Point p);
}</pre><div class="sect3" title="Nesting namespaces"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-6.2.1"/>Nesting namespaces</h3></div></div></div><p>You <a class="indexterm" id="IXT-2-218058"/>may also nest namespace declarations instead of using
          dots. This example is semantically identical to the previous
          example:</p><a id="I_2_tt131"/><pre class="programlisting">namespace MyCompany {
  namespace MyProduct {
    namespace Drawing {
      class Point {int x, y, z;}
      delegate void PointInvoker(Point p);
    }
  }
}</pre></div><div class="sect3" title="Using a type with its fully qualified name"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-6.2.2"/>Using a type with its fully qualified name</h3></div></div></div><p>The complete name of a type includes its namespace name. To
          use the <code class="literal">Point</code><a class="indexterm" id="IXT-2-218059"/> class from another namespace, you may refer to it
          with its fully qualified name:</p><a id="I_2_tt132"/><pre class="programlisting">namespace TestProject {
  class Test {
    static void Main(  ) {
      MyCompany.MyProduct.Drawing.Point x;
    }
  }
}</pre></div><div class="sect3" title="using keyword"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-6.2.3"/>using keyword</h3></div></div></div><p>The<a class="indexterm" id="IXT-2-218060"/> <code class="literal">using</code> keyword is a
          convenient way to avoid using the fully qualified names of types in
          other namespaces. This example is semantically identical to the
          previous example:</p><a id="I_2_tt133"/><pre class="programlisting">namespace TestProject {
  using MyCompany.MyProduct.Drawing;
  class Test {
    static void Main(  ) {
      Point x;
    }
  }
}</pre></div><div class="sect3" title="Aliasing types and namespaces"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-6.2.4"/>Aliasing types and namespaces</h3></div></div></div><p>Type <a class="indexterm" id="IXT-2-218061"/> <a class="indexterm" id="IXT-2-218062"/>names must be unique within a namespace. To avoid
          naming conflicts without having to use fully qualified names, C#
          allows you to specify an alias for a type or namespace. Here is an
          example:</p><a id="I_2_tt134"/><pre class="programlisting">using sys = System;        // Namespace alias
using txt = System.String; // Type alias
class Test {
  static void Main(  ) {
    txt s = "Hello, World!";
    sys.Console.WriteLine(s); // Hello, World!
    sys.Console.WriteLine(s.GetType(  )); // System.String
  }
}</pre></div><div class="sect3" title="Global namespace"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-6.2.5"/>Global namespace</h3></div></div></div><p>The<a class="indexterm" id="IXT-2-218063"/> outermost level within which all namespaces and types
          are implicitly declared is called the <span class="emphasis"><em>global
          namespace</em></span>. When a type isn’t explicitly declared within a
          namespace, it can be used without qualification from any other
          namespace, since it is a member of the global namespace. However, it
          is always good practice to organize types <a class="indexterm" id="IXTR3-5"/> <a class="indexterm" id="IXTR3-6"/>within <a class="indexterm" id="IXTR3-7"/>logical
          namespaces</p></div></div></div></body></html>