<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Interop with COM</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Interop with COM"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-3-SECT-14"/>Interop with COM</h1></div></div></div><p>The<a class="indexterm" id="csharpess2-IDXTERM-993"/> <a class="indexterm" id="csharpess2-IDXTERM-994"/> CLR<a class="indexterm" id="IXT-3-218841"/> provides support both for exposing C# objects as COM
      objects and for using COM objects from C#.</p><div class="sect2" title="Binding COM and C# Objects"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-14.1"/>Binding COM and C# Objects</h2></div></div></div><p>Interoperating<a class="indexterm" id="IXT-3-218842"/> between COM and C# works through either early or late
        binding. Early binding allows you to program with types known at
        compile time, while late binding forces you to program with types via
        dynamic discovery, using reflection on the C# side and <code class="literal">IDispatch</code> on the COM side.</p><p>When calling COM programs from C#, early binding works by
        providing metadata in the form of an assembly for the COM object and
        its interfaces.<a class="indexterm" id="IXT-3-218843"/> <span class="emphasis"><em>TlbImp.exe</em></span> takes a COM type
        library and generates the equivalent metadata in an assembly. With the
        generated assembly, it’s possible to instantiate and call methods on a
        COM object just as you would on any other C# object.</p><p>When calling C# programs from COM, early binding works via a
        type library. Both<a class="indexterm" id="IXT-3-218844"/> <span class="emphasis"><em>TlbExp.exe</em></span> and
        <span class="emphasis"><em>RegAsm.exe</em></span><a class="indexterm" id="IXT-3-218845"/> allow you to generate a COM type library from your
        assembly. You can then use this type library with tools that support
        early binding via type libraries such as Visual Basic 6.</p></div><div class="sect2" title="Exposing COM Objects to C#"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-14.2"/>Exposing COM Objects to C#</h2></div></div></div><p>When<a class="indexterm" id="IXT-3-218846"/> you instantiate a COM object you are actually working
        with a proxy known as the <a class="indexterm" id="IXT-3-218847"/> <a class="indexterm" id="IXT-3-218848"/>Runtime Callable Wrapper (RCW). The RCW is responsible
        for managing the lifetime requirements of the COM object and
        translating the methods called on it into the appropriate calls on the
        COM object. When the garbage collector finalizes the RCW, it releases
        all references to the object it was holding. For situations in which
        you need to release the COM object without waiting for the garbage
        collector to finalize the RCW, you can use the static <code class="literal">ReleaseComObject</code><a class="indexterm" id="IXT-3-218849"/> method of the <code class="literal">System.Runtime.InteropServices.Marshal</code><a class="indexterm" id="IXT-3-218850"/> type.</p><p>The following example demonstrates changing the friendly name of
        the user with MSN Instant Messenger from C# via COM Interop:</p><a id="I_3_tt408"/><pre class="programlisting">// SetFN.cs - compile with /r:Messenger.dll
// Run SetFN.exe &lt;Name&gt; to set the FriendlyName for
//   the currently logged-in user
// Run TlbImp.exe "C:\Program Files\Messenger\msmsgs.exe"
//   to create Messenger.dll
using Messenger; // COM API for MSN Instant Messenger
public class MyApp {
 public static void Main(string[] args) {
    MsgrObject mo = new MsgrObject(  );
    IMsgrService im = mo.Services.PrimaryService;
    im.FriendlyName = args[0];
    }
}</pre></div><div class="sect2" title="Exposing C# Objects to COM"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-14.3"/>Exposing C# Objects to COM</h2></div></div></div><p>Just <a class="indexterm" id="IXT-3-218851"/>as an RCW proxy wraps a COM object when you access it
        from C#, code that accesses a C# object as a COM object must do so
        through a proxy as well. When your C# object is marshaled out to COM,
        the runtime creates a <a class="indexterm" id="IXT-3-218852"/> <a class="indexterm" id="IXT-3-218853"/>COM Callable Wrapper (CCW). The CCW follows the same
        lifetime rules as other COM objects, and as long as it is alive, a CCW
        maintains a traceable reference to the object it wraps, which keeps
        the object alive when the garbage collector is run.</p><p>The following example shows how you can export both a class and
        an interface from C# and control the assigned Global Unique
        Identifiers (GUIDs) and Dispatch IDs (DISPIDs). After compiling
        <code class="literal">IRunInfo</code> and <code class="literal">StackSnapshot</code> you can register both using
        <span class="emphasis"><em>RegAsm.exe</em></span>.</p><a id="I_3_tt409"/><pre class="programlisting">// IRunInfo.cs
// Compile with:
// csc /t:library IRunInfo.cs
using System;
using System.Runtime.InteropServices;
[GuidAttribute("aa6b10a2-dc4f-4a24-ae5e-90362c2142c1")]
public interface IRunInfo {
  [DispId(1)]
  string GetRunInfo(  );
}

// StackSnapshot.cs
// compile with: csc /t:library /r:IRunInfo.dll StackSnapshot.cs
using System;
using System.Runtime.InteropServices;
using System.Diagnostics;
[GuidAttribute("b72ccf55-88cc-4657-8577-72bd0ff767bc")]
public class StackSnapshot : IRunInfo {
  public StackSnapshot(  ) {
    st = new StackTrace(  );
  }
  [DispId(1)]
  public string GetRunInfo(  ) {
    return st.ToString(  );
  }
  private StackTrace st;
}</pre></div><div class="sect2" title="COM Mapping in C#"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-14.4"/>COM Mapping in C#</h2></div></div></div><p>When <a class="indexterm" id="IXT-3-218854"/>you use a COM object from C#, the RCW makes a COM method
        look like a normal C# instance method. In COM, methods normally return
        an HRESULT to indicate success or failure and use an <code class="literal">out</code> parameter to return a value. In C#,
        however, methods normally return their result values and use
        exceptions to report errors. The RCW handles this by checking the
        HRESULT returned from the call to a COM method and throwing a C#
        exception when it finds a failure result. With a success result, the
        RCW returns the parameter marked as the return value in the COM method
        signature.</p><div class="tip" title="Tip"><h3 class="title"><a id="ch03-123-fm2xml"/>Tip</h3><p>For more information on the argument modifiers and default
          mappings from COM type library types to C# types, see <a class="link" href="apd.html" title="Appendix D. Data Marshaling">Appendix D</a>.</p></div></div><div class="sect2" title="Common COM Interop Support Attributes"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-14.5"/>Common COM Interop Support Attributes</h2></div></div></div><p>The <a class="indexterm" id="IXT-3-218855"/> <a class="indexterm" id="IXT-3-218856"/>FCL provides a set of attributes you can use to mark up
        your objects with information needed by the CLR interop services to
        expose managed types to the unmanaged world as COM objects.</p><p>This section describes the most common attributes you will use
        for this purpose. These attributes all exist in the <code class="literal">System.Runtime.InteropServices</code>
        namespace.</p><div class="sect3" title="ComVisible attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-14.5.1"/>ComVisible attribute</h3></div></div></div><a class="indexterm" id="IXT-3-218857"/><div class="informaltable"><a id="ch03-126-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt410"/><pre class="programlisting">[ComVisible(true|false)] <em class="lineannotation"><span class="lineannotation">(for assemblies, classes, structs, enums, interfaces, delegates)</span></em></pre></td></tr></tbody></table></div><p>When generating a type library, all public types in an
          assembly are exported by default. The <code class="literal">ComVisible</code> attribute specifies that
          particular public types (or even the entire assembly) should not be
          exposed.</p></div><div class="sect3" title="DispId attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-14.5.2"/>DispId attribute</h3></div></div></div><a class="indexterm" id="IXT-3-218858"/><div class="informaltable"><a id="ch03-128-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt411"/><pre class="programlisting">[DispId(<em class="replaceable"><code>dispatch-id</code></em>)] <em class="lineannotation"><span class="lineannotation">(for methods, properties, fields)</span></em></pre></td></tr></tbody></table></div><p>The <code class="literal">DispId</code> attribute
          specifies the <code class="literal">DispID</code> assigned to
          a method, field, or property for access via an <code class="literal">IDispatch</code> interface.</p></div><div class="sect3" title="ProgId attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-14.5.3"/>ProgId attribute</h3></div></div></div><a class="indexterm" id="IXT-3-218859"/><div class="informaltable"><a id="ch03-130-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt412"/><pre class="programlisting">[ProgId(<em class="replaceable"><code>progid</code></em>)] <em class="lineannotation"><span class="lineannotation">(for classes)</span></em></pre></td></tr></tbody></table></div><p>The <code class="literal">ProgId</code> attribute
          specifies the COM <code class="literal">ProgID</code> to be
          used for your class.</p></div><div class="sect3" title="Guid attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-14.5.4"/>Guid attribute</h3></div></div></div><a class="indexterm" id="IXT-3-218860"/><div class="informaltable"><a id="ch03-132-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt413"/><pre class="programlisting">[GuidAttribute(<em class="replaceable"><code>guid</code></em>)] <em class="lineannotation"><span class="lineannotation">(for assemblies, modules, classes, structs, enums, interfaces, delegates)</span></em></pre></td></tr></tbody></table></div><p>The <code class="literal">Guid</code> attribute
          specifies the COM GUID to be used for your class or interface. This
          attribute should be specified using its full type name to avoid
          clashes with the <code class="literal">Guid</code>
          type.</p></div><div class="sect3" title="InterfaceType attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-14.5.5"/>InterfaceType attribute</h3></div></div></div><a class="indexterm" id="IXT-3-218861"/><div class="informaltable"><a id="ch03-134-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt414"/><pre class="programlisting">[InterfaceType(<em class="replaceable"><code>ComInterfaceType</code></em>)] <em class="lineannotation"><span class="lineannotation">(for interfaces)</span></em></pre></td></tr></tbody></table></div><p>By default, interfaces are generated as dual interfaces in the
          type library, but you can use this attribute to use one of the three
          COM interface types (dual, dispatch, or a traditional <code class="literal">IUnknown</code>-derived interface).</p></div><div class="sect3" title="ComRegisterFunction attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-14.5.6"/>ComRegisterFunction attribute</h3></div></div></div><a class="indexterm" id="IXT-3-218862"/><div class="informaltable"><a id="ch03-136-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt415"/><pre class="programlisting">[ComRegisterFunction] <em class="lineannotation"><span class="lineannotation">(for methods)</span></em></pre></td></tr></tbody></table></div><p>Requests that <span class="emphasis"><em>RegAsm.exe</em></span><a class="indexterm" id="IXT-3-218863"/>call a method during the process of registering your
          assembly. If you use this attribute, you must also specify an
          unregistration method that reverses all the changes you made in the
          registration function. Use the <code class="literal">ComUnregisterFunction</code> <a class="indexterm" id="IXTR3-65"/> <a class="indexterm" id="IXTR3-66"/>attribute <a class="indexterm" id="IXTR3-67"/> <a class="indexterm" id="IXTR3-68"/>to mark that
          method.</p></div></div></div></body></html>