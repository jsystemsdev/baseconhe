<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Chapter 3. Programming the.NET Framework</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="chapter" title="Chapter 3. Programming the.NET Framework"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-3"/>Chapter 3. Programming the.NET Framework</h1></div></div></div><p>Most <a class="indexterm" id="csharpess2-IDXTERM-582"/> <a class="indexterm" id="csharpess2-IDXTERM-583"/>modern programming languages include some form of runtime
    that provides common services and access to the underlying operating
    systems and hardware. Examples of this range from a simple
    functionallibrary, such as the ANSI C Runtime used by C and C++, to the
    rich object-oriented class libraries provided by the Java Runtime
    Environment.</p><p>Similar to the way that Java programs depend on the Java class
    libraries and virtual machine, C# programs depend on the services in the
    .NET Framework such as the<a class="indexterm" id="IXT-3-218502"/> framework class library (FCL) and the <a class="indexterm" id="IXT-3-218503"/>Common Language Runtime (CLR).</p><p>For a high-level overview of the FCL, see <a class="link" href="ch04.html" title="Chapter 4. Framework Class Library Overview">Chapter 4</a>.</p><p>This chapter addresses the most common tasks you need to perform
    when building C# programs. These topics generally fall into one of two
    categories: leveraging functionality included in the FCL and interacting
    with elements of the CLR.</p><div class="sect1" title="Common Types"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-3-SECT-1"/>Common Types</h1></div></div></div><p>Certain <a class="indexterm" id="IXT-3-218504"/> <a class="indexterm" id="IXT-3-218505"/>types in the FCL are ubiquitous, in that they are
      fundamental to the way the FCL and CLR work and provide common
      functionality used throughout the entire FCL.</p><p>This section identifies some of the most common of these types and
      provides guidelines to their usage. The types mentioned in this section
      all exist in the <code class="literal">System</code><a class="indexterm" id="IXT-3-218506"/> namespace.</p><div class="sect2" title="Object Class"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-1.1"/>Object Class</h2></div></div></div><p><a class="indexterm" id="csharpess2-IDXTERM-589"/> <a class="indexterm" id="csharpess2-IDXTERM-590"/> <a class="indexterm" id="csharpess2-IDXTERM-591"/>The <code class="literal">System.Object</code>
        class is the root of the class hierarchy and serves as the base class
        for every other class. The <a class="indexterm" id="IXT-3-218507"/> <a class="indexterm" id="IXT-3-218508"/>C# <code class="literal">object</code> type
        aliases <code class="literal">System.Object</code>. <code class="literal">System.Object</code> provides a handful of useful
        methods that are present on all objects, and whose signatures are
        listed in the following fragment of the<a class="indexterm" id="IXT-3-218509"/> <a class="indexterm" id="IXT-3-218510"/> <code class="literal">System.Object</code> class
        definition:</p><a id="I_3_tt269"/><pre class="programlisting">public class Object {
   public Object(  ) {...}
   public virtual bool Equals(object o) {...}
   public virtual int GetHashCode(  ){...}
   public Type GetType(  ){...}
   public virtual string ToString(  ) {...}
   protected virtual void Finalize(  ) {...}
   protected object MemberwiseClone(  ) {...}
   public static bool Equals (object a, object b) {...}
   public static bool ReferenceEquals (object a, object b) {...}
}</pre><div class="variablelist"><dl><dt><span class="term"><a class="indexterm" id="IXT-3-218511"/><code class="literal">Object( )</code></span></dt><dd><p>The constructor for the <code class="literal">Object</code> base class.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-3-218512"/><code class="literal">Equals(object</code>
            <code class="literal">o)</code></span></dt><dd><p>This method evaluates whether two objects are
              equivalent.</p><p>The default implementation of this method on reference
              types compares the objects by reference, so classes are expected
              to override this method to compare two objects by value.</p><p>In C#, you can also overload the <code class="literal">==</code><a class="indexterm" id="IXT-3-218513"/> <a class="indexterm" id="IXT-3-218514"/> and <code class="literal">!=</code>
              operators. For more information see <a class="link" href="ch02s09.html#csharpess2-CHP-2-SECT-9.8.1" title="Implementing value equality">Section
              2.9.8.1</a>.</p></dd><dt><span class="term"><code class="literal">GetHashCode( )</code></span></dt><dd><p>This<a class="indexterm" id="IXT-3-218515"/> method allows types to hash their instances. A
              hashcode is an integer value that provides a “pretty good”
              unique ID for an object. If two objects hash to the same value,
              there’s a good chance that they are also equal. If they don’t
              hash to the same value, they are definitely not equal.</p><p>The hashcode is used when you store a key in a dictionary
              or hashtable collection, but you can also use it to optimize
              <code class="literal">Equals( )</code> by comparing hash
              codes and skipping comparisons of values that are obviously not
              equal. This is a gain only when it is cheaper to create the
              hashcode than perform an equality comparison. If your hashcode
              is based on immutable data members, you can make this a
              no-brainer by caching the hash code the first time it is
              computed.</p><p>The return value from this function should pass the
              following tests: (1) two objects representing the same value
              should return the same hashcode, and (2) the returned values
              should generate a random distribution at runtime.</p><p>The default implementation of <code class="literal">GetHashCode</code> doesn’t actually meet
              these criteria because it merely returns a number based on the
              object reference. For this reason, you should usually override
              this method in your own types.</p><p>An implementation of <code class="literal">GetHashCode</code> could simply add or
              multiply all the data members together. You can achieve a more
              random distribution of hash codes by combining each member with
              a prime number (see <a class="link" href="ch03.html#csharpess2-CHP-3-EX-1" title="Example 3-1. Defining new value type">Example 3-1</a>).</p><p>To learn more about how the hashcode is used by the
              predefined collection classes, see <a class="link" href="ch03s04.html" title="Collections">Section 3.4</a> later in
              this chapter.</p></dd><dt><span class="term"><code class="literal">GetType( )</code></span></dt><dd><p>This <a class="indexterm" id="IXT-3-218516"/>method provides access to the <code class="literal">Type</code><a class="indexterm" id="IXT-3-218517"/> object representing the type of the object and
              should never be implemented by your types. To learn more about
              the <code class="literal">Type</code> object and
              reflectionin general, see <a class="link" href="ch03s10.html" title="Reflection">Section 3.10</a>.</p></dd><dt><span class="term"><code class="literal">ToString( )</code></span></dt><dd><p>This <a class="indexterm" id="IXT-3-218518"/>method provides a string representation of the
              object and is generally intended for use when debugging or
              producing human-readable output.</p><p>The default implementation of this method merely returns
              the name of the type and should be overridden in your own types
              to return a meaningful string representation of the object. The
              predefined types such as <code class="literal">int</code>
              and <code class="literal">string</code> all override this
              method to return the value, as follows:</p><a id="I_3_tt270"/><pre class="programlisting">using System;
class Beeblebrox {}
class Test {
  static void Main(  ) {
    string s = "Zaphod";
    Beeblebrox b = new Beeblebrox(  );
    Console.WriteLine(s); // Prints "Zaphod"
    Console.WriteLine(b); // Prints "Beeblebrox"
  }
}</pre></dd><dt><span class="term"><code class="literal">Finalize( )</code></span></dt><dd><p>The <a class="indexterm" id="IXT-3-218519"/><code class="literal">Finalize</code> method
              cleans up nonmemory resources and is usually called by the
              garbage collector before reclaiming the memory for the object.
              The <code class="literal">Finalize</code> method can be
              overridden on any reference type, but this should be done only
              in a very few cases. For a discussion of finalizers and the
              garbage collector, see <a class="link" href="ch03s12.html" title="Automatic Memory Management">Section 3.12</a>.</p></dd><dt><span class="term"><code class="literal">MemberwiseClone( )</code></span></dt><dd><p>This <a class="indexterm" id="IXT-3-218520"/>method creates shallow copies of the object and
              should never be implemented by your types. To learn how to
              control shallow/deep copy semantics on your own types, see <a class="link" href="ch03.html#csharpess2-CHP-3-SECT-1.2" title="ICloneable Interface">Section 3.1.2</a> later
              in this chapter</p></dd><dt><span class="term"><code class="literal">Equals/ReferenceEquals             (object</code> <em class="replaceable"><code>a</code></em><code class="literal">, object</code>
            <em class="replaceable"><code>b</code></em><code class="literal">)</code></span></dt><dd><p><a class="indexterm" id="IXT-3-218521"/><code class="literal">Equals</code> tests
              for value quality, and <a class="indexterm" id="IXT-3-218522"/><code class="literal">ReferenceEquals</code>
              tests for reference equality. <code class="literal">Equals</code> basically calls the instance
              <code class="literal">Equals</code> method on the first
              object, using the second object as a parameter. In the case that
              both object references are null, it returns <code class="literal">true</code>, and in the case that only one
              reference is null, it returns <code class="literal">false</code>. The <code class="literal">ReferenceEquals</code> method returns
              <code class="literal">true</code> if both object
              references point to the same object or if both object references
              are null.</p></dd></dl></div><div class="sect3" title="Creating FCL-friendly types"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-1.1.1"/>Creating FCL-friendly types</h3></div></div></div><p>When <a class="indexterm" id="IXT-3-218523"/> <a class="indexterm" id="IXT-3-218524"/>defining new types that work well with the rest of the
          FCL, you should override several of these methods as
          appropriate.</p><p><a class="link" href="ch03.html#csharpess2-CHP-3-EX-1" title="Example 3-1. Defining new value type">Example 3-1</a> is an
          example of a new value type that is intended to be a good citizen in
          the FCL:</p><div class="example"><a id="csharpess2-CHP-3-EX-1"/><p class="title">Example 3-1. Defining new value type</p><div class="example-contents"><pre class="programlisting">// Point3D - a 3D point
// Compile with: csc /t:library Point3D.cs
using System;
public sealed class Point3D {
  int x, y, z;
  public Point3D(int x, int y, int z) {
    this.x=x; this.y=y; this.z=z; // Initialize data
  }
  public override bool Equals(object o) {
    if (o == (object) this) return true; // Identity test
    if (o == null) return false; // Safety test
    if (!(o is Point3D)) // Type equivalence test
      return false;
    Point3D p = (Point3D) o;
    return ((this.x==p.x) &amp;&amp; (this.y==p.y) &amp;&amp; (this.z==p.z));
  }
  public override int GetHashCode(  ){
    return ((((37+x)*37)+y)*37)+z; // :-)
  }
  public override string ToString(  ) {
    return String.Format("[{0},{1},{2}]", x, y, z);
  }
}</pre></div></div><p>This class overrides <code class="literal">Equals</code><a class="indexterm" id="IXT-3-218525"/> to provide value-based equality semantics, creates a
          hashcode that follows the rules described in the preceding section,
          and overrides <code class="literal">ToString</code> for easy
          debugging. It can be used as <a class="indexterm" id="IXTR3-30"/> <a class="indexterm" id="IXTR3-31"/> <a class="indexterm" id="IXTR3-32"/>follows:</p><a id="I_3_tt271"/><pre class="programlisting">// TestPoint3D - test the 3D point type
// Compile with: csc /r:Point3D.dll TestPoint3D.cs
using System;
using System.Collections;
class TestPoint3D {
  static void Main(  ) {
    // Uses ToString, prints "p1=[1,1,1] p2=[2,2,2] p3=[2,2,2]"
    Point3D p1 = new Point3D(1,1,1);
    Point3D p2 = new Point3D(2,2,2);
    Point3D p3 = new Point3D(2,2,2);
    Console.WriteLine("p1={0} p2={1} p3={2}", p1, p2, p3);

    // Tests for equality to demonstrate Equals
    Console.WriteLine(Equals(p1, p2)); // Prints "False"
    Console.WriteLine(Equals(p2, p3)); // Prints "True"

    // Use a hashtable to cache each point's variable name
    // (uses GetHashCode).
    Hashtable ht = new Hashtable(  );
    ht[p1] = "p1"; 
    ht[p2] = "p2";
    ht[p3] = "p3"; // replaces ht[p2], since p2 == p3

    // Prints:
    //    p1 is at [1,1,1]
    //    p3 is at [2,2,2] 
    foreach (DictionaryEntry de in ht)
      Console.WriteLine("{0} is at {1} ", de.Value, de.Key);
  }
}</pre></div></div><div class="sect2" title="ICloneable Interface"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-1.2"/>ICloneable Interface</h2></div></div></div><a id="I_3_tt272"/><pre class="programlisting">public interface ICloneable {
  object Clone(  );
}</pre><p><code class="literal">ICloneable</code><a class="indexterm" id="IXT-3-218526"/> <a class="indexterm" id="IXT-3-218527"/> <a class="indexterm" id="IXT-3-218528"/> <a class="indexterm" id="IXT-3-218529"/> allows class or struct instances to be cloned. It
        contains a single method named <code class="literal">Clone</code> that returns a copy of the instance.
        When implementing this interface your <code class="literal">Clone</code> method can simply return <code class="literal">this.MemberwiseClone( )</code>, which performs a
        shallow copy (the fields are copied directly), or you can perform a
        custom deep copy, in which you clone individual fields in the class or
        struct.The following example is the simplest implementation of
        <code class="literal">ICloneable</code>:</p><a id="I_3_tt273"/><pre class="programlisting">public class Foo : ICloneable {
      public object Clone(  ) {
       return this.MemberwiseClone(  );
   }
}</pre></div><div class="sect2" title="IComparable Interface"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-1.3"/>IComparable Interface</h2></div></div></div><a id="I_3_tt274"/><pre class="programlisting">interface IComparable {
  int CompareTo(object o);
}</pre><p><code class="literal">IComparable</code><a class="indexterm" id="IXT-3-218530"/> <a class="indexterm" id="IXT-3-218531"/> <a class="indexterm" id="IXT-3-218532"/> is implemented by types that have instances that can be
        ordered (see <a class="link" href="ch03s04.html" title="Collections">Section
        3.4</a>). It contains a single method named <code class="literal">CompareTo</code> that:</p><div class="itemizedlist"><ul class="itemizedlist"><li class="listitem"><p>Returns <code class="literal">-</code> if instance
            &lt; <code class="literal">o</code></p></li><li class="listitem"><p>Returns <code class="literal">+</code> if instance
            &gt; <code class="literal">o</code></p></li><li class="listitem"><p>Returns <code class="literal">0</code> if instance = =
            <code class="literal">o</code></p></li></ul></div><p>This interface is implemented by all numeric types: <code class="literal">string</code>, <code class="literal">DateTime</code>, etc. It may also be implemented by
        custom classes or structs to provide comparison semantics. For
        example:</p><a id="I_3_tt275"/><pre class="programlisting">using System;
using System.Collections;
class MyType : IComparable {
  public int x;
  public MyType(int x) {
    this.x = x;
  }
  public int CompareTo(object o) {
    return x -((MyType)o).x;
  }
}
class Test {
  static void Main(  ) {
    ArrayList a = new ArrayList(  );
    a.Add(new MyType(42));
    a.Add(new MyType(17));
    a.Sort(  );
    foreach(MyType t in a)
      Console.WriteLine(((MyType)t).x);
   }
}</pre></div><div class="sect2" title="IFormattable Interface"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-1.4"/>IFormattable Interface</h2></div></div></div><a id="I_3_tt276"/><pre class="programlisting">public interface IFormattable {
  string ToString(string format, IFormatProvider formatProvider);
}</pre><p>The <code class="literal">IFormattable</code><a class="indexterm" id="IXT-3-218533"/> <a class="indexterm" id="IXT-3-218534"/> <a class="indexterm" id="IXT-3-218535"/> interface is implemented by types that have formatting
        options for converting their value to a string representation. For
        instance, a <code class="literal">decimal</code> may be
        converted to a <code class="literal">string</code> representing
        currency or to a <code class="literal">string</code> that uses a
        comma for a decimal point. The formatting options are specified by the
        <span class="emphasis"><em>format string</em></span> (see <a class="link" href="ch03s03.html#csharpess2-CHP-3-SECT-3.4" title="Formatting Strings">Section 3.3.4</a> later in this
        chapter). If an <code class="literal">IFormatProvider</code>
        interface is supplied, it specifies the specific culture to be used
        for the conversion.</p><p><code class="literal">IFormattable</code> is commonly used
        when calling one of the <code class="literal">String</code>
        class <code class="literal">Format</code> methods (see <a class="link" href="ch03s03.html" title="Strings">Section 3.3</a>).</p><p>All the common types (<code class="literal">int</code>,
        <code class="literal">string</code>, <code class="literal">DateTime</code>, etc.) implement this interface,
        and you should implement it on your own types if you want them to be
        fully supported by the <code class="literal">String</code> class
        when formatting.</p></div></div></div></body></html>