<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Unsafe Code and Pointers</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Unsafe Code and Pointers"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-2-SECT-17"/>Unsafe Code and Pointers</h1></div></div></div><p>C# <a class="indexterm" id="csharpess2-IDXTERM-507"/> <a class="indexterm" id="csharpess2-IDXTERM-508"/>supports direct memory manipulation via pointers within
      blocks of code marked unsafe and compiled with the <code class="literal">/unsafe</code> compiler option. Pointer types are
      primarily useful for interoperability with C APIs but may also be used
      for accessing memory outside the managed heap or for
      performance-critical hotspots.</p><div class="sect2" title="Pointer Types"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-17.1"/>Pointer Types</h2></div></div></div><p>For every value type or pointer type V in a C# program, there is
        a corresponding C# pointer type named V*. A pointer instance holds the
        address of a value. That value is considered to be of type V, but
        pointer types can be (unsafely) cast to any other pointer type. <a class="link" href="ch02s17.html#csharpess2-CHP-2-TABLE-3" title="Table 2-3. Principal pointer operators">Table 2-3</a> summarizes the
        principal pointer operators supported by the C# language.</p><div class="table"><a id="csharpess2-CHP-2-TABLE-3"/><p class="title">Table 2-3. Principal pointer operators</p><div class="table-contents"><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; " summary="Principal pointer operators"><colgroup><col/><col/></colgroup><thead><tr><th style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><p>Operator</p></th><th style="border-bottom: 0.5pt solid ; "><p>Meaning</p></th></tr></thead><tbody><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-2-218283"/><a class="indexterm" id="IXT-2-218284"/><a id="I_2_tt237"/><pre class="programlisting">&amp;</pre></td><td style="border-bottom: 0.5pt solid ; "><p>The <span class="emphasis"><em>address-of</em></span> operator
                returns a pointer to the address of a value.</p></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-2-218285"/><a class="indexterm" id="IXT-2-218286"/><a id="I_2_tt238"/><pre class="programlisting">*</pre></td><td style="border-bottom: 0.5pt solid ; "><p>The <span class="emphasis"><em>dereference</em></span> operator
                returns the value at the address of a pointer.</p></td></tr><tr><td style="border-right: 0.5pt solid ; "><a class="indexterm" id="IXT-2-218287"/><a class="indexterm" id="IXT-2-218288"/><a id="I_2_tt239"/><pre class="programlisting">-&gt;</pre></td><td style=""><p>The <span class="emphasis"><em>pointer-to-member</em></span>
                operator is a syntactic shortcut, in which <code class="literal">x-&gt;y</code> is equivalent to <code class="literal">(*x).y</code>.</p></td></tr></tbody></table></div></div></div><div class="sect2" title="Unsafe Code"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-17.2"/>Unsafe Code</h2></div></div></div><p>By marking a type, type-member, or statement block with the
        <code class="literal">unsafe</code> keyword, you’re permitted to
        use pointer types and perform C++-style pointer operations on memory
        within that scope. Here is an example that uses pointers with a
        managed object:</p><a id="I_2_tt240"/><pre class="programlisting">unsafe void RedFilter(int[,] bitmap) {
  const int length = bitmap.Length;
  fixed (int* b = bitmap) {
    int* p = b;
    for(int i = 0; i &lt; length; i++)
      *p++ &amp;= 0xFF;
  }
}</pre><p>Unsafe code typically runs faster than a corresponding safe
        implementation, which in this case would have required a nested loop
        with array indexing and bounds checking. An unsafe C# method can be
        faster than calling an external C function too, since there is no
        overhead associated with leaving the managed execution
        environment.</p></div><div class="sect2" title="The fixed Statement"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-17.3"/>The fixed Statement</h2></div></div></div><div class="informaltable"><a id="ch02-195-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt241"/><pre class="programlisting">fixed ([<em class="replaceable"><code>value type</code></em> | void ]* <em class="replaceable"><code>name</code></em> = [&amp;]? <em class="replaceable"><code>expression</code></em> )
  <em class="replaceable"><code>statement-block</code></em></pre></td></tr></tbody></table></div><p>The <code class="literal">fixed</code><a class="indexterm" id="IXT-2-218289"/> statement is required to pin a managed object, such as
        the bitmap in the previous pointer example. During the execution of a
        program, many objects are allocated and deallocated from the heap. In
        order to avoid the unnecessary waste or fragmentation of memory, the
        garbage collector moves objects around. Pointing to an object would be
        futile if its address can change while referencing it, so the <code class="literal">fixed</code> statement tells the garbage collector
        to pin the object and not move it around. This can impact the
        efficiency of the runtime, so <code class="literal">fixed</code>
        blocks should be used only briefly, and preferably, heap allocation
        should be avoided within the <code class="literal">fixed</code>
        block.</p><p>C# returns a pointer only from a value type, never directly from
        a reference type. Arrays and strings are an exception to this, but
        only syntactically, since they actually return a pointer to their
        first element (which must be a value type), rather than the objects
        themselves.</p><p>Value types declared inline within reference types require the
        reference type to be pinned, as follows:</p><a id="I_2_tt242"/><pre class="programlisting">using System;
class Test {
  int x;
  static void Main(  ) {
    Test test = new Test(  );
    unsafe {
       fixed(int* p = &amp;test.x) { // pins Test
         *p = 9;
       }
       System.Console.WriteLine(test.x);
    }
  }
}</pre></div><div class="sect2" title="Pointer to Member Operator"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-17.4"/>Pointer to Member Operator</h2></div></div></div><p>In <a class="indexterm" id="IXT-2-218290"/>addition to the <code class="literal">&amp;</code>
        and <code class="literal">*</code> operators, C# also provides
        the C++-style <code class="literal">-&gt;</code> operator, which
        can be used on structs:</p><a id="I_2_tt243"/><pre class="programlisting">using System;
struct Test {
   int x;
   unsafe static void Main(  ) {
      Test test = new Test(  );
      Test* p = &amp;test;
      p-&gt;x = 9;
      System.Console.WriteLine(test.x);
   }
}</pre></div><div class="sect2" title="The stackalloc Keyword"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-17.5"/>The stackalloc Keyword</h2></div></div></div><p>Memory can be allocated in a block on the stack explicitly using
        the <code class="literal">stackalloc</code><a class="indexterm" id="IXT-2-218291"/> keyword. Since it is allocated on the stack, its
        lifetime is limited to the execution of the method in which it is
        used, just as with other local variables. The block may use <code class="literal">[]</code><a class="indexterm" id="IXT-2-218292"/> indexing but is purely a value type with no additional
        self-describing information or bounds checking, which an array
        provides:</p><a id="I_2_tt244"/><pre class="programlisting">int* a = stackalloc int [10];
for (int i = 0; i &lt; 10; ++i)
   Console.WriteLine(a[i]); // print raw memory</pre></div><div class="sect2" title="void*"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-17.6"/>void*</h2></div></div></div><p>Rather than pointing to a specific value type, a pointer may
        make no assumptions about the type of the underlying data. This is
        useful for functions that deal with raw memory. An implicit conversion
        exists from any pointer type to a <code class="literal">void*</code>. A <code class="literal">void*</code><a class="indexterm" id="IXT-2-218293"/> cannot be dereferenced, and arithmetic operations
        cannot be performed on void pointers. For example:</p><a id="I_2_tt245"/><pre class="programlisting">class Test {
  unsafe static void Main (  ) {
    short[  ] a = {1,1,2,3,5,8,13,21,34,55};
      fixed (short* p = a) {
        // sizeof returns size of value-type in bytes
        Zap (p, a.Length * sizeof (short));
      }
    foreach (short x in a)
      System.Console.WriteLine (x); // prints all zeros
  }
  unsafe static void Zap (void* memory, int byteCount) {
    byte* b = (byte*)memory;
      for (int i = 0; i &lt; byteCount; i++)
        *b++ = 0;
  }
}</pre></div><div class="sect2" title="Pointers to Unmanaged Code"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-17.7"/>Pointers to Unmanaged Code</h2></div></div></div><p>Pointers <a class="indexterm" id="IXT-2-218294"/>are also useful for accessing data outside the managed
        heap, such as when interacting with C DLLs or COM or when dealing with
        data not in the main memory, such as graphics memory or a storage
        medium on an embedded <a class="indexterm" id="IXTR3-17"/>
        <a class="indexterm" id="IXTR3-18"/>device.</p></div></div></body></html>