<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Statements</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Statements"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-2-SECT-5"/>Statements</h1></div></div></div><p>Execution of a C# program is specified by a series of
      <span class="emphasis"><em>statements</em></span> that execute sequentially in the textual
      order in which they appear. All statements in a procedural-based
      language such as C# are executed for their effect. The two most basic
      kinds of statement in C# are the <span class="emphasis"><em>declaration</em></span> and
      <span class="emphasis"><em>expression</em></span> statements. C# also provides flow
      control statements for selection, looping, and jumping. Finally, C#
      provides statements for special purposes, such as locking memory or
      handling exceptions.</p><p>So that multiple statements can be grouped together, zero or more
      statements may be enclosed in braces <code class="literal">({</code> <code class="literal">})</code> to
      form a statement block. A<a class="indexterm" id="IXT-2-218022"/> statement block can be used anywhere a single statement
      is valid.</p><div class="sect2" title="Expression Statements"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-5.1"/>Expression Statements</h2></div></div></div><div class="informaltable"><a id="ch02-41-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt91"/><pre class="programlisting">[<em class="replaceable"><code>variable</code></em> =]? <em class="replaceable"><code>expression</code></em>;</pre></td></tr></tbody></table></div><p>An <span class="emphasis"><em>expression statement</em></span><a class="indexterm" id="IXT-2-218023"/> evaluates an expression either by assigning its result
        to a variable or generating side effects, (i.e., invocation, <code class="literal">new</code>, <code class="literal">++</code>,
        or <code class="literal">--</code>). An expression statement
        ends in a <a class="indexterm" id="IXT-2-218024"/> <a class="indexterm" id="IXT-2-218025"/>semicolon (<code class="literal">;</code>). For
        example:</p><a id="I_2_tt92"/><pre class="programlisting">x = 5 + 6; // assign result
x++; // side effect
y = Math.Min(x, 20); // side effect and assign result
Math.Min (x, y); // discards result, but ok, there is a side effect
x == y; // error, has no side effect, and does not assign result</pre></div><div class="sect2" title="Declaration Statements"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-5.2"/>Declaration Statements</h2></div></div></div><div class="informaltable"><a id="ch02-43-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Variable declaration syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt93"/><pre class="programlisting">type [<em class="replaceable"><code>variable</code></em> [ = <em class="replaceable"><code>expression</code></em> ]?]+ ;</pre></td></tr></tbody></table></div><div class="informaltable"><a id="ch02-44-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Constant declaration syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt94"/><pre class="programlisting">const <em class="replaceable"><code>type</code></em> [<em class="replaceable"><code>variable</code></em> = <em class="replaceable"><code>constant-expression</code></em>]+ ;</pre></td></tr></tbody></table></div><p>A <span class="emphasis"><em>declaration statement</em></span> declares a new
        variable. You can initialize a variable at the time of its declaration
        by optionally assigning it the result of an expression.</p><p>The scope of a local or constant variable extends to the end of
        the current block. You can’t declare another local variable with the
        same name in the current block or in any nested blocks. For
        example:</p><a id="I_2_tt95"/><pre class="programlisting">bool a = true;
while(a) {
   int x = 5;
   if (x==5) {
      int y = 7;
      int x = 2; // error, x already defined
   }
   Console.WriteLine(y); // error, y is out of scope
}</pre><p>A <span class="emphasis"><em>constant declaration</em></span><a class="indexterm" id="IXT-2-218026"/> is like a variable declaration, except that the value
        of the variable can’t be changed after it has been declared:</p><a id="I_2_tt96"/><pre class="programlisting">const double speedOfLight = 2.99792458E08;
speedOfLight+=10; // error</pre></div><div class="sect2" title="Empty Statements"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-5.3"/>Empty Statements</h2></div></div></div><div class="informaltable"><a id="ch02-46-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt97"/><pre class="programlisting">;</pre></td></tr></tbody></table></div><p>The <span class="emphasis"><em>empty</em></span><a class="indexterm" id="IXT-2-218027"/> <a class="indexterm" id="IXT-2-218028"/> <a class="indexterm" id="IXT-2-218029"/> <a class="indexterm" id="IXT-2-218030"/> <span class="emphasis"><em>statement</em></span> does nothing. It is used
        as a placeholder when no operations need to be performed, but a
        statement is nevertheless required. For example:</p><a id="I_2_tt98"/><pre class="programlisting">while(!thread.IsAlive);</pre></div><div class="sect2" title="Selection Statements"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-5.4"/>Selection Statements</h2></div></div></div><p>C# <a class="indexterm" id="IXT-2-218031"/>has many ways to conditionally control the flow of
        program execution. This section covers the simplest two constructs,
        the <code class="literal">if-else</code> statement and the
        <code class="literal">switch</code> statement. In addition, C#
        also provides a conditional operator and loop statements that
        conditionally execute based on a Boolean expression. Finally, C#
        provides object-oriented ways of conditionally controlling the flow of
        execution, namely virtual method invocations and delegate
        invocations.</p><div class="sect3" title="if-else statement"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-5.4.1"/>if-else statement</h3></div></div></div><div class="informaltable"><a id="ch02-49-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt99"/><pre class="programlisting">if (<em class="replaceable"><code>Boolean-expression</code></em>)
  <em class="replaceable"><code>statement-or-statement-block</code></em>
[ else
  <em class="replaceable"><code>statement-or-statement-block</code></em> ]?</pre></td></tr></tbody></table></div><p>An <code class="literal">if-else</code><a class="indexterm" id="IXT-2-218032"/> statement executes code depending on whether a
          Boolean expression is <code class="literal">true</code>.
          Unlike C, C# permits only a Boolean expression. For example:</p><a id="I_2_tt100"/><pre class="programlisting">int Compare(int a, int b) {
   if (a&gt;b)
      return 1;
   else if (a&lt;b)
      return -1;
   return 0;
}</pre></div><div class="sect3" title="switch statement"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-5.4.2"/>switch statement</h3></div></div></div><div class="informaltable"><a id="ch02-50-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt101"/><pre class="programlisting">switch (<em class="replaceable"><code>expression</code></em>) {
[ case <em class="replaceable"><code>constant-expression</code></em> : <em class="replaceable"><code>statement</code></em>* ]*
[ default : <em class="replaceable"><code>statement</code></em>* ]?
}</pre></td></tr></tbody></table></div><p><code class="literal">switch</code><a class="indexterm" id="IXT-2-218033"/> statements let you branch program execution based on
          the value of a variable. <code class="literal">switch</code>
          statements can result in cleaner code than if you use multiple
          <code class="literal">if</code> statements, because the
          controlling expression is evaluated only once. For instance:</p><a id="I_2_tt102"/><pre class="programlisting">void Award(int x) {
  switch(x) {
    case 1:
      Console.WriteLine("Winner!");
      break;
    case 2:
      Console.WriteLine("Runner-up");
      break;
    case 3:
    case 4:
      Console.WriteLine("Highly commended");
      break;
    default:
      Console.WriteLine("Don't quit your day job!");
      break;
  }
}</pre><p>The <code class="literal">switch</code> statement can
          evaluate only a predefined type (including the <code class="literal">string</code><a class="indexterm" id="IXT-2-218034"/> <a class="indexterm" id="IXT-2-218035"/> type) or enum, though user-defined types may provide
          an implicit conversion to these types.</p><p>After a particular <code class="literal">case</code>
          statement is executed, control doesn’t automatically continue to the
          next statement or break out of the <code class="literal">switch</code> statement. Instead, you must
          explicitly control execution, typically by ending each <code class="literal">case</code> statement with a <code class="literal">jump</code> statement. The options are:</p><div class="itemizedlist"><ul class="itemizedlist"><li class="listitem"><p>Use the <code class="literal">break</code><a class="indexterm" id="IXT-2-218036"/> statement to jump to the end of the <code class="literal">switch</code> statement (this is by far the
              most common option).</p></li><li class="listitem"><p>Use the <code class="literal">goto</code><a class="indexterm" id="IXT-2-218037"/> <code class="literal">case</code>
              <span class="emphasis"><em>&lt;</em></span><em class="replaceable"><code>constant</code></em><em class="replaceable"><code>expression</code></em><span class="emphasis"><em>&gt;</em></span>
              or <code class="literal">goto</code> <code class="literal">default</code> statements to jump to either
              another <code class="literal">case</code> statement or the
              default <code class="literal">case</code>
              statement.</p></li><li class="listitem"><p>Use any other <code class="literal">jump</code>
              statement, namely <code class="literal">return</code>,
              <code class="literal">throw</code><a class="indexterm" id="IXT-2-218038"/>, <code class="literal">continue</code><a class="indexterm" id="IXT-2-218039"/>, or <code class="literal">goto</code><a class="indexterm" id="IXT-2-218040"/> <em class="replaceable"><code>label</code></em>.</p></li></ul></div><p>Unlike in Java and C++, the end of a <code class="literal">case</code> statement must explicitly state where
          to go next. There is no error-prone “default fall through” behavior,
          so not specifying a jump statement results in the next <code class="literal">case</code> statement being executed:</p><a id="I_2_tt103"/><pre class="programlisting">void Greet(string title) {
  switch (title) {
    case null:
      Console.WriteLine("And you are?");
      goto default;
    case "King":
      Console.WriteLine("Greetings your highness");
      // error, should specify break, otherwise...
    default :
      Console.WriteLine("How's it hanging?");
      break;
  }
}</pre></div></div><div class="sect2" title="Loop Statements"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-5.5"/>Loop Statements</h2></div></div></div><p>C# <a class="indexterm" id="IXT-2-218041"/>enables a group of statements to be executed repeatedly
        using the <code class="literal">while</code>, <code class="literal">do</code> <code class="literal">while</code>,
        and <code class="literal">for</code> statements.</p><div class="sect3" title="while loops"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-5.5.1"/>while loops</h3></div></div></div><div class="informaltable"><a id="ch02-53-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt104"/><pre class="programlisting">while (Boolean-expression)
  <em class="replaceable"><code>statement-or-statement-block</code></em></pre></td></tr></tbody></table></div><p><code class="literal">while</code><a class="indexterm" id="IXT-2-218042"/> loops repeatedly execute a statement block while a
          given Boolean expression remains <code class="literal">true</code>. The expression is tested before the
          statement block is executed:</p><a id="I_2_tt105"/><pre class="programlisting">int i = 0;
while (i&lt;5) {
  i++;
}</pre></div><div class="sect3" title="do-while loops"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-5.5.2"/>do-while loops</h3></div></div></div><div class="informaltable"><a id="ch02-55-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt106"/><pre class="programlisting">do 
  <em class="replaceable"><code>statement-or-statement-block</code></em>
while (<em class="replaceable"><code>Boolean-expression</code></em>);</pre></td></tr></tbody></table></div><p><code class="literal">do-while</code><a class="indexterm" id="IXT-2-218043"/> loops differ functionally from <code class="literal">while</code> loops only in that they test the
          controlling Boolean expression after the statement block has
          executed. Here’s an example:</p><a id="I_2_tt107"/><pre class="programlisting">int i = 0;
do
  i++;
while (i&lt;5);</pre></div><div class="sect3" title="for loops"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-5.5.3"/>for loops</h3></div></div></div><div class="informaltable"><a id="ch02-57-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt108"/><pre class="programlisting">for (<em class="replaceable"><code>statement</code></em>?; <em class="replaceable"><code>Boolean-expression</code></em>?; <em class="replaceable"><code>statement</code></em>?)
  <em class="replaceable"><code>statement-or-statement-block</code></em></pre></td></tr></tbody></table></div><p><code class="literal">for</code><a class="indexterm" id="IXT-2-218044"/> loops can be more convenient than <code class="literal">while</code> loops when you need to maintain an
          iterator value. As in Java and C, <code class="literal">for</code> loops contain three parts. The first
          part is a statement executed before the loop begins, and by
          convention, it initializes an iterator variable. The second part is
          a Boolean expression that, while <code class="literal">true</code>, permits the statement block to
          execute. The third part is a statement that is executed after each
          iteration of the statement block and, by convention, increments an
          iterator variable. Here’s an example:</p><a id="I_2_tt109"/><pre class="programlisting">for (int i=0; i&lt;10; i++)
  Console.WriteLine(i);</pre><p>Any of the three parts of the <code class="literal">for</code> statement may be omitted. You can
          implement an infinite loop like this, though alternatively a
          <code class="literal">while</code> (<code class="literal">true</code>) statement has the same
          result:</p><a id="I_2_tt110"/><pre class="programlisting">for (;;)
  Console.WriteLine("Hell ain't so bad");</pre></div><div class="sect3" title="foreach loops"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-5.5.4"/>foreach loops</h3></div></div></div><div class="informaltable"><a id="ch02-59-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt111"/><pre class="programlisting">foreach ( <em class="replaceable"><code>type-value</code></em> in IEnumerable )
  <em class="replaceable"><code>statement-or-statement-block</code></em></pre></td></tr></tbody></table></div><p>It’s <a class="indexterm" id="IXT-2-218045"/>common to use <code class="literal">for</code>
          loops to iterate over a collection, so C#, like Visual Basic,
          includes a <code class="literal">foreach</code> statement. For
          instance, instead of doing this:</p><a id="I_2_tt112"/><pre class="programlisting">for (int i=0; i&lt;dynamite.Length; i++)
  Console.WriteLine(dynamite [i]);</pre><p>you can do this:</p><a id="I_2_tt113"/><pre class="programlisting">foreach (Stick stick in dynamite)
  Console.WriteLine(stick);</pre><p>The <code class="literal">foreach</code> statement works
          on any <a class="indexterm" id="IXT-2-218046"/>collection (including <a class="indexterm" id="IXT-2-218047"/>arrays). Although not strictly necessary, all
          collections leverage this functionality by implementing the <code class="literal">IEnumerable</code><a class="indexterm" id="IXT-2-218048"/> and <code class="literal">IEnumerator</code>
          interfaces, which are explained in <a class="link" href="ch03s04.html" title="Collections">Section 3.4</a> in <a class="link" href="ch03.html" title="Chapter 3. Programming the.NET Framework">Chapter 3</a>. Here is an equivalent
          way to iterate over the collection:</p><a id="I_2_tt114"/><pre class="programlisting">IEnumerator ie = dynamite.GetEnumerator(  );
while (ie.MoveNext(  )) {
  Stick stick = (Stick)ie.Current;
  Console.WriteLine(stick);
}</pre></div></div><div class="sect2" title="jump Statements"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-5.6"/>jump Statements</h2></div></div></div><p>The C# <code class="literal">jump</code><a class="indexterm" id="IXT-2-218049"/> statements are: <code class="literal">break</code>, <code class="literal">continue</code>, <code class="literal">goto</code>, <code class="literal">return</code>, and <code class="literal">throw</code>. All <code class="literal">jump</code> statements obey sensible restrictions
        imposed by <code class="literal">try</code> statements (see
        <a class="link" href="ch02s15.html" title="try Statements and Exceptions">Section 2.15</a> later in
        this chapter). First, a <code class="literal">jump</code> out of
        a <code class="literal">try</code> block always executes the
        <code class="literal">try</code>’s <code class="literal">finally</code> block before reaching the target of
        the jump. Second, a jump can’t be made from the inside to the outside
        of a <code class="literal">finally</code> block.</p><div class="sect3" title="break statement"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-5.6.1"/>break statement</h3></div></div></div><div class="informaltable"><a id="ch02-62-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt115"/><pre class="programlisting">break;</pre></td></tr></tbody></table></div><p>The <code class="literal">break</code><a class="indexterm" id="IXT-2-218050"/> statement transfers execution from the enclosing
          <code class="literal">while</code> loop, <code class="literal">for</code> loop, or <code class="literal">switch</code> statement block to the next
          statement block:</p><a id="I_2_tt116"/><pre class="programlisting">int x = 0;
while (true) {
  x++;
  if (x&gt;5)
    break; // break from the loop
}</pre></div><div class="sect3" title="continue statement"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-5.6.2"/>continue statement</h3></div></div></div><div class="informaltable"><a id="ch02-64-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt117"/><pre class="programlisting">continue;</pre></td></tr></tbody></table></div><p>The <code class="literal">continue</code><a class="indexterm" id="IXT-2-218051"/> statement forgoes the remaining statements in the
          loop and makes an early start on the next iteration:</p><a id="I_2_tt118"/><pre class="programlisting">int x = 0;
int y = 0;
while (y&lt;100) {
  x++;
  if ((x%7)==0)
    continue; // continue with next iteration
  y++;
}</pre></div><div class="sect3" title="goto statement"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-5.6.3"/>goto statement</h3></div></div></div><div class="informaltable"><a id="ch02-66-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt119"/><pre class="programlisting">goto <em class="replaceable"><code>statement-label</code></em>;
goto <em class="replaceable"><code>case-constant</code></em>;</pre></td></tr></tbody></table></div><p>The<a class="indexterm" id="IXT-2-218052"/> <code class="literal">goto</code> statement
          transfers execution to another <span class="emphasis"><em>label</em></span> within the
          statement block. A label statement is just a placeholder in a
          method:</p><a id="I_2_tt120"/><pre class="programlisting">int x = 4;
start:
x++;
if (x==5)
 goto start;</pre><p>The <code class="literal">goto</code> <code class="literal">case</code><a class="indexterm" id="IXT-2-218053"/> statement transfers execution to another case
          <span class="emphasis"><em>label</em></span> in a <code class="literal">switch</code> block (as explained in <a class="link" href="ch02s05.html#csharpess2-CHP-2-SECT-5.4.2" title="switch statement">Section
          2.5.4.2</a>).</p></div><div class="sect3" title="return statement"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-5.6.4"/>return statement</h3></div></div></div><div class="informaltable"><a id="ch02-68-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt121"/><pre class="programlisting">return <em class="replaceable"><code>expression?</code></em>;</pre></td></tr></tbody></table></div><p>The <code class="literal">return</code><a class="indexterm" id="IXT-2-218054"/> statement exits a method. If the method is non-void,
          it must return an expression of the method’s <code class="literal">return</code> type:</p><a id="I_2_tt122"/><pre class="programlisting">int CalcX(int a) {
  int x = a * 100;
  return x; // return to the calling method with value
}</pre></div><div class="sect3" title="throw statement"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-5.6.5"/>throw statement</h3></div></div></div><div class="informaltable"><a id="ch02-70-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt123"/><pre class="programlisting">throw <em class="replaceable"><code>exception-expression?</code></em>;</pre></td></tr></tbody></table></div><p>The <code class="literal">throw</code><a class="indexterm" id="IXT-2-218055"/> statement throws an <code class="literal">Exception</code>to indicate that an abnormal
          condition has occurred (see <a class="link" href="ch02s15.html" title="try Statements and Exceptions">Section 2.15</a> later in this
          chapter):</p><a id="I_2_tt124"/><pre class="programlisting">if (w==null)
  throw new Exception("w can't be null");</pre></div><div class="sect3" title="lock statement"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-5.6.6"/>lock statement</h3></div></div></div><div class="informaltable"><a id="ch02-72-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt125"/><pre class="programlisting">lock (<em class="replaceable"><code>expression</code></em>)
  <em class="replaceable"><code>statement-or-statement-block</code></em></pre></td></tr></tbody></table></div><p>The <code class="literal">lock</code><a class="indexterm" id="IXT-2-218056"/> statement is actually a syntactic shortcut for
          calling the <code class="literal">Enter</code> and <code class="literal">Exit</code> methods of the <code class="literal">Monitor</code> class (see <a class="link" href="ch03s08.html" title="Threading">Section 3.8</a> in <a class="link" href="ch03.html" title="Chapter 3. Programming the.NET Framework">Chapter 3</a>).</p></div><div class="sect3" title="using statement"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-2-SECT-5.6.7"/>using statement</h3></div></div></div><div class="informaltable"><a id="ch02-74-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a class="indexterm" id="IXT-2-218057"/> <a id="I_2_tt126"/><pre class="programlisting">using (<em class="replaceable"><code>declaration-expression</code></em>)
   <em class="replaceable"><code>statement-or-statement-block</code></em></pre></td></tr></tbody></table></div><p>Many classes encapsulate non-memory resources, such as file
          handles, graphics handles, or database connections. These classes
          implement <code class="literal">System.IDisposable</code>,
          which defines a single parameterless method named <code class="literal">Dispose</code> that is called to clean up these
          resources. The <code class="literal">using</code> statement
          provides an elegant syntax for declaring, then calling, the <code class="literal">Dispose</code> method of variables that implement
          <code class="literal">IDisposable</code>. For example:</p><a id="I_2_tt127"/><pre class="programlisting">using (FileStream fs = new FileStream (fileName, FileMode.Open)) {
  // do something with fs
}</pre><p>This is precisely equivalent to:</p><a id="I_2_tt128"/><pre class="programlisting">FileStream fs = new FileStream (fileName, FileMode.Open);
try {
  // do something with fs
} finally {
  if (fs != null)
  ((IDisposable)fs).Dispose(  );
}</pre></div></div></div></body></html>