<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Interfaces</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Interfaces"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-2-SECT-10"/>Interfaces</h1></div></div></div><div class="informaltable"><a id="ch02-142-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_2_tt191"/><pre class="programlisting"><em class="replaceable"><code>attributes?</code></em> unsafe? <em class="replaceable"><code>access-modifier?</code></em>
new?
interface <em class="replaceable"><code>interface-name</code></em> [ : <em class="replaceable"><code>base-interface+</code></em> ]?
{ <em class="replaceable"><code>interface-members</code></em> }</pre></td></tr></tbody></table></div><p>An <a class="indexterm" id="csharpess2-IDXTERM-401"/>interface is like a class, but with these major
      differences:</p><div class="itemizedlist"><ul class="itemizedlist"><li class="listitem"><p>An interface provides a specification rather than an
          implementation for its members. This is similar to a pure <code class="literal">abstract</code> class, which is an abstract class
          consisting of only <a class="indexterm" id="IXT-2-218191"/>abstract members.</p></li><li class="listitem"><p>A class and struct can implement multiple interfaces; a class
          can inherit only from a single class.</p></li><li class="listitem"><p>A struct can implement an interface but can’t inherit from a
          class.</p></li></ul></div><p>Earlier we defined polymorphism as the ability to perform the same
      operations on many types, as long as each type shares a common subset of
      characteristics. The purpose of an interface is precisely for defining
      such a set of characteristics.</p><p>An interface is comprised of one or more methods, properties,
      indexers, and events. These members are always implicitly public and
      implicitly abstract (therefore virtual and nonstatic).</p><div class="sect2" title="Defining an Interface"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-10.1"/>Defining an Interface</h2></div></div></div><p>An <a class="indexterm" id="IXT-2-218192"/>interface declaration is like a class declaration, but
        it provides no implementation for its members, since all its members
        are implicitly abstract. These members are intended to be implemented
        by a class or struct that implements the interface.</p><p>Here’s a simple interface that defines a single method:</p><a id="I_2_tt192"/><pre class="programlisting">public interface IDelete {
  void Delete(  );
}</pre></div><div class="sect2" title="Implementing an Interface"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-10.2"/>Implementing an Interface</h2></div></div></div><p>Classes <a class="indexterm" id="IXT-2-218193"/>or structs that implement an interface may be said to
        “fulfill the contract of the interface.” In this example, GUI controls
        that support the concept of deleting, such as a <code class="literal">TextBox</code> or <code class="literal">TreeView</code>, or your own custom GUI control,
        can implement the <code class="literal">IDelete</code>
        interface:</p><a id="I_2_tt193"/><pre class="programlisting">public class TextBox : IDelete {
  public void Delete(  ) {...}
}
public class TreeView : IDelete {
  public void Delete(  ) {...}
}</pre><p>If a class inherits from a base class, the name of each
        interface to be implemented must appear after the base-class
        name:</p><a id="I_2_tt194"/><pre class="programlisting">public class TextBox : Control, IDelete {...}
public class TreeView : Control, IDelete {...}</pre></div><div class="sect2" title="Using an Interface"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-10.3"/>Using an Interface</h2></div></div></div><p>An interface is useful when you need multiple classes to share
        characteristics not present in a common base class. In addition, an
        interface is a good way to ensure that these classes provide their own
        implementation for the interface member, since interface members are
        implicitly abstract.</p><p>The following example assumes a form containing many GUI
        controls, including some <code class="literal">TextBox</code>
        and <code class="literal">TreeView</code> controls, in which the
        currently focused control is accessed with the <code class="literal">ActiveControl</code> property. When a user clicks
        <code class="literal">Delete</code> on a menu item or a toolbar
        button, you test to see if <code class="literal">ActiveControl</code> implements <code class="literal">IDelete</code>, and if so, cast it to <code class="literal">IDelete</code> to call its <code class="literal">Delete</code> method:</p><a id="I_2_tt195"/><pre class="programlisting">class MyForm {
   ...
   void DeleteClick(  ) {
      if (ActiveControl is IDelete) {
         IDelete d = (IDelete)ActiveControl;
         d.Delete(  );
      }
   }
}</pre></div><div class="sect2" title="Extending an Interface"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-10.4"/>Extending an Interface</h2></div></div></div><p>Interfaces <a class="indexterm" id="IXT-2-218194"/>may extend other interfaces. For instance:</p><a id="I_2_tt196"/><pre class="programlisting">interface ISuperDelete : IDelete {
   bool CanDelete {get;}
   event EventHandler CanDeleteChanged;
}</pre><p>In implementing the <code class="literal">ISuperDelete</code> interface, an <code class="literal">ActiveControl</code> implements the <code class="literal">CanDelete</code> property to indicate it has
        something to delete and isn’t read-only. The control also implements
        the <code class="literal">CanDeleteChanged</code> event to fire
        an event whenever its <code class="literal">CanDelete</code>
        property changes. This framework lets the application ghost its
        <code class="literal">Delete</code> menu item and toolbar button
        when the <code class="literal">ActiveControl</code> is unable to
        delete.</p></div><div class="sect2" title="Explicit Interface Implementation"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-10.5"/>Explicit Interface Implementation</h2></div></div></div><p>If <a class="indexterm" id="IXT-2-218195"/>there is a name collision between an interface member
        and an existing member in the class or struct, C# allows you to
        explicitly implement an interface member to resolve the conflict. In
        this example, we resolve a conflict that arises when we implement two
        interfaces that each define a <code class="literal">Delete</code> method:</p><a id="I_2_tt197"/><pre class="programlisting">public interface IDesignTimeControl {
   ...
   object Delete(  );
}
public class TextBox : IDelete, IDesignTimeControl {
   ...
   void IDelete.Delete(  ) {...}
   object IDesignTimeControl.Delete(  ) {...}
   // Note that explicitly implementing just one of them would
   // be enough to resolve the conflict
}</pre><p>Unlike implicit interface implementations, explicit interface
        implementations can’t be declared with <code class="literal">abstract</code>, <code class="literal">virtual</code>, <code class="literal">override</code>, or <code class="literal">new</code> modifiers. In addition, they are
        implicitly <code class="literal">public</code>, while an
        implicit implementation requires the use of the <code class="literal">public</code> modifier. However, to access the
        method, the class or struct must be cast to the appropriate interface
        first:</p><a id="I_2_tt198"/><pre class="programlisting">TextBox tb = new TextBox(  );
IDesignTimeControl idtc = (IDesignTimeControl)tb;
IDelete id = (IDelete)tb;
idtc.Delete(  );
id.Delete(  );</pre></div><div class="sect2" title="Reimplementing an Interface"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-10.6"/>Reimplementing an Interface</h2></div></div></div><p>If a <a class="indexterm" id="IXT-2-218196"/>base class implements an interface member with the
        <code class="literal">virtual</code> (or <code class="literal">abstract</code>) modifier, a derived class can
        override it. If not, the derived class must reimplement the interface
        to override that member:</p><a id="I_2_tt199"/><pre class="programlisting">public class RichTextBox : TextBox, IDelete {
   // TextBox's IDelete.Delete is not virtual (since explicit
   // interface implementations cannot be virtual)
   public void Delete(  ) {}
}</pre><p>The implementation in this example lets you use a <code class="literal">RichTextBox</code> object as an <code class="literal">IDelete</code> object and call <code class="literal">RichTextBox</code>’s version of <code class="literal">Delete</code>.</p></div><div class="sect2" title="Interface Conversions"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-10.7"/>Interface Conversions</h2></div></div></div><p>A <a class="indexterm" id="IXT-2-218197"/> <a class="indexterm" id="IXT-2-218198"/>class or struct T can be implicitly cast to an interface
        I that T implements. Similarly, an interface X can be implicitly cast
        to an interface Y that X inherits from. An interface can be explicitly
        cast to any other interface or nonsealed class. However, an explicit
        cast from an interface I to a sealed class or struct T is permitted
        only if T can implement I. For example:</p><a id="I_2_tt200"/><pre class="programlisting">interface IDelete {...}
interface IDesignTimeControl {...}
class TextBox : IDelete, IDesignTimeControl {...}
sealed class Timer : IDesignTimeControl {...}

TextBox tb1 = new TextBox (  );
IDelete d = tb1; // implicit cast
IDesignTimeControl dtc = (IDesignTimeControl)d;
TextBox tb2 = (TextBox)dtc;
Timer t = (Timer)d; // illegal, a Timer can never implement IDelete</pre><p>Standard boxing conversions happen when converting between
        structs and <a class="indexterm" id="IXTR3-10"/>interfaces.</p></div></div></body></html>