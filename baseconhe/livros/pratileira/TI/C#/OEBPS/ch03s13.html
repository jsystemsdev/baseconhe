<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Interop with Native DLLs</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Interop with Native DLLs"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-3-SECT-13"/>Interop with Native DLLs</h1></div></div></div><p>PInvoke,<a class="indexterm" id="csharpess2-IDXTERM-923"/> <a class="indexterm" id="csharpess2-IDXTERM-924"/> <a class="indexterm" id="csharpess2-IDXTERM-925"/> <a class="indexterm" id="csharpess2-IDXTERM-926"/> short for<a class="indexterm" id="IXT-3-218779"/> <a class="indexterm" id="IXT-3-218780"/> Platform Invocation Services, lets C# access functions,
      structs, and callbacks in unmanaged DLLs. For example, perhaps you wish
      to call the <code class="literal">MessageBox</code> function in
      the Windows <code class="literal">user32.dll</code>:</p><a id="I_3_tt355"/><pre class="programlisting">int MessageBox(HWND hWnd, LPCTSTR lpText, 
               LPCTSTR lpCation, UINT uType);</pre><p>To call this function, write a <code class="literal">static</code> <code class="literal">extern</code> method decorated with the <code class="literal">DllImport</code>attribute:</p><a id="I_3_tt356"/><pre class="programlisting">using System.Runtime.InteropServices;
class MsgBoxTest {
  [DllImport("user32.dll")]
  static extern int MessageBox(int hWnd, string text, 
                               string caption, int type);
  public static void Main(  ) {
    MessageBox(0, "Please do not press this button again.",
                  "Attention", 0);
  }
}</pre><p>PInvoke then finds and loads the required Win32 DLLs and resolves
      the entry point of the requested function. The CLR includes a marshaler
      that knows how to convert parameters and return values between .NET
      types and unmanaged types. In this example the <code class="literal">int</code> parameters translate directly to four-byte
      integers that the function expects, and the <code class="literal">string</code> parameters are converted to
      null-terminated arrays of characters using one-byte ANSI characters
      under Win9x or two-byte Unicode characters under Windows NT, 2000, and
      XP.</p><div class="sect2" title="Marshaling Common Types"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-13.1"/>Marshaling Common Types</h2></div></div></div><p>The <a class="indexterm" id="IXT-3-218781"/> <a class="indexterm" id="IXT-3-218782"/>CLR marshaler is a .NET facility that knows about the
        core types used by COM and the Windows API and provides default
        translations to CLR types for you. The <code class="literal">bool</code> type, for instance, can be translated
        into a two-byte Windows <code class="literal">BOOL</code> type
        or a four-byte <code class="literal">Boolean</code> type. You
        can override a default translation using the <code class="literal">MarshalAs</code> attribute:</p><a id="I_3_tt357"/><pre class="programlisting">using System.Runtime.InteropServices;
static extern int Foo([MarshalAs(UnmanagedType.LPStr)]
                      string s);</pre><p>In this case, the marshaler was told to use <code class="literal">LPStr</code>, so it will always use ANSI
        characters. Array classes and the <code class="literal">StringBuilder</code> class will copy the marshaled
        value from an external function back to the managed value, as
        follows:</p><a id="I_3_tt358"/><pre class="programlisting">using System;
using System.Text;
using System.Runtime.InteropServices;
class Test {
  [DllImport("kernel32.dll")]
  static extern int GetWindowsDirectory(StringBuilder sb,
                                        int maxChars);
   static void Main(  ) {
      StringBuilder s = new StringBuilder(256);
      GetWindowsDirectory(s, 256);
      Console.WriteLine(s);
   }
}</pre></div><div class="sect2" title="Marshaling Classes and Structs"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-13.2"/>Marshaling Classes and Structs</h2></div></div></div><p>Passing <a class="indexterm" id="IXT-3-218783"/> <a class="indexterm" id="IXT-3-218784"/> <a class="indexterm" id="IXT-3-218785"/>a class or struct to a C function requires marking the
        struct or class with the <code class="literal">StructLayout</code> attribute:</p><a id="I_3_tt359"/><pre class="programlisting">using System;
using System.Runtime.InteropServices;
[StructLayout(LayoutKind.Sequential)]
struct SystemTime {
   public ushort wYear; 
   public ushort wMonth;
   public ushort wDayOfWeek; 
   public ushort wDay; 
   public ushort wHour; 
   public ushort wMinute; 
   public ushort wSecond; 
   public ushort wMilliseconds; 
}
class Test {
   [DllImport("kernel32.dll")]
   static extern void GetSystemTime(ref SystemTime t);
   static void Main(  ) {
      SystemTime t = new SystemTime(  );
      GetSystemTime(ref t);
      Console.WriteLine(t.wYear);
   }
}</pre><p>In both C and C#, fields in an object are located at
        <span class="emphasis"><em>n</em></span> number of bytes from the address of that
        object. The difference is that a C# program finds this offset by
        looking it up using the field name; C field names are compiled
        directly into offsets. For instance, in C, <code class="literal">wDay</code> is just a token to represent whatever
        is at the address of a <code class="literal">SystemTime</code>
        instance plus 24 bytes.</p><p>For access speed and future widening of a datatype, these
        offsets are usually in multiples of a minimum width, called the
        <span class="emphasis"><em>pack size</em></span><a class="indexterm" id="IXT-3-218786"/>. For .NET types, the pack size is usually set at the
        discretion of the runtime, but by using the <code class="literal">StructLayout</code> attribute, field offsets can be
        controlled. The default pack size when using this attribute is 8
        bytes, but it can be set to 1, 2, 4, 8, or 16 bytes (pass <code class="literal">Pack=</code><em class="replaceable"><code>packsize</code></em> to
        the <code class="literal">StructLayout</code> constructor), and
        there are also explicit options to control individual field offsets.
        This lets a .NET type be passed to a C function.</p></div><div class="sect2" title="In and Out Marshaling"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-13.3"/>In and Out Marshaling</h2></div></div></div><p>The <a class="indexterm" id="IXT-3-218787"/>previous <code class="literal">Test</code> example
        works if <code class="literal">SystemTime</code> is a struct and
        <code class="literal">t</code> is a <code class="literal">ref</code> parameter, but is actually less
        efficient:</p><a id="I_3_tt360"/><pre class="programlisting">struct SystemTime {...}
static extern void GetSystemTime(ref SystemTime t);</pre><p>This is because the marshaler must always create fresh values
        for external parameters, so the previous method copies <code class="literal">t</code> when going <code class="literal">in</code> to the function and then copies the
        marshaled <code class="literal">t</code> when coming <code class="literal">out</code> of the function. By default,
        pass-by-value parameters are copied <code class="literal">in</code>, C# <code class="literal">ref</code> parameters are copied <code class="literal">in</code>/<code class="literal">out</code>,
        and C# <code class="literal">out</code> parameters are copied
        <code class="literal">out</code>, but there are exceptions for
        the types that have custom conversions. For instance, array classes
        and the <code class="literal">StringBuilder</code> class require
        copying when coming out of a function, so they are<code class="literal">in</code>/<code class="literal">out</code>.
        It is occasionally useful to override this behavior, with the <code class="literal">in</code> and <code class="literal">out</code> attributes. For example, if an array
        should be read-only, the <code class="literal">in</code>
        modifier indicates to copy only the array going into the function, and
        not the one coming out of it:</p><a id="I_3_tt361"/><pre class="programlisting">static extern void Foo([in] int[] array);</pre></div><div class="sect2" title="Callbacks from Unmanaged Code"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-13.4"/>Callbacks from Unmanaged Code</h2></div></div></div><p>C#<a class="indexterm" id="IXT-3-218788"/> <a class="indexterm" id="IXT-3-218789"/> can not only call C functions but can also be called by
        C functions, using callbacks. In C# a <code class="literal">delegate</code> type is used in place of a function
        pointer:</p><a id="I_3_tt362"/><pre class="programlisting">class Test {
   delegate bool CallBack(int hWnd, int lParam);
   [DllImport("user32.dll")]
   static extern int EnumWindows(CallBack hWnd, int lParam);
   static bool PrintWindow(int hWnd, int lParam) {
      Console.WriteLine(hWnd);
      return true;
   }
   static void Main(  ) {
      CallBack e = new CallBack(PrintWindow);
      EnumWindows(e, 0);
   }
}</pre></div><div class="sect2" title="Predefined Interop Support Attributes"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-13.5"/>Predefined Interop Support Attributes</h2></div></div></div><p>The<a class="indexterm" id="IXT-3-218790"/> FCL provides a set of attributes you can use to mark up
        your objects with information used by the CLR marshaling services to
        alter their default marshaling behavior.</p><p>This section describes the most common attributes you will need
        when interoperating with native Win32 DLLs. These attributes all exist
        in the <code class="literal">System.Runtime.InteropServices</code>
        namespace.</p><div class="sect3" title="DllImport attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-13.5.1"/>DllImport attribute</h3></div></div></div><a class="indexterm" id="IXT-3-218791"/><div class="informaltable"><a id="ch03-106-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt363"/><pre class="programlisting">[DllImport (<em class="replaceable"><code>dll-name</code></em>
  [, EntryPoint=<em class="replaceable"><code>function-name</code></em>]?
  [, CharSet=<em class="replaceable"><code>charset-enum</code></em>]?
  [, SetLastError=true|false]?
  [, ExactSpelling=true|false]?
  [, PreserveSig=true|false]?
  [, CallingConvention=<em class="replaceable"><code>callconv-enum</code></em>]?
)] <em class="lineannotation"><span class="lineannotation">(for methods)</span></em></pre></td></tr></tbody></table></div><p>The <code class="literal">DllImport</code> attribute
          annotates an external function that defines a DLL entry point. The
          parameters for this attribute are:</p><div class="variablelist"><dl><dt><span class="term"><a class="indexterm" id="IXT-3-218792"/><em class="replaceable"><code>dll-name</code></em></span></dt><dd><p>A string specifying the name of the DLL.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-3-218793"/><em class="replaceable"><code>function-name</code></em></span></dt><dd><p>A string specifying the function name in the DLL. This
                is useful if you want the name of your C# function to be
                different from the name of the DLL function.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-3-218794"/><em class="replaceable"><code>charset-enum</code></em></span></dt><dd><p>A <code class="literal">CharSet</code> enum,
                specifying how to marshal strings. The default value is
                <code class="literal">CharSet.Auto</code>, which
                converts strings to ANSI characters on Win9x and Unicode
                characters on Windows NT, 2000, and XP.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-3-218795"/><code class="literal">SetLastError</code></span></dt><dd><p>If <code class="literal">true</code>, preserves
                the Win32 error info. The default is <code class="literal">false</code>.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-3-218796"/><code class="literal">ExactSpelling</code></span></dt><dd><p>If <code class="literal">true</code>, the <code class="literal">EntryPoint</code> must exactly match the
                function. If <code class="literal">false</code>,
                name-matching heuristics are used. The default is <code class="literal">false</code>.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-3-218797"/><code class="literal">PreserveSig</code></span></dt><dd><p>If <code class="literal">true</code>, the method
                signature is preserved exactly as it was defined. If <code class="literal">false</code>, an HRESULT transformation is
                performed.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-3-218798"/><em class="replaceable"><code>callconv-enum</code></em></span></dt><dd><p>A <code class="literal">CallingConvention</code>
                enum, specifying the mode to use with the <code class="literal">EntryPoint</code>. The default is <code class="literal">StdCall</code>.</p></dd></dl></div></div><div class="sect3" title="StructLayout attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-13.5.2"/>StructLayout attribute</h3></div></div></div><div class="informaltable"><a id="ch03-108-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt364"/><pre class="programlisting">[StructLayout(<em class="replaceable"><code>layout-enum</code></em>
  [, Pack=<em class="replaceable"><code>packing-size</code></em>]?
  [, CharSet=<em class="replaceable"><code>charset-enum</code></em>]?
  [, Size=<em class="replaceable"><code>absolute-size</code></em>])?
] <em class="lineannotation"><span class="lineannotation">(for classes, structs)</span></em></pre></td></tr></tbody></table></div><p>The <code class="literal">StructLayout</code><a class="indexterm" id="IXT-3-218799"/> attribute specifies how the data members of a class
          or struct should be laid out in memory. Although this attribute is
          commonly used when declaring structures that are passed to or
          returned from native DLLs, it can also define data structures suited
          to file and network I/O. The parameters for this attribute
          are:</p><div class="variablelist"><dl><dt><span class="term"><a class="indexterm" id="IXT-3-218800"/><em class="replaceable"><code>layout-enum</code></em></span></dt><dd><p>A <code class="literal">LayoutKind</code> enum,
                which can be 1) <span class="emphasis"><em>sequential</em></span>, which lays
                out fields one after the next with a minimum pack size; 2)
                <span class="emphasis"><em>union</em></span>, which makes all fields have an
                offset of 0, so long as they are value types; or 3)
                <span class="emphasis"><em>explicit</em></span>, which lets each field have a
                custom offset.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-3-218801"/><em class="replaceable"><code>packing-size</code></em></span></dt><dd><p>An <code class="literal">int</code> specifying
                whether the packing size is 1, 2, 4, 8, or 16 bytes. The
                default value is 8.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-3-218802"/><em class="replaceable"><code>charset-enum</code></em></span></dt><dd><p>A <code class="literal">CharSet</code> enum,
                specifying how to marshal strings. The default value is
                <code class="literal">CharSet.Auto</code>, which
                converts strings to ANSI characters on Win9x and Unicode
                characters on Windows NT, 2000, and XP.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-3-218803"/><code class="literal">absolute-size</code></span></dt><dd><p>Specifies the size of the struct or class. This has to
                be at least as large as the sum of all the members.</p></dd></dl></div></div><div class="sect3" title="FieldOffset attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-13.5.3"/>FieldOffset attribute</h3></div></div></div><a class="indexterm" id="IXT-3-218804"/><div class="informaltable"><a id="ch03-110-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt365"/><pre class="programlisting">[FieldOffset (<em class="replaceable"><code>byte-offset</code></em>)] <em class="lineannotation"><span class="lineannotation">(for fields)</span></em></pre></td></tr></tbody></table></div><p>The <code class="literal">FieldOffset</code> attribute
          is used within a class or struct that has explicit field layout.
          This attribute can be applied to a field and specifies the field
          offset in bytes from the start of the class or struct. Note that
          these offsets don’t have to be strictly increasing and can overlap,
          thus creating a union data structure.</p></div><div class="sect3" title="MarshalAs attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-13.5.4"/>MarshalAs attribute</h3></div></div></div><div class="informaltable"><a id="ch03-112-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt366"/><pre class="programlisting">[MarshalAs(<em class="replaceable"><code>unmanaged-type</code></em>
  [, <em class="replaceable"><code>named-parameters</code></em>])?
] <em class="lineannotation"><span class="lineannotation">(for fields, parameters, return values)</span></em></pre></td></tr></tbody></table></div><p>The<a class="indexterm" id="IXT-3-218805"/> <code class="literal">MarshalAs</code>
          attribute overrides the default marshaling behavior that the
          marshaler applies to a parameter or field. The
          <em class="replaceable"><code>unmanaged-type</code></em> value is taken from the
          <code class="literal">UnmanagedType</code><a class="indexterm" id="IXT-3-218806"/> enum; see the following list for the permissible
          values:</p><div class="informaltable"><a id="ch03-113-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/><col/><col/></colgroup><tbody><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218807"/><a id="I_3_tt367"/><pre class="programlisting">Bool</pre></td><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218808"/><a id="I_3_tt368"/><pre class="programlisting">LPStr</pre></td><td style="border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218809"/><a id="I_3_tt369"/><pre class="programlisting">VBByRefStr</pre></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218810"/><a id="I_3_tt370"/><pre class="programlisting">I1</pre></td><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218811"/><a id="I_3_tt371"/><pre class="programlisting">LPWStr</pre></td><td style="border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218812"/><a id="I_3_tt372"/><pre class="programlisting">AnsiBStr</pre></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218813"/><a id="I_3_tt373"/><pre class="programlisting">U1</pre></td><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218814"/><a id="I_3_tt374"/><pre class="programlisting">LPTStr</pre></td><td style="border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218815"/><a id="I_3_tt375"/><pre class="programlisting">TBStr</pre></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a id="I_3_tt376"/><pre class="programlisting">I2</pre></td><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218816"/><a id="I_3_tt377"/><pre class="programlisting">ByValTStr</pre></td><td style="border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218817"/><a id="I_3_tt378"/><pre class="programlisting">VariantBool</pre></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a id="I_3_tt379"/><pre class="programlisting">U2</pre></td><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218818"/><a id="I_3_tt380"/><pre class="programlisting">IUnknown</pre></td><td style="border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218819"/><a id="I_3_tt381"/><pre class="programlisting">FunctionPtr</pre></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a id="I_3_tt382"/><pre class="programlisting">I4</pre></td><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218820"/><a id="I_3_tt383"/><pre class="programlisting">IDispatch</pre></td><td style="border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218821"/><a id="I_3_tt384"/><pre class="programlisting">LPVoid</pre></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a id="I_3_tt385"/><pre class="programlisting">U4</pre></td><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218822"/><a id="I_3_tt386"/><pre class="programlisting">Struct</pre></td><td style="border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218823"/><a id="I_3_tt387"/><pre class="programlisting">AsAny</pre></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a id="I_3_tt388"/><pre class="programlisting">I8</pre></td><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218824"/><a id="I_3_tt389"/><pre class="programlisting">Interface</pre></td><td style="border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218825"/><a id="I_3_tt390"/><pre class="programlisting">RPrecise</pre></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a id="I_3_tt391"/><pre class="programlisting">U8</pre></td><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218826"/><a id="I_3_tt392"/><pre class="programlisting">SafeArray</pre></td><td style="border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218827"/><a id="I_3_tt393"/><pre class="programlisting">LPArray</pre></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218828"/><a id="I_3_tt394"/><pre class="programlisting">R4</pre></td><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218829"/><a id="I_3_tt395"/><pre class="programlisting">ByValArray</pre></td><td style="border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218830"/><a id="I_3_tt396"/><pre class="programlisting">LPStruct</pre></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218831"/><a id="I_3_tt397"/><pre class="programlisting">R8</pre></td><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218832"/><a id="I_3_tt398"/><pre class="programlisting">SysInt</pre></td><td style="border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218833"/><a id="I_3_tt399"/><pre class="programlisting">CustomMarshaler</pre></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218834"/><a id="I_3_tt400"/><pre class="programlisting">BStr</pre></td><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218835"/><a id="I_3_tt401"/><pre class="programlisting">SysUInt</pre></td><td style="border-bottom: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218836"/><a id="I_3_tt402"/><pre class="programlisting">NativeTypeMax</pre></td></tr><tr><td style="border-right: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218837"/><a id="I_3_tt403"/><pre class="programlisting">Error</pre></td><td style="border-right: 0.5pt solid ; "><a class="indexterm" id="IXT-3-218838"/><a id="I_3_tt404"/><pre class="programlisting">Currency</pre></td><td style=""><a id="I_3_tt405"/><pre class="programlisting"/></td></tr></tbody></table></div><p>For a detailed description of how and when to use each of
          these enum values, as well as other legal
          <em class="replaceable"><code>named-parameters</code></em>, see the .NET Framework
          SDK documentation.</p></div><div class="sect3" title="In attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-13.5.5"/>In attribute</h3></div></div></div><div class="informaltable"><a id="ch03-115-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt406"/><pre class="programlisting">[In] <em class="lineannotation"><span class="lineannotation">(for parameters)</span></em></pre></td></tr></tbody></table></div><p>The <code class="literal">In</code><a class="indexterm" id="IXT-3-218839"/> attribute specifies that data should be marshaled
          into the caller and can be combined with the <code class="literal">Out</code> attribute.</p></div><div class="sect3" title="Out attribute"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-13.5.6"/>Out attribute</h3></div></div></div><div class="informaltable"><a id="ch03-117-fm2xml"/><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; "><colgroup><col/></colgroup><thead><tr><th style="border-bottom: 0.5pt solid ; "><p>Syntax:</p></th></tr></thead><tbody><tr><td style=""><a id="I_3_tt407"/><pre class="programlisting">[Out] <em class="lineannotation"><span class="lineannotation">(for parameters)</span></em></pre></td></tr></tbody></table></div><p>The <code class="literal">Out</code><a class="indexterm" id="IXT-3-218840"/> attribute specifies that data should be marshaled out
          from the called method to the caller and can be combined with the
          <code class="literal">In</code> <a class="indexterm" id="IXTR3-61"/> <a class="indexterm" id="IXTR3-62"/> <a class="indexterm" id="IXTR3-63"/> <a class="indexterm" id="IXTR3-64"/>attribute.</p></div></div></div></body></html>