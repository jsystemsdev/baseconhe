<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Strings</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Strings"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-3-SECT-3"/>Strings</h1></div></div></div><p>C# <a class="indexterm" id="csharpess2-IDXTERM-643"/> <a class="indexterm" id="csharpess2-IDXTERM-644"/>offers a wide range of string-handling features. Support
      is provided for both mutable and immutable strings, extensible string
      formatting, locale-aware string comparisons, and multiple string
      encoding systems.</p><p>This section introduces and demonstrates the most common types
      you’ll use when working with strings. Unless otherwise stated, the types
      mentioned in this section all exist in the<a class="indexterm" id="IXT-3-218551"/> <code class="literal">System</code> or <code class="literal">System.Text</code> namespaces.</p><div class="sect2" title="String Class"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-3.1"/>String Class</h2></div></div></div><p>A <a class="indexterm" id="IXT-3-218552"/> <a class="indexterm" id="IXT-3-218553"/>C# string represents an immutable sequence of characters
        and aliases the <code class="literal">System.String</code>
        class. Strings have comparison, appending, inserting, conversion,
        copying, formatting, indexing, joining, splitting, padding, trimming,
        removing, replacing, and searching methods. The compiler converts
        <code class="literal">+</code> operations on operands where the
        left operand is a string to <code class="literal">Concat</code>
        methods and preevaluates and interns string constants wherever
        possible.</p></div><div class="sect2" title="Immutability of Strings"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-3.2"/>Immutability of Strings</h2></div></div></div><p>Strings <a class="indexterm" id="IXT-3-218554"/>are immutable, which means they can’t be modified after
        creation. Consequently, many of the methods that initially appear to
        modify a string actually create a new string:</p><a id="I_3_tt280"/><pre class="programlisting">string a = "Heat";
string b = a.Insert(3, "r");
Console.WriteLine(b); // Prints Heart</pre><p>If you need a <a class="indexterm" id="IXT-3-218555"/> <a class="indexterm" id="IXT-3-218556"/>mutable string, see the <code class="literal">StringBuilder</code><a class="indexterm" id="IXT-3-218557"/> class.</p></div><div class="sect2" title="String Interning"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-3.3"/>String Interning</h2></div></div></div><p>In <a class="indexterm" id="IXT-3-218558"/> <a class="indexterm" id="IXT-3-218559"/> <a class="indexterm" id="IXT-3-218560"/>addition, the immutability of strings enable all strings
        in an application to be interned. <span class="emphasis"><em>Interning</em></span>
        describes the process whereby all the constant strings in an
        application are stored in a common place, and any duplicate strings
        are eliminated. This saves space at runtime but creates the
        possibility that multiple string references will point at the same
        spot in memory. This can be the source of unexpected results when
        comparing two constant strings, as follows:</p><a id="I_3_tt281"/><pre class="programlisting">string a = "hello";
string b = "hello";
Console.WriteLine(a == b); // True for String only
Console.WriteLine(a.Equals(b)); // True for all objects
Console.WriteLine(Object.ReferenceEquals(a, b)); // True!!</pre></div><div class="sect2" title="Formatting Strings"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-3.4"/>Formatting Strings</h2></div></div></div><p>The <code class="literal">Format</code><a class="indexterm" id="IXT-3-218561"/> <a class="indexterm" id="IXT-3-218562"/> method provides a convenient way to build strings that
        embed string representations of a variable number of parameters. Each
        parameter can be of any type, including both predefined types and
        user-defined types.</p><p>The <code class="literal">Format</code> method takes a
        format-specification string and a variable number of parameters. The
        format-specification string defines the template for the string and
        includes format specifications for each of the parameters. The syntax
        of a format specifier looks like this:</p><a id="I_3_tt282"/><pre class="programlisting">{ParamIndex[,MinWidth][:FormatString]}</pre><div class="variablelist"><dl><dt><span class="term"><a class="indexterm" id="IXT-3-218563"/><code class="literal">ParamIndex</code></span></dt><dd><p>The zero-based index of the parameter to be
              formatted.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-3-218564"/><code class="literal">MinWidth</code></span></dt><dd><p>The minimum number of characters for the string
              representation of the parameter, to be padded by spaces if
              necessary (negative is left-justified, positive is
              right-justified).</p></dd><dt><span class="term"><a class="indexterm" id="IXT-3-218565"/><code class="literal">FormatString</code></span></dt><dd><p>If the parameter represents an object that implements
              <code class="literal">IFormattable</code>, the <code class="literal">FormatString</code> is passed to the <code class="literal">ToString</code> method on <code class="literal">IFormattable</code> to construct the string.
              If not, the <code class="literal">ToString</code> method
              on <code class="literal">Object</code> is used to
              construct the string.</p></dd></dl></div><div class="tip" title="Tip"><h3 class="title"><a id="ch03-15-fm2xml"/>Tip</h3><p>All of the common types (<code class="literal">int</code>, <code class="literal">string</code>, <code class="literal">DateTime</code>, etc.) implement <code class="literal">IFormattable</code>. A table of the numeric and
          picture format specifiers supported by the common predefined types
          is provided in <a class="link" href="apc.html" title="Appendix C. Format Specifiers">Appendix
          C</a>.</p></div><p>In the following example, we embed a basic string representation
        of the account variable (param 0) and a monetary string representation
        of the cash variable (param 1, C=Currency):</p><a id="I_3_tt283"/><pre class="programlisting">using System;
class TestFormatting {
  static void Main(  ) {
    int i = 2;
    decimal m = 42.73m;
    string s = String.Format("Account {0} has {1:C}.", i, m);
    Console.WriteLine(s); // Prints "Account 2 has $42.73"
  }
}</pre></div><div class="sect2" title="Indexing Strings"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-3.5"/>Indexing Strings</h2></div></div></div><p>Consistent <a class="indexterm" id="IXT-3-218566"/> <a class="indexterm" id="IXT-3-218567"/>with all other indexing in the CLR, the characters in a
        string are accessed with a zero-based index:</p><a id="I_3_tt284"/><pre class="programlisting">using System;
class TestIndexing {
  static void Main(  ) {
    string s = "Going down?";
    for (int i=0; i&lt;s.Length; i++)
      Console.WriteLine(s[i]); // Prints s vertically
  }
}</pre></div><div class="sect2" title="Encoding Strings"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-3.6"/>Encoding Strings</h2></div></div></div><p>Strings <a class="indexterm" id="IXT-3-218568"/> <a class="indexterm" id="IXT-3-218569"/>can be converted between different character encodings
        using the <code class="literal">Encoding</code> type. The
        <code class="literal">Encoding</code> type can’t be created
        directly, but the ASCII, Unicode, UTF7, UTF8, and <code class="literal">BigEndianUnicode</code> static properties on the
        <code class="literal">Encoding</code> type return correctly
        constructed instances.</p><p>Here is an example that converts an array of bytes into a string
        using the ASCII encoding:</p><a id="I_3_tt285"/><pre class="programlisting">using System;
using System.Text;
class TestEncoding {
  static void Main(  ) {
    byte[] ba = new byte[] { 67, 35, 32, 105, 115, 
                             32, 67, 79, 79, 76, 33 };
    string s = Encoding.ASCII.GetString(ba);
    Console.WriteLine(s);
  }
}</pre></div><div class="sect2" title="StringBuilder Class"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-3.7"/>StringBuilder Class</h2></div></div></div><p>The <code class="literal">StringBuilder</code><a class="indexterm" id="IXT-3-218570"/> <a class="indexterm" id="IXT-3-218571"/> <a class="indexterm" id="IXT-3-218572"/> class is used to represent mutable strings. It starts
        at a predefined size (16 characters by default) and grows dynamically
        as more string data is added. It can either grow unbounded or up to a
        configurable maximum. For <a class="indexterm" id="IXTR3-35"/>
        <a class="indexterm" id="IXTR3-36"/>example:</p><a id="I_3_tt286"/><pre class="programlisting">using System;
using System.Text;
class TestStringBuilder {
  static void Main(  ) {
    StringBuilder sb = new StringBuilder("Hello, ");
    sb.Append("World?");
    sb[12] = '!';
    Console.WriteLine(sb); // Hello, World!
  }
}</pre></div></div></body></html>