<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Networking</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Networking"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-3-SECT-7"/>Networking</h1></div></div></div><p>The <a class="indexterm" id="csharpess2-IDXTERM-752"/> <a class="indexterm" id="csharpess2-IDXTERM-753"/> <a class="indexterm" id="csharpess2-IDXTERM-754"/>FCL includes a number of types that make accessing
      networked resources easy. Offering different levels of abstraction,
      these types allow an application to ignore much of the detail normally
      required to access networked resources, while retaining a high degree of
      control.</p><p>This section describes the core networking support in the FCL and
      provides numerous examples leveraging the predefined classes. The types
      mentioned in this section all exist in the <code class="literal">System.Net</code> and <code class="literal">System.Net.Sockets</code> namespaces.</p><div class="sect2" title="Network Programming Models"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-7.1"/>Network Programming Models</h2></div></div></div><p>High-level<a class="indexterm" id="IXT-3-218642"/> <a class="indexterm" id="IXT-3-218643"/> access is performed using a set of types that implement
        a generic request/response architecture that is extensible to support
        new protocols. The implementation of this architecture in the FCL also
        includes HTTP-specific extensions to make interacting with web servers
        easy.</p><p>Should the application require lower-level access to the
        network, types exist to support the<a class="indexterm" id="IXT-3-218644"/> <a class="indexterm" id="IXT-3-218645"/> Transmission Control Protocol (TCP) and <a class="indexterm" id="IXT-3-218646"/> <a class="indexterm" id="IXT-3-218647"/>User Datagram Protocol (UDP). Finally, in situations in
        which direct transport-level access is required, there are types that
        provide raw socket access.</p></div><div class="sect2" title="Generic Request/Response Architecture"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-7.2"/>Generic Request/Response Architecture</h2></div></div></div><p>The <a class="indexterm" id="IXT-3-218648"/> <a class="indexterm" id="IXT-3-218649"/> <a class="indexterm" id="IXT-3-218650"/> <a class="indexterm" id="IXT-3-218651"/>request/response architecture is based on <a class="indexterm" id="IXT-3-218652"/> <a class="indexterm" id="IXT-3-218653"/>Uniform Resource Indicator (URI) and stream I/O, follows
        the factory design pattern, and makes good use of abstract types and
        interfaces.</p><p>A factory type (<code class="literal">WebRequest</code><a class="indexterm" id="IXT-3-218654"/>) parses the URI and creates the appropriate protocol
        handler to fulfill the request.</p><p><a class="indexterm" id="IXT-3-218655"/>Protocol handlers share a common abstract base type
        (<code class="literal">WebRequest</code>) that exposes
        properties that configure the request and methods used to retrieve the
        response.</p><p>Responses are also represented as types and share a common
        abstract base type (<code class="literal">WebResponse</code>)
        that exposes a <code class="literal">Stream</code>, providing
        simple streams-based I/O and easy integration into the rest of the
        FCL.</p><p>This example is a simple implementation of the popular
        Unix<a class="indexterm" id="IXT-3-218656"/> <a class="indexterm" id="IXT-3-218657"/> <span class="emphasis"><em>snarf</em></span> utility. It demonstrates the
        use of the <code class="literal">WebRequest</code> and <code class="literal">WebResponse</code> classes to retrieve the contents
        of a URI and print them to the console:</p><a id="I_3_tt310"/><pre class="programlisting">// Snarf.cs
// Run Snarf.exe &lt;http-uri&gt; to retrieve a web page
using System;
using System.IO;
using System.Net;
using System.Text;
class Snarf {
  static void Main(string[] args) {

    // Retrieve the data at the URL with an WebRequest ABC
    WebRequest req = WebRequest.Create(args[0]);
    WebResponse resp = req.GetResponse(  );

    // Read in the data, performing ASCII-&gt;Unicode encoding
    Stream s = resp.GetResponseStream(  );
    StreamReader sr = new StreamReader(s, Encoding.ASCII);
    string doc = sr.ReadToEnd(  );

    Console.WriteLine(doc); // Print result to console
  }
}</pre></div><div class="sect2" title="HTTP-Specific Support"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-7.3"/>HTTP-Specific Support</h2></div></div></div><p>The <a class="indexterm" id="IXT-3-218658"/> <a class="indexterm" id="IXT-3-218659"/>request/response architecture inherently supports
        protocol-specific extensions via the use of subtyping.</p><p>Since the <code class="literal">WebRequest</code> creates
        and returns the appropriate handler type based on the URI, accessing
        protocol-specific features is as easy as downcasting the returned
        <code class="literal">WebRequest</code> object to the
        appropriate protocol-specific handler and accessing the extended
        functionality.</p><p>The FCL includes specific support for the HTTP protocol,
        including the ability to easily access and control elements of an
        interactive web session, such as the HTTP headers, user-agent strings,
        proxy support, user credentials, authentication, keep-alives,
        pipelining, and more.</p><p>This example demonstrates the use of the HTTP-specific
        request/response classes to control the user-agent string for the
        request and retrieve the server type:</p><a id="I_3_tt311"/><pre class="programlisting">// ProbeSvr.cs
// Run ProbeSvr.exe &lt;servername&gt; to retrieve the server type
using System;
using System.Net;
class ProbeSvr {
  static void Main(string[] args) {

    // Get instance of WebRequest ABC, convert to HttpWebRequest
    WebRequest req = WebRequest.Create(args[0]);
    HttpWebRequest httpReq = (HttpWebRequest)req;

    // Access HTTP-specific features such as User-Agent
    httpReq.UserAgent = "CSPRProbe/1.0";

    // Retrieve response and print to console
    WebResponse resp = req.GetResponse(  );
    HttpWebResponse httpResp = (HttpWebResponse)resp;
    Console.WriteLine(httpResp.Server);
  }
}</pre></div><div class="sect2" title="Adding New Protocol Handlers"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-7.4"/>Adding New Protocol Handlers</h2></div></div></div><p>Adding <a class="indexterm" id="IXT-3-218660"/> <a class="indexterm" id="IXT-3-218661"/>handlers to support new protocols is trivial: simply
        implement a new set of derived types based on <code class="literal">WebRequest</code> and <code class="literal">WebResponse</code>, implement the <code class="literal">IWebRequestCreate</code><a class="indexterm" id="IXT-3-218662"/> interface on your <code class="literal">WebRequest</code>-derived type, and register it as
        a new protocol handler with <code class="literal">Web-Request.RegisterPrefix( )</code> at runtime.
        Once this is done, any code that uses the request/response
        architecture can access networked resources using the new URI format
        (and underlying protocol).</p></div><div class="sect2" title="Using TCP, UDP, and Sockets"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-7.5"/>Using TCP, UDP, and Sockets</h2></div></div></div><p>The <code class="literal">System.Net.Sockets</code><a class="indexterm" id="IXT-3-218663"/><a class="indexterm" id="IXT-3-218664"/> <a class="indexterm" id="IXT-3-218665"/> <a class="indexterm" id="IXT-3-218666"/>namespace includes types that provide protocol-level
        support for TCP and UDP. These types are built on the underlying
        <code class="literal">Socket</code> type, which is itself
        directly accessible for transport-level access to the network.</p><p>Two classes provide the TCP support: <code class="literal">TcpListener</code><a class="indexterm" id="IXT-3-218667"/> <a class="indexterm" id="IXT-3-218668"/> and <code class="literal">TcpClient</code>.
        <code class="literal">TcpListener</code> listens for incoming
        connections, creating <code class="literal">Socket</code>
        instances that respond to the connection request. <code class="literal">TcpClient</code> connects to a remote host, hiding
        the details of the underlying socket in a <code class="literal">Stream</code>-derived type that allows stream I/O
        over the network.</p><p>A class called <code class="literal">UdpClient</code>
        provides the UDP support. <code class="literal">UdpClient</code>
        serves as both a client and a listener and includes multicast support,
        allowing individual datagrams to be sent and received as byte
        arrays.</p><p>Both the TCP and the UDP classes help access the underlying
        network socket (represented by the <code class="literal">Socket</code> class). The <code class="literal">Socket</code> class is a thin wrapper over the
        native Windows sockets functionality and is the lowest level of
        networking accessible to managed code.</p><p>The following example is a simple implementation of the Quote of
        the Day (QUOTD) protocol, as defined by the IETF in RFC 865. It
        demonstrates the use of a TCP listener to accept incoming requests and
        the use of the lower-level <code class="literal">Socket</code>
        type to fulfill the request:</p><a id="I_3_tt312"/><pre class="programlisting">// QOTDListener.cs 
// Run QOTDListener.exe to service incoming QOTD requests
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
class QOTDListener {
  static string[] quotes = {
    @"Sufficiently advanced magic is indistinguishable from technology
         -- Terry Pratchett",
    @"Sufficiently advanced technology is indistinguishable from magic
         -- Arthur C. Clarke" };
  static void Main(  ) {

    // Start a TCP listener on port 17
    TcpListener l = new TcpListener(17);
    l.Start(  );
    Console.WriteLine("Waiting for clients to connect");
    Console.WriteLine("Press Ctrl+C to quit...");
    int numServed = 1;
    while (true) {

      // Block waiting for an incoming socket connect request
      Socket s = l.AcceptSocket(  );

      // Encode alternating quotes as bytes for sending 
      Char[] carr = quotes[numServed%2].ToCharArray(  );
      Byte[] barr = Encoding.ASCII.GetBytes(carr);

      // Return data to client, then clean up socket and repeat
      s.Send(barr, barr.Length, 0);
      s.Shutdown(SocketShutdown.Both);
      s.Close(  );
      Console.WriteLine("{0} quotes served...", numServed++);
    }
  }
}</pre><p>To test this example, run the listener and try connecting to
        port 17 on <span class="emphasis"><em>localhost</em></span> using a
        <span class="emphasis"><em>telnet</em></span> client. (Under Windows, this can be done
        from the command line by entering <code class="literal">telnet         localhost 17</code>).</p><p>Notice the use of <code class="literal">Socket.Shutdown</code> and <code class="literal">Socket.Close</code> at the end of the <code class="literal">while</code> loop. This is required to flush and
        close the socket immediately, rather than wait for the garbage
        collector to finalize and collect unreachable <code class="literal">Socket</code> objects later.</p></div><div class="sect2" title="Using DNS"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-7.6"/>Using DNS</h2></div></div></div><p>The networking types in the base class library also support
        normal and reverse <a class="indexterm" id="IXT-3-218669"/> <a class="indexterm" id="IXT-3-218670"/> <a class="indexterm" id="IXT-3-218671"/>Domain Name System (DNS) resolution. Here’s an example
        using these <a class="indexterm" id="IXTR3-44"/>
        <a class="indexterm" id="IXTR3-45"/> <a class="indexterm" id="IXTR3-46"/>types:</p><a id="I_3_tt313"/><pre class="programlisting">// DNSLookup.cs
// Run DNSLookup.exe &lt;servername&gt; to determine IP addresses
using System;
using System.Net;
class DNSLookup {
  static void Main(string[] args) {
    IPHostEntry he = Dns.GetHostByName(args[0]);
    IPAddress[] addrs = he.AddressList;
    foreach (IPAddress addr in addrs)
      Console.WriteLine(addr);
  }
}</pre></div></div></body></html>