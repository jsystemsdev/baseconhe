<html xmlns="http://www.w3.org/1999/xhtml"><head><title>XML Documentation</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="XML Documentation"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-2-SECT-19"/>XML Documentation</h1></div></div></div><p>C# offers<a class="indexterm" id="csharpess2-IDXTERM-539"/> three different styles of source code documentation:
      single-line comments, multiline comments, and documentation
      comments.</p><div class="sect2" title="C/C++-Style Comments"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-19.1"/>C/C++-Style Comments</h2></div></div></div><p>Single- and multiline <a class="indexterm" id="IXT-2-218311"/> <a class="indexterm" id="IXT-2-218312"/> <a class="indexterm" id="IXT-2-218313"/>comments use the <a class="indexterm" id="IXT-2-218314"/>C++ syntax: <code class="literal">//</code> and
        <code class="literal">/*...*/</code>:</p><a id="I_2_tt247"/><pre class="programlisting">int x = 3; // this is a comment
MyMethod(  ); /* this is a
comment that spans two lines */</pre><p>The disadvantage of this style of commenting is that there is no
        predetermined standard for documenting your types. Consequently, it
        can’t be easily parsed to automate the production of documentation. C#
        improves on this by allowing you to embed documentation comments in
        the source and by providing an automated mechanism for extracting and
        validating documentation at compile time.</p></div><div class="sect2" title="Documentation Comments"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-19.2"/>Documentation Comments</h2></div></div></div><p>Documentation comments are similar to C# single-line comments
        but start with <code class="literal">///</code><a class="indexterm" id="IXT-2-218315"/> <a class="indexterm" id="IXT-2-218316"/> and can be applied to any user-defined type or member.
        These comments can include embedded XML tags as well as descriptive
        text. These tags allow you to mark up the descriptive text to better
        define the semantics of the type or member and also to incorporate
        cross-references.</p><p>These comments can then be extracted at compile time into a
        separate output file containing the documentation. The compiler
        validates the comments for internal consistency, expands
        cross-references into fully qualified type IDs, and outputs a
        well-formed XML file. Further processing is left up to you, although a
        common next step is to run the XML through an XSL/T, generating HTML
        documentation.</p><p>Here is an example documentation for a simple type:</p><a id="I_2_tt248"/><pre class="programlisting">// Filename: DocTest.cs
using System;
class MyClass {
  /// &lt;summary&gt;
  /// The Foo method is called from
  ///   &lt;see cref="Main"&gt;Main&lt;/see&gt; 
  /// &lt;/summary&gt;
  /// &lt;mytag&gt;Secret stuff&lt;/mytag&gt;
  /// &lt;param name="s"&gt;Description for s&lt;/param&gt;
  static void Foo(string s) { Console.WriteLine(s); }
  static void Main(  ) { Foo("42"); }
}</pre></div><div class="sect2" title="XML Documentation Files"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-19.3"/>XML Documentation Files</h2></div></div></div><p>When <a class="indexterm" id="IXT-2-218317"/>the preceding source file is run through the compiler
        with the <code class="literal">/doc:&lt;</code><em class="replaceable"><code>filename</code></em><code class="literal">&gt;</code> command-line options, this XML file is
        generated:</p><a id="I_2_tt249"/><pre class="programlisting">&lt;?xml version="1.0"?&gt;
&lt;doc&gt;
  &lt;assembly&gt;
    &lt;name&gt;DocTest&lt;/name&gt;
  &lt;/assembly&gt;
  &lt;members&gt;
    &lt;member name="M:MyClass.Foo(System.String)"&gt;
      &lt;summary&gt;
      The Foo method is called from
        &lt;see cref="M:MyClass.Main"&gt;Main&lt;/see&gt; 
      &lt;/summary&gt;
      &lt;mytag&gt;Secret stuff&lt;/mytag&gt;
      &lt;param name="s"&gt;Description for s&lt;/param&gt;
     &lt;/member&gt;
  &lt;/members&gt;
&lt;/doc&gt;</pre><p>The <code class="literal">&lt;?xml...&gt;</code>, <code class="literal">&lt;doc&gt;</code>, and <code class="literal">&lt;members&gt;</code> tags are generated
        automatically and form the skeleton for the XML file. The <code class="literal">&lt;assembly&gt;</code> and <code class="literal">&lt;name&gt;</code> tags indicate the assembly that
        this type lives in. Every member preceded by a documentation comment
        is included in the XML file via a <code class="literal">&lt;member&gt;</code> tag with a name attribute
        that identifies the member. Note that the <code class="literal">cref</code> attribute in the <code class="literal">&lt;see&gt;</code> tag has also been expanded to
        refer to a fully qualified type and member. The predefined XML
        documentation tags embedded in the documentation comments are also
        included in the XML file. The tags have been validated to ensure that
        all parameters are documented, that the names are accurate, and that
        any cross-references to other types or members can be resolved.
        Finally, any additional user-defined tags are transferred
        verbatim.</p></div><div class="sect2" title="Predefined XML Tags"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-19.4"/>Predefined XML Tags</h2></div></div></div><p>This<a class="indexterm" id="csharpess2-IDXTERM-547"/> <a class="indexterm" id="csharpess2-IDXTERM-548"/> section lists the predefined set of XML tags that can
        be used to mark up the descriptive text:</p><div class="variablelist"><dl><dt><span class="term"><a class="indexterm" id="IXT-2-218318"/><a class="indexterm" id="IXT-2-218319"/><code class="literal">&lt;summary&gt;</code>,
            <code class="literal">&lt;remarks&gt;</code></span></dt><dd><a id="I_2_tt250"/><pre class="programlisting">&lt;summary&gt;<em class="replaceable"><code>description</code></em>&lt;/summary&gt;
&lt;remarks&gt;<em class="replaceable"><code>description</code></em>&lt;/remarks&gt;</pre><p>These tags describe a type or member. Typically, <code class="literal">&lt;summary&gt;</code>contains the
              description of a member, and <code class="literal">&lt;remarks&gt;</code> contains a full
              description of a type.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-2-218320"/><code class="literal">&lt;param&gt;</code></span></dt><dd><a id="I_2_tt251"/><pre class="programlisting">&lt;param name="<em class="replaceable"><code>name</code></em>"&gt;<em class="replaceable"><code>description</code></em>&lt;/param&gt;</pre><p>This tag describes a parameter on a method. The
              <em class="replaceable"><code>name</code></em> attribute is mandatory and must
              refer to a parameter on the method. If this tag is applied to
              any parameter on a method, all parameters on that method must be
              documented. You must enclose <em class="replaceable"><code>name</code></em> in
              double quotation marks (<code class="literal">""</code>).</p></dd><dt><span class="term"><a class="indexterm" id="IXT-2-218321"/><code class="literal">&lt;returns&gt;</code></span></dt><dd><a id="I_2_tt252"/><pre class="programlisting">&lt;returns&gt;<em class="replaceable"><code>description</code></em>&lt;/returns&gt;</pre><p>This tag describes the return values for a method.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-2-218322"/><code class="literal">&lt;exception&gt;</code></span></dt><dd><a id="I_2_tt253"/><pre class="programlisting">&lt;exception [cref="<em class="replaceable"><code>type</code></em>"]&gt;<em class="replaceable"><code>description</code></em>&lt;/exception&gt;</pre><p>This tag describes the exceptions a method may throw. If
              present, the optional <code class="literal">cref</code>
              attribute should refer to the type of exception. You must
              enclose the type name in double quotation marks (<code class="literal">""</code>).</p></dd><dt><span class="term"><a class="indexterm" id="IXT-2-218323"/><code class="literal">&lt;permission&gt;</code></span></dt><dd><a id="I_2_tt254"/><pre class="programlisting">&lt;permission [cref="<em class="replaceable"><code>type</code></em>"]&gt;<em class="replaceable"><code>description</code></em>&lt;/permission&gt;</pre><p>This tag describes the permission requirements for a type
              or member. If present, the optional <code class="literal">cref</code> attribute should refer to the
              type that represents the permission set required by the member,
              although the compiler doesn’t validate this. You must enclose
              the type name in double quotation marks (<code class="literal">""</code>).</p></dd><dt><span class="term"><a class="indexterm" id="IXT-2-218324"/><a class="indexterm" id="IXT-2-218325"/><a class="indexterm" id="IXT-2-218326"/><code class="literal">&lt;example&gt;</code>,
            <code class="literal">&lt;c&gt;</code>, <code class="literal">&lt;code&gt;</code></span></dt><dd><a id="I_2_tt255"/><pre class="programlisting">&lt;example&gt;<em class="replaceable"><code>description</code></em>&lt;/example&gt; 
&lt;c&gt;<em class="replaceable"><code>code</code></em>&lt;/c&gt;
&lt;code&gt;<em class="replaceable"><code>code</code></em>&lt;/code&gt;</pre><p>These tags provide a description and sample source code
              explaining the use of a type or member. Typically, the <code class="literal">&lt;example&gt;</code> tag provides the
              description and contains the <code class="literal">&lt;c&gt;</code> and <code class="literal">&lt;code&gt;</code> tags, although these can
              also be used independently. If you need to include an inline
              code snippet, use the <code class="literal">&lt;c&gt;</code> tag. If you need to include
              multiline snippets, use the <code class="literal">&lt;code&gt;</code> tag.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-2-218327"/><a class="indexterm" id="IXT-2-218328"/><code class="literal">&lt;see&gt;</code>,
            <code class="literal">&lt;seealso&gt;</code></span></dt><dd><a id="I_2_tt256"/><pre class="programlisting">&lt;see cref="<em class="replaceable"><code>member</code></em>"&gt;<em class="replaceable"><code>text</code></em>&lt;/see&gt;
&lt;seealso cref="<em class="replaceable"><code>member</code></em>"&gt;<em class="replaceable"><code>text</code></em>&lt;/seealso&gt;</pre><p>These tags identify cross-references in the documentation
              to other types or members. Typically, the <code class="literal">&lt;see&gt;</code> tag is used inline within
              a description, while the <code class="literal">&lt;seealso&gt;</code> tag is broken out into
              a separate “See Also” section. These tags are useful because
              they allow tools to generate cross-references, indexes, and
              hyperlinked views of the documentation. Member names must be
              enclosed by double quotation marks (<code class="literal">""</code>).</p></dd><dt><span class="term"><a class="indexterm" id="IXT-2-218329"/><code class="literal">&lt;value&gt;</code></span></dt><dd><a id="I_2_tt257"/><pre class="programlisting">&lt;value&gt;<em class="replaceable"><code>description</code></em>&lt;/value&gt;</pre><p>This tag describes a property on a class.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-2-218330"/><code class="literal">&lt;paramref&gt;</code></span></dt><dd><a id="I_2_tt258"/><pre class="programlisting">&lt;paramref name="<em class="replaceable"><code>name</code></em>"/&gt;</pre><p>This tag identifies the use of a parameter name within
              descriptive text, such as <code class="literal">&lt;remarks&gt;</code> or <code class="literal">&lt;summary&gt;</code>. The name must be
              enclosed by double quotation marks (<code class="literal">""</code>).</p></dd><dt><span class="term"><a class="indexterm" id="IXT-2-218331"/><a class="indexterm" id="IXT-2-218332"/><code class="literal">&lt;list&gt;</code>,
            <code class="literal">&lt;para&gt;</code></span></dt><dd><a id="I_2_tt259"/><pre class="programlisting">&lt;list type=[ <em class="replaceable"><code>bullet</code></em>| <em class="replaceable"><code>number</code></em>| <em class="replaceable"><code>table</code></em>]&gt;
 &lt;listheader&gt;
 &lt;term&gt;<em class="replaceable"><code>name</code></em>&lt;/term&gt;
 &lt;description&gt;<em class="replaceable"><code>description</code></em>&lt;/description&gt;
 &lt;/listheader&gt;
 &lt;item&gt;
 &lt;term&gt;<em class="replaceable"><code>name</code></em>&lt;/term&gt;
 &lt;description&gt;<em class="replaceable"><code>description</code></em>&lt;/description&gt;
 &lt;/item&gt; 
&lt;/list&gt; 
&lt;para&gt;<em class="replaceable"><code>text</code></em>&lt;/para&gt;</pre><p>These tags provide hints to documentation generators on
              how to format the documentation.</p></dd><dt><span class="term"><a class="indexterm" id="IXT-2-218333"/><code class="literal">&lt;include&gt;</code></span></dt><dd><a id="I_2_tt260"/><pre class="programlisting">&lt;include file='<em class="replaceable"><code>filename</code></em>' path='<em class="replaceable"><code>path-to-element</code></em>'&gt;</pre><p>This tag specirfies an external file that contains
              documentation and an XPath path to a specific element in that
              file. For example, a path of <code class="literal">docs[@id="001"]/*</code> would retrieve
              whatever is inside of <code class="literal">&lt;docs               id="001"/&gt;</code>. The filename and path must be enclosed
              by single quotation marks (<code class="literal">''</code>).</p></dd></dl></div></div><div class="sect2" title="User-Defined Tags"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-19.5"/>User-Defined Tags</h2></div></div></div><p>There<a class="indexterm" id="IXT-2-218334"/> <a class="indexterm" id="IXT-2-218335"/> <a class="indexterm" id="IXT-2-218336"/> is little that is special about the predefined XML tags
        recognized by the C# compiler, and you are free to define your own.
        The only special processing done by the compiler is on the <code class="literal">&lt;param&gt;</code><a class="indexterm" id="IXT-2-218337"/> tag (where it verifies the parameter name and confirms
        that all the parameters on the method are documented) and the <code class="literal">cref</code> attribute (where it verifies that the
        attribute refers to a real type or member and expands it to a fully
        qualified type or member ID). The <code class="literal">cref</code> attribute can also be used in your own
        tags and is verified and expanded just as it is in the predefined
        <code class="literal">&lt;exception&gt;</code>, <code class="literal">&lt;permission&gt;</code>, <code class="literal">&lt;see&gt;</code>, and <code class="literal">&lt;seealso&gt;</code> tags.</p></div><div class="sect2" title="Type or Member Cross-References"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-2-SECT-19.6"/>Type or Member Cross-References</h2></div></div></div><p><a class="indexterm" id="IXT-2-218338"/> <a class="indexterm" id="IXT-2-218339"/> <a class="indexterm" id="IXT-2-218340"/>Type names and type or member cross-references are
        translated into IDs that uniquely define the type or member. These
        names are composed of a prefix that defines what the ID represents and
        a signature of the type or member. <a class="link" href="ch02s19.html#csharpess2-CHP-2-TABLE-5" title="Table 2-5. XML type ID prefixes">Table 2-5</a> lists the set of
        type and member prefixes.<a class="indexterm" id="IXT-2-218341"/> <a class="indexterm" id="IXT-2-218342"/> <a class="indexterm" id="IXT-2-218343"/> <a class="indexterm" id="IXT-2-218344"/> <a class="indexterm" id="IXT-2-218345"/> <a class="indexterm" id="IXT-2-218346"/> <a class="indexterm" id="IXT-2-218347"/></p><div class="table"><a id="csharpess2-CHP-2-TABLE-5"/><p class="title">Table 2-5. XML type ID prefixes</p><div class="table-contents"><table style="border-collapse: collapse;border-top: 0.5pt solid ; border-bottom: 0.5pt solid ; border-left: 0.5pt solid ; border-right: 0.5pt solid ; " summary="XML type ID prefixes"><colgroup><col/><col/></colgroup><thead><tr><th style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><p>Prefix</p></th><th style="border-bottom: 0.5pt solid ; "><p>Applied to</p></th></tr></thead><tbody><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a id="I_2_tt261"/><pre class="programlisting">N</pre></td><td style="border-bottom: 0.5pt solid ; "><p>Namespace</p></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a id="I_2_tt262"/><pre class="programlisting">T</pre></td><td style="border-bottom: 0.5pt solid ; "><p>Type (class, struct, enum, interface,
                delegate)</p></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a id="I_2_tt263"/><pre class="programlisting">F</pre></td><td style="border-bottom: 0.5pt solid ; "><p>Field</p></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a id="I_2_tt264"/><pre class="programlisting">P</pre></td><td style="border-bottom: 0.5pt solid ; "><p>Property (includes indexers)</p></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a id="I_2_tt265"/><pre class="programlisting">M</pre></td><td style="border-bottom: 0.5pt solid ; "><p>Method (includes special methods)</p></td></tr><tr><td style="border-right: 0.5pt solid ; border-bottom: 0.5pt solid ; "><a id="I_2_tt266"/><pre class="programlisting">E</pre></td><td style="border-bottom: 0.5pt solid ; "><p>Event</p></td></tr><tr><td style="border-right: 0.5pt solid ; "><a id="I_2_tt267"/><pre class="programlisting">!</pre></td><td style=""><p>Error</p></td></tr></tbody></table></div></div><p>The rules describing how the signatures are generated are
        well-documented, although fairly complex.</p><p>Here is an example of a type and the IDs <a class="indexterm" id="IXTR3-19"/> <a class="indexterm" id="IXTR3-20"/>that <a class="indexterm" id="IXTR3-21"/>are generated:</p><a id="I_2_tt268"/><pre class="programlisting">// Namespaces do not have independent signatures
namespace NS {
  // T:NS.MyClass
  class MyClass {
    // F:NS.MyClass.aField
    string aField;
    // P:NS.MyClass.aProperty
    short aProperty {get {...} set {...}}
    // T:NS.MyClass.NestedType
    class NestedType {...};
    // M:NS.MyClass.X(  )
    void X(  ) {...}
    // M:NS.MyClass.Y(System.Int32,System.Double@,System.Decimal@)
    void Y(int p1, ref double p2, out decimal p3) {...}
    // M:NS.MyClass.Z(System.Char[],System.Single[0:,0:])
    void Z(char[] p1, float[,] p2) {...}
    // M:NS.MyClass.op_Addition(NS.MyClass,NS.MyClass)
    public static MyClass operator+(MyClass c1, MyClass c2) {...}
    // M:NS.MyClass.op_Implicit(NS.MyClass)~System.Int32
    public static implicit operator int(MyClass c) {...}
    // M:NS.MyClass.#ctor
    MyClass(  ) {...}
    // M:NS.MyClass.Finalize
    ~MyClass(  ) {...}
    // M:NS.MyClass.#cctor
    static MyClass(  ) {...}
  }
}</pre></div></div></body></html>