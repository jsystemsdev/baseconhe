<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Assemblies</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Assemblies"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-3-SECT-9"/>Assemblies</h1></div></div></div><p>An <span class="emphasis"><em>assembly</em></span><a class="indexterm" id="csharpess2-IDXTERM-815"/> <a class="indexterm" id="csharpess2-IDXTERM-816"/> is a logical package (similar to a DLL in Win32) that
      consists of a manifest, a set of one or more modules, and an optional
      set of resources. This package forms the basic unit of deployment and
      versioning, and creates a boundary for type resolution and security
      permissioning.</p><div class="sect2" title="Elements of an Assembly"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-9.1"/>Elements of an Assembly</h2></div></div></div><p>Every <a class="indexterm" id="IXT-3-218691"/>.NET application consists of at least one assembly,
        which is in turn built from a number of basic elements.</p><p>The <span class="emphasis"><em>manifest</em></span><a class="indexterm" id="IXT-3-218692"/> contains a set of metadata that describes everything
        the runtime needs to know about the assembly. This information
        includes:</p><div class="itemizedlist"><ul class="itemizedlist"><li class="listitem"><p>The textual name of the assembly</p></li><li class="listitem"><p>The version number of the assembly</p></li><li class="listitem"><p>An optional shared name and signed assembly hash</p></li><li class="listitem"><p>The list of files in the assembly with file hashes</p></li><li class="listitem"><p>The list of referenced assemblies, including versioning
            information and an optional public key</p></li><li class="listitem"><p>The list of types included in the assembly, with a mapping
            to the module containing the type</p></li><li class="listitem"><p>The set of minimum and optional security permissions
            requested by the assembly</p></li><li class="listitem"><p>The set of security permissions explicitly refused by the
            assembly</p></li><li class="listitem"><p>Culture, processor, and OS information</p></li><li class="listitem"><p>A set of custom attributes to capture details such as
            product name, owner information, etc.</p></li></ul></div><p><span class="emphasis"><em>Modules</em></span> <a class="indexterm" id="IXT-3-218693"/>contain types described using metadata and implemented
        using MSIL.</p><p><span class="emphasis"><em>Resources</em></span><a class="indexterm" id="IXT-3-218694"/> contain nonexecutable data that is logically included
        with the assembly. Examples of this include bitmaps, localizable text,
        persisted objects, etc.</p></div><div class="sect2" title="Packaging"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-9.2"/>Packaging</h2></div></div></div><p>The<a class="indexterm" id="IXT-3-218695"/> <a class="indexterm" id="IXT-3-218696"/> simplest assembly contains a manifest and a single
        module containing the application’s types, packaged as an EXE with a
        <code class="literal">Main</code> entry point. More complex
        assemblies can include multiple modules (<a class="indexterm" id="IXT-3-218697"/> <a class="indexterm" id="IXT-3-218698"/>Portable Executable (PE) files), separate resource
        files, a manifest, etc.</p><p>The manifest is generally included in one of the existing PE
        files in the assembly, although the manifest can also be a standalone
        PE file.</p><p>Modules are PE files, typically DLLs or EXEs. Only one module in
        an assembly can contain an entry point (either <code class="literal">Main</code>, <code class="literal">WinMain</code>, or <code class="literal">DllMain</code>).</p><p>An assembly may also contain multiple modules. This technique
        can reduce the working set of the application, because the CLR loads
        only the required modules. In addition, each module can be written in
        a different language, allowing a mixture of C#, VB.NET, and raw MSIL.
        Although not common, a single module can also be included in several
        different assemblies.</p><p>Finally, an assembly may contain a set of resources, which can
        be kept either in standalone files or included in one of the PE files
        in the assembly.</p></div><div class="sect2" title="Deployment"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-9.3"/>Deployment</h2></div></div></div><p>An <a class="indexterm" id="IXT-3-218699"/>assembly is the smallest .NET unit of deployment. Due to
        the self-describing nature of a manifest, deployment can be as simple
        as copying the assembly (and in the case of a multifile assembly, all
        the associated files) into a directory.</p><p>This is a vast improvement over traditional COM development in
        which components, their supporting DLLs, and their configuration
        information are spread out over multiple directories and the Windows
        registry.</p><p>Generally, assemblies are deployed into the application
        directory and are not shared. These are called <span class="emphasis"><em>private
        assemblies</em></span><a class="indexterm" id="IXT-3-218700"/>. However, assemblies can also be shared between
        applications; these are called <span class="emphasis"><em>shared
        assemblies</em></span><a class="indexterm" id="IXT-3-218701"/>. To share an assembly you need to give it a
        <span class="emphasis"><em>shared</em></span><a class="indexterm" id="IXT-3-218702"/> <span class="emphasis"><em>name</em></span> (also known as a “strong”
        name) and deploy it in the global assembly cache.</p><p>The shared name consists of a name, a public key, and a digital
        signature. The shared name is included in the assembly manifest and
        forms the unique identifier for the assembly.</p><p>The global assembly cache is a machine-wide storage area that
        contains assemblies intended for use by multiple applications.</p><p>For more information on working with shared assemblies and the
        global assembly cache, see <a class="link" href="ape.html#csharpess2-APP-E-SECT-1.4" title="Sharing Assemblies">Section E.1.4</a> in <a class="link" href="ape.html" title="Appendix E. Working with Assemblies">Appendix E</a>.</p></div><div class="sect2" title="Versioning"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-9.4"/>Versioning</h2></div></div></div><p>The<a class="indexterm" id="IXT-3-218703"/> <a class="indexterm" id="IXT-3-218704"/> manifest of an application contains a version number
        for the assembly and a list of all the referenced assemblies with
        associated version information. Assembly version numbers are divided
        into four parts and look like this:</p><a class="indexterm" id="IXT-3-218705"/><a class="indexterm" id="IXT-3-218706"/><a class="indexterm" id="IXT-3-218707"/><a class="indexterm" id="IXT-3-218708"/><a id="I_3_tt322"/><pre class="programlisting"><em class="replaceable"><code>&lt;major&gt;.&lt;minor&gt;.&lt;build&gt;.&lt;revision&gt;</code></em></pre><p>This information is used to mitigate versioning problems as
        assemblies evolve over time.</p><p>At runtime, the CLR uses the version information specified in
        the manifest and a set of versioning policies defined for the machine
        to determine which versions of each dependent, shared assembly to
        load.</p><p>The default versioning policy for shared assemblies restricts
        you to the version that your application was built against. By
        changing a configuration file, the application or an administrator can
        override this behavior.</p><p>Private assemblies have no versioning policy, and the CLR simply
        loads the newest assemblies found in the application directory.</p></div><div class="sect2" title="Type Resolution"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-9.5"/>Type Resolution</h2></div></div></div><p>The<a class="indexterm" id="IXT-3-218709"/> <a class="indexterm" id="IXT-3-218710"/> unique identifier for a type (known as a <code class="literal">TypeRef</code><a class="indexterm" id="IXT-3-218711"/>) consists of a reference to the assembly it was defined
        in and the fully qualified type name (including any namespaces). For
        example, this local variable declaration:</p><a id="I_3_tt323"/><pre class="programlisting">System.Xml.XmlReader xr;</pre><p>is represented in MSIL assembly language as follows:</p><a id="I_3_tt324"/><pre class="programlisting">.assembly extern System.Xml { .ver 1:0:3300:0 ... }
.locals init (class [System.Xml]System.Xml.XmlReader V_0)</pre><p>In this example, the <code class="literal">System.Xml.XmlReader</code> type is scoped to the
        <code class="literal">System.Xml</code>sharedassembly, which is
        identified using a shared name and associated version number.</p><p>When your application starts, the CLR attempts to resolve all
        static TypeRefs by locating the correct versions of each dependent
        assembly (as determined by the versioning policy) and verifying that
        the types actually exist (ignoring any access modifiers).</p><p>When your application attempts to use the type, the CLR verifies
        that you have the correct level of access and throws runtime
        exceptions if there is a versioning incompatibility.</p></div><div class="sect2" title="Security Permissions"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-9.6"/>Security Permissions</h2></div></div></div><p>The<a class="indexterm" id="IXT-3-218712"/> <a class="indexterm" id="IXT-3-218713"/> assembly forms a boundary for security
        permissioning.</p><p>The assembly manifest contains hashes for any referenced
        assemblies (determined at compile time), a list of the minimum set of
        security permissions the assembly <span class="emphasis"><em>requires</em></span> in
        order to function, a list of the optional permissions that it
        <span class="emphasis"><em>requests</em></span>, and a list of the permissions that it
        explicitly <span class="emphasis"><em>refuses</em></span> (i.e., never wants to
        receive).</p><p>To illustrate how these permissions might be used, imagine an
        email client similar to Microsoft Outlook, developed using the .NET
        Framework. It probably requiresthe ability to communicate over the
        network on ports 110 (POP3), 25 (SMTP), and 143 (IMAP4). It might
        request the ability to run Java Script functions in a sandbox to allow
        for full interactivity when presenting HTML emails. Finally, it
        probably refusesever being granted the ability to write to disk or
        read the local address book, thus avoiding scripting attacks such as
        the <span class="emphasis"><em>ILoveYou</em></span> virus.</p><p>Essentially, the assembly declares its security needs and
        assumptions, but leaves the final decision on permissioning up to the
        CLR, which enforces local security policy.</p><p>At runtime the CLR uses the hashes to determine if a dependent
        assembly has been tampered with and combines the assembly permission
        information with local security policy to determine whether to load
        the assembly and which permissions to grant it.</p><p>This mechanism provides fine-grained control over security and
        is a major advantage of the .NET Framework over traditional Windows
        <a class="indexterm" id="IXTR3-51"/> <a class="indexterm" id="IXTR3-52"/>applications.</p></div></div></body></html>