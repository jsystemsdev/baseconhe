<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Reflection</title><link href="core.css" rel="stylesheet" type="text/css"/><meta content="DocBook XSL Stylesheets V1.74.0" name="generator"/>
<meta content="urn:uuid:e0000000-0000-0000-0000-000000536657" name="Adept.expected.resource"/></head><body><div class="sect1" title="Reflection"><div class="titlepage"><div><div><h1 class="title"><a id="csharpess2-CHP-3-SECT-10"/>Reflection</h1></div></div></div><p>Many<a class="indexterm" id="csharpess2-IDXTERM-842"/> <a class="indexterm" id="csharpess2-IDXTERM-843"/> of the services available in .NET and exposed via C#
      (such as late binding, serialization, remoting, attributes, etc.) depend
      on the presence of metadata. Your own programs can also take advantage
      of this metadata and even extend it with new information.</p><p>Manipulating existing types via their metadata is termed
      <span class="emphasis"><em>reflection</em></span> and is done using a rich set of types in
      the <code class="literal">System.Reflection</code> namespace.
      Creating new types (and associated metadata) is termed <code class="literal">Reflection.Emit</code> and is done via the types in
      the <code class="literal">System.Reflection.Emit</code> namespace.
      You can extend the metadata for existing types with custom attributes.
      For more information, see <a class="link" href="ch03s11.html" title="Custom Attributes">Section 3.11</a> later in this
      chapter.</p><div class="sect2" title="Type Hierarchy"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-10.1"/>Type Hierarchy</h2></div></div></div><p>Reflection <a class="indexterm" id="csharpess2-IDXTERM-844"/>involves traversing and manipulating an object model
        that represents an application, including all its compile-time and
        runtime elements. Consequently, it is important to understand the
        various logical units of a .NET application and their roles and
        relationships.</p><p>The fundamental units of an application are its types, which
        contain members and nested types. In addition to types, an application
        contains one or more modules and one or more assemblies. All these
        elements are static and are described in metadata produced by the
        compiler at compile time. The one exception to this rule is elements
        (such as types, modules, assemblies, etc.) that are created on the fly
        via <code class="literal">Reflection.Emit</code>, which is
        described in the later section <a class="link" href="ch03s10.html#csharpess2-CHP-3-SECT-10.8" title="Creating New Types at Runtime">Section 3.10.8</a>.</p><p>At runtime, these elements are all contained within an <code class="literal">AppDomain</code>. An <code class="literal">AppDomain</code> isn’t described with metadata, yet
        it plays an important role in reflection because it forms the root of
        the type hierarchy of a .NET application at runtime.</p><p>In any given application, the relationship between these units
        is hierarchical, as depicted by the following diagram:</p><a id="I_3_tt325"/><pre class="programlisting">AppDomain (runtime root of hierarchy)
  Assemblies
    Modules
      Types
        Members
        Nested types</pre><p>Each of these elements is discussed in the following
        sections.</p><div class="sect3" title="Types, members, and nested types"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-10.1.1"/>Types, members, and nested types</h3></div></div></div><p>The<a class="indexterm" id="IXT-3-218714"/> <a class="indexterm" id="IXT-3-218715"/> <a class="indexterm" id="IXT-3-218716"/> most basic element that reflection deals with is the
          type. This class represents the metadata for each type declaration
          in an application (both predefined and user-defined types).</p><p>Types contain members, which include constructors, fields,
          properties, events, and methods. In addition, types may contain
          nested types, which exist within the scope of an outer type and are
          typically used as helper classes. Types are grouped into modules,
          which are, in turn, contained within assemblies.</p></div><div class="sect3" title="Assemblies and modules"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-10.1.2"/>Assemblies and modules</h3></div></div></div><p>Assemblies<a class="indexterm" id="IXT-3-218717"/> <a class="indexterm" id="IXT-3-218718"/>are the logical equivalent of DLLs in Win32 and the
          basic units of deployment, versioning, and reuse for types. In
          addition, assembliescreate a security, visibility, and scope
          resolution boundary for types (see <a class="link" href="ch03s09.html" title="Assemblies">Section 3.9</a>).</p><p>A module is a physical file such as a DLL, an EXE, or a
          resource (such as GIFs or JPGs). While it isn’t common practice, an
          assemblycan be composed of multiple modules, allowing you to control
          application working set size, use multiple languages within one
          assembly, and share a module across multiple assemblies.</p></div><div class="sect3" title="AppDomains"><div class="titlepage"><div><div><h3 class="title"><a id="csharpess2-CHP-3-SECT-10.1.3"/>AppDomains</h3></div></div></div><p>From <a class="indexterm" id="IXT-3-218719"/>the perspective of reflection, an <code class="literal">AppDomain</code> is the root of the type
          hierarchy and serves as the container for assemblies and types when
          they are loaded into memory at runtime. A helpful way to think about
          an <code class="literal">AppDomain</code>is to view it as the
          logical equivalent of a process in a Win32 application.</p><p><code class="literal">AppDomain</code>s provide
          isolation, creating a hard boundary for managed code just like the
          process boundary under Win32. Similar to processes, <code class="literal">AppDomain</code>s can be started and stopped
          independently, and application faults take down only the <code class="literal">AppDomain</code> the fault occurs in, not the
          process hosting the<a class="indexterm" id="IXTR3-53"/>
          <code class="literal">AppDomain</code>.</p></div></div><div class="sect2" title="Retrieving the Type for an Instance"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-10.2"/>Retrieving the Type for an Instance</h2></div></div></div><p>At<a class="indexterm" id="IXT-3-218720"/> <a class="indexterm" id="IXT-3-218721"/> <a class="indexterm" id="IXT-3-218722"/> <a class="indexterm" id="IXT-3-218723"/> the heart of reflection is <code class="literal">System.Type</code>, which is an abstract base class
        that provides access to the metadata of a type.</p><p>You can access the <code class="literal">Type</code> class
        for any instance using <code class="literal">GetType</code>,
        which is a nonvirtual method of <code class="literal">System.Object</code>. When you call <code class="literal">GetType</code>, the method returns a concrete
        subtype of <code class="literal">System.Type</code>, which can
        reflect over and manipulate the type.</p></div><div class="sect2" title="Retrieving a Type Directly"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-10.3"/>Retrieving a Type Directly</h2></div></div></div><p>You can also retrieve a <code class="literal">Type</code>
        class by name (without needing an instance) using the static method
        <code class="literal">GetType</code> on the <code class="literal">Type</code> class, as follows:</p><a id="I_3_tt326"/><pre class="programlisting">Type t = Type.GetType("System.Int32");</pre><p>Finally, C# provides the <code class="literal">typeof</code> operator, which returns the <code class="literal">Type</code> class for any type known at compile
        time:</p><a id="I_3_tt327"/><pre class="programlisting">Type t = typeof(System.Int32);</pre><p>The main difference between these two approaches is that
        <code class="literal">Type.GetType</code> is evaluated at
        runtime and is more dynamic, binding by name, while the <code class="literal">typeof</code> operator is evaluated at compile
        time, uses a type token, and is slightly faster to call.</p></div><div class="sect2" title="Reflecting Over a Type Hierarchy"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-10.4"/>Reflecting Over a Type Hierarchy</h2></div></div></div><p>Once you have retrieved a <code class="literal">Type</code> instance you can navigate the
        application hierarchy described earlier, accessing the metadata via
        types that represent members, modules, assemblies, namespaces,
        <code class="literal">AppDomain</code>s, and nested types. You
        can also inspect the metadata and any custom attributes, create new
        instances of the types, and invoke members.</p><p>Here is an example that uses reflection to display the members
        in three different types:</p><a id="I_3_tt328"/><pre class="programlisting">using System;
using System.Reflection;
class Test {
  static void Main(  ) {
    object o = new Object(  );
    DumpTypeInfo(o.GetType(  ));
    DumpTypeInfo(typeof(int));
    DumpTypeInfo(Type.GetType("System.String"));
  }
  static void DumpTypeInfo(Type t) {
    Console.WriteLine("Type: {0}", t);

    // Retrieve the list of members in the type
    MemberInfo[] miarr = t.GetMembers(  );

    // Print out details on each of them
    foreach (MemberInfo mi in miarr)
      Console.WriteLine("  {0}={1}", mi.MemberType, mi);
  }
}</pre></div><div class="sect2" title="Late Binding to Types"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-10.5"/>Late Binding to Types</h2></div></div></div><p>Reflection can also perform<a class="indexterm" id="IXT-3-218724"/> <a class="indexterm" id="IXT-3-218725"/> <span class="emphasis"><em>late binding</em></span>, in which the
        application dynamically loads, instantiates, and uses a type at
        runtime. This provides greater flexibility at the expense of
        invocation overhead.</p><p>In this section, we create an example that uses very late
        binding, dynamically discovers new types at runtime, and uses
        them.</p><p>In the example one or more assemblies are loaded by name (as
        specified on the command line) and iterated through the types in the
        assembly, looking for subtypes of the <code class="literal">Greeting</code> abstract base class. When one is
        found, the type is instantiated and its <code class="literal">SayHello</code> method invoked, which displays an
        appropriate greeting.</p><p>To perform the runtime discovery of types, we use an abstract
        base class that’s compiled into an assembly as follows (see the source
        comment for filename and compilation information):</p><a id="I_3_tt329"/><pre class="programlisting">// Greeting.cs - compile with /t:library
public abstract class Greeting { 
  public abstract void SayHello(  );
}</pre><p>Compiling this code produces a file named
        <span class="emphasis"><em>Greeting.dll</em></span>, which the other parts of the sample
        can use.</p><p>We now create a new assembly containing two concrete subtypes of
        the abstract type <code class="literal">Greeting</code>, as
        follows (see the source comment for filename and compilation
        information):</p><a id="I_3_tt330"/><pre class="programlisting">// English.cs - compile with /t:library /r:Greeting.dll
using System;
public class AmericanGreeting : Greeting {
  private string msg = "Hey, dude. Wassup!";
  public override void SayHello(  ) {
    Console.WriteLine(msg);
  }
}
public class BritishGreeting : Greeting {
  private string msg = "Good morning, old chap!";
  public override void SayHello(  ) {
    Console.WriteLine(msg);
  }
}</pre><p>Compiling the source file <em class="filename">English.cs</em> produces a file named
        <span class="emphasis"><em>English.dll,</em></span> which the main program can now
        dynamically reflect over and use.</p><p>Now we create the main sample, as follows (see the source
        comment for filename and compilation information):</p><a id="I_3_tt331"/><pre class="programlisting">// SayHello.cs - compile with /r:Greeting.dll
// Run with SayHello.exe &lt;dllname1&gt; &lt;dllname2&gt; ... &lt;dllnameN&gt;
using System;
using System.Reflection;
class Test {
  static void Main (string[] args) {

    // Iterate over the cmd-line options,
    // trying to load each assembly
    foreach (string s in args) {
      Assembly a = Assembly.LoadFrom(s);
      
      // Pick through all the public type, looking for
      // subtypes of the abstract base class Greeting
      foreach (Type t in a.GetTypes(  ))
        if (t.IsSubclassOf(typeof(Greeting))) {

          // Having found an appropriate subtype, create it
          object o = Activator.CreateInstance(t);

          // Retrieve the SayHello MethodInfo and invoke it
          MethodInfo mi = t.GetMethod("SayHello");
          mi.Invoke(o, null);
        }
    }
  }
}</pre><p>Running the sample now with <span class="emphasis"><em>SayHello
        English.dll</em></span> produces the following output:</p><a id="I_3_tt332"/><pre class="programlisting">Hey, dude. Wassup!
Good morning, old chap!</pre><p>The interesting aspect of the preceding sample is that it’s
        completely late-bound; i.e., long after the
        <span class="emphasis"><em>SayHello</em></span> program is shipped you can create a new
        type and have <span class="emphasis"><em>SayHello</em></span> automatically take
        advantage of it by simply specifying it on the command line. This is
        one of the key benefits of late binding via reflection.</p></div><div class="sect2" title="Activation"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-10.6"/>Activation</h2></div></div></div><p>In <a class="indexterm" id="IXT-3-218726"/> <a class="indexterm" id="IXT-3-218727"/>the previous examples, we loaded an assembly by hand and
        used the <code class="literal">System.Activator</code> class to
        create a new instance based on a type. There are many overrides of the
        <code class="literal">CreateInstance</code> method that provide
        a wide range of creation options, including the ability to
        short-circuit the process and create a type directly:</p><a id="I_3_tt333"/><pre class="programlisting">object o = Activator.CreateInstance("Assem1.dll",
                                    "Friendly.Greeting");</pre><p>Other capabilities of the <code class="literal">Activator</code> type include creating types on
        remote machines, creating types in specific <code class="literal">AppDomain</code>s (sandboxes), and creating types
        by invoking a specific constructor (rather than using the default
        constructor as these examples show).</p></div><div class="sect2" title="Advanced Uses of Reflection"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-10.7"/>Advanced Uses of Reflection</h2></div></div></div><p>The <a class="indexterm" id="IXT-3-218728"/>preceding example demonstrates the use of reflection,
        but doesn’t perform any tasks you can’t accomplish using normal C#
        language constructs. However, reflection can also manipulate types in
        ways not supported directly in C#, as is demonstrated in this
        section.</p><p>While the CLR enforces access controls on type members
        (specified using access modifiers such as <code class="literal">private</code> and <code class="literal">protected</code>), these restrictions don’t apply
        to reflection. Assuming you have the correct set of permissions, you
        can use reflection to access and manipulate private data and function
        members, as this example using the <code class="literal">Greeting</code> subtypes from the previous section
        shows (see the source comment for filename and compilation
        information):</p><a id="I_3_tt334"/><pre class="programlisting">// InControl.cs - compile with /r:Greeting.dll,English.dll
using System;
using System.Reflection;
class TestReflection {
  // Note: This method requires the ReflectionPermission perm.
  static void ModifyPrivateData(object o, string msg) {

    // Get a FieldInfo type for the private data member
    Type t = o.GetType(  ); 
    FieldInfo fi = t.GetField("msg", BindingFlags.NonPublic|
                                     BindingFlags.Instance);

    // Use the FieldInfo to adjust the data member value
    fi.SetValue(o, msg);
  }
  static void Main(  ) {
    // Create instances of both types
    BritishGreeting bg = new BritishGreeting(  );
    AmericanGreeting ag = new AmericanGreeting(  );

    // Adjust the private data via reflection
    ModifyPrivateData(ag, "Things are not the way they seem");
    ModifyPrivateData(bg, "The runtime is in total control!");
    
    // Display the modified greeting strings
    ag.SayHello(  ); // "Things are not the way they seem"
    bg.SayHello(  ); // "The runtime is in total control!"
  }
}</pre><p>When run, this sample generates the following output:</p><a id="I_3_tt335"/><pre class="programlisting">Things are not the way they seem
The runtime is in total control!</pre><p>This demonstrates that the private <code class="literal">msg</code> data members in both types are modified
        via reflection, although there are no public members defined on the
        types that allow that operation. Note that while this technique can
        bypass access controls, it still doesn’t violate type safety.</p><p>Although this is a somewhat contrived example, the capability
        can be useful when building utilities such as class browsers and test
        suite automation tools that need to inspect and interact with a type
        at a deeper level than its public interface.</p></div><div class="sect2" title="Creating New Types at Runtime"><div class="titlepage"><div><div><h2 class="title"><a id="csharpess2-CHP-3-SECT-10.8"/>Creating New Types at Runtime</h2></div></div></div><p>The <code class="literal">System.Reflection.Emit</code><a class="indexterm" id="IXT-3-218729"/> <a class="indexterm" id="IXT-3-218730"/> namespace contains classes that can create entirely new
        types at runtime. These classes can define a dynamic assembly in
        memory; define a dynamic module in the assembly; define a new type in
        the module, including all its members; and emit the MSIL opcodes
        needed to implement the application logic in the members.</p><p>Here is an example that creates and uses a new type called
        <code class="literal">HelloWorld</code> with a member called
        <code class="literal">SayHello</code>:</p><a id="I_3_tt336"/><pre class="programlisting">using System;
using System.Reflection;
using System.Reflection.Emit;
public class Test {
  static void Main(  )  {
    // Create a dynamic assembly in the current AppDomain
    AppDomain ad = AppDomain.CurrentDomain;
    AssemblyName an = new AssemblyName(  );
    an.Name = "DynAssembly";
    AssemblyBuilder ab = 
      ad.DefineDynamicAssembly(an, AssemblyBuilderAccess.Run);
    
    // Create a module in the assembly and a type in the module
    ModuleBuilder modb = ab.DefineDynamicModule("DynModule");
    TypeBuilder tb = modb.DefineType("AgentSmith", 
                                     TypeAttributes.Public);
 
    // Add a SayHello member to the type 
    MethodBuilder mb = tb.DefineMethod("SayHello",        
                                       MethodAttributes.Public,
                                       null, null);
                                        
    // Generate the MSIL for the SayHello Member
    ILGenerator ilg = mb.GetILGenerator(  );
    ilg.EmitWriteLine("Never send a human to do a machine's job.");
    ilg.Emit(OpCodes.Ret);

    // Finalize the type so we can create it
    Type t = tb.CreateType(  );

    // Create an instance of the new type
    object o = Activator.CreateInstance(t);
    
    // Prints "Never send a human to do a machine's job."
    t.GetMethod("SayHello").Invoke(o, null);
  }
}</pre><p>A common example using <code class="literal">Reflection.Emit</code> is the regular expression
        support in the FCL, which can emit new types that are tuned to search
        for specific regular expressions, eliminating the overhead of
        interpreting the regular expression at runtime.</p><p>Other uses of <code class="literal">Reflection.Emit</code>
        in the FCL include dynamically generating transparent proxies for
        remoting and generating types that perform specific XSLT transforms
        with the minimum runtime<a class="indexterm" id="IXTR3-54"/>
        <a class="indexterm" id="IXTR3-55"/> overhead.</p></div></div></body></html>