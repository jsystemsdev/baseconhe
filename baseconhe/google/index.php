<?php
namespace Google\Cloud\Samples\Storage;

# [START list_buckets]
use Google\Cloud\Storage\StorageClient;
session_start();
$url_array = explode('?', 'http://'.$_SERVER ['HTTP_HOST'].$_SERVER['REQUEST_URI']);
$url = $url_array[0];

require_once 'google-api-php-client/src/Google_Client.php';
require_once 'google-api-php-client/src/contrib/Google_DriveService.php';
$client = new Google_Client();
$client->setClientId('1009138791673-tnv7s6m7j8ov8s05r99qudl94i2mh57m.apps.googleusercontent.com');
$client->setClientSecret('uDmSH5rwdymloLJvFRLs6NLe');
$client->setRedirectUri($url);
$client->setScopes(array('https://www.googleapis.com/auth/drive'));
if (isset($_GET['code'])) {
    $_SESSION['accessToken'] = $client->authenticate($_GET['code']);
    header('location:'.$url);exit;
} elseif (!isset($_SESSION['accessToken'])) {
    $client->authenticate();
}
$files= array();
$dir = dir('files');
while ($file = $dir->read()) {
    if ($file != '.' && $file != '..') {
        $files[] = $file;
    }
}
$dir->close();
if (!empty($_POST)) {
    $client->setAccessToken($_SESSION['accessToken']);
    $service = new Google_DriveService($client);
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $file = new Google_DriveFile();
    foreach ($files as $file_name) {
        $file_path = 'files/'.$file_name;
        $mime_type = finfo_file($finfo, $file_path);
        $file->setTitle($file_name);
        $file->setDescription('This is a '.$mime_type.' document');
        $file->setMimeType($mime_type);
        $service->files->insert(
            $file,
            array(
                'data' => file_get_contents($file_path),
                'mimeType' => $mime_type
            )
        );
    }
    finfo_close($finfo);
    //header('location:'.$url);
    //exit;
    $id = "17xqarEW1GczCnNloJIYm8rZ2CGxzcOxi";
    $teste = printFile($service,$id);
    echo $teste;  
}
include 'index.phtml';

/**
 * Print a file's metadata.
 *
 * @param Google_Service_Drive $service Drive API service instance.
 * @param string $fileId ID of the file to print metadata for.
 */
function printFile($service, $fileId) {
    try {
      $file = $service->files->get($fileId);
      //var_dump($file);
      
     downloadFile($service, $file['downloadUrl']);
     // print "Title: " . $file['downloadUrl'];
     // print "Description: " . $file->getDescription();
     // print "MIME type: " . $file->getMimeType();
    } catch (Exception $e) {
      print "An error occurred: " . $e->getMessage();
    }
  }
  
  /**
   * Download a file's content.
   *
   * @param Google_Service_Drive $service Drive API service instance.
   * @param File $file Drive File instance.
   * @return String The file's content if successful, null otherwise.
   */
  function downloadFile($service, $file) {
    $downloadUrl = $file;
    if ($downloadUrl) {
      $request = new Google_Http_Request($downloadUrl, 'GET', null, null);
      $httpRequest = $service->getClient()->getAuth()->authenticatedRequest($request);
      if ($httpRequest->getResponseHttpCode() == 200) {
        return $httpRequest->getResponseBody();
        var_dump($httpRequest);
      } else {
        // An error occurred.
        return null;
      }
    } else {
      // The file doesn't have any content stored on Drive.
      return null;
    }
  }
  ?>