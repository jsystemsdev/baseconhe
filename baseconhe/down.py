from __future__ import print_function
import pickle
import os.path
import sys 
import io
from apiclient import errors
from apiclient import http
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from googleapiclient.http import MediaIoBaseDownload
from googleapiclient import discovery
from httplib2 import Http
from oauth2client import file as oauth2file, client, tools
from googleapiclient.http import MediaFileUpload
import os
import glob
# ...


def main():
     print('tentando baixar:')  
     service = build('drive', 'v3', credentials='token.pickle')
     file_id = '1N3HGe9GpS3jMzOo_TpqEz4AlZ7e-BnKx'
     request = service.files().get_media(fileId=file_id)
     fh = io.FileIO('vocetaaqui.zip', mode='wb')
     downloader = MediaIoBaseDownload(fh, request, chunksize=1024*1024)
     print(downloader._progress)
     done = False
     while done is False:
        status, done = downloader.next_chunk()
        print ("Download %d%%." % int(status.progress() * 100))    



if __name__ == '__main__':
  main()