<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Etrain</title>
    <link rel="icon" href="img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <!-- style CSS -->
	<link rel="stylesheet" href="css/style.css">
	<?PHP
	
		 $IP =$_SERVER["REMOTE_ADDR"];
		

	  ?>  
	
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu single_page_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand logo_1" href="index.html"> <img src="img/single_page_logo.png" alt="logo"> </a>
                        <a class="navbar-brand logo_2" href="index.html"> <img src="img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                                <li class="nav-item active">
                                    <a class="nav-link" href="index.html">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="about.html">Sobre</a>
                                </li>
                              
                                <li class="nav-item">
                                    <a class="nav-link" href="contact.html">Contato</a>
                                </li>
                                <li class="d-none d-lg-block">
                                    <a class="btn_1" href="#">Criar Conta</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->
	
	<!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
							<h2>Seja Bem Vindo</h2>
							<p>Biblioteca de Ebooks <span>/</span>Degustação</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	<!-- End banner Area -->

	<!-- Start Sample Area 1-->
	<section class="sample-text-area">
		<div class="container box_1170">
			<h3 class="text-heading">O que é ?</h3>
			<p class="sample-text">
			A Bibkioteca de ebooks é uma iniciativa de uma empresa em abrir 
			sua biblioteca de livros digitais para acesso ao puplbico, 
			por se tratar de uma empresa privada ela precisa de recursos
			para direcionar a esta iniciativa e manter no ar.
			Como compromisso de entrega de valor, foi criado este espaço
			para que os clientes possam degustar e usufruir de momentos 
			agradaveis na letura de um livro.

			</p>
		</div>
	</section>
	<!-- End Sample Area1 -->
	<!-- Start Sample Area2 -->
	<section class="sample-text-area">
		<div class="container box_1170">
			<h3 class="text-heading">Como funciona a degustação ?</h3>
			<p class="sample-text">
			A degustação funciona da seguinte maneria :
			<p>Para cada visitante sera disponibilizado a visualização de 1 exemplar.</p>
			Para visualizar:
			<li class="nav-item active">
			1. Selecione o filtro desejado AUTOR ou TITULO
			</li>
			<li class="nav-item active">
			2. Digite o Autor ou o Titulo no campo de pesquisa.
			</li>
			<li class="nav-item active">
			3. Clique em Enviar.
			</li>
			</p>
		</div>
	 </section>
	 <!-- End Sample Area2 -->

	 <!-- Checa o filtro -->
	 <form action="degusta.php"  method="post"> 
	  <div class="col-lg-3 col-md-4 mt-sm-30">
	  <div class="container box_1170">
		  <h3 class="container box_1170">Filto:</h3>
			<div class="default-select" id="default-select">
			  <select name=selectoption>
				<option value=1>Autor</option>
				<option value=2>Titulo</option>
				</select>
			            </div>
		               </div>
    	              </div> 
                      <div  class="section-top-border">
					     <h3 class="container box_1170">Dados para pesquisa:</h3>	 
				        <div class="row">
					  <div class="col-lg-8 col-md-8">
					  <h4 class="container box_1170"><input type="text" name="ip" value="<?php echo $IP; ?>"  required class="single-input" readonly > 
						 <div class="container box_1170">
				         
			             </div>
						 </h4> 
						<h4 class="container box_1170"><input type="text" name="busca" placeholder="Titulo ou Autor" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Titulo ou Autor'"
						 required class="single-input"> 
						 <div class="container box_1170">
				         <input class="col-lg-8 col-md-8"  type=submit>
			             </div>
						 </h4>
					<div class="mt-10">
						</div>
						
			 
				
				
				     
						
					
					
					
					
				
		    </div>
					 
		</div>
     <!-- fim  Checa o filtro -->
	  
	

	 

	<!-- footer part start-->
    <footer class="footer-area">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-sm-6 col-md-4 col-xl-3">
                    <div class="single-footer-widget footer_1">
                        <a href="index.html"> <img src="img/logo.png" alt=""> </a>
                        <p>Jether Systems </p>
                        <p> Banca De Ebooks </p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-xl-4">
                    <div class="single-footer-widget footer_2">
                        <h4>Newsletter</h4>
                        <p>Caso esteja interessado em receber nossas atualizaçãoes, por favor
                            deixe seu e-mail.
                        </p>
                        <form action="#">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder='Enter email address'
                                        onfocus="this.placeholder = ''"
                                        onblur="this.placeholder = 'Enter email address'">
                                    <div class="input-group-append">
                                        <button class="btn btn_1" type="button"><i class="ti-angle-right"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="social_icon">
                            <a href="#"> <i class="ti-facebook"></i> </a>
                            <a href="#"> <i class="ti-twitter-alt"></i> </a>
                            <a href="#"> <i class="ti-instagram"></i> </a>
                            <a href="#"> <i class="ti-skype"></i> </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 col-md-4">
                    <div class="single-footer-widget footer_2">
                        <h4>Contact us</h4>
                        <div class="contact_info">
                            <p><span> Address :</span>Campinas, SP </p>
                            <p><span> Phone :</span> +55 19 982-227092</p>
                            <p><span> Email : </span>suportebook@jethersystems.com.br</p>
                            <a href="Ferramentas/view/login.php" class="genric-btn primary-border">Admin login</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright_part_text text-center">
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Tecnologia desenvolvida com <i class="ti-heart" aria-hidden="true"></i> por <a href="http://www.jethersystems.com.br/" target="_blank">JetherSytems</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer part end-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="js/masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <!-- swiper js -->
    <script src="js/slick.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="js/custom.js"></script>
</body>

</html>