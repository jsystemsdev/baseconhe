
function AjaxF()
{
	var ajax;
	
	try
	{
		ajax = new XMLHttpRequest();
	} 
	catch(e) 
	{
		try
		{
			ajax = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch(e) 
		{
			try 
			{
				ajax = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e) 
			{
				alert("Seu browser não da suporte à AJAX!");
				return false;
			}
		}
	}
	return ajax;
}
// Função que faz as requisição Ajax ao arquivo PHP
function AlteraConteudo()
{
	var ajax = AjaxF();	
	
	ajax.onreadystatechange = function(){
		if(ajax.readyState == 4)
		{
			document.getElementById('conteudo').innerHTML = ajax.responseText;
			
		}
	}
	
	// Variável com os dados que serão enviados ao PHP
	var dados = "nome="+document.getElementById('txtnome').value;
	function reqListener () {
		console.log(this.responseText);
	  }
	
	var oReq = new XMLHttpRequest();
	oReq.addEventListener("load", reqListener);
	oReq.open("GET", "https://drive.google.com/file/d/1KPF9wyaPVMEJHJ9B4iBnm8aQWdJIekub/view?usp=drivesdk/view");
	oReq.send();
}