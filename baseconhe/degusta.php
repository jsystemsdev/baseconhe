<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Etrain</title>
    <link rel="icon" href="img/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <!-- style CSS -->
	<link rel="stylesheet" href="css/style.css">
	<?PHP
     require_once("bd/conexao_bd.php");
       //recebe o ip para comparar antes de proceguir  
      if(isset( $_POST["ip"])){
        $IPvisi =$_SERVER["REMOTE_ADDR"]; // ip visitante
        $chec = $_POST["selectoption"];  // 1 autor / 2 titulo 
        $dado = $_POST["busca"];         // dado pesquiza
        $ippost = $_POST["ip"];          // ip vindo do post
         //compara
        if ($ippost == $IPvisi ){
         $validaip = true;
        } else{
          $validaip = false;
          $msg="Algo errado com endereço do visitante, ERRO:171";
          echo  "<script>alert('$msg');</script>";   
        }
   

        

    }
       
		
	  ?>  
	
</head>

<body>
    <!--::header part start::-->
    <header class="main_menu single_page_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand logo_1" href="index.html"> <img src="img/single_page_logo.png" alt="logo"> </a>
                        <a class="navbar-brand logo_2" href="index.html"> <img src="img/logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">
                            <ul class="navbar-nav align-items-center">
                                <li class="nav-item active">
                                    <a class="nav-link" href="index.html">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="about.html">Sobre</a>
                                </li>
                              
                                <li class="nav-item">
                                    <a class="nav-link" href="contact.html">Contato</a>
                                </li>
                                <li class="d-none d-lg-block">
                                    <a class="btn_1" href="#">Criar Conta</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->
	
    <!-- breadcrumb start-->
    <?PHP
      
      if ($validaip = true){ // valida ture
        echo" <section class='breadcrumb breadcrumb_bg'>
        <div class='container'>
            <div class='row'>
                <div class='col-lg-12'>
                    <div class='breadcrumb_iner text-center'>
                        <div class='breadcrumb_iner_item'>
							<h2>Degustação</h2>
							<p>Banca de Ebooks</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	<!-- End banner Area -->

	<!-- Start Sample Area 1-->
	<section class='sample-text-area'>
		<div class='container box_1170'>
			<h3 class='text-heading'>O que é ?</h3>
			<p class='sample-text'>
			A Banca de ebooks é uma iniciativa de uma empresa em abrir 
			sua biblioteca de livros digitais para acesso ao puplbico, 
			por se tratar de uma empresa privada ela precisa de recursos
			para direcionar a esta iniciativa e manter no ar.
			Como compromisso de entrega de valor, foi criado este espaço
			para que os clientes possam degustar e usufruir de momentos 
			agradaveis na letura de um livro.

			</p>
		</div>
    </section>";
     // consulta o visitante na tabela degustação
     $degusta = new Degusta();
     $retorno = $degusta->busca($IPvisi,$dado,$chec); 
      
      if ($retorno == "N"){
          // segue o fluxo de busca 
          $condados = new Condados();
          $Ultconsulta = $condados->ultconsulta($IPvisi); 
        
      }
      if ($retorno == "S"){

     
    }
     
     
      }else{// fim valida true 
        
        // caso o valida for false 
        echo" <section class='breadcrumb breadcrumb_bg'>
        <div class='container'>
            <div class='row'>
                <div class='col-lg-12'>
                    <div class='breadcrumb_iner text-center'>
                        <div class='breadcrumb_iner_item'>
							<h2>Algo deu Errado com sua consulta</h2>
							<p>Banca de Ebooks <span>/</span>Degustação</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	<!-- End banner Area -->

	<!-- Start Sample Area 1-->
	<section class='sample-text-area'>
		<div class='container box_1170'>
			<h3 class='text-heading'>ERRO: 171 </h3>
			<p class='sample-text'>
            A validação do visitante se da por geolicalização.
            O ip da consulta nao é o mesmo do visitante !
            Volte a pagina anterior atualize e refaça a consulta.
            IP recebido do formulario de consulta: $ippost
             </p>
           <p class='sample-text' >
            IP recebido do visitante detectado : $IPvisi
            </p>  
		</div>
    </section>";  
      }// fim valida false

   


    ?>
	<!-- End Sample Area1 -->
	
	

	 
	  
	

	 

	<!-- footer part start-->
    <footer class="footer-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copyright_part_text text-center">
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Tecnologia desenvolvida com <i class="ti-heart" aria-hidden="true"></i> por <a href="http://www.jethersystems.com.br/" target="_blank">JetherSytems</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer part end-->

    <!-- jquery plugins here-->
    <!-- jquery -->
    <script src="js/jquery-1.12.1.min.js"></script>
    <!-- popper js -->
    <script src="js/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- easing js -->
    <script src="js/jquery.magnific-popup.js"></script>
    <!-- swiper js -->
    <script src="js/swiper.min.js"></script>
    <!-- swiper js -->
    <script src="js/masonry.pkgd.js"></script>
    <!-- particles js -->
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <!-- swiper js -->
    <script src="js/slick.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <!-- custom js -->
    <script src="js/custom.js"></script>
</body>

</html>

<?php
  // classes PHP
     //Class Degusta
  class Degusta{
    function __construct(){
    $this->con = ConexaoBD::obter_conexao();
     }
     function busca($IPvisi,$dado,$chek){
     $meu_comando = $this->con->query("SELECT * FROM degusta WHERE (ip ='" .$IPvisi."')");
     $linha = $meu_comando->fetch(PDO::FETCH_ASSOC);
     // verifica se houve retorno do banco !
      if ($linha > 0){ // encontrou
        // caso encontre pega se ja usou o link
        $dgaux = "N";
        while($linha = $meu_comando->fetch(PDO::FETCH_ASSOC)){
            if ($linha['degustado'] == "S") {
             $dgaux = $linha['degustado'];
            }
      }
        if($dgaux == "N"){
            $meu_comando = $this->con->query("INSERT INTO degusta (ip, dado,chek,degustado) VALUES ('" . $IPvisi . "', '" . $dado . "' ,'" . $chek . "','" . $dgaux . "')");
            if($meu_comando->rowCount() > 0){
            return  $dgaux; 
            }else{
             return  $dgaux;    
            }     

        }
   }else{ 
        $degustado = "N";
        $meu_comando = $this->con->query("INSERT INTO degusta (ip, dado,chek,degustado) VALUES ('" . $IPvisi . "', '" . $dado . "','" . $chek . "' ,'" . $degustado . "')");
        if($meu_comando->rowCount() > 0){
         return $degustado; 
        } 
      
    }//fim encontrou

     }//fim function

}//fim class tour
 
  // Class Condados
  Class Condados{
         function __construct(){
         $this->con = ConexaoBD::obter_conexao();
         }
         function ultconsulta($IPvisi){ 
         $meu_comandodeg = $this->con->query("SELECT * FROM degusta WHERE (ip ='" .$IPvisi."') order by id DESC limit 1"); 
         $ldeg = $meu_comandodeg->fetch(PDO::FETCH_ASSOC);
         if ($ldeg > 0){
          echo $ldeg['dado'];
          echo $ldeg['ip'];

         }

        }
        

  }



 ?>