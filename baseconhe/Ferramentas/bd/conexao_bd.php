<?php
class ConexaoBD {
    public static $instancia_unica;
 
    private function __construct() {
        
    }
 
    public static function obter_conexao() {
        if (!isset(self::$instancia_unica)) {
            self::$instancia_unica = ConexaoBD::criar_conexao();
        }
 
        return self::$instancia_unica;
    }

    public static function criar_conexao(){
    	$usuario = 'root';
		$senha = '';
		$host = 'localhost';
		$nome_banco ='hardcomponents';

		$str_con = 'mysql:host=' . $host . ';dbname=' . $nome_banco;
    	
    	try{
			$pdo = new PDO($str_con, $usuario, $senha);
  			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
			$pdo->query("SET character_set_results=utf8");
           		$pdo->query("SET character_set_client=utf8");
  			return $pdo;
		}
		catch(PDOException $e){
			echo "Erro de conexão";
		}
	}
	
}
 
?>