<?php 
	require_once("../model/ClienteDAO.php");
	require_once("../model/ClienteDTO.php");

	$dao = new ClienteDAO();
	$cliente = new Cliente();

	$email = $_POST['email'];
	$nomeCompleto = $_POST['nome'];
	$dataNascimento = $_POST['data_nasc'];
	$sexo = $_POST['sexo'];
	$cpf = $_POST['cpf'];
	$telefone = $_POST['telefone'];
	$senha = sha1($_POST['senha']);

	$cliente->setEmail($email);
	$cliente->setNomeCompleto($nomeCompleto);
	$cliente->setDataNascimento($dataNascimento);
	$cliente->setSexo($sexo);
	$cliente->setCpf($cpf);
	$cliente->setTelefone($telefone);
	$cliente->setSenha($senha);

	if($dao->criar($cliente)){
		echo "Cadastro realizado com sucesso!";
	} else {
		echo "Erro ao realizar o cadastro!";
	}

?>