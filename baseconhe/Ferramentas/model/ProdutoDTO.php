<?php 

class Produto{
	private $codigo;
	private $nome;
	private $categoria;
	private $preco;
	private $parcelas_numero;
	private $parcelas_valor;
	private $caracteristicas;
	private $especificacoes;
	private $conteudo_embalagem;
	private $garantia;
	private $peso;
	private $observacoes;
	private $url_img;

	public function getCodigo(){
		return $this->codigo;
	}

	public function setCodigo($codigo){
		$this->codigo = $codigo;
	}

	public function getNome(){
		return $this->nome;
	}

	public function setNome($nomeCompleto){
		$this->nome = $nomeCompleto;
	}

	public function getCategoria(){
		return $this->categoria;
	}

	public function setCategoria($x){
		$this->getCategoria = $x;
	}

	public function getPreco(){
		return $this->preco;
	}

	public function setPreco($x){
		$this->preco = $x;
	}

	public function getParcelasNumero(){
		return $this->parcelas_numero;
	}

	public function setParcelasNumero($x){
		$this->parcelas_numero = $x;
	}

	public function getParcelasValor(){
		return $this->parcelas_valor;
	}

	public function setParcelasValor($x){
		$this->parcelas_valor = $x;
	}

	public function getCaracteristicas(){
		return $this->caracteristicas;
	}

	public function setCaracteristicas($x){
		$this->caracteristicas = $x;
	}

	public function getEspecificacoes(){
		return $this->especificacoes;
	}

	public function setEspecificacoes($x){
		$this->especificacoes = $x;
	}

	public function getConteudoEmbalagem(){
		return $this->conteudo_embalagem;
	}

	public function setConteudoEmbalagem($x){
		$this->conteudo_embalagem = $x;
	}
	
	public function getGarantia(){
		return $this->garantia;
	}

	public function setGarantia($x){
		$this->garantia = $x;
	}

	public function getPeso(){
		return $this->peso;
	}

	public function setPeso($x){
		$this->peso = $x;
	}

	public function getObservacoes(){
		return $this->observacoes;
	}

	public function setObservacoes($x){
		$this->observacoes;
	}

	public function getUrlImg(){
		return $this->url_img;
	}

	public function setUrlImg($x){
		$this->url_img = $x;
	}
}
?>