<?php 

class cliente{
	private $codigo;
	private $email;
	private $nomeCompleto;
	private $dataNascimento;
	private $sexo;
	private $cpf;
	private $telefone;
	private $senha;

	public function getCodigo(){
		return $this->codigo;
	}

	public function setCodigo($codigo){
		$this->codigo = $codigo;
	}

	public function getEmail(){
		return $this->email;
	}

	public function setEmail($email){
		$this->email = $email;
	}

	public function getNomeCompleto(){
		return $this->nomeCompleto;
	}

	public function setNomeCompleto($nomeCompleto){
		$this->nomeCompleto = $nomeCompleto;
	}

	public function getDataNascimento(){
		return $this->dataNascimento;
	}

	public function setDataNascimento($dataNascimento){
		$this->dataNascimento = $dataNascimento;
    }

    public function getSexo(){
		return $this->sexo;
	}

	public function setSexo($sexo){
		$this->sexo = $sexo;
    }

    public function getCpf(){
		return $this->cpf;
	}

	public function setCpf($cpf){
		$this->cpf = $cpf;
    }

    public function getTelefone(){
		return $this->telefone;
	}

	public function setTelefone($telefone){
		$this->telefone = $telefone;
    }


    public function getSenha(){
		return $this->senha;
	}

	public function setSenha($senha){
		$this->senha = $senha;
    }
}

?>