<?php 

	require_once("../bd/conexao_bd.php");
	require_once("ClienteDTO.php");

	class ClienteDAO{
		
		private $con;

		function __construct(){
			$this->con = ConexaoBD::obter_conexao();
		}

		function criar($cliente){
			$meu_comando = $this->con->query("INSERT INTO cliente (email, nomeCompleto, dataNascimento, sexo, cpf, telefone, senha) VALUES ('" . $cliente->getEmail() . "', '" . $cliente->getNomeCompleto() . "', '" . $cliente->getDataNascimento() . "', '" . $cliente->getSexo() . "', '" . $cliente->getCpf() . "', '" . $cliente->getTelefone() . "', '" . $cliente->getSenha() . "')");

			if($meu_comando->rowCount() > 0){
				return true;
			} else {
				return false;
			}
		}

		function adquirir($codigo){
			$meu_comando = $this->con->query("SELECT codigo, email, nomeCompleto, dataNascimento, sexo, cpf, telefone, senha FROM cliente WHERE (codigo = '" . $codigo ."')");
			$linha = $meu_comando->fetch(PDO::FETCH_ASSOC);

			$c = new Cliente();
			$c->setCodigo($linha['codigo']);
			$c->setEmail($linha['email']);
			$c->setNomeCompleto($linha['nomeCompleto']);
			$c->setDataNascimento($linha['dataNascimento']);
			$c->setSexo($linha['sexo']);
			$c->setCpf($linha['cpf']);
			$c->setTelefone($linha['telefone']);
			$c->setSenha($linha['senha']);

			return $c;
		}

		function adquirir_todos(){
			$lista = [];
			$meu_comando = $this->con->query("SELECT codigo, email, nomeCompleto, dataNascimento, sexo, cpf, telefone, senha FROM cliente");
			while($linha = $meu_comando->fetch(PDO::FETCH_ASSOC)){
				$c = new Cliente();
				$c->setCodigo($linha['codigo']);
				$c->setEmail($linha['email']);
				$c->setNomeCompleto($linha['nomeCompleto']);
				$c->setDataNascimento($linha['dataNascimento']);
				$c->setSexo($linha['sexo']);
				$c->setCpf($linha['cpf']);
				$c->setTelefone($linha['telefone']);
				$c->setSenha($linha['senha']);
				array_push($lista, $c);

			}
			return $lista;
		}
       

	}

?>