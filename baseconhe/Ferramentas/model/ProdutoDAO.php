<?php 

	require_once("../bd/conexao_bd.php");
	require_once("ProdutoDTO.php");

	class ProdutoDAO{
		
		private $con;

		function __construct(){
			$this->con = ConexaoBD::obter_conexao();
		}

		function criar($cliente){
			$meu_comando = $this->con->query("INSERT INTO produto (nome, preco, categoria, parcelas_numero, parcelas_valor, caracteristicas, especificacoes, conteudo_embalagem, garantia, peso, observacoes_gerais) VALUES ('" . $cliente->getNome() . "', '" . $cliente->getPreco() . "', '" . $cliente->getCategoria() . "','" . $cliente->getParcelasNumero() . "', '" . $cliente->getParcelasValor() . "', '" . $cliente->getCaracteristicas() . "', '" . $cliente->getEspecificacoes() . "', '" . $cliente->getConteudoEmbalagem() . "','" . $cliente->getGarantia() . "','" . $cliente->getPeso() . "','" . $cliente->getObservacoes() . "','" . $cliente->getUrlImg() . "')");

			if($meu_comando->rowCount() > 0){
				return true;
			} else {
				return false;
			}
		}

		function obter($codigo){
			$meu_comando = $this->con->query("SELECT nome, preco, categoria, parcelas_numero, parcelas_valor, caracteristicas, especificacoes, conteudo_embalagem, garantia, peso, observacoes_gerais, url_imagem FROM produto WHERE (codigo = '" . $codigo ."')");
			$linha = $meu_comando->fetch(PDO::FETCH_ASSOC);

			$p = new Produto();
			$p->setCodigo($codigo);
			$p->setNome($linha['nome']);
			$p->setPreco($linha['preco']);
			$p->setCategoria($linha['categoria']);
			$p->setParcelasNumero($linha['parcelas_numero']);
			$p->setParcelasValor($linha['parcelas_valor']);
			$p->setCaracteristicas($linha['caracteristicas']);
			$p->setEspecificacoes($linha['especificacoes']);
			$p->setConteudoEmbalagem($linha['conteudo_embalagem']);
			$p->setGarantia($linha['garantia']);
			$p->setPeso($linha['peso']);
			$p->setObservacoes($linha['observacoes_gerais']);
			$p->setUrlImg($linha['url_imagem']);

			return $p;
		}

		function adquirir_todos(){     
			$lista = [];     
			$meu_comando = $this->con->query("SELECT codigo, nome, preco, categoria,
			parcelas_numero, parcelas_valor, caracteristicas, especificacoes,
			conteudo_embalagem, garantia, peso, observacoes_gerais, url_imagem
			FROM produto ");  
			while($linha = $meu_comando->fetch(PDO::FETCH_ASSOC)){ 
				$p = new Produto();
				$p->setCodigo($linha['codigo']); $p->setNome($linha['nome']);
				$p->setPreco($linha['preco']); $p->setCategoria($linha['categoria']);
				$p->setParcelasNumero($linha['parcelas_numero']);
				$p->setParcelasValor($linha['parcelas_valor']);
				$p->setCaracteristicas($linha['caracteristicas']);
				$p->setEspecificacoes($linha['especificacoes']);
				$p->setConteudoEmbalagem($linha['conteudo_embalagem']);
				$p->setGarantia($linha['garantia']); $p->setPeso($linha['peso']);
				$p->setObservacoes($linha['observacoes_gerais']);
				$p->setUrlImg($linha['url_imagem']);
				array_push($lista, $p);
			}
			return $lista;
		}



		function adquirir_por_categoria($nome_categoria){     
			$lista = [];     
			$meu_comando = $this->con->query("SELECT codigo, nome, preco, categoria,
			parcelas_numero, parcelas_valor, caracteristicas, especificacoes,
			conteudo_embalagem, garantia, peso, observacoes_gerais, url_imagem
			FROM produto WHERE (categoria = '" . $nome_categoria . "')");  
			while($linha = $meu_comando->fetch(PDO::FETCH_ASSOC)){ 
				$p = new Produto();
				$p->setCodigo($linha['codigo']); $p->setNome($linha['nome']);
				$p->setPreco($linha['preco']); $p->setCategoria($linha['categoria']);
				$p->setParcelasNumero($linha['parcelas_numero']);
				$p->setParcelasValor($linha['parcelas_valor']);
				$p->setCaracteristicas($linha['caracteristicas']);
				$p->setEspecificacoes($linha['especificacoes']);
				$p->setConteudoEmbalagem($linha['conteudo_embalagem']);
				$p->setGarantia($linha['garantia']); $p->setPeso($linha['peso']);
				$p->setObservacoes($linha['observacoes_gerais']);
				$p->setUrlImg($linha['url_imagem']);
				array_push($lista, $p);
			}
			return $lista;
		}

		function adquirir_por_nome($nome){     
			$lista = [];     
			$meu_comando = $this->con->query("SELECT codigo, nome, preco, categoria,
			parcelas_numero, parcelas_valor, caracteristicas, especificacoes,
			conteudo_embalagem, garantia, peso, observacoes_gerais, url_imagem
			FROM produto WHERE (nome like '%" . $nome . "%')");  
			while($linha = $meu_comando->fetch(PDO::FETCH_ASSOC)){ 
				$p = new Produto();
				$p->setCodigo($linha['codigo']); $p->setNome($linha['nome']);
				$p->setPreco($linha['preco']); $p->setCategoria($linha['categoria']);
				$p->setParcelasNumero($linha['parcelas_numero']);
				$p->setParcelasValor($linha['parcelas_valor']);
				$p->setCaracteristicas($linha['caracteristicas']);
				$p->setEspecificacoes($linha['especificacoes']);
				$p->setConteudoEmbalagem($linha['conteudo_embalagem']);
				$p->setGarantia($linha['garantia']); $p->setPeso($linha['peso']);
				$p->setObservacoes($linha['observacoes_gerais']);
				$p->setUrlImg($linha['url_imagem']);
				array_push($lista, $p);
			}
			return $lista;
		}

	}

?>