<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Hard Components</title>
  <link rel="icon" type="image/jpg" href="img/logohc.png" />
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script type="text/javascript" src="js/script.js"></script>

</head>
<body>

<?php
include("Menu.php");
?>

<section class="team">
	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="z-index:1 !important">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="img/slide1.jpg" alt="Primeiro Slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="img/slide2.jpg" alt="Segundo Slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="img/slide3.jpg" alt="Terceiro Slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Anterior</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Próximo</span>
  </a>
</div>
</section>

<div class="corpo"><br>

<div style="width: 100%; margin: 0 0 0 9.5%;">
  <?php 
    require_once('../model/ProdutoDAO.php');
    require_once('../model/ProdutoDTO.php');
    $dao = new ProdutoDao();

    if(isset($_GET['categoria'])){
      $lstProdutos = $dao->adquirir_por_categoria($_GET['categoria']);
    }
    else if(isset($_GET['nome'])){
      $lstProdutos = $dao->adquirir_por_nome($_GET['nome']);
    }
    else{
      $lstProdutos = $dao->adquirir_todos();
    }

    foreach($lstProdutos as $x){
      echo '<section class="vitrine">
      <div class="H-img">
        <a target="_top" href="detalhesProduto.php?codigo=' . $x->getCodigo() . '">
          <img src="' . $x->getUrlImg() . '" alt="' . $x->getNome() . '" title="' .$x->getNome() . '" border="0" width="210" height="210" valign="middle">
        </a>
      </div>
      <div class="padding-prime">
        <div class="align-list">
          <div style="height: 75%;">
            <a target="_top" href="detalhesProduto.php?codigo=' . $x->getCodigo() . '">
              <h2 class="H-titulo" style="font-weight: normal; margin-bottom: 0;">' . $x->getNome() .'</h2>
            </a>
          </div>
          <br>
          <div class="preco-hc">
          <div class="H-preco"><b>R$ ' . $x->getPreco() .'</b></div>
          <div class="H-preco12x"><b>até ' . $x->getParcelasNumero() . 'x de R$ ' . $x->getParcelasValor() . '</b></div>
          <div class="H-semjuros"><b>sem juros no cartão</b></div>
          <br>
          </div>
          <div class="H-comprar">
          <button onclick="pc()" class="btn2 btn-amarelo">Comprar </button>
          </div>
        </div>
      </div>
    </section>';
  }
  ?>  

  

</div>
</body>
</html>