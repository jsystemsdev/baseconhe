<!DOCTYPE html>
<html>
<head>
	<title> Cadastro </title>
	<meta charset="utf-8">
	<link rel="icon" type="image/jpg" href="img/logohc.png" />
	<link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body>

<?php 
	include("Menu.php");
?>

<div class="cadastro">
	<form class="box2" action="../controller/cadastroController.php" method="POST">
		<h4>Complete as Informações Abaixo</h4>
		<input type="email" name="email" placeholder="Email" required>
		<input type="text" name="nome" placeholder="Nome Completo" required>
		<div style="width: 100%; display: inline-flex;">
		<input type="text" style="margin-right: 1%;" name="data_nasc" maxlength="10" placeholder="Data de Nascimento" required>
		<select name="sexo" style="margin-left: 1%;">
			<option value="" id="">Sexo</option>	
			<option value="Masculino" id="">Masculino</option>			
			<option value="Feminino">Feminino</option>
			<option value="Outros">Outros</option>
		</select>
		</div>
		<div style="width: 100%; display: inline-flex;">
		<input type="text" style="margin-right: 1%;" name="cpf" maxlength="14" placeholder="CPF"required> 
		<input type="text" style="margin-left: 1%;" name="telefone" maxlength="14" placeholder="Telefone" required>
		</div>
		<input type="password" name="senha" placeholder="Senha" required>
		<button class="btn-cadastro"><img class="imgCadastro" src="img/btn-cadastro.png">
	</form>
</div>
</body>
</html>