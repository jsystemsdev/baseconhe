<!DOCTYPE html>
<html>
<head>
	<title> Realizar Compra </title>
	<meta charset="utf-8">
	<link rel="icon" type="image/jpg" href="img/logohc.png" />
	<link rel="stylesheet" type="text/css" href="estilo.css">
</head>
<body>

<?php 
	include("Menu.php");
?>

<div class="compra">
	<form class="boxcompra">
		<h4>Dados de Entrega</h4>
		<input type="text" name="rua" placeholder="Rua" required>
		<input type="text" name="numero" placeholder="Número" required>
		<input type="text" name="complemento" placeholder="Complemento" required>
		<input type="text" name="bairro" placeholder="Bairro" required>
		<div style="width: 100%; display: inline-flex;">
		<input type="text" style=" margin-right: 1%;" name="cidade" placeholder="Cidade" required>
		<select name="estado" style="margin-left: 1%;">
			<option value="" id="">Estado</option>
			<option value="AC" id="">AC</option>
			<option value="AL" id="">AL</option>
			<option value="AM" id="">AM</option>
			<option value="AP" id="">AP</option>
			<option value="BA" id="">BA</option>
			<option value="CE" id="">CE</option>
			<option value="DF" id="">DF</option>
			<option value="ES" id="">ES</option>
			<option value="GO" id="">GO</option>
			<option value="MA" id="">MA</option>
			<option value="MG" id="">MG</option>
			<option value="MS" id="">MS</option>
			<option value="MT" id="">MT</option>
			<option value="PA" id="">PA</option>
			<option value="PB" id="">PB</option>
			<option value="PE" id="">PE</option>
			<option value="PI" id="">PI</option>
			<option value="PR" id="">PR</option>
			<option value="RJ" id="">RJ</option>
			<option value="RN" id="">RN</option>
			<option value="RO" id="">RO</option>
			<option value="RR" id="">RR</option>
			<option value="RS" id="">RS</option>
			<option value="SC" id="">SC</option>
			<option value="SE" id="">SE</option>
			<option value="SP" id="">SP</option>
			<option value="TO" id="">TO</option>
		</select>
		</div>
		<h4>Formas de Entrega</h4>
		<input type="radio" name="entrega" value="pac"> PAC (10 dias úteis) - R$25,00 <br>
		<input type="radio" name="entrega" value="sedex"> SEDEX (5 dias úteis) - R$35,00 <br>
		<input type="radio" name="entrega" value="transportadora"> Transportadora (7 dias úteis) - R$50,00 <br><br>
		<h4>Informações de Pagamento</h4>
		<input type="text" name="num_cred" placeholder="Número do Cartão de Crédito" required>
		<input type="text" name="nome_titular" placeholder="Nome do Titular" required>
		<div style="width: 100%; display: inline-flex;">
		<select name="mês" style="margin-right: 1%;">
			<option value="" id="">Mês</option>
			<option value="jan" id="">Janeiro</option>
			<option value="fev" id="">Fevereiro</option>
			<option value="mar" id="">Março</option>
			<option value="abr" id="">Abril</option>
			<option value="mai" id="">Maio</option>
			<option value="jun" id="">Junho</option>
			<option value="jul" id="">Julho</option>
			<option value="ago" id="">Agosto</option>
			<option value="set" id="">Setembro</option>
			<option value="out" id="">Outubro</option>
			<option value="nov" id="">Novembro</option>
			<option value="dez" id="">Dezembro</option>
		</select>
		<input type="text" style="margin-left: 1%;" name="ano" placeholder="Ano" required>
		</div>
		<input type="text" name="cpf" placeholder="CPF" required>
		<input type="text" name="nasc_data" placeholder="Data de Nascimento" required>
		<div style="width: 100%; display: inline-flex;">
		<select name="parcelas">
			<option value="" id="">Parcelas</option>
			<option value="1x" id="">1x</option>
			<option value="2x" id="">2x</option>
			<option value="3x" id="">3x</option>
			<option value="4x" id="">4x</option>
			<option value="5x" id="">5x</option>
			<option value="6x" id="">6x</option>
			<option value="7x" id="">7x</option>
			<option value="8x" id="">8x</option>
			<option value="9x" id="">9x</option>
			<option value="10x" id="">10x</option>
			<option value="11x" id="">11x</option>
			<option value="12x" id="">12x</option>
		</select>
		</div>
	</form>
</div>
</body>
</html>