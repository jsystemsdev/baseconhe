<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/jpg" href="img/logohc.png" />
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script type="text/javascript" src="../js/script.js"></script>

</head>
<body>

	<header class="menu-principal">
	<main>
		

		<div class="container-menu">
			<div class="lg">
				<a href="index.php">
				<img src="img/logo.png" class="logo">
				</a>
			</div>
			<div class="container-seach">
				<form class="container-search" method="GET" action="index.php">
						<div class="search-box">
							<div class="search-input">
								<input class="search-txt" type="text" name="nome" placeholder="Digite aqui sua busca" size="45">
							</div>
							<div class="search-icon">
								<button type="submit" class="search-btn">
									<i class="fas fa-search"></i>
								</button>
							</div>
						</div>
				</form>
			</div>

			<div class="container-icons">
				<div class="container-login">
						<button onclick="login()" type="button" class="btn-login">
							<img src="img/login.png">
					</button>

					
				</div>
				<div class="carrinho">
					<button type="button" class="btn-carrinho">
						<img src="img/carrinho.png">
					</button> 
				</div>
			</div>

			
		</div>

		<nav class="menu">
			<ul>
				<li> <a href="index.php">Home</a></li>
				<li> <a href="">Hardware</a>
					<ul>
						<li> <a href="index.php?categoria=PlacaMae">Placa Mãe</a></li>
						<li> <a href="index.php?categoria=Memorias">Memórias</a></li>
						<li> <a href="index.php?categoria=PlacaDeVideo">Placa de Vídeo</a></li>
						<li> <a href="index.php?categoria=Hd">HDD</a></li>
						<li> <a href="index.php?categoria=Ssd">SSD</a></li>
						<li> <a href="index.php?categoria=Gabinete">Gabinete</a></li>
						<li> <a href="index.php?categoria=Fonte">Fonte</a></li>
						<li> <a href="index.php?categoria=Cooler">Coolers</a></li>
					</ul>
				</li>
				<li> <a href="">Periféricos</a>
					<ul>
						<li> <a href="index.php?categoria=Headset">Fones</a></li>
						<li> <a href="index.php?categoria=Teclado">Teclado</a></li>
						<li> <a href="index.php?categoria=Mouse">Mouse</a></li>
						<li> <a href="index.php?categoria=Mousepad">Mousepad</a></li>
						<li> <a href="index.php?categoria=Webcam">Webcam</a></li>
						<li> <a href="index.php?categoria=Cadeira">Cadeiras Gamer</a></li>
						<li> <a href="index.php?categoria=Cabo">Cabos</a></li>
						<li> <a href="index.php?categoria=Assessorio">Acessórios</a></li>
					</ul>
				</li>
				<li> <a href="">Monitores</a>
					<ul>
						<li> <a href="index.php?categoria=Monitor18">18 polegadas</a></li>
						<li> <a href="index.php?categoria=Monitor21">21 polegadas</a></li>
						<li> <a href="index.php?categoria=Monitor24">24 polegadas</a></li>
						<li> <a href="index.php?categoria=Monitor28">28 polegadas</a></li>
						<li> <a href="index.php?categoria=Monitor32">32 polegadas</a></li>
					</ul>
				</li>
				<li> <a href="index.php?categoria=Computadores">Computadores</a></li>
			</ul>
		</nav>

	</main>

</header>

<div class="espaco-cabecalho"></div>

</body>
</html>