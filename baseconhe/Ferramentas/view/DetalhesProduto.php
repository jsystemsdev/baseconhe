<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Computador Gamer NTC Vulcano 7016, AMD Ryzen 3 2200G, 8GB, 1TB – 15688</title>
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<link rel="icon" type="image/jpg" href="logohc.png"/>
</head>
<body>

<?php 
	require_once('../model/ProdutoDAO.php');
	require_once('../model/ProdutoDTO.php');

	include("Menu.php");

	$codigo = $_GET['codigo'];
	$produtoDao = new ProdutoDAO();

	$produto = $produtoDao->obter($codigo);
?>

<div class="corpo1">
	<br>
	<br>
	<div class="title1">
	<?php
		echo "<h5>" . $produto->getNome() . "</h5>";
	?>
	
	</div>
	<div>
		<?php
			echo "<img src = '" . $produto->getUrlImg() . "' class='pc1'>";
		?>
		
		<div class="borda">
			<div class="preco">
				<?php echo "R$ " . $produto->getPreco()?>
			</div>
			<div class="juros">
				<strong style="font-size: 13px; color: #999;">Em <b><?php echo $produto->getParcelasNumero()?>x sem juros no cartão</b> de <strong style="font-size: 18px; color: #21805D;">R$ <?php echo $produto->getParcelasValor() ?></strong></strong>
			</div>
			<button class="btn-comprar"><img src="img/btn-comprar-carrim.png" class="img-comprar"></button><br>
		</div>
		
	</div>
	<div class="pc2">
	<form>
	<div class="title2">
	<b>Descrição do Produto</b>
	</div>
	</form>
	<form class="pcbox">
	<b>Computador Gamer NTC Vulcano 7016, AMD Ryzen 3 2200G, 8GB, 1TB – 15688</b><br><br>
		<div class="title3">
		<b>Especificações Técnicas</b><br><br>
		</div>
		<b>Características:</b><br>
		- Marca: NTC<br>
		- Modelo: 15688<br><br>

		<b>Especificações:</b><br>
		- Gabinete Gamer Crimson 211<br>
		- Fonte: 500W 80 Plus Bronze C/PFC Ativo<br>
		- Placa-Mãe: ASRock A320M-HD<br>
		- Processador: AMD Ryzen 3 2200G (AM4 / 3.5Ghz - 3.7Ghz / 4MB Cache L3)<br>
		- Memória: Gamer 8GB DDR4 2400 MHZ (1X8GB) - Com Dissipador<br>
		- Disco Rígido: 1.0TB SATA 3 7.200 RPM 3.5"<br>
		- Placa de Vídeo: Onboard - Radeon Vega 8<br>
		- Sistema Operacional: Windows 10 PRO 64 Bits (Versão de Avaliação 30 Dias)<br><br>

		<b>Conteúdo da Embalagem:</b><br>
		- Computador Gamer NTC<br><br>

		<b>*ATENÇÃO: NÃO ACOMPANHA GRAVADOR DE CD/DVD, TECLADO, MOUSE E CAIXAS DE SOM</b><br><br>

		<b>Garantia:</b><br>
		12 meses de garantia<br>
		(9 meses de garantia contratual junto ao fabricante + 3 meses referentes à garantia legal, nos termos do artigo 26, II, do Código de Defesa do Consumidor)<br><br>

		<b>Peso:</b><br>
		8120 gramas (bruto com embalagem)
	</form>
	</div>
</div>
</body>
</html>