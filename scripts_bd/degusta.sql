-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 19-Dez-2019 às 03:23
-- Versão do servidor: 10.4.8-MariaDB
-- versão do PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `ebooks`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `degusta`
--

CREATE TABLE `degusta` (
  `id` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `dado` varchar(250) NOT NULL,
  `chek` int(11) NOT NULL,
  `datacon` timestamp NOT NULL DEFAULT current_timestamp(),
  `datadow` datetime NOT NULL,
  `degustado` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabela degustação';

--
-- Extraindo dados da tabela `degusta`
--

INSERT INTO `degusta` (`id`, `ip`, `dado`, `chek`, `datacon`, `datadow`, `degustado`) VALUES
(1, '::1', 'lobato', 1, '2019-12-01 05:11:04', '0000-00-00 00:00:00', 'N'),
(2, '::1', 'stridente', 1, '2019-12-01 05:59:12', '0000-00-00 00:00:00', 'N'),
(3, '::1', 'maracatu azedo', 1, '2019-12-01 06:00:09', '0000-00-00 00:00:00', 'N'),
(4, '::1', 'stabicus anatomins', 2, '2019-12-01 06:08:36', '0000-00-00 00:00:00', 'N'),
(5, '::1', 'australia vai dormir', 2, '2019-12-01 06:51:58', '0000-00-00 00:00:00', 'N'),
(6, '::1', 'australia vai dormir', 2, '2019-12-01 06:53:48', '0000-00-00 00:00:00', 'N'),
(7, '::1', 'australia vai dormir', 2, '2019-12-01 06:54:23', '0000-00-00 00:00:00', 'N'),
(8, '::1', 'australia vai dormir', 2, '2019-12-01 06:55:05', '0000-00-00 00:00:00', 'N'),
(9, '::1', 'australia vai dormir', 2, '2019-12-01 06:56:18', '0000-00-00 00:00:00', 'N'),
(10, '::1', 'australia vai dormir', 2, '2019-12-01 06:59:49', '0000-00-00 00:00:00', 'N'),
(11, '::1', 'aveves truz', 2, '2019-12-01 07:00:37', '0000-00-00 00:00:00', 'N'),
(12, '::1', 'f', 2, '2019-12-01 07:01:00', '0000-00-00 00:00:00', 'N'),
(13, '::1', 'ff', 1, '2019-12-01 16:03:30', '0000-00-00 00:00:00', 'N'),
(14, '::1', 'ff', 1, '2019-12-01 16:27:58', '0000-00-00 00:00:00', 'N'),
(15, '::1', 'teste', 1, '2019-12-09 16:41:16', '0000-00-00 00:00:00', 'N');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `degusta`
--
ALTER TABLE `degusta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `degusta`
--
ALTER TABLE `degusta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
