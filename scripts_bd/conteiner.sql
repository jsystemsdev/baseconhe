-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 19-Dez-2019 às 03:15
-- Versão do servidor: 10.4.8-MariaDB
-- versão do PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `ebooks`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `conteiner`
--

CREATE TABLE `conteiner` (
  `id` int(11) NOT NULL,
  `direbook` varchar(1000) NOT NULL,
  `dircontainer` varchar(1000) NOT NULL,
  `nomearq` varchar(500) NOT NULL,
  `iduser` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `datagrav` timestamp NOT NULL DEFAULT current_timestamp(),
  `statuslivro` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Caminho dos conteiners';

--
-- Extraindo dados da tabela `conteiner`
--

INSERT INTO `conteiner` (`id`, `direbook`, `dircontainer`, `nomearq`, `iduser`, `user`, `datagrav`, `statuslivro`) VALUES
(1, 'livros/pratileira/rita_de_cassia/', 'livros/pratileira/rita_de_cassia/META-INF', 'meulivro.zip', 1, 'jether', '2019-12-03 18:06:44', 'C'),
(2, 'livros/pratileira/h/r/', 'livros/pratileira/h/r/META-INF', 'meulivro.zip', 1, 'jether', '2019-12-10 13:19:53', 'C'),
(3, 'livros/pratileira/h/r/', 'livros/pratileira/h/r/META-INF', 'meulivro.zip', 1, 'jether', '2019-12-11 12:24:10', 'C');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `conteiner`
--
ALTER TABLE `conteiner`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `conteiner`
--
ALTER TABLE `conteiner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
