-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 19-Dez-2019 às 03:24
-- Versão do servidor: 10.4.8-MariaDB
-- versão do PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `ebooks`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tour`
--

CREATE TABLE `tour` (
  `id` int(5) NOT NULL,
  `ulrimg` varchar(500) CHARACTER SET utf8 NOT NULL,
  `autor` varchar(250) CHARACTER SET utf8 NOT NULL,
  `titulo` varchar(250) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Tabela pg tour';

--
-- Extraindo dados da tabela `tour`
--

INSERT INTO `tour` (`id`, `ulrimg`, `autor`, `titulo`) VALUES
(1, 'special_cource_1.png', 'Vascaranha Neto', 'Amanha continua mano'),
(2, 'special_cource_2.png', 'Sandra de Sá', 'Um outro Alguem'),
(3, 'special_cource_3.png', 'Mae e filho', 'Um filho uma vida '),
(4, 'special_cource_2.png', 'Leonardo d\'15', 'um cara em auto mar sem agua');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `tour`
--
ALTER TABLE `tour`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `tour`
--
ALTER TABLE `tour`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
